/* eslint-disable func-names */
// eslint-disable-next-line no-undef
module.exports = function (api) {
    const babelEnv = api.env();
    api.cache(true);
    const plugins = [
        [
            'module-resolver',
            {
                root: ['./src'],
                extensions: ['.ts', '.tsx', '.jsx', '.js', '.json'],
            },
        ],
        [
            'module:react-native-dotenv',
            {
                moduleName: 'react-native-dotenv',
                path: '.env',
                blacklist: null,
                whitelist: null,
                safe: false,
                allowUndefined: true,
            },
        ],
    ];

    if (babelEnv !== 'development') {
        plugins.push(['transform-remove-console']);
    }

    return {
        presets: ['babel-preset-expo'],
        plugins,
    };
};
