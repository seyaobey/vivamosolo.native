declare module 'react-native-dotenv' {
    export const ENV_CAN_USE_LOCAL: string;

    export const ENV_GRAPHQL_URL: string;
    export const ENV_GRAPHQL_URL_DEV: string;

    export const ENV_NHOST_URL: string;
    export const ENV_NHOST_URL_DEV: string;

    export const ENV_HASURA_ADMIN: string;
    export const ENV_HASURA_ADMIN_DEV: string;

    export const ENV_AUTO_LOGIN: boolean;
    export const ENV_IGNORE_NOTIFICATIONS: boolean;
}
