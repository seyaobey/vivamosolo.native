import React, { useEffect } from 'react';

import { LogBox, Platform } from 'react-native';
import * as Sentry from 'sentry-expo';

import 'modules/localization';

import { Application } from 'modules/application';
import { RootNavigator } from 'modules/navigation';
// import { Navigation } from 'modules/navigation';

Sentry.init({
    dsn: 'https://a169d5c0783d44e3a66acaa7e31a98c0@o397503.ingest.sentry.io/5835261',
    enableInExpoDevelopment: true,
    debug: true,
});

export default function App() {
    // LogBox.ignoreAllLogs(true);
    useEffect(() => {
        if (Platform.OS !== 'web') {
            LogBox.ignoreLogs([
                'VirtualizedList',
                'UNSAFE_componentWillReceiveProps',
                'This synthetic event',
                'findHostInstance',
                'If you do not provide children',
                'Setting a timer for a long period of time',
                'NativeBase: The contrast ratio ',
                'Maximum update depth',
            ]);
        }
    }, []);

    return (
        <Application>
            <RootNavigator />
        </Application>
    );
}
