/* eslint-disable max-len */

module.exports = {
    preset: 'jest-expo',
    setupFilesAfterEnv: ['<rootDir>/setupTest.ts'],
    moduleDirectories: ['node_modules', 'src/testing-setup', '__dirname'],
};
