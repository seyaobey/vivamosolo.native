import { createMachine, ContextFrom, EventFrom } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { MakeTypeState } from 'machine-types';
import { ActionRequestPaymentInput } from 'modules/api-graphql/api-react-query';

export const paymentModel = createModel(
    {
        data: undefined as unknown as ActionRequestPaymentInput,
        url: undefined as unknown as string,
        error: undefined as unknown as Record<string, unknown>,
    },
    {
        events: {
            START_PAYMENT: () => ({}),
            EXIT_PAYMENT: () => ({}),
            EXIT_PAYMENT_WEBVIEW: () => ({}),
            REQUEST_URL: (data: ActionRequestPaymentInput) => ({ data }),
            REQUEST_URL_SUCCEEDED: (url: string) => ({ url }),
            REQUEST_URL_FAILED: (error: Record<string, unknown>) => ({ error }),
            START_PROCESSING: () => ({}),
        },
    }
);

export type PaymentContext = ContextFrom<typeof paymentModel>;
export type PaymentEvent = EventFrom<typeof paymentModel>;

type PaymentTypeState = MakeTypeState<
    {
        payment_open: undefined;
        payment_url_requesting: undefined;
        payment_webviewing: undefined;
        payment_processing: undefined;
        payment_exited: undefined;
    },
    PaymentContext
>;

export const paymentMachine = createMachine<
    PaymentContext,
    PaymentEvent,
    PaymentTypeState
>(
    {
        initial: 'payment_closed',
        states: {
            payment_closed: {
                entry: 'set_data',
                on: {
                    START_PAYMENT: {
                        target: 'payment_open',
                        actions: 'set_data',
                    },
                },
            },

            /**
             * Open payment swipeable modal to allow user to enter payment amount
             */

            payment_open: {
                on: {
                    EXIT_PAYMENT: {
                        target: 'payment_exited',
                    },
                    REQUEST_URL: {
                        target: 'payment_url_requesting',
                        actions: 'set_data',
                    },
                },
            },

            /**
             * Call backend action to request ub payment url
             */

            payment_url_requesting: {
                entry: 'start_progress_animation',
                invoke: {
                    src: 'request_url_service',
                },
                on: {
                    REQUEST_URL_SUCCEEDED: {
                        actions: [
                            /** Set Ub pay payment url */
                            paymentModel.assign(
                                {
                                    url: (_, e) => e.url,
                                },
                                'REQUEST_URL_SUCCEEDED'
                            ),

                            'exit_progress_animation',

                            'open_payment_webview',
                        ],
                        target: 'payment_webviewing',
                    },
                    REQUEST_URL_FAILED: {
                        target: 'payment_url_requesting_failed',
                        actions: [
                            /** Set Ub payment request url error */
                            paymentModel.assign(
                                {
                                    error: (_, e) => e.error!,
                                },
                                'REQUEST_URL_FAILED'
                            ),
                        ],
                    },
                },
            },

            /**
             * Request ub payment url failed:
             * Display Error dialog (without error --> todo)
             */
            payment_url_requesting_failed: {
                entry: 'display_progress_failed',
                always: 'payment_exited',
            },

            payment_webviewing: {
                // always: 'payment_closed',
                on: {
                    EXIT_PAYMENT_WEBVIEW: {
                        actions: 'close_payment_webview',
                        target: 'payment_exited',
                    },
                },
            },

            payment_processing: {},

            payment_exited: {
                always: 'payment_closed',
            },
        },
    },
    {
        actions: {
            set_data: paymentModel.assign(
                {
                    data: (_, e) => e.data!,
                },
                'REQUEST_URL'
            ) as never,
        },
    }
);
