import React, { createContext, useContext } from 'react';

import { usePaymentMachine as useBasePaymentMachine } from './use-payment-machine';

type PaymentMachine = ReturnType<typeof useBasePaymentMachine>;
type PaymentMachineProps = {
    children?: React.ReactNode;
};
const context = createContext<PaymentMachine>({} as never);

export const PaymentMachineProvider = ({ children }: PaymentMachineProps) => (
    <context.Provider value={useBasePaymentMachine()}>
        {children}
    </context.Provider>
);

export const usePaymentMachine = () => useContext(context);
