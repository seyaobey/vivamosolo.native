import { useMemo } from 'react';

import { useNavigation } from '@react-navigation/native';
import { useMachine } from '@xstate/react';

import { useProgress } from 'machines/progress';
import { clientApi } from 'modules/api-client';
import { NavigationRouteParams } from 'modules/navigation/navigation-routes';

import { paymentMachine } from './payment-machine';

export const usePaymentMachine = () => {
    const { navigate, goBack } = useNavigation<NavigationRouteParams>();
    const { send: progressSend } = useProgress();
    const [state, send] = useMachine(paymentMachine, {
        actions: {
            start_progress_animation: () => {
                progressSend('START_PROCESSING');
            },

            exit_progress_animation: () => {
                progressSend('RESET');
            },

            display_progress_failed: () => {
                progressSend('FAILED');
            },

            open_payment_webview: () => {
                navigate('payment_webview');
            },

            close_payment_webview: () => {
                goBack();
            },
        },

        services: {
            request_url_service: (ctx) => (cb) => {
                clientApi
                    .action_request_payment({
                        args: ctx.data!,
                    })
                    .then(({ action_request_payment: res }) => {
                        cb({ type: 'REQUEST_URL_SUCCEEDED', url: res!.url });
                    })
                    .catch((err) => {
                        cb({ type: 'REQUEST_URL_FAILED', error: err });
                    });
            },
        },
    });

    const active = useMemo(
        () =>
            state.matches('payment_open') ||
            state.matches('payment_url_requesting'),
        [state.value]
    );

    return { state, send, active };
};
