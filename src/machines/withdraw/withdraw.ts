import { createMachine, ContextFrom, EventFrom, interpret } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { MakeTypeState } from 'machine-types';
import { progress } from 'machines/progress';
import { clientApi } from 'modules/api-client';
import {
    GroupDtoFragment,
    Group_Recurrencies_Enum,
} from 'modules/api-graphql/api-react-query';
import { auth } from 'modules/application';

const model = createModel(
    {
        group: undefined as unknown as GroupDtoFragment,
        amount: undefined as unknown as number,
        recipientPhone: undefined as unknown as string,
        recipientName: undefined as unknown as string,
        error: undefined as unknown as object,
    },
    {
        events: {
            START_WITHDRAW: (group: GroupDtoFragment) => ({ group }),
            RESET_WITHDRAW: () => ({}),
            OPEN_CONTACTS: (amount: number) => ({ amount }),
            RECIPIENT_PICKED: (
                recipientPhone: string,
                recipientName: string
            ) => ({ recipientPhone, recipientName }),
            CLOSE_CONTACTS: () => ({}),
            CANCEL_WITHDRAW: () => ({}),
            MAKE_PAYMENT: (group: GroupDtoFragment, amount: number) => ({
                group,
                amount,
            }),
            PAYMENT_SUCCEEDED: () => ({}),
            PAYMENT_FAILED: (error: object) => ({ error }),
        },
    }
);

export type WithdrawContext = ContextFrom<typeof model>;
type WithdrawEvent = EventFrom<typeof model>;
type WithdrawState = MakeTypeState<
    {
        idle: unknown;
        active: unknown;
        processing: unknown;
        contacts: unknown;
        exit: unknown;
    },
    WithdrawContext
>;

const clearContext = model.assign({
    amount: undefined as unknown as number,
    recipientPhone: () => undefined as unknown as string,
    recipientName: () => undefined as unknown as string,
    error: () => undefined as unknown as object,
});

export const withdrawMachine = createMachine<
    WithdrawContext,
    WithdrawEvent,
    WithdrawState
>(
    {
        initial: 'idle',
        context: model.initialContext,
        states: {
            idle: {
                entry: clearContext,
                on: {
                    START_WITHDRAW: {
                        target: 'active',
                        actions: [
                            model.assign(
                                {
                                    group: (_, e) => e.group,
                                },
                                'START_WITHDRAW'
                            ) as never,
                            clearContext,
                        ],
                    },
                },
            },

            active: {
                on: {
                    RESET_WITHDRAW: {
                        target: 'exit',
                    },

                    MAKE_PAYMENT: {
                        target: 'processing',
                    },

                    OPEN_CONTACTS: {
                        target: 'contacts',
                        actions: [
                            model.assign(
                                {
                                    amount: (_, e) => e.amount,
                                },
                                'OPEN_CONTACTS'
                            ),
                        ],
                    },
                },
            },

            contacts: {
                on: {
                    RECIPIENT_PICKED: {
                        target: 'active',
                        actions: [
                            model.assign(
                                {
                                    recipientPhone: (_, e) => e.recipientPhone,
                                    recipientName: (_, e) => e.recipientName,
                                },
                                'RECIPIENT_PICKED'
                            ) as never,
                        ],
                    },

                    CLOSE_CONTACTS: {
                        target: 'active',
                    },

                    CANCEL_WITHDRAW: {
                        target: 'exit',
                    },
                },
            },

            processing: {
                entry: () => progress.send('START_PROCESSING'),
                invoke: {
                    src: 'make_payment',
                },
                on: {
                    PAYMENT_SUCCEEDED: {
                        target: 'active',
                        actions: [() => progress.send('COMPLETED')],
                    },

                    PAYMENT_FAILED: {
                        target: 'active',
                        actions: [
                            model.assign(
                                {
                                    error: (_, e) => e.error,
                                },
                                'PAYMENT_FAILED'
                            ) as never,
                            () => progress.send('FAILED'),
                        ],
                    },
                },
            },

            exit: {
                always: 'idle',
            },
        },
    },
    {
        services: {
            make_payment: (ctx) => (cb) => {
                const { group, amount } = ctx;
                const memberId = group.members.find(
                    (m) => m.user.id === auth.user()?.id
                )!.id;

                const periodId =
                    group.group_recurrency_type !== Group_Recurrencies_Enum.None
                        ? group.periods[0].id
                        : undefined;

                clientApi
                    .action_withdraw_payment({
                        args: {
                            amount,
                            group_id: group.id,
                            member_id: memberId,
                            period_id: periodId,
                            recipientName: ctx.recipientName,
                            recipientPhone: ctx.recipientPhone,
                        },
                    })
                    .then(
                        () => cb('PAYMENT_SUCCEEDED'),
                        (error: object) => cb({ type: 'PAYMENT_FAILED', error })
                    );
            },
        },
    }
);

export const withdraw = interpret(withdrawMachine).start();
