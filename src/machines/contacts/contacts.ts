import * as Contacts from 'expo-contacts';
import { createMachine, ContextFrom, EventFrom, interpret } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { MakeTypeState } from 'machine-types';

const CURSOR = 20;

const model = createModel(
    {
        cursor: CURSOR,
        search: undefined as unknown as string,
        contacts: [] as Contacts.Contact[],
    },
    {
        events: {
            RESET_CONTACTS: () => ({}),
            REQUEST_PERMISSION: () => ({}),
            FETCH_CONTACTS: () => ({}),
            FETCH_MORE: () => ({}),
            SAVE_SEARCH_TERM: (term: string) => ({ term }),
            STORE_CONTACTS: (contacts: Contacts.Contact[]) => ({ contacts }),
            PERMISSION_ACQUIRED: () => ({}),
            PERMISSION_REJECTED: () => ({}),
        },
    }
);

type ContactsContext = ContextFrom<typeof model>;
type ContactsEvent = EventFrom<typeof model>;
type ContactsState =
    | MakeTypeState<
          {
              idle: unknown;
              request_permission: unknown;
              acquired: unknown;
              rejected: unknown;
          },
          ContactsContext
      >
    | {
          value: {
              acquired: 'fetching_contacts';
          };
          context: ContactsContext;
      }
    | {
          value: {
              contacts: 'displaying_contacts';
          };
          context: ContactsContext;
      };

const resetContext = model.assign({
    cursor: () => CURSOR,
    search: () => undefined as unknown as string,
});
const increaseCursor = model.assign({
    cursor: (ctx) => ctx.cursor + CURSOR,
});

const contactsMachine = createMachine<
    ContactsContext,
    ContactsEvent,
    ContactsState
>({
    context: model.initialContext,
    initial: 'idle',
    states: {
        idle: {
            on: {
                REQUEST_PERMISSION: {
                    target: 'request_permission',
                },
            },
        },

        request_permission: {
            invoke: {
                src: () => async (cb) => {
                    const { status } = await Contacts.requestPermissionsAsync();
                    if (status === Contacts.PermissionStatus.GRANTED) {
                        cb('PERMISSION_ACQUIRED');
                    } else {
                        cb('PERMISSION_REJECTED');
                    }
                },
            },
            on: {
                PERMISSION_ACQUIRED: {
                    target: 'acquired',
                },

                PERMISSION_REJECTED: {
                    target: 'rejected',
                },
            },
        },

        acquired: {
            initial: 'idle',
            states: {
                idle: {
                    on: {
                        FETCH_CONTACTS: {
                            target: 'fetching_contacts',
                        },
                    },
                },
                fetching_contacts: {
                    entry: resetContext,
                    invoke: {
                        src: (ctx) => async (cb) => {
                            let { contacts } = ctx;
                            if (!contacts.length) {
                                const { data } =
                                    await Contacts.getContactsAsync({
                                        fields: [
                                            Contacts.Fields.FirstName,
                                            Contacts.Fields.LastName,
                                            Contacts.Fields.Image,
                                            Contacts.Fields.PhoneNumbers,
                                        ],
                                    });
                                contacts = data.sort((a, b) => {
                                    const _a = a.name?.toLowerCase();
                                    const _b = b.name?.toLowerCase();
                                    if (_a < _b) {
                                        return -1;
                                    }
                                    if (_a > _b) {
                                        return 1;
                                    }
                                    return 0;
                                });
                            }
                            cb({ type: 'STORE_CONTACTS', contacts });
                        },
                    },
                    on: {
                        STORE_CONTACTS: {
                            target: 'displaying_contacts',
                            actions: [
                                model.assign(
                                    {
                                        contacts: (_, e) => e.contacts,
                                    },
                                    'STORE_CONTACTS'
                                ) as never,
                            ],
                        },
                    },
                },

                displaying_contacts: {
                    on: {
                        RESET_CONTACTS: {
                            target: 'idle',
                        },
                        FETCH_MORE: {
                            actions: [increaseCursor],
                        },
                        SAVE_SEARCH_TERM: {
                            actions: [
                                model.assign(
                                    {
                                        search: (_, e) => e.term,
                                    },
                                    'SAVE_SEARCH_TERM'
                                ) as never,
                            ],
                        },
                    },
                },
            },
        },

        rejected: {
            type: 'final',
        },
    },
});

export const contacts = interpret(contactsMachine).start();
