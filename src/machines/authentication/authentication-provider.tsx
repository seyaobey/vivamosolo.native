import React from 'react';

import { useBaseAuthenticationMachine } from './authentication-machine-use';

type AuthenticationMachine = ReturnType<typeof useBaseAuthenticationMachine>;
type AuthenticationProviderProps = {
    children?: React.ReactNode;
};
const context = React.createContext<AuthenticationMachine>({} as never);
export const AuthenticationMachineProvider = ({
    children,
}: AuthenticationProviderProps) => (
    <context.Provider value={useBaseAuthenticationMachine()}>
        {children}
    </context.Provider>
);

export const useAuthenticationMachine = () => React.useContext(context);
