import * as ExpoNotifications from 'expo-notifications';
import { createMachine, assign } from 'xstate';

import { MakeEvents, MakeEvent, MakeTypeState } from 'machine-types';
import { clientApi } from 'modules/api-client';
import { auth } from 'modules/application';
import { Credentials } from 'types';

export type AuthContext = {
    data?: Credentials;
};

export type AuthState = MakeTypeState<
    {
        mounting: unknown;
        authenticated: unknown;
        login: unknown;
        user_is_logging: unknown;
        register: unknown;
        user_is_registering: unknown;
    },
    AuthContext
>;

type IsAuthenticatedEvent = MakeEvent<'is_authenticated'>;
export type LoggingUserEvent = MakeEvent<
    'login_user_event',
    { data: Credentials }
>;
export type IsUnAuthenticatedEvent = MakeEvent<
    'is_unauthenticated',
    {
        error: {
            response: {
                data: { message: string };
            };
        };
    }
>;

export type AuthEvent =
    | IsAuthenticatedEvent
    | LoggingUserEvent
    | IsUnAuthenticatedEvent
    | MakeEvents<{
          switch_to_register: unknown;
          switch_to_login: unknown;
          register_user_event: {
              data: Credentials;
          };
      }>;

export type Test = MakeEvents<{
    is_unauthenticated: unknown;
    switch_to_register: unknown;
    switch_to_login: unknown;
    register_user: {
        data: Credentials;
    };
}>;

export const authenticationMachine = createMachine<
    AuthContext,
    AuthEvent,
    AuthState
>(
    {
        initial: 'mounting',
        states: {
            mounting: {
                on: {
                    is_authenticated: {
                        target: 'authenticated',
                    },
                    is_unauthenticated: {
                        target: 'login',
                    },
                },
            },

            authenticated: {
                invoke: {
                    src: async () => {
                        const expoPushToken = (
                            await ExpoNotifications.getExpoPushTokenAsync()
                        ).data;

                        return clientApi.set_user_expo_push_token({
                            userid: auth.user()?.id,
                            token: expoPushToken,
                        });
                    },
                },
            },

            login: {
                entry: ['switch_to_login_action'],
                on: {
                    login_user_event: {
                        target: 'logging_user',
                        actions: 'set_context',
                    },
                    switch_to_register: {
                        target: 'register',
                    },
                },
            },

            logging_user: {
                invoke: {
                    src: 'login_user_service',
                },
                on: {
                    is_authenticated: {
                        target: 'authenticated',
                    },

                    is_unauthenticated: {
                        target: 'login',
                        actions: 'authentication_failed',
                    },
                },
            },

            register: {
                entry: ['switch_to_register_action'],
                on: {
                    register_user_event: {
                        target: 'registering_user',
                        actions: 'set_context',
                    },
                    switch_to_login: {
                        target: 'login',
                    },
                },
            },

            registering_user: {
                invoke: {
                    src: 'register_user_service',
                },
                on: {
                    is_authenticated: {
                        target: 'authenticated',
                    },
                    is_unauthenticated: {
                        target: 'register',
                        actions: 'authentication_failed',
                    },
                },
            },
        },
    },
    {
        actions: {
            set_context: assign<AuthContext, AuthEvent>({
                data: (_, e) => (e as LoggingUserEvent)?.data,
            }),
        },
    }
);
