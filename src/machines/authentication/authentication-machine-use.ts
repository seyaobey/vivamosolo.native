import { useEffect } from 'react';

import { useAuth } from '@nhost/react-auth';
import { useNavigation } from '@react-navigation/native';
import { useMachine } from '@xstate/react';
import { useToast } from 'native-base';

import { getEnvVariables } from 'env-variables';
import { auth } from 'modules/application';
import { NavigationRouteParams } from 'modules/navigation/navigation-routes';

import {
    authenticationMachine,
    IsUnAuthenticatedEvent,
} from './authentication-machine';

export const useBaseAuthenticationMachine = () => {
    const toast = useToast();
    const { navigate } = useNavigation<NavigationRouteParams>();
    const { signedIn } = useAuth();

    const [state, send] = useMachine(authenticationMachine, {
        actions: {
            switch_to_login_action: () => {
                navigate('login');
            },
            switch_to_register_action: () => {
                navigate('register');
            },
            authentication_failed: (_, e) => {
                handleError((e as IsUnAuthenticatedEvent).error);
            },
        },
        services: {
            login_user_service: (ctx) => (cb) => {
                auth.login({
                    email: ctx.data!.email,
                    password: ctx.data!.password,
                })
                    .then(() => {
                        cb('is_authenticated');
                    })
                    .catch((err) => {
                        cb({ type: 'is_unauthenticated', error: err });
                    });
            },
            register_user_service: (ctx) => (cb) => {
                const { email, password, name, surname } = ctx.data!;
                auth.register({
                    email,
                    password,
                    options: {
                        userData: {
                            name,
                            surname,
                            display_name: `${name} ${surname}`,
                        },
                    },
                })
                    .then(() => cb('is_authenticated'))
                    .catch((err) => {
                        cb({ type: 'is_unauthenticated', error: err });
                    });
            },
        },
    });

    const handleError = (err: unknown) => {
        const {
            response: {
                data: { message },
            },
        } = err as never;

        toast.show({
            title: 'Error',
            description: message,
            status: 'error',
        });
    };

    useEffect(() => {
        if (signedIn !== null && signedIn !== undefined) {
            if (state.matches('mounting')) {
                const { ENV_AUTO_LOGIN } = getEnvVariables();
                send(
                    signedIn && ENV_AUTO_LOGIN
                        ? 'is_authenticated'
                        : 'is_unauthenticated'
                );
            }
        }
    }, [signedIn]);

    return {
        state,
        send,
    };
};
