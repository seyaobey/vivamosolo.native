// import { useMemo } from 'react';

// import { useMachine } from '@xstate/react';

// import { progressMachine, ProgressStatus } from './progress-machine';

// export const useProgressMachine = () => {
//     const [state, send] = useMachine(progressMachine);

//     const visible = useMemo(() => !state.matches('idle'), [state.value]);
//     const status = useMemo<ProgressStatus>(
//         () => state.value as never,
//         [state.value]
//     );

//     const reset = () => send('RESET');

//     return {
//         send,
//         reset,
//         state,
//         status,
//         visible,
//     };
// };
