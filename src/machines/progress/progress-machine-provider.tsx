// import React, { createContext, useContext } from 'react';

// import { useProgressMachine as useBaseProgressMachine } from './use-progress-machine';

// type ProgressMachine = ReturnType<typeof useBaseProgressMachine>;
// type ProgressMachineProviderProps = {
//     children?: React.ReactNode;
// };
// const context = createContext<ProgressMachine>({} as never);

// export const ProgressMachineProvider = ({
//     children,
// }: ProgressMachineProviderProps) => (
//     <context.Provider value={useBaseProgressMachine()}>
//         {children}
//     </context.Provider>
// );

// export const useProgressMachine = () => useContext(context);
