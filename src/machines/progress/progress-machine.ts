import { useMemo } from 'react';

import { createMachine, ContextFrom, EventFrom, interpret } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { MakeTypeState } from 'machine-types';
import { useXMachine } from 'machines/use-xmachine';

export type ProgressStatus = 'idle' | 'processing' | 'success' | 'failed';

type ProgressTypeState = MakeTypeState<
    {
        idle: unknown;
        processing: unknown;
        success: unknown;
        failed: unknown;
    },
    ProgressContext
>;

const model = createModel(
    {},
    {
        events: {
            START_PROCESSING: () => ({}),
            COMPLETED: () => ({}),
            FAILED: () => ({}),
            RESET: () => ({}),
        },
    }
);

type ProgressContext = ContextFrom<typeof model>;
type ProgressEvents = EventFrom<typeof model>;

export const progressMachine = createMachine<
    ProgressContext,
    ProgressEvents,
    ProgressTypeState
>({
    initial: 'idle',
    states: {
        idle: {
            on: {
                START_PROCESSING: {
                    target: 'processing',
                },
            },
        },
        processing: {
            on: {
                COMPLETED: {
                    target: 'success',
                },
                FAILED: {
                    target: 'failed',
                },
                RESET: {
                    target: 'idle',
                },
            },
        },

        success: {
            on: {
                RESET: {
                    target: 'idle',
                },
            },
        },

        failed: {
            on: {
                RESET: {
                    target: 'idle',
                },
            },
        },
    },
});

export const progress = interpret(progressMachine).start();

export const useProgress = () => {
    const { state, send } = useXMachine(progress);

    const visible = useMemo(() => !state.matches('idle'), [state.value]);
    const status = useMemo<ProgressStatus>(
        () => state.value as never,
        [state.value]
    );

    const reset = () => send('RESET');

    return {
        state,
        status,
        visible,
        send,
        reset,
    };
};
