/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useState } from 'react';

import { EventObject, Interpreter, Typestate } from 'xstate';

export const useXMachine = <
    TContext extends object,
    TEvent extends EventObject,
    TState extends Typestate<TContext> = {
        value: any;
        context: TContext;
    }
>(
    machine: Interpreter<TContext, any, TEvent, TState>
) => {
    const [state, setState] = useState(machine.state);

    useEffect(() => {
        const subs = machine.subscribe((_state) => {
            setState(_state);
        });
        return () => subs.unsubscribe();
    }, []);

    return {
        state,
        send: machine.send,
    };
};
