/* eslint-disable no-nested-ternary */
import Constants from 'expo-constants';
import {
    ENV_AUTO_LOGIN,
    ENV_CAN_USE_LOCAL,
    ENV_IGNORE_NOTIFICATIONS,
    ENV_GRAPHQL_URL,
    ENV_GRAPHQL_URL_DEV,
    ENV_HASURA_ADMIN,
    ENV_HASURA_ADMIN_DEV,
    ENV_NHOST_URL,
    ENV_NHOST_URL_DEV,
} from 'react-native-dotenv';

import { EnvironmentPayload } from 'types-variables';
import { stringToBool } from 'utils';

const isProd = () => !__DEV__;
const useLocal = () => (__DEV__ ? stringToBool(ENV_CAN_USE_LOCAL) : false);

const resolveNhostUrl = (local: string, port: string, prod: string) => {
    if (isProd()) {
        return prod;
    }
    return useLocal() &&
        !!Constants.manifest?.packagerOpts?.dev &&
        !Constants.isDevice
        ? local
        : useLocal() &&
          typeof Constants.manifest?.packagerOpts === `object` &&
          !!Constants.manifest?.packagerOpts.dev
        ? `http://${Constants.manifest?.debuggerHost
              ?.split(`:`)
              ?.shift()
              ?.concat(`:${port}`)}`
        : prod;
};

const prod = isProd();
const strAutoLogin = stringToBool(ENV_AUTO_LOGIN);
const autologin = prod ? true : strAutoLogin;

console.log('prod', prod);
console.log('strAutoLogin', strAutoLogin);
console.log('autologin', autologin);

export const getEnvVariables = (): EnvironmentPayload => ({
    ENV_GRAPHQL_URL: resolveNhostUrl(
        ENV_GRAPHQL_URL_DEV,
        '8080/v1/graphql',
        ENV_GRAPHQL_URL
    ),

    ENV_NHOST_URL: resolveNhostUrl(ENV_NHOST_URL_DEV, '9001', ENV_NHOST_URL),

    ENV_HASURA_ADMIN:
        isProd() || !useLocal() ? ENV_HASURA_ADMIN : ENV_HASURA_ADMIN_DEV,

    ENV_AUTO_LOGIN: autologin,

    ENV_IGNORE_NOTIFICATIONS: isProd()
        ? false
        : stringToBool(ENV_IGNORE_NOTIFICATIONS),
});

console.log('getEnvVariables', getEnvVariables());
