/* eslint-disable no-plusplus */
/* eslint-disable @typescript-eslint/no-explicit-any */
const MIN = 60e3;
const HOUR = MIN * 60;
const DAY = HOUR * 24;
const YEAR = DAY * 365;
const MONTH = DAY * 30;

type FromNowOptions = {
    max?: number;
    suffix?: boolean;
    and?: boolean;
    zero?: boolean;
};

export const fromNow = (date: Date, options?: FromNowOptions) => {
    const opts = options || {};

    const del = new Date(date).getTime() - Date.now();
    const abs = Math.abs(del);

    if (abs < MIN) return 'just now';

    const periods = {
        year: abs / YEAR,
        month: (abs % YEAR) / MONTH,
        day: (abs % MONTH) / DAY,
        hour: (abs % DAY) / HOUR,
        minute: (abs % HOUR) / MIN,
    } as const;

    let k;
    let val;
    const keep: string[] = [];
    let max: number | string = opts.max || MIN; // large number

    Object.keys(periods).forEach((_k: any) => {
        if (keep.length < max) {
            val = Math.floor((periods as any)[_k] as number);
            if (val || opts.zero) {
                keep.push(`${val} ${val === 1 ? _k : `${_k}s`}`);
            }
        }
    });

    k = keep.length; // reuse
    max = ', '; // reuse

    if (k > 1 && opts.and) {
        if (k === 2) max = ' ';
        keep[--k] = `and ${keep[k]}`;
    }

    val = keep.join(max); // reuse

    if (opts.suffix) {
        if (del < 0) {
            val = `${val} ago`;
        } else {
            val = `in ${val}`;
        }
        // val += del < 0 ? ' ago' : ' from now';
    }

    return val;
};
