export type ExtractValuePart<T, K extends keyof T> = T[K];

export type Typify<T, K extends keyof T> = {
    [k in K]: {
        type: `${string & keyof T}`;
    };
};

export type Eventify<T, K extends keyof T> = ExtractValuePart<Typify<T, K>, K>;
export type TypeStatify<TypeState, Context> = TypeState & { context: Context };
