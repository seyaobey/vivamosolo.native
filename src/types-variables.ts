export type EnvironmentPayload = {
    ENV_GRAPHQL_URL: string;
    ENV_NHOST_URL: string;
    ENV_HASURA_ADMIN: string;
    ENV_AUTO_LOGIN?: boolean;
    ENV_IGNORE_NOTIFICATIONS?: boolean;
};
