import React from 'react';

import { render, RenderOptions } from '@testing-library/react-native';
import { NativeBaseProvider } from 'native-base';
import { Provider as PaperProvider, Portal } from 'react-native-paper';

import 'modules/localization';

const inset = {
    frame: { x: 0, y: 0, width: 0, height: 0 },
    insets: { top: 0, left: 0, right: 0, bottom: 0 },
};

type TestingWrapperProps = {
    children: React.ReactNode;
};

const TestingWrapperProvider = ({ children }: TestingWrapperProps) => (
    <NativeBaseProvider initialWindowMetrics={inset}>
        <PaperProvider>
            <Portal>{children}</Portal>
        </PaperProvider>
    </NativeBaseProvider>
);

const customRender = (
    ui: React.ReactElement<unknown>,
    options?: RenderOptions
) => render(ui, { wrapper: TestingWrapperProvider, ...options });

export * from '@testing-library/react-native';

// override render method
export { customRender as render };
