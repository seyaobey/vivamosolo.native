import { auth } from 'modules/application';

export type ThenArg<T> = T extends PromiseLike<infer U> ? U : T;

export type LoginResults = ThenArg<ReturnType<typeof auth.login>>;
export type RegisterResults = ThenArg<ReturnType<typeof auth.register>>;

export type Credentials = {
    name: string;
    surname: string;
    email: string;
    password: string;
};
