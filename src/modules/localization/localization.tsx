/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import localEn from 'locales/locale-en.json';
import localFr from 'locales/locale-fr.json';

export const defaultNS = 'en';

export const resources = {
    en: {
        en: localEn,
        fr: localFr,
    },
    fr: {
        fr: localFr,
    },
} as const;

i18n.use(initReactI18next).init({
    lng: 'en',
    ns: ['en', 'fr'],
    defaultNS,
    resources,
});

export default i18n;
