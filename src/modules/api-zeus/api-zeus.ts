import { getEnvVariables } from 'env-variables';

import { Chain } from '../../../generated/zeus';

export const apiZeus = Chain(getEnvVariables().ENV_GRAPHQL_URL, {
    headers: {
        'X-Hasura-Role': 'admin',
        'X-Hasura-Admin-Secret': getEnvVariables().ENV_HASURA_ADMIN,
    },
});
