import React, { ReactNode } from 'react';

import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
        },
    },
});

type AppQueryCacheProps = {
    children?: ReactNode;
};
export const ApplicationQueryProvider = ({ children }: AppQueryCacheProps) => (
    <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
);
