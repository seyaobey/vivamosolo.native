import React, { createContext, useContext } from 'react';

import { useMachine } from '@xstate/react';
import { createMachine } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { Eventify, TypeStatify } from 'helpers/types';
import { GroupDtoFragment } from 'modules/api-graphql/api-react-query';

type Context = {
    groups: GroupDtoFragment[];
};

type TypeState =
    | { value: 'starting' }
    | { value: 'ready' }
    | { value: 'welcome' };

const model = createModel(
    {
        groups: [],
    } as Context,
    {
        events: {
            update_groups: (groups: GroupDtoFragment[]) => ({ groups }),
            no_group_found: () => ({}),
        },
    }
);

const appMachine = createMachine<
    typeof model,
    Context,
    Eventify<typeof model.events, keyof typeof model.events>,
    TypeStatify<TypeState, Context>
>(
    {
        context: model.initialContext,
        initial: 'starting',
        states: {
            starting: {
                on: {
                    update_groups: {
                        target: 'ready',
                        actions: 'update_context',
                    },
                    no_group_found: {
                        target: 'welcome',
                        actions: 'update_context',
                    },
                },
            },
            welcome: {
                on: {
                    update_groups: {
                        target: 'ready',
                    },
                },
            },
            ready: {
                on: {
                    update_groups: {
                        target: 'ready',
                        actions: 'update_context',
                    },
                },
            },
        },
    },
    {
        actions: {
            update_context: model.assign(
                {
                    groups: (ctx, e) => e.groups.map((gr) => ({ ...gr })),
                },
                'update_groups'
            ) as never,
        },
    }
);

const useBaseAppMachine = () =>
    useMachine(
        appMachine.withContext({
            groups: [],
        }),
        {
            actions: {},
        }
    );

export const context = createContext<ReturnType<typeof useBaseAppMachine>>(
    {} as never
);

type ApplicationMachineProps = {
    children: React.ReactNode;
};
export const ApplicationMachineProviderOld = ({
    children,
}: ApplicationMachineProps) => {
    const ctx = useBaseAppMachine();
    return <context.Provider value={ctx}>{children}</context.Provider>;
};

export const useApplicationMachine = () => useContext(context);
