import React from 'react';

import { FontAwesome5 } from '@expo/vector-icons';
import { Row, Pressable, VStack, Text, Icon } from 'native-base';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import { useApplicationMachine } from 'modules/application-machine';
import { appTheme } from 'modules/application-theme/app-theme';

import { NavigationRouteParamList } from './navigation-routes';

type ITabBarProps = {
    state: {
        index: number;
    };
};

type ButtonTabItem = {
    title: string;
    active?: boolean;
    icon: React.ComponentProps<typeof FontAwesome5>['name'];
    target: keyof NavigationRouteParamList;
};

const items: ButtonTabItem[] = [
    { title: 'groups', icon: 'th-list', target: 'listing' },
    { title: 'messages', icon: 'envelope', target: 'messages' },
    { title: 'profile', icon: 'user', target: 'profile' },
];

const TabBar: React.FC<ITabBarProps> = (props) => <TabContent {...props} />;

const TabContent = ({ state }: ITabBarProps) => {
    const { navigateTo } = useTabContent();
    return (
        <Row
            bg="page_bg"
            h={`${hp(appTheme.sizes.tabHeight)}px`}
            justifyContent="space-around"
            borderTopColor="gray.300"
            borderTopWidth={1}>
            {items.map((item, i) => (
                <TabItem
                    key={item.target}
                    item={item}
                    active={state.index === i}
                    onPress={() => {
                        navigateTo(item.target);
                    }}
                />
            ))}
        </Row>
    );
};

const useTabContent = () => {
    const {
        app: { send },
    } = useApplicationMachine();

    const navigateTo = (target: ButtonTabItem['target']) =>
        send({ type: target as never });

    return { navigateTo };
};

type TabItemProps = {
    item: ButtonTabItem;
    active: boolean;
    onPress: () => void;
};
const TabItem = ({ item, active, onPress }: TabItemProps) => {
    const color = active ? 'tab_active' : 'tab_inactive';
    return (
        <Pressable onPress={onPress}>
            {({}) => (
                <VStack pt={1} alignItems="center" mt={1}>
                    <Icon
                        color={color}
                        size={`${hp(3)}px`}
                        as={<FontAwesome5 name={item.icon} />}
                    />
                    <Text
                        color={color}
                        fontSize={wp(4.5)}
                        fontWeight={active ? 'semibold' : 'light'}>
                        {item.title}
                    </Text>
                </VStack>
            )}
        </Pressable>
    );
};

export { TabBar };
