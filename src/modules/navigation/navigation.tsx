/* eslint-disable react/jsx-key */
import React, { useMemo } from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import {
    createStackNavigator,
    TransitionPresets,
} from '@react-navigation/stack';
import { enableScreens } from 'react-native-screens';
import { MultiProvider } from 'react-pendulum';

import {
    AuthenticationMachineProvider,
    useAuthenticationMachine,
} from 'machines/authentication';
import { PaymentMachineProvider } from 'machines/payment';
import { AppMachineProvider } from 'modules/application-machine';
import { Authentication } from 'pages/authentication';
import { GroupItem } from 'pages/group-item';
import { GroupListing } from 'pages/group-listing';
import { Step01Name } from 'pages/group-steps/step_01_name';
import { Step02Contacts } from 'pages/group-steps/step_02_contacts';
import { Step03Contrib } from 'pages/group-steps/step_03_contrib';
import { Step04Recurrency } from 'pages/group-steps/step_04_recurrency';
import { Messages } from 'pages/messages';
import { PaymentWebView } from 'pages/payment';
import { Profile } from 'pages/profile';

import { NavigationRouteParamList } from './navigation-routes';
import { TabBar } from './tab-bar';

enableScreens();

const AuthNavigatorStack = createStackNavigator<NavigationRouteParamList>();
const TabsNavigatorStack = createBottomTabNavigator<NavigationRouteParamList>();
const RootNavigatorStack = createStackNavigator<NavigationRouteParamList>();
const MainNavigatorStack = createStackNavigator<NavigationRouteParamList>();

const AuthNavigator = () => (
    <AuthNavigatorStack.Navigator
        screenOptions={{
            ...TransitionPresets.SlideFromRightIOS,
            headerShown: false,
        }}>
        <AuthNavigatorStack.Screen name="login" component={Authentication} />

        <AuthNavigatorStack.Screen name="register" component={Authentication} />
    </AuthNavigatorStack.Navigator>
);

const TabsNavigator = () => (
    <TabsNavigatorStack.Navigator tabBar={TabBar}>
        <TabsNavigatorStack.Screen name="listing" component={GroupListing} />
        <TabsNavigatorStack.Screen name="messages" component={Messages} />
        <TabsNavigatorStack.Screen name="profile" component={Profile} />
    </TabsNavigatorStack.Navigator>
);

const MainNavigator = () => (
    <MainNavigatorStack.Navigator
        initialRouteName="main"
        screenOptions={{
            ...TransitionPresets.SlideFromRightIOS,
            headerShown: false,
        }}>
        <MainNavigatorStack.Screen name="main" component={TabsNavigator} />

        <MainNavigatorStack.Screen name="listing_item" component={GroupItem} />

        <MainNavigatorStack.Screen name="step_01_name" component={Step01Name} />
        <MainNavigatorStack.Screen
            name="step_02_contacts"
            component={Step02Contacts}
        />
        <MainNavigatorStack.Screen
            name="step_03_contributions"
            component={Step03Contrib}
        />
        <MainNavigatorStack.Screen
            name="step_04_recurring_cycle"
            component={Step04Recurrency}
        />

        <MainNavigatorStack.Screen
            name="payment_webview"
            component={PaymentWebView}
        />
    </MainNavigatorStack.Navigator>
);

const SwitchNaviator = () => {
    const { state } = useAuthenticationMachine();
    const authenticated = useMemo(
        () => state.matches('authenticated'),
        [state.value]
    );
    return authenticated ? <MainNavigator /> : <AuthNavigator />;
};

const RootNavigatorContent = () => (
    <MultiProvider
        providers={[
            <AuthenticationMachineProvider />,
            <AppMachineProvider />,
            <PaymentMachineProvider />,
        ]}>
        <SwitchNaviator />
    </MultiProvider>
);

export const RootNavigator = () => (
    <NavigationContainer>
        <RootNavigatorStack.Navigator
            mode="modal"
            screenOptions={{
                ...TransitionPresets.SlideFromRightIOS,
                headerShown: false,
            }}>
            <RootNavigatorStack.Screen
                name="root"
                component={RootNavigatorContent}
            />
        </RootNavigatorStack.Navigator>
    </NavigationContainer>
);
