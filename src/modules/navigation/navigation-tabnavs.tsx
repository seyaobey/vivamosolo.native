import React from 'react';

import { FontAwesome5 } from '@expo/vector-icons';
import { Text, Row, Box, Icon, VStack, Pressable } from 'native-base';
import { Platform } from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import { NavigationRouteParamList } from './navigation-routes';

type TabItem = {
    title: string;
    active?: boolean;
    icon: React.ComponentProps<typeof FontAwesome5>['name'];
    target: keyof NavigationRouteParamList;
};

type NavigationTabNavsProps = {
    state: {
        index: number;
    };
    navigation: {
        navigate: (name: keyof NavigationRouteParamList) => void;
    };
};

const items: TabItem[] = [
    { title: 'groups', icon: 'list', target: 'listing' },
    { title: 'messages', icon: 'envelope', target: 'messages' },
    { title: 'profile', icon: 'user', target: 'profile' },
];
export const NavigationTabNavs = ({
    state,
    navigation,
}: NavigationTabNavsProps) => (
    <Box
        bg="main"
        {...Platform.select({
            ios: { h: `${hp(12)}px`, pt: 3 },
            android: { h: `${hp(11)}px`, pt: 2 },
        })}>
        <Row alignItems="center" justifyContent="space-around" bg="main">
            <>
                {items.map((item, i) => (
                    <TabItemComponent
                        key={item.title}
                        active={state.index === i}
                        title={item.title}
                        icon={item.icon}
                        onPress={() => {
                            navigation.navigate(item.target);
                        }}
                    />
                ))}
            </>
        </Row>
    </Box>
);

type TabItemComponentProps = Partial<TabItem> & {
    onPress: () => void;
};
const TabItemComponent = ({
    title,
    icon,
    active,
    onPress,
}: TabItemComponentProps) => {
    const color = active ? 'tab_active' : 'tab_inactive';
    const bg = active ? 'tab_bg_active' : 'main';

    return (
        <Pressable onPress={onPress}>
            {({}) => (
                <VStack
                    w={`${wp(23)}px`}
                    alignItems="center"
                    bg={bg}
                    p={2}
                    borderRadius={8}>
                    <Icon
                        color={color}
                        size={`${hp(2)}px`}
                        as={<FontAwesome5 name={icon} />}
                    />
                    <Text
                        color={color}
                        fontSize={wp(4)}
                        fontWeight={active ? 'semibold' : 'light'}>
                        {title}
                    </Text>
                </VStack>
            )}
        </Pressable>
    );
};
