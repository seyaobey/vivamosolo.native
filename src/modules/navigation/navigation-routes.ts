import { StackNavigationProp } from '@react-navigation/stack';

export type NavigationPages = {
    Main: undefined;
    Login: undefined;
    Register: undefined;
    Groups: undefined;
    // GroupCreateContacts: undefined;
    CreateGroupTest: undefined;
    CreateGroupPermission: undefined;
    CreateGroupCustomize: undefined;
    CreateGroupPayments: undefined;
    GroupJoin: undefined;
    Payment: undefined;
    PaymentWebView: { url: string };

    GroupStep01NameScreen: undefined;
    GroupStep02Contacts: undefined;
    GroupStep03ContribScreen: undefined;
    GroupStep04CycleScreen: undefined;

    PageTest01: undefined;
    PageTest02: undefined;
};

export type NavigationRouteParamList = {
    root: undefined;
    main: undefined;

    login: undefined;
    register: undefined;

    listing: undefined;
    listing_item: undefined;
    messages: undefined;
    profile: undefined;

    payment_webview: undefined;

    step_01_name: undefined;
    step_02_contacts: undefined;
    step_03_contributions: undefined;
    step_04_recurring_cycle: undefined;
};

export type NavigationRouteParams =
    StackNavigationProp<NavigationRouteParamList>;

export type NavigationPagesStack = StackNavigationProp<NavigationPages>;
