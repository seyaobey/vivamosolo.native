import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import {
    createStackNavigator,
    TransitionPresets,
} from '@react-navigation/stack';
import { view } from '@risingstack/react-easy-state';

import { ApplicationMachineProviderOld } from 'modules/application-machine.old';
import { applicationStore } from 'modules/application/application-store';
import { Login, Register } from 'pages/old.auth';
import {
    PageGroupCreatePermission,
    PageGroupCreateCustomize,
    PageGroupCreateFinalize,
} from 'pages/old.group-create';
import { GroupJoin } from 'pages/old.group-join';
import { GroupStep01NameScreen } from 'pages/old.group-steps';
import { GroupStep02ContactsScreen } from 'pages/old.group-steps/group-step-02-contacts';
import { GroupStep03ContribScreen } from 'pages/old.group-steps/group-step-03-contrib-screen';
import { GroupStep04CycleScreen } from 'pages/old.group-steps/group-step-04-cycle-screen';
import { GroupStepsMachineProvider } from 'pages/old.group-steps/group-steps-master-provider';
import { Groups } from 'pages/old.groups';
import { Payment, PaymentWebView } from 'pages/old.payment';

import { NavigationPages } from './navigation-routes';

const RootStack = createStackNavigator<NavigationPages>();
const MainStack = createStackNavigator<NavigationPages>();

const MainStackNavigation = view(() => (
    <ApplicationMachineProviderOld>
        <GroupStepsMachineProvider>
            <MainStack.Navigator
                headerMode="none"
                initialRouteName="Login"
                screenOptions={{
                    ...TransitionPresets.SlideFromRightIOS,
                }}>
                {!applicationStore.loggedIn ? (
                    <>
                        <MainStack.Screen name="Login" component={Login} />
                        <MainStack.Screen
                            name="Register"
                            component={Register}
                        />
                    </>
                ) : (
                    <>
                        <MainStack.Screen name="Groups" component={Groups} />

                        <MainStack.Screen
                            name="CreateGroupPermission"
                            component={PageGroupCreatePermission}
                        />

                        <MainStack.Screen
                            name="CreateGroupCustomize"
                            component={PageGroupCreateCustomize}
                        />

                        <MainStack.Screen
                            name="CreateGroupPayments"
                            component={PageGroupCreateFinalize}
                        />

                        <MainStack.Screen
                            name="GroupJoin"
                            component={GroupJoin}
                        />

                        <MainStack.Screen name="Payment" component={Payment} />

                        <MainStack.Screen
                            name="GroupStep01NameScreen"
                            component={GroupStep01NameScreen}
                        />

                        <MainStack.Screen
                            name="GroupStep02Contacts"
                            component={GroupStep02ContactsScreen}
                        />

                        <MainStack.Screen
                            name="GroupStep03ContribScreen"
                            component={GroupStep03ContribScreen}
                        />

                        <MainStack.Screen
                            name="GroupStep04CycleScreen"
                            component={GroupStep04CycleScreen}
                        />
                    </>
                )}
            </MainStack.Navigator>
        </GroupStepsMachineProvider>
    </ApplicationMachineProviderOld>
));

export const Navigation = view(() => (
    <NavigationContainer>
        <RootStack.Navigator mode="modal">
            <RootStack.Screen
                name="Main"
                component={MainStackNavigation}
                options={{ headerShown: false }}
            />
            <RootStack.Screen
                name="PaymentWebView"
                component={PaymentWebView}
                options={{ headerShown: false }}
            />
        </RootStack.Navigator>
    </NavigationContainer>
));
