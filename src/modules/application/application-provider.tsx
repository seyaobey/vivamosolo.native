import React from 'react';

import { NhostApolloProvider } from '@nhost/react-apollo';
import { NhostAuthProvider } from '@nhost/react-auth';
import { MultiProvider } from 'react-pendulum';

import { getEnvVariables } from 'env-variables';
import { ApplicationQueryProvider } from 'modules/application-query';
import { ApplicationTheme } from 'modules/application-theme';
import { ContactsPermissions } from 'modules/contacts-permission';
import { Notifications } from 'modules/notifications';

import { auth } from './application-auth';

type ApplicationProviderProps = {
    children: React.ReactNode;
};

export const ApplicationProvider = ({ children }: ApplicationProviderProps) => (
    <MultiProvider
        providers={[
            <NhostAuthProvider auth={auth} key={1} />,
            <NhostApolloProvider
                key={2}
                auth={auth}
                gqlEndpoint={getEnvVariables().ENV_GRAPHQL_URL}
                headers={{
                    'X-Hasura-Admin-Secret': getEnvVariables().ENV_HASURA_ADMIN,
                    'X-Hasura-Role': 'admin',
                }}
            />,
            <ApplicationQueryProvider key={3} />,
            <ApplicationTheme key={4} />,
            <Notifications key={5} />,
            <ContactsPermissions key={6} />,
        ]}>
        <>{children}</>
    </MultiProvider>
);
