import * as SecureStore from 'expo-secure-store';
import { createClient } from 'nhost-js-sdk';

import { getEnvVariables } from 'env-variables';

const { auth, storage } = createClient({
    baseURL: getEnvVariables().ENV_NHOST_URL,
    clientStorage: SecureStore,
    clientStorageType: 'expo-secure-storage',
});

export { auth, storage };
