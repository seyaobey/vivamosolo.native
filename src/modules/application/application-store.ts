import { store } from '@risingstack/react-easy-state';

export const applicationStore = store({
    loggedIn: false,
});
