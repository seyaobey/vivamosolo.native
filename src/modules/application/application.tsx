import React from 'react';

import { ApplicationProvider } from './application-provider';

type ApplicationProps = {
    children: React.ReactNode;
};

export const Application = ({ children }: ApplicationProps) => (
    <ApplicationProvider>{children}</ApplicationProvider>
);
