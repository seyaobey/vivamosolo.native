// import { useEffect } from 'react';

// import * as ExpoNotifications from 'expo-notifications';

// import { graphClient } from 'modules/api-client';
// import { useSet_User_Expo_Push_TokenMutation } from 'modules/api-graphql/api-react-query';
// import { auth } from 'modules/application';

// const useRegisterNotificationsToken = () => {
//     const { mutate: setExpoPushToken } =
//         useSet_User_Expo_Push_TokenMutation(graphClient);

//     const storePushToken = async () => {
//         const expoPushToken = (await ExpoNotifications.getExpoPushTokenAsync())
//             .data;
//         await setExpoPushToken({
//             userid: auth.user()?.id,
//             token: expoPushToken,
//         });
//     };
//     useEffect(() => {
//         storePushToken();
//     }, []);
// };

// export { useRegisterNotificationsToken as useNotificationsToken };
