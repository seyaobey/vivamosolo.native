import React, { useEffect, useState } from 'react';

import Constants from 'expo-constants';
import * as ExpoNotifications from 'expo-notifications';
import { Center, Spinner, Text } from 'native-base';
import { Platform } from 'react-native';

import { getEnvVariables } from 'env-variables';

ExpoNotifications.setNotificationHandler({
    handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: true,
        shouldSetBadge: true,
    }),
});

export type INotificationsProps = {
    children?: React.ReactNode;
};

const Notifications: React.FC<INotificationsProps> = ({ children }) => {
    const [status, setStatus] = useState<'loading' | 'granted' | 'rejected'>(
        'loading'
    );

    useEffect(() => {
        registerNotifications().then((granted) => {
            setStatus(granted ? 'granted' : 'rejected');
        });
    }, []);

    if (status === 'loading') {
        return (
            <Center flex={1}>
                <Spinner accessibilityLabel="registering notification" />
            </Center>
        );
    }

    if (!getEnvVariables().ENV_IGNORE_NOTIFICATIONS && status === 'rejected') {
        return (
            <Center flex={1}>
                <Text color="amber.500">Notifications should be allowed</Text>
            </Center>
        );
    }

    return <>{children}</>;
};

const registerNotifications = async (): Promise<boolean> => {
    if (Constants.isDevice) {
        const { status: existingStatus } =
            await ExpoNotifications.getPermissionsAsync();
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
            const { status } =
                await ExpoNotifications.requestPermissionsAsync();
            finalStatus = status;
        }
        if (finalStatus === 'granted') {
            if (Platform.OS === 'android') {
                ExpoNotifications.setNotificationChannelAsync('default', {
                    name: 'default',
                    importance: ExpoNotifications.AndroidImportance.MAX,
                    vibrationPattern: [0, 250, 250, 250],
                    lightColor: '#FF231F7C',
                });
            }
            return true;
        }
        // token = (await ExpoNotifications.getExpoPushTokenAsync()).data;
    }
    return false;
};

export { Notifications };
