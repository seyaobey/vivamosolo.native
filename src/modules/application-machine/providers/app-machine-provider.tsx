/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { createContext, useContext } from 'react';

import { useApplicationMachine as useBaseApplicationMachine } from '../hooks/use-app-machine';

type ApplicationMachine = ReturnType<typeof useBaseApplicationMachine>;

const context = createContext<ApplicationMachine>({} as any);

type ApplicationProviderProps = {
    children?: React.ReactNode;
};

export const AppMachineProvider = ({ children }: ApplicationProviderProps) => (
    <context.Provider value={useBaseApplicationMachine()}>
        {children}
    </context.Provider>
);

export const useApplicationMachine = () => useContext(context);
