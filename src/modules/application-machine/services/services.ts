import { clientApi } from 'modules/api-client';

import { ValuedPaymentRequestUrlPayload } from '../types';

export const requestPaymentUrl = async ({
    value: { args },
}: ValuedPaymentRequestUrlPayload) =>
    clientApi.action_request_payment({
        args,
    });
