import { useNavigation } from '@react-navigation/native';
import { useMachine } from '@xstate/react';
import { useToast } from 'native-base';

import { getApiError } from 'modules/api-client';
import { NavigationRouteParams } from 'modules/navigation/navigation-routes';

import {
    ApplicationMachineContext as Context,
    appMachine,
} from '../machines/app-machine';
import { requestPaymentUrl } from '../services';
import { useStepsMachine } from './use-steps-machine';

export const useApplicationMachine = () => {
    const toast = useToast();
    const { navigate, goBack } = useNavigation<NavigationRouteParams>();
    const [appState, appSend] = useMachine(appMachine, {
        actions: {
            go_back: goBack,
            go_to_listing: () => navigate('listing'),
            go_to_messages: () => navigate('messages'),
            go_to_profile: () => navigate('profile'),
            go_to_listing_item: () => navigate('listing_item'),
            request_payment_url: (_: Context, event: unknown) => {
                requestPaymentUrl(event as never)
                    .then((res) => {
                        toast.show({
                            title: 'Request payment url succeeded',
                            status: 'success',
                        });
                        appSend({
                            type: 'payment_requesting_url_sucess',
                            value: {
                                url: res.action_request_payment!.url!,
                            },
                        });
                    })
                    .catch((err: unknown) => {
                        toast.show({
                            title: 'Oups! An error has occured',
                            description: getApiError(err),
                            status: 'error',
                        });
                        appSend({ type: 'payment_requesting_url_failed' });
                    });
            },
        },
    });
    const steps = useStepsMachine();

    return {
        app: {
            state: appState,
            send: appSend,
        },
        steps,
    };
};
