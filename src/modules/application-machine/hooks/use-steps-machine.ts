import { useNavigation } from '@react-navigation/native';
import { useMachine } from '@xstate/react';
import { useToast } from 'native-base';

import { clientApi, getApiError } from 'modules/api-client';
import {
    Contribution_Types_Enum,
    Group_Recurrencies_Enum,
} from 'modules/api-graphql/api-react-query';
import { auth } from 'modules/application';
import {
    StepsContext,
    stepsMachine,
} from 'modules/application-machine/machines/steps-machine';
import { NavigationRouteParams } from 'modules/navigation/navigation-routes';

export const useStepsMachine = () => {
    const toast = useToast();
    const { navigate, goBack } = useNavigation<NavigationRouteParams>();

    const [state, send, service] = useMachine(stepsMachine, {
        actions: {
            go_back: goBack,
            go_to_step_name: () => navigate('step_01_name'),
            go_to_step_participants: () => navigate('step_02_contacts'),
            go_to_step_contributions: () => navigate('step_03_contributions'),
            go_to_step_recurring_cycle: () =>
                navigate('step_04_recurring_cycle'),
            exit: () => navigate('listing'),
            submit_values: (ctx: StepsContext) => {
                const payload = {
                    creator_id: auth.user()?.id,
                    group_name: ctx.data.group_name!,
                    group_contribution_type:
                        ctx.data.contrib_type ??
                        Contribution_Types_Enum.AnyAmount,
                    group_contribution_amount: ctx.data.contrib_amount ?? 0,
                    group_recurrency_type:
                        ctx.data.recurrent_type ?? Group_Recurrencies_Enum.None,
                    group_recurrency_day: ctx.data.recurrent_day ?? 0,
                };
                clientApi
                    .action_create_group(payload)
                    .then(() => send('submission_succeeded'))
                    .catch((err: unknown) => {
                        toast.show({
                            title: 'Oups! An error has occured',
                            description: getApiError(err),
                            status: 'error',
                        });
                        send('submission_failed');
                    });
            },
        },
    });

    const startSteps = () => {
        service.start();
        send('start');
    };

    return {
        state,
        send,
        startSteps,
    };
};
