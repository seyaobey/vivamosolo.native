import { useMemo, useState, useEffect } from 'react';

import { useAuth } from '@nhost/react-auth';
import { useMachine } from '@xstate/react';
import { useToast } from 'native-base';

import { getEnvVariables } from 'env-variables';
import { auth } from 'modules/application';
import { NavigationRouteParams } from 'modules/navigation/navigation-routes';

import { authMachine } from '../machines/auth-machine';

export const useAuthMachine = () => {
    const { signedIn } = useAuth();
    const toast = useToast();
    const [navigator, setNavigator] = useState<NavigationRouteParams>();

    const [state, send, service] = useMachine(authMachine, {
        actions: {
            switch_to_login: () => navigator?.navigate('login'),
            switch_to_register: () => navigator!.navigate('register'),
        },

        services: {
            login_user: (ctx) => (cb) => {
                auth.login({
                    email: ctx.payload.email!,
                    password: ctx.payload.password!,
                })
                    .then(() => cb('IS_AUTHENTICATED'))
                    .catch(
                        (err: { response: { data: { message: string } } }) => {
                            toast.show({
                                title: 'Error',
                                description: err.response.data.message,
                                status: 'error',
                            });
                            cb('IS_UNAUTHENTICATED');
                        }
                    );
            },

            register_user: (ctx) => (cb) => {
                const { payload } = ctx;
                auth.register({
                    email: payload.email!,
                    password: payload.password!,
                    options: {
                        userData: {
                            display_name: `${payload.name} ${payload.surname}`,
                        },
                    },
                })
                    .then(() => cb('IS_AUTHENTICATED'))
                    .catch(
                        (err: { response: { data: { message: string } } }) => {
                            toast.show({
                                title: 'Error',
                                description: err.response.data.message,
                                status: 'error',
                            });
                            cb('IS_UNAUTHENTICATED');
                        }
                    );
            },
        },
    });

    const status = useMemo(
        () => ({
            mounting: state.matches('mounting'),
            submitting:
                state.matches('logging_user') ||
                state.matches('registering_user'),
            isLogin: state.matches('login') || state.matches('logging_user'),
            isRegister:
                state.matches('register') || state.matches('registering_user'),
        }),
        [state.value]
    );

    useEffect(() => {
        if (signedIn !== null && signedIn !== undefined) {
            const { ENV_AUTO_LOGIN } = getEnvVariables();
            if (signedIn && ENV_AUTO_LOGIN) {
                send('IS_AUTHENTICATED');
            } else {
                send('IS_UNAUTHENTICATED');
            }
        }
    }, [signedIn]);

    return {
        state,
        status,
        send,
        service,
        setNavigator,
    };
};
