import { createMachine, MachineConfig } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { Eventify, TypeStatify } from 'helpers/types';
import { Credentials } from 'types';

export type AuthContext = {
    payload: Partial<Credentials>;
};

type TypeState =
    | { value: 'mounting' }
    | { value: 'login' }
    | { value: 'register' }
    | { value: 'logging_user' }
    | { value: 'registering_user' }
    | { value: 'authenticated' };

const model = createModel(
    {
        payload: undefined as unknown as Partial<Credentials>,
    },
    {
        events: {
            SWITCH_LOGIN: () => ({}),
            LOGIN_USER: (payload: Partial<Credentials>) => ({ payload }),

            SWITCH_REGISTER: () => ({}),
            REGISTER_USER: () => ({}),

            IS_AUTHENTICATED: () => ({}),
            IS_UNAUTHENTICATED: () => ({}),
        },
    }
);

export type AuthEvents = Eventify<
    typeof model.events,
    keyof typeof model.events
>;

export const config: MachineConfig<
    AuthContext,
    {
        states: {
            mounting: {};
            foo: {};
            bar: {};
        };
    },
    AuthEvents
> = {
    states: {
        mounting: {
            on: {
                IS_AUTHENTICATED: {
                    target: 'bar',
                },
            },
        },
        bar: {},
        foo: {},
    },
};

export const authMachine = createMachine<
    typeof model,
    AuthContext,
    AuthEvents,
    TypeStatify<TypeState, AuthContext>
>(
    {
        context: {
            payload: {},
        },

        initial: 'mounting',

        states: {
            mounting: {
                on: {
                    IS_AUTHENTICATED: {
                        target: 'authenticated',
                    },

                    IS_UNAUTHENTICATED: {
                        target: 'login',
                    },
                },
            },

            login: {
                entry: 'switch_to_login',
                on: {
                    LOGIN_USER: {
                        target: 'logging_user',
                        actions: 'assign_payload',
                    },
                    SWITCH_REGISTER: {
                        target: 'register',
                    },
                },
            },

            logging_user: {
                invoke: {
                    src: 'login_user',
                },

                on: {
                    IS_AUTHENTICATED: {
                        target: 'authenticated',
                    },
                    IS_UNAUTHENTICATED: {
                        target: 'login',
                    },
                },
            },

            register: {
                entry: 'switch_to_register',
                on: {
                    REGISTER_USER: {
                        target: 'registering_user',
                        actions: 'assign_payload',
                    },
                    SWITCH_LOGIN: {
                        target: 'login',
                    },
                },
            },

            registering_user: {
                invoke: {
                    src: 'register_user',
                },
                on: {
                    IS_AUTHENTICATED: {
                        target: 'authenticated',
                    },
                    IS_UNAUTHENTICATED: {
                        target: 'register',
                    },
                },
            },

            authenticated: {
                type: 'final',
            },
        },
    },
    {
        actions: {
            assign_payload: model.assign(
                {
                    payload: (_, e) => e.payload,
                },
                'LOGIN_USER'
            ) as never,
        },
    }
);
