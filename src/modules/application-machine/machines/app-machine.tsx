import { useMachine } from '@xstate/react';
import { createMachine, interpret } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { TypeStatify } from 'helpers/types';
import { GroupDtoFragment } from 'modules/api-graphql/api-apollo';
import { ActionRequestPaymentInput } from 'modules/api-graphql/api-react-query';

import { PaymentRequestUrlPayload } from '../types';
import { fetchProfile } from './services';

type UserProfile = {
    id: string;
    created_at: string;
};

const model = createModel(
    {
        groups: [] as GroupDtoFragment[],
        groupId: undefined as unknown as string | undefined,
        profile: undefined as unknown as UserProfile,
    },
    {
        events: {
            IS_AUTHENTICATED: () => ({}),
            update_groups: (groups: GroupDtoFragment[]) => ({
                value: { groups },
            }),
            listing: () => ({}),
            messages: () => ({}),
            profile: () => ({}),
            listing_item: (id: string) => ({
                value: { groupId: id },
            }),
            assign_profile: (profile: UserProfile) => ({
                value: { profile },
            }),
            start_payment: () => ({}),
            cancel_payment: () => ({}),
            open_payment_view: () => ({}),
            start_payment_processing: () => ({}),
            payment_succeded: () => ({}),
            payment_failed: () => ({}),
            request_payment_url: (args: ActionRequestPaymentInput) => ({
                value: args,
            }),
            payment_requesting_url_sucess: (url: string) => ({
                value: url,
            }),
            payment_requesting_url_failed: () => ({}),
        },
    }
);

export type ApplicationMachineContext = {
    groupId?: string;
    groups: GroupDtoFragment[];
    profile?: UserProfile;
};
type TypeState =
    | { value: 'unauthenticated' }
    | { value: 'authenticated' }
    | {
          value: {
              authenticated: 'mounting';
          };
      }
    | {
          value: {
              authenticated: 'listing';
          };
      }
    | {
          value: {
              authenticated: 'messages';
          };
      }
    | {
          value: {
              authenticated: 'profile';
          };
      }
    | {
          value: {
              authenticated: {
                  profile: 'fetching';
              };
          };
      }
    | {
          value: {
              authenticated: {
                  profile: 'ready';
              };
          };
      }
    | {
          value: {
              authenticated: 'group_info';
          };
      }
    | {
          value: {
              authenticated: {
                  listing_item: 'payment_viewing';
              };
          };
      }
    | {
          value: {
              authenticated: {
                  listing_item: 'payment_preparing';
              };
          };
      }
    | {
          value: {
              authenticated: {
                  listing_item: 'payment_processing';
              };
          };
      }
    | {
          value: {
              authenticated: {
                  listing_item: 'ready';
              };
          };
      }
    | {
          value: {
              authenticated: {
                  listing_item: 'payment_preparing';
              };
          };
      }
    | {
          value: {
              authenticated: {
                  listing_item: 'payment_viewing';
              };
          };
      }
    | {
          value: {
              authenticated: {
                  listing_item: 'payment_processing';
              };
          };
      }
    | {
          value: {
              authenticated: {
                  listing_item: 'ready';
              };
          };
      };

type Events =
    | { type: 'IS_AUTHENTICATED' }
    | {
          type: 'update_groups';
          value: {
              groups: GroupDtoFragment[];
          };
      }
    | { type: 'listing' }
    | { type: 'messages' }
    | { type: 'profile' }
    | {
          type: 'update_profile';
          value: {
              profile: {
                  id: string;
                  created_at: string;
              };
          };
      }
    | { type: 'listing_item'; value: { groupId: string } }
    | { type: 'start_payment' }
    | { type: 'open_payment_view' }
    | { type: 'cancel_payment' }
    | { type: 'start_payment_processing' }
    | { type: 'payment_succeded' }
    | { type: 'payment_failed' }
    | {
          type: 'request_payment_url';
          value: PaymentRequestUrlPayload;
      }
    | { type: 'payment_requesting_url_sucess'; value: { url: string } }
    | { type: 'payment_requesting_url_failed' }
    | { type: 'go_back' };

export const appMachine = createMachine<
    typeof model,
    ApplicationMachineContext,
    Events,
    TypeStatify<TypeState, ApplicationMachineContext>
>(
    {
        context: {
            groups: [],
        },
        initial: 'authenticated',

        states: {
            unauthenticated: {
                on: {
                    IS_AUTHENTICATED: {
                        target: 'authenticated',
                    },
                },
            },

            authenticated: {
                initial: 'mounting',
                states: {
                    mounting: {
                        on: {
                            update_groups: {
                                actions: ['update_groups', 'go_to_listing'],
                                target: 'listing',
                            },
                        },
                    },

                    listing: {
                        on: {
                            update_groups: {
                                actions: 'update_groups',
                            },
                            messages: {
                                target: 'messages',
                                actions: 'go_to_messages',
                            },
                            profile: {
                                target: 'profile',
                                actions: 'go_to_profile',
                            },
                            listing_item: {
                                target: 'listing_item',
                                actions: ['set_group_id', 'go_to_listing_item'],
                            },
                        },
                    },

                    messages: {
                        on: {
                            update_groups: {
                                actions: 'update_groups',
                            },
                            listing: {
                                target: 'listing',
                                actions: 'go_to_listing',
                            },
                            profile: {
                                target: 'profile',
                                actions: 'go_to_profile',
                            },
                        },
                    },

                    profile: {
                        on: {
                            update_groups: {
                                actions: 'update_groups',
                            },
                            listing: {
                                target: 'listing',
                                actions: 'go_to_listing',
                            },
                            messages: {
                                target: 'messages',
                                actions: 'go_to_messages',
                            },
                        },
                        initial: 'fetching',
                        states: {
                            fetching: {
                                invoke: {
                                    src: () => async (cb) => {
                                        const profile = await fetchProfile();
                                        cb({
                                            type: 'update_profile',
                                            value: {
                                                profile: profile as UserProfile,
                                            },
                                        });
                                    },
                                },
                                on: {
                                    update_profile: {
                                        target: 'ready',
                                        actions: 'update_profile',
                                    },
                                },
                            },
                            ready: {},
                        },
                    },

                    listing_item: {
                        on: {
                            update_groups: {
                                actions: 'update_groups',
                            },
                            go_back: {
                                target: 'listing',
                                actions: 'go_back',
                            },
                        },
                        initial: 'ready',
                        states: {
                            ready: {
                                on: {
                                    start_payment: {
                                        target: 'payment_preparing',
                                    },
                                },
                            },

                            payment_preparing: {
                                on: {
                                    cancel_payment: {
                                        target: 'ready',
                                    },
                                    request_payment_url: {
                                        target: 'payment_requesting_url',
                                        actions: 'request_payment_url',
                                    },
                                },
                            },

                            payment_requesting_url: {
                                on: {
                                    payment_requesting_url_sucess: {
                                        target: 'payment_viewing',
                                    },
                                    payment_requesting_url_failed: {
                                        target: 'ready',
                                    },
                                },
                            },

                            payment_viewing: {
                                on: {
                                    cancel_payment: {
                                        target: 'ready',
                                    },
                                    start_payment_processing: {
                                        target: 'payment_processing',
                                    },
                                },
                            },

                            payment_processing: {
                                on: {
                                    payment_succeded: {
                                        target: 'ready',
                                    },
                                    payment_failed: {
                                        target: 'ready',
                                    },
                                },
                            },
                        },
                    },
                },
            },
        },
    },
    {
        actions: {
            update_groups: model.assign(
                {
                    groups: (_, e) => e.value.groups,
                },
                'update_groups'
            ) as never,

            update_profile: model.assign(
                {
                    profile: (_, e) => e.value.profile,
                },
                'assign_profile'
            ) as never,

            set_group_id: model.assign(
                { groupId: (_, e) => e.value.groupId },
                'listing_item'
            ) as never,
        },
    }
);

const srv = interpret(appMachine).start();

export const useAppService = () => {
    const [srvState] = useMachine(srv.machine);
    return {
        srvState,
    };
};
