import { clientApi } from 'modules/api-client';
import { auth } from 'modules/application';

export const fetchProfile = async () => {
    const { users_by_pk: profile } = await clientApi.users_by_pk({
        user_id: auth.user()!.id,
    });
    return profile;
};
