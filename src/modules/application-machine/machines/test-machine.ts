import { createMachine, assign, interpret } from 'xstate';

import { MakeContextTypeState } from 'machine-types';

type Context = {
    foo: string;
    bar: string;
};

type SomeEventType = {
    type: 'some_event';
    id: string;
};

type Event =
    | SomeEventType
    | {
          type: 'another_event';
          key: number;
      };

type AuthState = MakeContextTypeState<
    Context,
    {
        mounting: unknown;
        authenticated: unknown;
        login: unknown;
        logging_user: unknown;
        register: unknown;
        registering_user: unknown;
    }
>;

export const testMachine = createMachine<Context, Event, AuthState>(
    {
        initial: 'mounting',
        states: {
            mounting: {
                on: {
                    another_event: {
                        target: 'ready',
                    },
                },
            },
            ready: {},
            transitioning: {},
            nestlevel: {},
        },
    },
    {
        actions: {
            test_action: assign<Context, Event>({
                bar: (_, e) => (e as SomeEventType).id,
            }),
        },
    }
);

export const srv = interpret(testMachine).start();

srv.state.matches('login');
