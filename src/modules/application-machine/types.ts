import { ActionRequestPaymentInput } from 'modules/api-graphql/api-react-query';

export type PaymentRequestUrlPayload = {
    args: ActionRequestPaymentInput;
};

export type ValuedPaymentRequestUrlPayload = {
    value: PaymentRequestUrlPayload;
};
