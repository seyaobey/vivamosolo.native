/* eslint-disable @typescript-eslint/no-explicit-any */
import { createMachine } from 'xstate';
import { createModel } from 'xstate/lib/model';

import {
    BuildFormMachineEvents,
    FormMachineContext,
    TypeStates,
} from './types';

type FormMachineProps<TValues> = {
    id?: string;
    model: FormMachineContext<TValues>;
    validate?: (
        values: Partial<TValues>
    ) => FormMachineContext<TValues>['errors'] | undefined;
};
export const buildFormMachine = <TValues>({
    id,
    model,
    validate,
}: FormMachineProps<TValues>) => {
    const formModel = createModel(model, {
        events: {
            read_input: (values: TValues) => ({ values }),
            input_validation_requested: () => ({}),
            validation_passed: () => ({}),
            validation_failed: (errors?: TValues) => ({ errors }),
            abort_requested: () => ({}),
            reset: () => ({}),
        },
    });

    const machine = createMachine<
        typeof formModel,
        FormMachineContext<TValues>,
        BuildFormMachineEvents<TValues>,
        TypeStates<TValues>
    >(
        {
            id,
            context: formModel.initialContext,
            initial: 'ready',
            states: {
                abort: {},
                ready: {
                    initial: 'reading_input',
                    on: {
                        abort_requested: { target: 'abort' },
                    },
                    states: {
                        reading_input: {
                            on: {
                                read_input: {
                                    actions: 'update_group_name',
                                },

                                request_validation: {
                                    target: 'validating_input',
                                },
                            },
                        },

                        validating_input: {
                            invoke: {
                                id: 'validation',
                                src: (ctx) => (cb) => {
                                    const errors = validate?.(ctx.values);
                                    const hasErrors =
                                        errors &&
                                        !!Object.entries(errors).find(
                                            (kp) => !!kp[1]
                                        );
                                    cb(
                                        !hasErrors
                                            ? 'validation_passed'
                                            : {
                                                  type: 'validation_failed',
                                                  values: ctx.values,
                                                  errors,
                                              }
                                    );
                                },
                            },
                            on: {
                                validation_passed: {
                                    target: 'input_is_valid',
                                },
                                validation_failed: {
                                    target: 'input_is_invalid',
                                    actions: 'set_validation_errors',
                                },
                            },
                        },

                        input_is_valid: {
                            invoke: {
                                id: 'submit',
                                src: 'submit',
                            },
                            on: {
                                reset: {
                                    target: 'reading_input',
                                },
                            },
                        },

                        input_is_invalid: {
                            on: {
                                read_input: {
                                    target: 'reading_input',
                                    actions: 'update_group_name',
                                },
                            },
                        },
                    },
                },
            },
        },
        {
            actions: {
                update_group_name: formModel.assign(
                    {
                        values: (_, e) => {
                            const { values } = e;
                            return values;
                        },
                        errors: () => undefined,
                    },
                    'read_input'
                ) as any,

                set_validation_errors: formModel.assign(
                    {
                        errors: (ctx, event) => event.errors,
                    },
                    'validation_failed'
                ) as any,
            },
        }
    );

    return machine;
};
