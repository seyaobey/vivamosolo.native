type Errors<TValues> = {
    [name in keyof TValues]+?: string;
};

export type FormMachineContext<TValues> = {
    values: Partial<TValues>;
    initial?: TValues;
    errors?: Errors<TValues>;
};

type FormMachineContextValuesErrors<TContext> = Pick<
    FormMachineContext<TContext>,
    'values' | 'errors'
>;

type ContextFormMachineContextValuesErrors<TContext> = {
    context: FormMachineContextValuesErrors<TContext>;
};

export type FormMachineEventEnums =
    | 'read_input'
    | 'request_validation'
    | 'validation_passed'
    | 'validation_failed'
    | 'submit_values'
    | 'abort_requested'
    | 'reset';

type FormMachineEvents<TContext> = {
    [K in FormMachineEventEnums]: {
        type: `${K}`;
    } & FormMachineContextValuesErrors<TContext>;
};

export type BuildMachineEvents<T, K extends keyof T> = T[K];

export type BuildFormMachineEvents<TContext> = BuildMachineEvents<
    FormMachineEvents<TContext>,
    FormMachineEventEnums
>;

export type TypeStates<TContext> =
    | ({
          value: 'ready';
      } & ContextFormMachineContextValuesErrors<TContext>)
    | ({
          value: { ready: 'validating_input' };
      } & ContextFormMachineContextValuesErrors<TContext>)
    | ({
          value: { ready: 'input_is_valid' };
      } & ContextFormMachineContextValuesErrors<TContext>)
    | ({
          value: { ready: 'input_is_invalid' };
      } & ContextFormMachineContextValuesErrors<TContext>)
    | ({ value: 'abort' } & ContextFormMachineContextValuesErrors<TContext>);
