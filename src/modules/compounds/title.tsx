import React from 'react';

import { Text } from 'native-base';

export const Title = ({
    fontSize = '3xl',
    fontWeight = 'light',
    color = 'main',
    ...rest
}: React.ComponentProps<typeof Text>) => (
    <Text fontSize={fontSize} fontWeight={fontWeight} color={color} {...rest} />
);
