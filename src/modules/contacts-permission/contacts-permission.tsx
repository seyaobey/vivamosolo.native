import React, { createContext, useEffect, useMemo } from 'react';

import { Center, Spinner, Text } from 'native-base';

import { contacts } from 'machines/contacts';
import { useXMachine } from 'machines/use-xmachine';

const useContactsPermission = () => {
    const { state } = useXMachine(contacts);
    const mounting = useMemo(
        () => state.matches('idle') || state.matches('request_permission'),
        [state.value]
    );
    const granted = useMemo(() => state.matches('acquired'), [state.value]);
    const rejected = useMemo(() => state.matches('rejected'), [state.value]);

    useEffect(() => {
        contacts.send('REQUEST_PERMISSION');
    }, []);

    return {
        granted,
        mounting,
        rejected,
    };
};

const context = createContext<unknown>({} as never);

type ContactsPermissionProps = {
    children?: React.ReactNode;
};

export const ContactsPermissions = ({ children }: ContactsPermissionProps) => {
    const { granted, mounting, rejected } = useContactsPermission();
    if (mounting) {
        <Center flex={1}>
            <Spinner accessibilityLabel="request contacts perimissions" />
        </Center>;
    }
    if (rejected || !granted) {
        <Center flex={1}>
            <Text color="amber.500">
                Application should be granted permission to access contacts
            </Text>
        </Center>;
    }
    return <context.Provider value={{}}>{children}</context.Provider>;
};
