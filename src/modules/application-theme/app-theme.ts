import React from 'react';

import { extendTheme, theme as defaultTheme, Text } from 'native-base';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

export const appTheme = extendTheme({
    fontConfig: {
        Roboto: {
            100: {
                normal: 'Roboto-Light',
                italic: 'Roboto-LightItalic',
            },
            200: {
                normal: 'Roboto-Light',
                italic: 'Roboto-LightItalic',
            },
            300: {
                normal: 'Roboto-Light',
                italic: 'Roboto-LightItalic',
            },
            400: {
                normal: 'Roboto-Regular',
                italic: 'Roboto-Italic',
            },
            500: {
                normal: 'Roboto-Medium',
            },
            600: {
                normal: 'Roboto-Medium',
                italic: 'Roboto-MediumItalic',
            },

            700: {
                normal: 'Roboto-Bold',
            },
        },
    },
    fonts: {
        heading: 'Roboto',
        body: 'Roboto',
        mono: 'Roboto',
    },
    components: {
        Text: {
            baseStyle: () => ({
                color: 'gray.600',
                fontFamily: 'body',
                fontWeight: 400,
            }),
        },
        Button: {
            defaultProps: {
                _text: {
                    fontFamily: 'Roboto-Regular',
                },
            },
        },
        Input: {
            defaultProps: {
                _focus: {
                    borderColor: 'blue.900',
                },
            },

            baseStyle: () => ({
                _focus: {
                    borderColor: 'blue.900',
                },
            }),
        },
    },

    colors: {
        main: defaultTheme.colors.teal[500],
        main_dark: defaultTheme.colors.teal[600],

        accent: defaultTheme.colors.blue[500],

        page_bg: defaultTheme.colors.gray[100],
        tab_bg: defaultTheme.colors.gray[300],
        tab_active: defaultTheme.colors.teal[500],
        tab_inactive: defaultTheme.colors.warmGray[400],
        tab_bg_active: defaultTheme.colors.teal[400],

        btn_change_pressed: defaultTheme.colors.teal[100],

        modalize_bg: defaultTheme.colors.gray[100],

        animated_header: {
            divider_bg: defaultTheme.colors.gray[300],
        },

        animated_scrollview: {
            bg: defaultTheme.colors.gray[100],
        },

        listing_item: {
            balance: defaultTheme.colors.green[500],
        },
    },

    sizes: {
        tabHeight: 11,
    },

    space: {
        animated_scrollview: {
            zIndex: -1,
            mb: 32,
        },

        listing: {
            divider_my: 1,
        },
    },

    fontSizes: {
        listing_item: {
            group_name: wp(6),
            balance: '2xl' as React.ComponentProps<typeof Text>['fontSize'],
        },
    },
});

export type ApplicationDefaultTheme = typeof appTheme;
