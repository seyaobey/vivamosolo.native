/* eslint-disable global-require */
import React from 'react';

import { NativeBaseProvider } from 'native-base';
import { Provider as PaperProvider, Portal } from 'react-native-paper';

import { appTheme } from './app-theme';
import { ApplicationFonts } from './application-fonts';

type ApplicationThemeProps = {
    children?: React.ReactNode;
};

export const ApplicationTheme = ({ children }: ApplicationThemeProps) => (
    <ApplicationFonts>
        <NativeBaseProvider theme={appTheme}>
            <PaperProvider>
                <Portal>{children}</Portal>
            </PaperProvider>
        </NativeBaseProvider>
    </ApplicationFonts>
);
