/* eslint-disable global-require */
import React from 'react';

import { useFonts } from 'expo-font';
import { StyleSheet, View, ActivityIndicator } from 'react-native';

type ApplicationFontsProps = {
    children: React.ReactNode;
};

export const ApplicationFonts = ({ children }: ApplicationFontsProps) => {
    const [loaded] = useFonts({
        'Roboto-Light': require('../../../assets/fonts/Roboto-Light.ttf'),
        'Roboto-LightItalic': require('../../../assets/fonts/Roboto-LightItalic.ttf'),

        'Roboto-Regular': require('../../../assets/fonts/Roboto-Regular.ttf'),
        'Roboto-Italic': require('../../../assets/fonts/Roboto-Italic.ttf'),

        'Roboto-Medium': require('../../../assets/fonts/Roboto-Medium.ttf'),
        'Roboto-MediumItalic': require('../../../assets/fonts/Roboto-MediumItalic.ttf'),

        'Roboto-Bold': require('../../../assets/fonts/Roboto-Bold.ttf'),
    });

    if (!loaded) {
        return (
            <View style={styles.center}>
                <ActivityIndicator size="large" />
            </View>
        );
    }

    return <>{children}</>;
};

const styles = StyleSheet.create({
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
