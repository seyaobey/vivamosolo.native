import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  citext: any;
  json: any;
  jsonb: any;
  numeric: any;
  timestamp: any;
  timestamptz: any;
  uuid: any;
};

export type ActionCreateGroupInput = {
  creator_id: Scalars['uuid'];
  group_contribution_amount?: Maybe<Scalars['numeric']>;
  group_contribution_type: Scalars['String'];
  group_name: Scalars['String'];
  group_recurrency_day?: Maybe<Scalars['numeric']>;
  group_recurrency_type: Scalars['String'];
};

export type ActionCreateGroupOutpu = {
  ok: Scalars['Boolean'];
};

export type ActionCreateGroupOutput = {
  ok: Scalars['Boolean'];
};

export type ActionRequestPaymentInput = {
  amount: Scalars['numeric'];
  group_id: Scalars['uuid'];
  member_id: Scalars['uuid'];
  period_id?: Maybe<Scalars['uuid']>;
};

export type ActionRequestPaymentOutput = {
  id: Scalars['uuid'];
  url: Scalars['String'];
};

export type ActionWithdrawPaymentInput = {
  amount: Scalars['numeric'];
  group_id: Scalars['uuid'];
  member_id: Scalars['uuid'];
  period_id?: Maybe<Scalars['uuid']>;
  recipientName: Scalars['String'];
  recipientPhone: Scalars['String'];
};

export type ActionWithdrawPaymentOutput = {
  id: Scalars['uuid'];
};

/** expression to compare columns of type Boolean. All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
  _eq?: Maybe<Scalars['Boolean']>;
  _gt?: Maybe<Scalars['Boolean']>;
  _gte?: Maybe<Scalars['Boolean']>;
  _in?: Maybe<Array<Scalars['Boolean']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Boolean']>;
  _lte?: Maybe<Scalars['Boolean']>;
  _neq?: Maybe<Scalars['Boolean']>;
  _nin?: Maybe<Array<Scalars['Boolean']>>;
};

export type InsertCreateInput = {
  amount: Scalars['numeric'];
  group_id: Scalars['uuid'];
  member_id: Scalars['uuid'];
  period_id: Scalars['uuid'];
};

export type InsertPaymentOneActionInput = {
  amount: Scalars['numeric'];
  group_id: Scalars['uuid'];
  period_id: Scalars['uuid'];
  user_id: Scalars['uuid'];
};

export type InsertPaymentOneActionOutput = {
  url: Scalars['String'];
};

export type InsertPaymentOutput = {
  id: Scalars['uuid'];
  url: Scalars['String'];
};

/** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: Maybe<Scalars['Int']>;
  _gt?: Maybe<Scalars['Int']>;
  _gte?: Maybe<Scalars['Int']>;
  _in?: Maybe<Array<Scalars['Int']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Int']>;
  _lte?: Maybe<Scalars['Int']>;
  _neq?: Maybe<Scalars['Int']>;
  _nin?: Maybe<Array<Scalars['Int']>>;
};

export type PaymentCreateInput = {
  amount: Scalars['numeric'];
  group_id: Scalars['uuid'];
  period_id: Scalars['uuid'];
  user_id: Scalars['uuid'];
};

export type PaymentCreatedOutput = {
  url: Scalars['String'];
};

/** expression to compare columns of type String. All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: Maybe<Scalars['String']>;
  _gt?: Maybe<Scalars['String']>;
  _gte?: Maybe<Scalars['String']>;
  _ilike?: Maybe<Scalars['String']>;
  _in?: Maybe<Array<Scalars['String']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _like?: Maybe<Scalars['String']>;
  _lt?: Maybe<Scalars['String']>;
  _lte?: Maybe<Scalars['String']>;
  _neq?: Maybe<Scalars['String']>;
  _nilike?: Maybe<Scalars['String']>;
  _nin?: Maybe<Array<Scalars['String']>>;
  _nlike?: Maybe<Scalars['String']>;
  _nsimilar?: Maybe<Scalars['String']>;
  _similar?: Maybe<Scalars['String']>;
};

/** columns and relationships of "auth.account_providers" */
export type Auth_Account_Providers = {
  /** An object relationship */
  account: Auth_Accounts;
  account_id: Scalars['uuid'];
  auth_provider: Scalars['String'];
  auth_provider_unique_id: Scalars['String'];
  created_at: Scalars['timestamptz'];
  id: Scalars['uuid'];
  /** An object relationship */
  provider: Auth_Providers;
  updated_at: Scalars['timestamptz'];
};

/** aggregated selection of "auth.account_providers" */
export type Auth_Account_Providers_Aggregate = {
  aggregate?: Maybe<Auth_Account_Providers_Aggregate_Fields>;
  nodes: Array<Auth_Account_Providers>;
};

/** aggregate fields of "auth.account_providers" */
export type Auth_Account_Providers_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Auth_Account_Providers_Max_Fields>;
  min?: Maybe<Auth_Account_Providers_Min_Fields>;
};


/** aggregate fields of "auth.account_providers" */
export type Auth_Account_Providers_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Account_Providers_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "auth.account_providers" */
export type Auth_Account_Providers_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Auth_Account_Providers_Max_Order_By>;
  min?: Maybe<Auth_Account_Providers_Min_Order_By>;
};

/** input type for inserting array relation for remote table "auth.account_providers" */
export type Auth_Account_Providers_Arr_Rel_Insert_Input = {
  data: Array<Auth_Account_Providers_Insert_Input>;
  on_conflict?: Maybe<Auth_Account_Providers_On_Conflict>;
};

/** Boolean expression to filter rows from the table "auth.account_providers". All fields are combined with a logical 'AND'. */
export type Auth_Account_Providers_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Account_Providers_Bool_Exp>>>;
  _not?: Maybe<Auth_Account_Providers_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Auth_Account_Providers_Bool_Exp>>>;
  account?: Maybe<Auth_Accounts_Bool_Exp>;
  account_id?: Maybe<Uuid_Comparison_Exp>;
  auth_provider?: Maybe<String_Comparison_Exp>;
  auth_provider_unique_id?: Maybe<String_Comparison_Exp>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  provider?: Maybe<Auth_Providers_Bool_Exp>;
  updated_at?: Maybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "auth.account_providers" */
export enum Auth_Account_Providers_Constraint {
  /** unique or primary key constraint */
  AccountProvidersAccountIdAuthProviderKey = 'account_providers_account_id_auth_provider_key',
  /** unique or primary key constraint */
  AccountProvidersAuthProviderAuthProviderUniqueIdKey = 'account_providers_auth_provider_auth_provider_unique_id_key',
  /** unique or primary key constraint */
  AccountProvidersPkey = 'account_providers_pkey'
}

/** input type for inserting data into table "auth.account_providers" */
export type Auth_Account_Providers_Insert_Input = {
  account?: Maybe<Auth_Accounts_Obj_Rel_Insert_Input>;
  account_id?: Maybe<Scalars['uuid']>;
  auth_provider?: Maybe<Scalars['String']>;
  auth_provider_unique_id?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  provider?: Maybe<Auth_Providers_Obj_Rel_Insert_Input>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type Auth_Account_Providers_Max_Fields = {
  account_id?: Maybe<Scalars['uuid']>;
  auth_provider?: Maybe<Scalars['String']>;
  auth_provider_unique_id?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "auth.account_providers" */
export type Auth_Account_Providers_Max_Order_By = {
  account_id?: Maybe<Order_By>;
  auth_provider?: Maybe<Order_By>;
  auth_provider_unique_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Auth_Account_Providers_Min_Fields = {
  account_id?: Maybe<Scalars['uuid']>;
  auth_provider?: Maybe<Scalars['String']>;
  auth_provider_unique_id?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "auth.account_providers" */
export type Auth_Account_Providers_Min_Order_By = {
  account_id?: Maybe<Order_By>;
  auth_provider?: Maybe<Order_By>;
  auth_provider_unique_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** response of any mutation on the table "auth.account_providers" */
export type Auth_Account_Providers_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Account_Providers>;
};

/** input type for inserting object relation for remote table "auth.account_providers" */
export type Auth_Account_Providers_Obj_Rel_Insert_Input = {
  data: Auth_Account_Providers_Insert_Input;
  on_conflict?: Maybe<Auth_Account_Providers_On_Conflict>;
};

/** on conflict condition type for table "auth.account_providers" */
export type Auth_Account_Providers_On_Conflict = {
  constraint: Auth_Account_Providers_Constraint;
  update_columns: Array<Auth_Account_Providers_Update_Column>;
  where?: Maybe<Auth_Account_Providers_Bool_Exp>;
};

/** ordering options when selecting data from "auth.account_providers" */
export type Auth_Account_Providers_Order_By = {
  account?: Maybe<Auth_Accounts_Order_By>;
  account_id?: Maybe<Order_By>;
  auth_provider?: Maybe<Order_By>;
  auth_provider_unique_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  provider?: Maybe<Auth_Providers_Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** primary key columns input for table: "auth.account_providers" */
export type Auth_Account_Providers_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "auth.account_providers" */
export enum Auth_Account_Providers_Select_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  AuthProvider = 'auth_provider',
  /** column name */
  AuthProviderUniqueId = 'auth_provider_unique_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "auth.account_providers" */
export type Auth_Account_Providers_Set_Input = {
  account_id?: Maybe<Scalars['uuid']>;
  auth_provider?: Maybe<Scalars['String']>;
  auth_provider_unique_id?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** update columns of table "auth.account_providers" */
export enum Auth_Account_Providers_Update_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  AuthProvider = 'auth_provider',
  /** column name */
  AuthProviderUniqueId = 'auth_provider_unique_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** columns and relationships of "auth.account_roles" */
export type Auth_Account_Roles = {
  /** An object relationship */
  account: Auth_Accounts;
  account_id: Scalars['uuid'];
  created_at: Scalars['timestamptz'];
  id: Scalars['uuid'];
  role: Scalars['String'];
  /** An object relationship */
  roleByRole: Auth_Roles;
};

/** aggregated selection of "auth.account_roles" */
export type Auth_Account_Roles_Aggregate = {
  aggregate?: Maybe<Auth_Account_Roles_Aggregate_Fields>;
  nodes: Array<Auth_Account_Roles>;
};

/** aggregate fields of "auth.account_roles" */
export type Auth_Account_Roles_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Auth_Account_Roles_Max_Fields>;
  min?: Maybe<Auth_Account_Roles_Min_Fields>;
};


/** aggregate fields of "auth.account_roles" */
export type Auth_Account_Roles_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Account_Roles_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "auth.account_roles" */
export type Auth_Account_Roles_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Auth_Account_Roles_Max_Order_By>;
  min?: Maybe<Auth_Account_Roles_Min_Order_By>;
};

/** input type for inserting array relation for remote table "auth.account_roles" */
export type Auth_Account_Roles_Arr_Rel_Insert_Input = {
  data: Array<Auth_Account_Roles_Insert_Input>;
  on_conflict?: Maybe<Auth_Account_Roles_On_Conflict>;
};

/** Boolean expression to filter rows from the table "auth.account_roles". All fields are combined with a logical 'AND'. */
export type Auth_Account_Roles_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Account_Roles_Bool_Exp>>>;
  _not?: Maybe<Auth_Account_Roles_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Auth_Account_Roles_Bool_Exp>>>;
  account?: Maybe<Auth_Accounts_Bool_Exp>;
  account_id?: Maybe<Uuid_Comparison_Exp>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  role?: Maybe<String_Comparison_Exp>;
  roleByRole?: Maybe<Auth_Roles_Bool_Exp>;
};

/** unique or primary key constraints on table "auth.account_roles" */
export enum Auth_Account_Roles_Constraint {
  /** unique or primary key constraint */
  AccountRolesPkey = 'account_roles_pkey',
  /** unique or primary key constraint */
  UserRolesAccountIdRoleKey = 'user_roles_account_id_role_key'
}

/** input type for inserting data into table "auth.account_roles" */
export type Auth_Account_Roles_Insert_Input = {
  account?: Maybe<Auth_Accounts_Obj_Rel_Insert_Input>;
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  role?: Maybe<Scalars['String']>;
  roleByRole?: Maybe<Auth_Roles_Obj_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type Auth_Account_Roles_Max_Fields = {
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  role?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "auth.account_roles" */
export type Auth_Account_Roles_Max_Order_By = {
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  role?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Auth_Account_Roles_Min_Fields = {
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  role?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "auth.account_roles" */
export type Auth_Account_Roles_Min_Order_By = {
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  role?: Maybe<Order_By>;
};

/** response of any mutation on the table "auth.account_roles" */
export type Auth_Account_Roles_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Account_Roles>;
};

/** input type for inserting object relation for remote table "auth.account_roles" */
export type Auth_Account_Roles_Obj_Rel_Insert_Input = {
  data: Auth_Account_Roles_Insert_Input;
  on_conflict?: Maybe<Auth_Account_Roles_On_Conflict>;
};

/** on conflict condition type for table "auth.account_roles" */
export type Auth_Account_Roles_On_Conflict = {
  constraint: Auth_Account_Roles_Constraint;
  update_columns: Array<Auth_Account_Roles_Update_Column>;
  where?: Maybe<Auth_Account_Roles_Bool_Exp>;
};

/** ordering options when selecting data from "auth.account_roles" */
export type Auth_Account_Roles_Order_By = {
  account?: Maybe<Auth_Accounts_Order_By>;
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  role?: Maybe<Order_By>;
  roleByRole?: Maybe<Auth_Roles_Order_By>;
};

/** primary key columns input for table: "auth.account_roles" */
export type Auth_Account_Roles_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "auth.account_roles" */
export enum Auth_Account_Roles_Select_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Role = 'role'
}

/** input type for updating data in table "auth.account_roles" */
export type Auth_Account_Roles_Set_Input = {
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  role?: Maybe<Scalars['String']>;
};

/** update columns of table "auth.account_roles" */
export enum Auth_Account_Roles_Update_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Role = 'role'
}

/** columns and relationships of "auth.accounts" */
export type Auth_Accounts = {
  /** An array relationship */
  account_providers: Array<Auth_Account_Providers>;
  /** An aggregated array relationship */
  account_providers_aggregate: Auth_Account_Providers_Aggregate;
  /** An array relationship */
  account_roles: Array<Auth_Account_Roles>;
  /** An aggregated array relationship */
  account_roles_aggregate: Auth_Account_Roles_Aggregate;
  active: Scalars['Boolean'];
  created_at: Scalars['timestamptz'];
  custom_register_data?: Maybe<Scalars['jsonb']>;
  default_role: Scalars['String'];
  email?: Maybe<Scalars['citext']>;
  id: Scalars['uuid'];
  is_anonymous: Scalars['Boolean'];
  mfa_enabled: Scalars['Boolean'];
  new_email?: Maybe<Scalars['citext']>;
  otp_secret?: Maybe<Scalars['String']>;
  password_hash?: Maybe<Scalars['String']>;
  /** An array relationship */
  refresh_tokens: Array<Auth_Refresh_Tokens>;
  /** An aggregated array relationship */
  refresh_tokens_aggregate: Auth_Refresh_Tokens_Aggregate;
  /** An object relationship */
  role: Auth_Roles;
  ticket: Scalars['uuid'];
  ticket_expires_at: Scalars['timestamptz'];
  updated_at: Scalars['timestamptz'];
  /** An object relationship */
  user: Users;
  user_id: Scalars['uuid'];
};


/** columns and relationships of "auth.accounts" */
export type Auth_AccountsAccount_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>;
  where?: Maybe<Auth_Account_Providers_Bool_Exp>;
};


/** columns and relationships of "auth.accounts" */
export type Auth_AccountsAccount_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>;
  where?: Maybe<Auth_Account_Providers_Bool_Exp>;
};


/** columns and relationships of "auth.accounts" */
export type Auth_AccountsAccount_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>;
  where?: Maybe<Auth_Account_Roles_Bool_Exp>;
};


/** columns and relationships of "auth.accounts" */
export type Auth_AccountsAccount_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>;
  where?: Maybe<Auth_Account_Roles_Bool_Exp>;
};


/** columns and relationships of "auth.accounts" */
export type Auth_AccountsCustom_Register_DataArgs = {
  path?: Maybe<Scalars['String']>;
};


/** columns and relationships of "auth.accounts" */
export type Auth_AccountsRefresh_TokensArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>;
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>;
};


/** columns and relationships of "auth.accounts" */
export type Auth_AccountsRefresh_Tokens_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>;
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>;
};

/** aggregated selection of "auth.accounts" */
export type Auth_Accounts_Aggregate = {
  aggregate?: Maybe<Auth_Accounts_Aggregate_Fields>;
  nodes: Array<Auth_Accounts>;
};

/** aggregate fields of "auth.accounts" */
export type Auth_Accounts_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Auth_Accounts_Max_Fields>;
  min?: Maybe<Auth_Accounts_Min_Fields>;
};


/** aggregate fields of "auth.accounts" */
export type Auth_Accounts_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Accounts_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "auth.accounts" */
export type Auth_Accounts_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Auth_Accounts_Max_Order_By>;
  min?: Maybe<Auth_Accounts_Min_Order_By>;
};

/** append existing jsonb value of filtered columns with new jsonb value */
export type Auth_Accounts_Append_Input = {
  custom_register_data?: Maybe<Scalars['jsonb']>;
};

/** input type for inserting array relation for remote table "auth.accounts" */
export type Auth_Accounts_Arr_Rel_Insert_Input = {
  data: Array<Auth_Accounts_Insert_Input>;
  on_conflict?: Maybe<Auth_Accounts_On_Conflict>;
};

/** Boolean expression to filter rows from the table "auth.accounts". All fields are combined with a logical 'AND'. */
export type Auth_Accounts_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Accounts_Bool_Exp>>>;
  _not?: Maybe<Auth_Accounts_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Auth_Accounts_Bool_Exp>>>;
  account_providers?: Maybe<Auth_Account_Providers_Bool_Exp>;
  account_roles?: Maybe<Auth_Account_Roles_Bool_Exp>;
  active?: Maybe<Boolean_Comparison_Exp>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  custom_register_data?: Maybe<Jsonb_Comparison_Exp>;
  default_role?: Maybe<String_Comparison_Exp>;
  email?: Maybe<Citext_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  is_anonymous?: Maybe<Boolean_Comparison_Exp>;
  mfa_enabled?: Maybe<Boolean_Comparison_Exp>;
  new_email?: Maybe<Citext_Comparison_Exp>;
  otp_secret?: Maybe<String_Comparison_Exp>;
  password_hash?: Maybe<String_Comparison_Exp>;
  refresh_tokens?: Maybe<Auth_Refresh_Tokens_Bool_Exp>;
  role?: Maybe<Auth_Roles_Bool_Exp>;
  ticket?: Maybe<Uuid_Comparison_Exp>;
  ticket_expires_at?: Maybe<Timestamptz_Comparison_Exp>;
  updated_at?: Maybe<Timestamptz_Comparison_Exp>;
  user?: Maybe<Users_Bool_Exp>;
  user_id?: Maybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "auth.accounts" */
export enum Auth_Accounts_Constraint {
  /** unique or primary key constraint */
  AccountsEmailKey = 'accounts_email_key',
  /** unique or primary key constraint */
  AccountsNewEmailKey = 'accounts_new_email_key',
  /** unique or primary key constraint */
  AccountsPkey = 'accounts_pkey',
  /** unique or primary key constraint */
  AccountsUserIdKey = 'accounts_user_id_key'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Auth_Accounts_Delete_At_Path_Input = {
  custom_register_data?: Maybe<Array<Maybe<Scalars['String']>>>;
};

/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
export type Auth_Accounts_Delete_Elem_Input = {
  custom_register_data?: Maybe<Scalars['Int']>;
};

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Auth_Accounts_Delete_Key_Input = {
  custom_register_data?: Maybe<Scalars['String']>;
};

/** input type for inserting data into table "auth.accounts" */
export type Auth_Accounts_Insert_Input = {
  account_providers?: Maybe<Auth_Account_Providers_Arr_Rel_Insert_Input>;
  account_roles?: Maybe<Auth_Account_Roles_Arr_Rel_Insert_Input>;
  active?: Maybe<Scalars['Boolean']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  custom_register_data?: Maybe<Scalars['jsonb']>;
  default_role?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['citext']>;
  id?: Maybe<Scalars['uuid']>;
  is_anonymous?: Maybe<Scalars['Boolean']>;
  mfa_enabled?: Maybe<Scalars['Boolean']>;
  new_email?: Maybe<Scalars['citext']>;
  otp_secret?: Maybe<Scalars['String']>;
  password_hash?: Maybe<Scalars['String']>;
  refresh_tokens?: Maybe<Auth_Refresh_Tokens_Arr_Rel_Insert_Input>;
  role?: Maybe<Auth_Roles_Obj_Rel_Insert_Input>;
  ticket?: Maybe<Scalars['uuid']>;
  ticket_expires_at?: Maybe<Scalars['timestamptz']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  user?: Maybe<Users_Obj_Rel_Insert_Input>;
  user_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Auth_Accounts_Max_Fields = {
  created_at?: Maybe<Scalars['timestamptz']>;
  default_role?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['citext']>;
  id?: Maybe<Scalars['uuid']>;
  new_email?: Maybe<Scalars['citext']>;
  otp_secret?: Maybe<Scalars['String']>;
  password_hash?: Maybe<Scalars['String']>;
  ticket?: Maybe<Scalars['uuid']>;
  ticket_expires_at?: Maybe<Scalars['timestamptz']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "auth.accounts" */
export type Auth_Accounts_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  default_role?: Maybe<Order_By>;
  email?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  new_email?: Maybe<Order_By>;
  otp_secret?: Maybe<Order_By>;
  password_hash?: Maybe<Order_By>;
  ticket?: Maybe<Order_By>;
  ticket_expires_at?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Auth_Accounts_Min_Fields = {
  created_at?: Maybe<Scalars['timestamptz']>;
  default_role?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['citext']>;
  id?: Maybe<Scalars['uuid']>;
  new_email?: Maybe<Scalars['citext']>;
  otp_secret?: Maybe<Scalars['String']>;
  password_hash?: Maybe<Scalars['String']>;
  ticket?: Maybe<Scalars['uuid']>;
  ticket_expires_at?: Maybe<Scalars['timestamptz']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "auth.accounts" */
export type Auth_Accounts_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  default_role?: Maybe<Order_By>;
  email?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  new_email?: Maybe<Order_By>;
  otp_secret?: Maybe<Order_By>;
  password_hash?: Maybe<Order_By>;
  ticket?: Maybe<Order_By>;
  ticket_expires_at?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "auth.accounts" */
export type Auth_Accounts_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Accounts>;
};

/** input type for inserting object relation for remote table "auth.accounts" */
export type Auth_Accounts_Obj_Rel_Insert_Input = {
  data: Auth_Accounts_Insert_Input;
  on_conflict?: Maybe<Auth_Accounts_On_Conflict>;
};

/** on conflict condition type for table "auth.accounts" */
export type Auth_Accounts_On_Conflict = {
  constraint: Auth_Accounts_Constraint;
  update_columns: Array<Auth_Accounts_Update_Column>;
  where?: Maybe<Auth_Accounts_Bool_Exp>;
};

/** ordering options when selecting data from "auth.accounts" */
export type Auth_Accounts_Order_By = {
  account_providers_aggregate?: Maybe<Auth_Account_Providers_Aggregate_Order_By>;
  account_roles_aggregate?: Maybe<Auth_Account_Roles_Aggregate_Order_By>;
  active?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  custom_register_data?: Maybe<Order_By>;
  default_role?: Maybe<Order_By>;
  email?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  is_anonymous?: Maybe<Order_By>;
  mfa_enabled?: Maybe<Order_By>;
  new_email?: Maybe<Order_By>;
  otp_secret?: Maybe<Order_By>;
  password_hash?: Maybe<Order_By>;
  refresh_tokens_aggregate?: Maybe<Auth_Refresh_Tokens_Aggregate_Order_By>;
  role?: Maybe<Auth_Roles_Order_By>;
  ticket?: Maybe<Order_By>;
  ticket_expires_at?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  user?: Maybe<Users_Order_By>;
  user_id?: Maybe<Order_By>;
};

/** primary key columns input for table: "auth.accounts" */
export type Auth_Accounts_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Auth_Accounts_Prepend_Input = {
  custom_register_data?: Maybe<Scalars['jsonb']>;
};

/** select columns of table "auth.accounts" */
export enum Auth_Accounts_Select_Column {
  /** column name */
  Active = 'active',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CustomRegisterData = 'custom_register_data',
  /** column name */
  DefaultRole = 'default_role',
  /** column name */
  Email = 'email',
  /** column name */
  Id = 'id',
  /** column name */
  IsAnonymous = 'is_anonymous',
  /** column name */
  MfaEnabled = 'mfa_enabled',
  /** column name */
  NewEmail = 'new_email',
  /** column name */
  OtpSecret = 'otp_secret',
  /** column name */
  PasswordHash = 'password_hash',
  /** column name */
  Ticket = 'ticket',
  /** column name */
  TicketExpiresAt = 'ticket_expires_at',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "auth.accounts" */
export type Auth_Accounts_Set_Input = {
  active?: Maybe<Scalars['Boolean']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  custom_register_data?: Maybe<Scalars['jsonb']>;
  default_role?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['citext']>;
  id?: Maybe<Scalars['uuid']>;
  is_anonymous?: Maybe<Scalars['Boolean']>;
  mfa_enabled?: Maybe<Scalars['Boolean']>;
  new_email?: Maybe<Scalars['citext']>;
  otp_secret?: Maybe<Scalars['String']>;
  password_hash?: Maybe<Scalars['String']>;
  ticket?: Maybe<Scalars['uuid']>;
  ticket_expires_at?: Maybe<Scalars['timestamptz']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['uuid']>;
};

/** update columns of table "auth.accounts" */
export enum Auth_Accounts_Update_Column {
  /** column name */
  Active = 'active',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CustomRegisterData = 'custom_register_data',
  /** column name */
  DefaultRole = 'default_role',
  /** column name */
  Email = 'email',
  /** column name */
  Id = 'id',
  /** column name */
  IsAnonymous = 'is_anonymous',
  /** column name */
  MfaEnabled = 'mfa_enabled',
  /** column name */
  NewEmail = 'new_email',
  /** column name */
  OtpSecret = 'otp_secret',
  /** column name */
  PasswordHash = 'password_hash',
  /** column name */
  Ticket = 'ticket',
  /** column name */
  TicketExpiresAt = 'ticket_expires_at',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** columns and relationships of "auth.providers" */
export type Auth_Providers = {
  /** An array relationship */
  account_providers: Array<Auth_Account_Providers>;
  /** An aggregated array relationship */
  account_providers_aggregate: Auth_Account_Providers_Aggregate;
  provider: Scalars['String'];
};


/** columns and relationships of "auth.providers" */
export type Auth_ProvidersAccount_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>;
  where?: Maybe<Auth_Account_Providers_Bool_Exp>;
};


/** columns and relationships of "auth.providers" */
export type Auth_ProvidersAccount_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>;
  where?: Maybe<Auth_Account_Providers_Bool_Exp>;
};

/** aggregated selection of "auth.providers" */
export type Auth_Providers_Aggregate = {
  aggregate?: Maybe<Auth_Providers_Aggregate_Fields>;
  nodes: Array<Auth_Providers>;
};

/** aggregate fields of "auth.providers" */
export type Auth_Providers_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Auth_Providers_Max_Fields>;
  min?: Maybe<Auth_Providers_Min_Fields>;
};


/** aggregate fields of "auth.providers" */
export type Auth_Providers_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Providers_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "auth.providers" */
export type Auth_Providers_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Auth_Providers_Max_Order_By>;
  min?: Maybe<Auth_Providers_Min_Order_By>;
};

/** input type for inserting array relation for remote table "auth.providers" */
export type Auth_Providers_Arr_Rel_Insert_Input = {
  data: Array<Auth_Providers_Insert_Input>;
  on_conflict?: Maybe<Auth_Providers_On_Conflict>;
};

/** Boolean expression to filter rows from the table "auth.providers". All fields are combined with a logical 'AND'. */
export type Auth_Providers_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Providers_Bool_Exp>>>;
  _not?: Maybe<Auth_Providers_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Auth_Providers_Bool_Exp>>>;
  account_providers?: Maybe<Auth_Account_Providers_Bool_Exp>;
  provider?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "auth.providers" */
export enum Auth_Providers_Constraint {
  /** unique or primary key constraint */
  ProvidersPkey = 'providers_pkey'
}

/** input type for inserting data into table "auth.providers" */
export type Auth_Providers_Insert_Input = {
  account_providers?: Maybe<Auth_Account_Providers_Arr_Rel_Insert_Input>;
  provider?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Auth_Providers_Max_Fields = {
  provider?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "auth.providers" */
export type Auth_Providers_Max_Order_By = {
  provider?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Auth_Providers_Min_Fields = {
  provider?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "auth.providers" */
export type Auth_Providers_Min_Order_By = {
  provider?: Maybe<Order_By>;
};

/** response of any mutation on the table "auth.providers" */
export type Auth_Providers_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Providers>;
};

/** input type for inserting object relation for remote table "auth.providers" */
export type Auth_Providers_Obj_Rel_Insert_Input = {
  data: Auth_Providers_Insert_Input;
  on_conflict?: Maybe<Auth_Providers_On_Conflict>;
};

/** on conflict condition type for table "auth.providers" */
export type Auth_Providers_On_Conflict = {
  constraint: Auth_Providers_Constraint;
  update_columns: Array<Auth_Providers_Update_Column>;
  where?: Maybe<Auth_Providers_Bool_Exp>;
};

/** ordering options when selecting data from "auth.providers" */
export type Auth_Providers_Order_By = {
  account_providers_aggregate?: Maybe<Auth_Account_Providers_Aggregate_Order_By>;
  provider?: Maybe<Order_By>;
};

/** primary key columns input for table: "auth.providers" */
export type Auth_Providers_Pk_Columns_Input = {
  provider: Scalars['String'];
};

/** select columns of table "auth.providers" */
export enum Auth_Providers_Select_Column {
  /** column name */
  Provider = 'provider'
}

/** input type for updating data in table "auth.providers" */
export type Auth_Providers_Set_Input = {
  provider?: Maybe<Scalars['String']>;
};

/** update columns of table "auth.providers" */
export enum Auth_Providers_Update_Column {
  /** column name */
  Provider = 'provider'
}

/** columns and relationships of "auth.refresh_tokens" */
export type Auth_Refresh_Tokens = {
  /** An object relationship */
  account: Auth_Accounts;
  account_id: Scalars['uuid'];
  created_at: Scalars['timestamptz'];
  expires_at: Scalars['timestamptz'];
  refresh_token: Scalars['uuid'];
};

/** aggregated selection of "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Aggregate = {
  aggregate?: Maybe<Auth_Refresh_Tokens_Aggregate_Fields>;
  nodes: Array<Auth_Refresh_Tokens>;
};

/** aggregate fields of "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Auth_Refresh_Tokens_Max_Fields>;
  min?: Maybe<Auth_Refresh_Tokens_Min_Fields>;
};


/** aggregate fields of "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Auth_Refresh_Tokens_Max_Order_By>;
  min?: Maybe<Auth_Refresh_Tokens_Min_Order_By>;
};

/** input type for inserting array relation for remote table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Arr_Rel_Insert_Input = {
  data: Array<Auth_Refresh_Tokens_Insert_Input>;
  on_conflict?: Maybe<Auth_Refresh_Tokens_On_Conflict>;
};

/** Boolean expression to filter rows from the table "auth.refresh_tokens". All fields are combined with a logical 'AND'. */
export type Auth_Refresh_Tokens_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Refresh_Tokens_Bool_Exp>>>;
  _not?: Maybe<Auth_Refresh_Tokens_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Auth_Refresh_Tokens_Bool_Exp>>>;
  account?: Maybe<Auth_Accounts_Bool_Exp>;
  account_id?: Maybe<Uuid_Comparison_Exp>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  expires_at?: Maybe<Timestamptz_Comparison_Exp>;
  refresh_token?: Maybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "auth.refresh_tokens" */
export enum Auth_Refresh_Tokens_Constraint {
  /** unique or primary key constraint */
  RefreshTokensPkey = 'refresh_tokens_pkey'
}

/** input type for inserting data into table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Insert_Input = {
  account?: Maybe<Auth_Accounts_Obj_Rel_Insert_Input>;
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  expires_at?: Maybe<Scalars['timestamptz']>;
  refresh_token?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Auth_Refresh_Tokens_Max_Fields = {
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  expires_at?: Maybe<Scalars['timestamptz']>;
  refresh_token?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Max_Order_By = {
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  expires_at?: Maybe<Order_By>;
  refresh_token?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Auth_Refresh_Tokens_Min_Fields = {
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  expires_at?: Maybe<Scalars['timestamptz']>;
  refresh_token?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Min_Order_By = {
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  expires_at?: Maybe<Order_By>;
  refresh_token?: Maybe<Order_By>;
};

/** response of any mutation on the table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Refresh_Tokens>;
};

/** input type for inserting object relation for remote table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Obj_Rel_Insert_Input = {
  data: Auth_Refresh_Tokens_Insert_Input;
  on_conflict?: Maybe<Auth_Refresh_Tokens_On_Conflict>;
};

/** on conflict condition type for table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_On_Conflict = {
  constraint: Auth_Refresh_Tokens_Constraint;
  update_columns: Array<Auth_Refresh_Tokens_Update_Column>;
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>;
};

/** ordering options when selecting data from "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Order_By = {
  account?: Maybe<Auth_Accounts_Order_By>;
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  expires_at?: Maybe<Order_By>;
  refresh_token?: Maybe<Order_By>;
};

/** primary key columns input for table: "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Pk_Columns_Input = {
  refresh_token: Scalars['uuid'];
};

/** select columns of table "auth.refresh_tokens" */
export enum Auth_Refresh_Tokens_Select_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  ExpiresAt = 'expires_at',
  /** column name */
  RefreshToken = 'refresh_token'
}

/** input type for updating data in table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Set_Input = {
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  expires_at?: Maybe<Scalars['timestamptz']>;
  refresh_token?: Maybe<Scalars['uuid']>;
};

/** update columns of table "auth.refresh_tokens" */
export enum Auth_Refresh_Tokens_Update_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  ExpiresAt = 'expires_at',
  /** column name */
  RefreshToken = 'refresh_token'
}

/** columns and relationships of "auth.roles" */
export type Auth_Roles = {
  /** An array relationship */
  account_roles: Array<Auth_Account_Roles>;
  /** An aggregated array relationship */
  account_roles_aggregate: Auth_Account_Roles_Aggregate;
  /** An array relationship */
  accounts: Array<Auth_Accounts>;
  /** An aggregated array relationship */
  accounts_aggregate: Auth_Accounts_Aggregate;
  role: Scalars['String'];
};


/** columns and relationships of "auth.roles" */
export type Auth_RolesAccount_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>;
  where?: Maybe<Auth_Account_Roles_Bool_Exp>;
};


/** columns and relationships of "auth.roles" */
export type Auth_RolesAccount_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>;
  where?: Maybe<Auth_Account_Roles_Bool_Exp>;
};


/** columns and relationships of "auth.roles" */
export type Auth_RolesAccountsArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>;
  where?: Maybe<Auth_Accounts_Bool_Exp>;
};


/** columns and relationships of "auth.roles" */
export type Auth_RolesAccounts_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>;
  where?: Maybe<Auth_Accounts_Bool_Exp>;
};

/** aggregated selection of "auth.roles" */
export type Auth_Roles_Aggregate = {
  aggregate?: Maybe<Auth_Roles_Aggregate_Fields>;
  nodes: Array<Auth_Roles>;
};

/** aggregate fields of "auth.roles" */
export type Auth_Roles_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Auth_Roles_Max_Fields>;
  min?: Maybe<Auth_Roles_Min_Fields>;
};


/** aggregate fields of "auth.roles" */
export type Auth_Roles_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Roles_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "auth.roles" */
export type Auth_Roles_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Auth_Roles_Max_Order_By>;
  min?: Maybe<Auth_Roles_Min_Order_By>;
};

/** input type for inserting array relation for remote table "auth.roles" */
export type Auth_Roles_Arr_Rel_Insert_Input = {
  data: Array<Auth_Roles_Insert_Input>;
  on_conflict?: Maybe<Auth_Roles_On_Conflict>;
};

/** Boolean expression to filter rows from the table "auth.roles". All fields are combined with a logical 'AND'. */
export type Auth_Roles_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Roles_Bool_Exp>>>;
  _not?: Maybe<Auth_Roles_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Auth_Roles_Bool_Exp>>>;
  account_roles?: Maybe<Auth_Account_Roles_Bool_Exp>;
  accounts?: Maybe<Auth_Accounts_Bool_Exp>;
  role?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "auth.roles" */
export enum Auth_Roles_Constraint {
  /** unique or primary key constraint */
  RolesPkey = 'roles_pkey'
}

/** input type for inserting data into table "auth.roles" */
export type Auth_Roles_Insert_Input = {
  account_roles?: Maybe<Auth_Account_Roles_Arr_Rel_Insert_Input>;
  accounts?: Maybe<Auth_Accounts_Arr_Rel_Insert_Input>;
  role?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Auth_Roles_Max_Fields = {
  role?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "auth.roles" */
export type Auth_Roles_Max_Order_By = {
  role?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Auth_Roles_Min_Fields = {
  role?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "auth.roles" */
export type Auth_Roles_Min_Order_By = {
  role?: Maybe<Order_By>;
};

/** response of any mutation on the table "auth.roles" */
export type Auth_Roles_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Roles>;
};

/** input type for inserting object relation for remote table "auth.roles" */
export type Auth_Roles_Obj_Rel_Insert_Input = {
  data: Auth_Roles_Insert_Input;
  on_conflict?: Maybe<Auth_Roles_On_Conflict>;
};

/** on conflict condition type for table "auth.roles" */
export type Auth_Roles_On_Conflict = {
  constraint: Auth_Roles_Constraint;
  update_columns: Array<Auth_Roles_Update_Column>;
  where?: Maybe<Auth_Roles_Bool_Exp>;
};

/** ordering options when selecting data from "auth.roles" */
export type Auth_Roles_Order_By = {
  account_roles_aggregate?: Maybe<Auth_Account_Roles_Aggregate_Order_By>;
  accounts_aggregate?: Maybe<Auth_Accounts_Aggregate_Order_By>;
  role?: Maybe<Order_By>;
};

/** primary key columns input for table: "auth.roles" */
export type Auth_Roles_Pk_Columns_Input = {
  role: Scalars['String'];
};

/** select columns of table "auth.roles" */
export enum Auth_Roles_Select_Column {
  /** column name */
  Role = 'role'
}

/** input type for updating data in table "auth.roles" */
export type Auth_Roles_Set_Input = {
  role?: Maybe<Scalars['String']>;
};

/** update columns of table "auth.roles" */
export enum Auth_Roles_Update_Column {
  /** column name */
  Role = 'role'
}


/** expression to compare columns of type citext. All fields are combined with logical 'AND'. */
export type Citext_Comparison_Exp = {
  _eq?: Maybe<Scalars['citext']>;
  _gt?: Maybe<Scalars['citext']>;
  _gte?: Maybe<Scalars['citext']>;
  _ilike?: Maybe<Scalars['String']>;
  _in?: Maybe<Array<Scalars['citext']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _like?: Maybe<Scalars['String']>;
  _lt?: Maybe<Scalars['citext']>;
  _lte?: Maybe<Scalars['citext']>;
  _neq?: Maybe<Scalars['citext']>;
  _nilike?: Maybe<Scalars['String']>;
  _nin?: Maybe<Array<Scalars['citext']>>;
  _nlike?: Maybe<Scalars['String']>;
  _nsimilar?: Maybe<Scalars['String']>;
  _similar?: Maybe<Scalars['String']>;
};

/** columns and relationships of "contribution_types" */
export type Contribution_Types = {
  description: Scalars['String'];
  /** An array relationship */
  groups: Array<Groups>;
  /** An aggregated array relationship */
  groups_aggregate: Groups_Aggregate;
  value: Scalars['String'];
};


/** columns and relationships of "contribution_types" */
export type Contribution_TypesGroupsArgs = {
  distinct_on?: Maybe<Array<Groups_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Groups_Order_By>>;
  where?: Maybe<Groups_Bool_Exp>;
};


/** columns and relationships of "contribution_types" */
export type Contribution_TypesGroups_AggregateArgs = {
  distinct_on?: Maybe<Array<Groups_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Groups_Order_By>>;
  where?: Maybe<Groups_Bool_Exp>;
};

/** aggregated selection of "contribution_types" */
export type Contribution_Types_Aggregate = {
  aggregate?: Maybe<Contribution_Types_Aggregate_Fields>;
  nodes: Array<Contribution_Types>;
};

/** aggregate fields of "contribution_types" */
export type Contribution_Types_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Contribution_Types_Max_Fields>;
  min?: Maybe<Contribution_Types_Min_Fields>;
};


/** aggregate fields of "contribution_types" */
export type Contribution_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Contribution_Types_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "contribution_types" */
export type Contribution_Types_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Contribution_Types_Max_Order_By>;
  min?: Maybe<Contribution_Types_Min_Order_By>;
};

/** input type for inserting array relation for remote table "contribution_types" */
export type Contribution_Types_Arr_Rel_Insert_Input = {
  data: Array<Contribution_Types_Insert_Input>;
  on_conflict?: Maybe<Contribution_Types_On_Conflict>;
};

/** Boolean expression to filter rows from the table "contribution_types". All fields are combined with a logical 'AND'. */
export type Contribution_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Contribution_Types_Bool_Exp>>>;
  _not?: Maybe<Contribution_Types_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Contribution_Types_Bool_Exp>>>;
  description?: Maybe<String_Comparison_Exp>;
  groups?: Maybe<Groups_Bool_Exp>;
  value?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "contribution_types" */
export enum Contribution_Types_Constraint {
  /** unique or primary key constraint */
  ContributionTypesDescriptionKey = 'contribution_types_description_key',
  /** unique or primary key constraint */
  ContributionTypesPkey = 'contribution_types_pkey'
}

export enum Contribution_Types_Enum {
  /** Any amount */
  AnyAmount = 'AnyAmount',
  /** Fix amount */
  FixAmount = 'FixAmount',
  /** Minimum amount */
  MinimumAmount = 'MinimumAmount'
}

/** expression to compare columns of type contribution_types_enum. All fields are combined with logical 'AND'. */
export type Contribution_Types_Enum_Comparison_Exp = {
  _eq?: Maybe<Contribution_Types_Enum>;
  _in?: Maybe<Array<Contribution_Types_Enum>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _neq?: Maybe<Contribution_Types_Enum>;
  _nin?: Maybe<Array<Contribution_Types_Enum>>;
};

/** input type for inserting data into table "contribution_types" */
export type Contribution_Types_Insert_Input = {
  description?: Maybe<Scalars['String']>;
  groups?: Maybe<Groups_Arr_Rel_Insert_Input>;
  value?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Contribution_Types_Max_Fields = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "contribution_types" */
export type Contribution_Types_Max_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Contribution_Types_Min_Fields = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "contribution_types" */
export type Contribution_Types_Min_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** response of any mutation on the table "contribution_types" */
export type Contribution_Types_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Contribution_Types>;
};

/** input type for inserting object relation for remote table "contribution_types" */
export type Contribution_Types_Obj_Rel_Insert_Input = {
  data: Contribution_Types_Insert_Input;
  on_conflict?: Maybe<Contribution_Types_On_Conflict>;
};

/** on conflict condition type for table "contribution_types" */
export type Contribution_Types_On_Conflict = {
  constraint: Contribution_Types_Constraint;
  update_columns: Array<Contribution_Types_Update_Column>;
  where?: Maybe<Contribution_Types_Bool_Exp>;
};

/** ordering options when selecting data from "contribution_types" */
export type Contribution_Types_Order_By = {
  description?: Maybe<Order_By>;
  groups_aggregate?: Maybe<Groups_Aggregate_Order_By>;
  value?: Maybe<Order_By>;
};

/** primary key columns input for table: "contribution_types" */
export type Contribution_Types_Pk_Columns_Input = {
  value: Scalars['String'];
};

/** select columns of table "contribution_types" */
export enum Contribution_Types_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "contribution_types" */
export type Contribution_Types_Set_Input = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** update columns of table "contribution_types" */
export enum Contribution_Types_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** columns and relationships of "group_cycles" */
export type Group_Cycles = {
  description: Scalars['String'];
  value: Scalars['String'];
};

/** aggregated selection of "group_cycles" */
export type Group_Cycles_Aggregate = {
  aggregate?: Maybe<Group_Cycles_Aggregate_Fields>;
  nodes: Array<Group_Cycles>;
};

/** aggregate fields of "group_cycles" */
export type Group_Cycles_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Group_Cycles_Max_Fields>;
  min?: Maybe<Group_Cycles_Min_Fields>;
};


/** aggregate fields of "group_cycles" */
export type Group_Cycles_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Group_Cycles_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "group_cycles" */
export type Group_Cycles_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Group_Cycles_Max_Order_By>;
  min?: Maybe<Group_Cycles_Min_Order_By>;
};

/** input type for inserting array relation for remote table "group_cycles" */
export type Group_Cycles_Arr_Rel_Insert_Input = {
  data: Array<Group_Cycles_Insert_Input>;
  on_conflict?: Maybe<Group_Cycles_On_Conflict>;
};

/** Boolean expression to filter rows from the table "group_cycles". All fields are combined with a logical 'AND'. */
export type Group_Cycles_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Group_Cycles_Bool_Exp>>>;
  _not?: Maybe<Group_Cycles_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Group_Cycles_Bool_Exp>>>;
  description?: Maybe<String_Comparison_Exp>;
  value?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "group_cycles" */
export enum Group_Cycles_Constraint {
  /** unique or primary key constraint */
  GroupCyclesPkey = 'group_cycles_pkey'
}

/** input type for inserting data into table "group_cycles" */
export type Group_Cycles_Insert_Input = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Group_Cycles_Max_Fields = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "group_cycles" */
export type Group_Cycles_Max_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Group_Cycles_Min_Fields = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "group_cycles" */
export type Group_Cycles_Min_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** response of any mutation on the table "group_cycles" */
export type Group_Cycles_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Group_Cycles>;
};

/** input type for inserting object relation for remote table "group_cycles" */
export type Group_Cycles_Obj_Rel_Insert_Input = {
  data: Group_Cycles_Insert_Input;
  on_conflict?: Maybe<Group_Cycles_On_Conflict>;
};

/** on conflict condition type for table "group_cycles" */
export type Group_Cycles_On_Conflict = {
  constraint: Group_Cycles_Constraint;
  update_columns: Array<Group_Cycles_Update_Column>;
  where?: Maybe<Group_Cycles_Bool_Exp>;
};

/** ordering options when selecting data from "group_cycles" */
export type Group_Cycles_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** primary key columns input for table: "group_cycles" */
export type Group_Cycles_Pk_Columns_Input = {
  value: Scalars['String'];
};

/** select columns of table "group_cycles" */
export enum Group_Cycles_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "group_cycles" */
export type Group_Cycles_Set_Input = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** update columns of table "group_cycles" */
export enum Group_Cycles_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** columns and relationships of "group_recurrencies" */
export type Group_Recurrencies = {
  description: Scalars['String'];
  /** An array relationship */
  groups: Array<Groups>;
  /** An aggregated array relationship */
  groups_aggregate: Groups_Aggregate;
  value: Scalars['String'];
};


/** columns and relationships of "group_recurrencies" */
export type Group_RecurrenciesGroupsArgs = {
  distinct_on?: Maybe<Array<Groups_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Groups_Order_By>>;
  where?: Maybe<Groups_Bool_Exp>;
};


/** columns and relationships of "group_recurrencies" */
export type Group_RecurrenciesGroups_AggregateArgs = {
  distinct_on?: Maybe<Array<Groups_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Groups_Order_By>>;
  where?: Maybe<Groups_Bool_Exp>;
};

/** aggregated selection of "group_recurrencies" */
export type Group_Recurrencies_Aggregate = {
  aggregate?: Maybe<Group_Recurrencies_Aggregate_Fields>;
  nodes: Array<Group_Recurrencies>;
};

/** aggregate fields of "group_recurrencies" */
export type Group_Recurrencies_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Group_Recurrencies_Max_Fields>;
  min?: Maybe<Group_Recurrencies_Min_Fields>;
};


/** aggregate fields of "group_recurrencies" */
export type Group_Recurrencies_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Group_Recurrencies_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "group_recurrencies" */
export type Group_Recurrencies_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Group_Recurrencies_Max_Order_By>;
  min?: Maybe<Group_Recurrencies_Min_Order_By>;
};

/** input type for inserting array relation for remote table "group_recurrencies" */
export type Group_Recurrencies_Arr_Rel_Insert_Input = {
  data: Array<Group_Recurrencies_Insert_Input>;
  on_conflict?: Maybe<Group_Recurrencies_On_Conflict>;
};

/** Boolean expression to filter rows from the table "group_recurrencies". All fields are combined with a logical 'AND'. */
export type Group_Recurrencies_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Group_Recurrencies_Bool_Exp>>>;
  _not?: Maybe<Group_Recurrencies_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Group_Recurrencies_Bool_Exp>>>;
  description?: Maybe<String_Comparison_Exp>;
  groups?: Maybe<Groups_Bool_Exp>;
  value?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "group_recurrencies" */
export enum Group_Recurrencies_Constraint {
  /** unique or primary key constraint */
  PaymentFrequenciesPkey = 'payment_frequencies_pkey'
}

export enum Group_Recurrencies_Enum {
  /** Daily */
  Daily = 'Daily',
  /** Monthly */
  Monthly = 'Monthly',
  /** None */
  None = 'None',
  /** Weekly */
  Weekly = 'Weekly'
}

/** expression to compare columns of type group_recurrencies_enum. All fields are combined with logical 'AND'. */
export type Group_Recurrencies_Enum_Comparison_Exp = {
  _eq?: Maybe<Group_Recurrencies_Enum>;
  _in?: Maybe<Array<Group_Recurrencies_Enum>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _neq?: Maybe<Group_Recurrencies_Enum>;
  _nin?: Maybe<Array<Group_Recurrencies_Enum>>;
};

/** input type for inserting data into table "group_recurrencies" */
export type Group_Recurrencies_Insert_Input = {
  description?: Maybe<Scalars['String']>;
  groups?: Maybe<Groups_Arr_Rel_Insert_Input>;
  value?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Group_Recurrencies_Max_Fields = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "group_recurrencies" */
export type Group_Recurrencies_Max_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Group_Recurrencies_Min_Fields = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "group_recurrencies" */
export type Group_Recurrencies_Min_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** response of any mutation on the table "group_recurrencies" */
export type Group_Recurrencies_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Group_Recurrencies>;
};

/** input type for inserting object relation for remote table "group_recurrencies" */
export type Group_Recurrencies_Obj_Rel_Insert_Input = {
  data: Group_Recurrencies_Insert_Input;
  on_conflict?: Maybe<Group_Recurrencies_On_Conflict>;
};

/** on conflict condition type for table "group_recurrencies" */
export type Group_Recurrencies_On_Conflict = {
  constraint: Group_Recurrencies_Constraint;
  update_columns: Array<Group_Recurrencies_Update_Column>;
  where?: Maybe<Group_Recurrencies_Bool_Exp>;
};

/** ordering options when selecting data from "group_recurrencies" */
export type Group_Recurrencies_Order_By = {
  description?: Maybe<Order_By>;
  groups_aggregate?: Maybe<Groups_Aggregate_Order_By>;
  value?: Maybe<Order_By>;
};

/** primary key columns input for table: "group_recurrencies" */
export type Group_Recurrencies_Pk_Columns_Input = {
  value: Scalars['String'];
};

/** select columns of table "group_recurrencies" */
export enum Group_Recurrencies_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "group_recurrencies" */
export type Group_Recurrencies_Set_Input = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** update columns of table "group_recurrencies" */
export enum Group_Recurrencies_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** columns and relationships of "groups" */
export type Groups = {
  /** An object relationship */
  contribution_type: Contribution_Types;
  created_at: Scalars['timestamp'];
  /** An object relationship */
  creator: Users;
  creator_id: Scalars['uuid'];
  group_balance: Scalars['numeric'];
  group_code: Scalars['String'];
  group_contribution_amount?: Maybe<Scalars['numeric']>;
  group_contribution_type: Contribution_Types_Enum;
  group_name: Scalars['String'];
  group_recurrency_amount?: Maybe<Scalars['numeric']>;
  group_recurrency_day?: Maybe<Scalars['Int']>;
  group_recurrency_type: Group_Recurrencies_Enum;
  id: Scalars['uuid'];
  /** An array relationship */
  members: Array<Members>;
  /** An aggregated array relationship */
  members_aggregate: Members_Aggregate;
  /** An object relationship */
  payment_frequency: Group_Recurrencies;
  /** An array relationship */
  payments: Array<Payments>;
  /** An aggregated array relationship */
  payments_aggregate: Payments_Aggregate;
  /** An array relationship */
  periods: Array<Periods>;
  /** An aggregated array relationship */
  periods_aggregate: Periods_Aggregate;
  updated_at: Scalars['timestamptz'];
};


/** columns and relationships of "groups" */
export type GroupsMembersArgs = {
  distinct_on?: Maybe<Array<Members_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Members_Order_By>>;
  where?: Maybe<Members_Bool_Exp>;
};


/** columns and relationships of "groups" */
export type GroupsMembers_AggregateArgs = {
  distinct_on?: Maybe<Array<Members_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Members_Order_By>>;
  where?: Maybe<Members_Bool_Exp>;
};


/** columns and relationships of "groups" */
export type GroupsPaymentsArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};


/** columns and relationships of "groups" */
export type GroupsPayments_AggregateArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};


/** columns and relationships of "groups" */
export type GroupsPeriodsArgs = {
  distinct_on?: Maybe<Array<Periods_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Periods_Order_By>>;
  where?: Maybe<Periods_Bool_Exp>;
};


/** columns and relationships of "groups" */
export type GroupsPeriods_AggregateArgs = {
  distinct_on?: Maybe<Array<Periods_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Periods_Order_By>>;
  where?: Maybe<Periods_Bool_Exp>;
};

/** aggregated selection of "groups" */
export type Groups_Aggregate = {
  aggregate?: Maybe<Groups_Aggregate_Fields>;
  nodes: Array<Groups>;
};

/** aggregate fields of "groups" */
export type Groups_Aggregate_Fields = {
  avg?: Maybe<Groups_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Groups_Max_Fields>;
  min?: Maybe<Groups_Min_Fields>;
  stddev?: Maybe<Groups_Stddev_Fields>;
  stddev_pop?: Maybe<Groups_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Groups_Stddev_Samp_Fields>;
  sum?: Maybe<Groups_Sum_Fields>;
  var_pop?: Maybe<Groups_Var_Pop_Fields>;
  var_samp?: Maybe<Groups_Var_Samp_Fields>;
  variance?: Maybe<Groups_Variance_Fields>;
};


/** aggregate fields of "groups" */
export type Groups_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Groups_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "groups" */
export type Groups_Aggregate_Order_By = {
  avg?: Maybe<Groups_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Groups_Max_Order_By>;
  min?: Maybe<Groups_Min_Order_By>;
  stddev?: Maybe<Groups_Stddev_Order_By>;
  stddev_pop?: Maybe<Groups_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Groups_Stddev_Samp_Order_By>;
  sum?: Maybe<Groups_Sum_Order_By>;
  var_pop?: Maybe<Groups_Var_Pop_Order_By>;
  var_samp?: Maybe<Groups_Var_Samp_Order_By>;
  variance?: Maybe<Groups_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "groups" */
export type Groups_Arr_Rel_Insert_Input = {
  data: Array<Groups_Insert_Input>;
  on_conflict?: Maybe<Groups_On_Conflict>;
};

/** aggregate avg on columns */
export type Groups_Avg_Fields = {
  group_balance?: Maybe<Scalars['Float']>;
  group_contribution_amount?: Maybe<Scalars['Float']>;
  group_recurrency_amount?: Maybe<Scalars['Float']>;
  group_recurrency_day?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "groups" */
export type Groups_Avg_Order_By = {
  group_balance?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "groups". All fields are combined with a logical 'AND'. */
export type Groups_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Groups_Bool_Exp>>>;
  _not?: Maybe<Groups_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Groups_Bool_Exp>>>;
  contribution_type?: Maybe<Contribution_Types_Bool_Exp>;
  created_at?: Maybe<Timestamp_Comparison_Exp>;
  creator?: Maybe<Users_Bool_Exp>;
  creator_id?: Maybe<Uuid_Comparison_Exp>;
  group_balance?: Maybe<Numeric_Comparison_Exp>;
  group_code?: Maybe<String_Comparison_Exp>;
  group_contribution_amount?: Maybe<Numeric_Comparison_Exp>;
  group_contribution_type?: Maybe<Contribution_Types_Enum_Comparison_Exp>;
  group_name?: Maybe<String_Comparison_Exp>;
  group_recurrency_amount?: Maybe<Numeric_Comparison_Exp>;
  group_recurrency_day?: Maybe<Int_Comparison_Exp>;
  group_recurrency_type?: Maybe<Group_Recurrencies_Enum_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  members?: Maybe<Members_Bool_Exp>;
  payment_frequency?: Maybe<Group_Recurrencies_Bool_Exp>;
  payments?: Maybe<Payments_Bool_Exp>;
  periods?: Maybe<Periods_Bool_Exp>;
  updated_at?: Maybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "groups" */
export enum Groups_Constraint {
  /** unique or primary key constraint */
  GroupsAdminIdGroupNameKey = 'groups_admin_id_group_name_key',
  /** unique or primary key constraint */
  GroupsPkey = 'groups_pkey'
}

/** input type for incrementing integer column in table "groups" */
export type Groups_Inc_Input = {
  group_balance?: Maybe<Scalars['numeric']>;
  group_contribution_amount?: Maybe<Scalars['numeric']>;
  group_recurrency_amount?: Maybe<Scalars['numeric']>;
  group_recurrency_day?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "groups" */
export type Groups_Insert_Input = {
  contribution_type?: Maybe<Contribution_Types_Obj_Rel_Insert_Input>;
  created_at?: Maybe<Scalars['timestamp']>;
  creator?: Maybe<Users_Obj_Rel_Insert_Input>;
  creator_id?: Maybe<Scalars['uuid']>;
  group_balance?: Maybe<Scalars['numeric']>;
  group_code?: Maybe<Scalars['String']>;
  group_contribution_amount?: Maybe<Scalars['numeric']>;
  group_contribution_type?: Maybe<Contribution_Types_Enum>;
  group_name?: Maybe<Scalars['String']>;
  group_recurrency_amount?: Maybe<Scalars['numeric']>;
  group_recurrency_day?: Maybe<Scalars['Int']>;
  group_recurrency_type?: Maybe<Group_Recurrencies_Enum>;
  id?: Maybe<Scalars['uuid']>;
  members?: Maybe<Members_Arr_Rel_Insert_Input>;
  payment_frequency?: Maybe<Group_Recurrencies_Obj_Rel_Insert_Input>;
  payments?: Maybe<Payments_Arr_Rel_Insert_Input>;
  periods?: Maybe<Periods_Arr_Rel_Insert_Input>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type Groups_Max_Fields = {
  created_at?: Maybe<Scalars['timestamp']>;
  creator_id?: Maybe<Scalars['uuid']>;
  group_balance?: Maybe<Scalars['numeric']>;
  group_code?: Maybe<Scalars['String']>;
  group_contribution_amount?: Maybe<Scalars['numeric']>;
  group_name?: Maybe<Scalars['String']>;
  group_recurrency_amount?: Maybe<Scalars['numeric']>;
  group_recurrency_day?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "groups" */
export type Groups_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  creator_id?: Maybe<Order_By>;
  group_balance?: Maybe<Order_By>;
  group_code?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_name?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Groups_Min_Fields = {
  created_at?: Maybe<Scalars['timestamp']>;
  creator_id?: Maybe<Scalars['uuid']>;
  group_balance?: Maybe<Scalars['numeric']>;
  group_code?: Maybe<Scalars['String']>;
  group_contribution_amount?: Maybe<Scalars['numeric']>;
  group_name?: Maybe<Scalars['String']>;
  group_recurrency_amount?: Maybe<Scalars['numeric']>;
  group_recurrency_day?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "groups" */
export type Groups_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  creator_id?: Maybe<Order_By>;
  group_balance?: Maybe<Order_By>;
  group_code?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_name?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** response of any mutation on the table "groups" */
export type Groups_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Groups>;
};

/** input type for inserting object relation for remote table "groups" */
export type Groups_Obj_Rel_Insert_Input = {
  data: Groups_Insert_Input;
  on_conflict?: Maybe<Groups_On_Conflict>;
};

/** on conflict condition type for table "groups" */
export type Groups_On_Conflict = {
  constraint: Groups_Constraint;
  update_columns: Array<Groups_Update_Column>;
  where?: Maybe<Groups_Bool_Exp>;
};

/** ordering options when selecting data from "groups" */
export type Groups_Order_By = {
  contribution_type?: Maybe<Contribution_Types_Order_By>;
  created_at?: Maybe<Order_By>;
  creator?: Maybe<Users_Order_By>;
  creator_id?: Maybe<Order_By>;
  group_balance?: Maybe<Order_By>;
  group_code?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_contribution_type?: Maybe<Order_By>;
  group_name?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
  group_recurrency_type?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  members_aggregate?: Maybe<Members_Aggregate_Order_By>;
  payment_frequency?: Maybe<Group_Recurrencies_Order_By>;
  payments_aggregate?: Maybe<Payments_Aggregate_Order_By>;
  periods_aggregate?: Maybe<Periods_Aggregate_Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** primary key columns input for table: "groups" */
export type Groups_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "groups" */
export enum Groups_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatorId = 'creator_id',
  /** column name */
  GroupBalance = 'group_balance',
  /** column name */
  GroupCode = 'group_code',
  /** column name */
  GroupContributionAmount = 'group_contribution_amount',
  /** column name */
  GroupContributionType = 'group_contribution_type',
  /** column name */
  GroupName = 'group_name',
  /** column name */
  GroupRecurrencyAmount = 'group_recurrency_amount',
  /** column name */
  GroupRecurrencyDay = 'group_recurrency_day',
  /** column name */
  GroupRecurrencyType = 'group_recurrency_type',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "groups" */
export type Groups_Set_Input = {
  created_at?: Maybe<Scalars['timestamp']>;
  creator_id?: Maybe<Scalars['uuid']>;
  group_balance?: Maybe<Scalars['numeric']>;
  group_code?: Maybe<Scalars['String']>;
  group_contribution_amount?: Maybe<Scalars['numeric']>;
  group_contribution_type?: Maybe<Contribution_Types_Enum>;
  group_name?: Maybe<Scalars['String']>;
  group_recurrency_amount?: Maybe<Scalars['numeric']>;
  group_recurrency_day?: Maybe<Scalars['Int']>;
  group_recurrency_type?: Maybe<Group_Recurrencies_Enum>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate stddev on columns */
export type Groups_Stddev_Fields = {
  group_balance?: Maybe<Scalars['Float']>;
  group_contribution_amount?: Maybe<Scalars['Float']>;
  group_recurrency_amount?: Maybe<Scalars['Float']>;
  group_recurrency_day?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "groups" */
export type Groups_Stddev_Order_By = {
  group_balance?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Groups_Stddev_Pop_Fields = {
  group_balance?: Maybe<Scalars['Float']>;
  group_contribution_amount?: Maybe<Scalars['Float']>;
  group_recurrency_amount?: Maybe<Scalars['Float']>;
  group_recurrency_day?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "groups" */
export type Groups_Stddev_Pop_Order_By = {
  group_balance?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Groups_Stddev_Samp_Fields = {
  group_balance?: Maybe<Scalars['Float']>;
  group_contribution_amount?: Maybe<Scalars['Float']>;
  group_recurrency_amount?: Maybe<Scalars['Float']>;
  group_recurrency_day?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "groups" */
export type Groups_Stddev_Samp_Order_By = {
  group_balance?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Groups_Sum_Fields = {
  group_balance?: Maybe<Scalars['numeric']>;
  group_contribution_amount?: Maybe<Scalars['numeric']>;
  group_recurrency_amount?: Maybe<Scalars['numeric']>;
  group_recurrency_day?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "groups" */
export type Groups_Sum_Order_By = {
  group_balance?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
};

/** update columns of table "groups" */
export enum Groups_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatorId = 'creator_id',
  /** column name */
  GroupBalance = 'group_balance',
  /** column name */
  GroupCode = 'group_code',
  /** column name */
  GroupContributionAmount = 'group_contribution_amount',
  /** column name */
  GroupContributionType = 'group_contribution_type',
  /** column name */
  GroupName = 'group_name',
  /** column name */
  GroupRecurrencyAmount = 'group_recurrency_amount',
  /** column name */
  GroupRecurrencyDay = 'group_recurrency_day',
  /** column name */
  GroupRecurrencyType = 'group_recurrency_type',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate var_pop on columns */
export type Groups_Var_Pop_Fields = {
  group_balance?: Maybe<Scalars['Float']>;
  group_contribution_amount?: Maybe<Scalars['Float']>;
  group_recurrency_amount?: Maybe<Scalars['Float']>;
  group_recurrency_day?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "groups" */
export type Groups_Var_Pop_Order_By = {
  group_balance?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Groups_Var_Samp_Fields = {
  group_balance?: Maybe<Scalars['Float']>;
  group_contribution_amount?: Maybe<Scalars['Float']>;
  group_recurrency_amount?: Maybe<Scalars['Float']>;
  group_recurrency_day?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "groups" */
export type Groups_Var_Samp_Order_By = {
  group_balance?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Groups_Variance_Fields = {
  group_balance?: Maybe<Scalars['Float']>;
  group_contribution_amount?: Maybe<Scalars['Float']>;
  group_recurrency_amount?: Maybe<Scalars['Float']>;
  group_recurrency_day?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "groups" */
export type Groups_Variance_Order_By = {
  group_balance?: Maybe<Order_By>;
  group_contribution_amount?: Maybe<Order_By>;
  group_recurrency_amount?: Maybe<Order_By>;
  group_recurrency_day?: Maybe<Order_By>;
};


/** expression to compare columns of type json. All fields are combined with logical 'AND'. */
export type Json_Comparison_Exp = {
  _eq?: Maybe<Scalars['json']>;
  _gt?: Maybe<Scalars['json']>;
  _gte?: Maybe<Scalars['json']>;
  _in?: Maybe<Array<Scalars['json']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['json']>;
  _lte?: Maybe<Scalars['json']>;
  _neq?: Maybe<Scalars['json']>;
  _nin?: Maybe<Array<Scalars['json']>>;
};


/** expression to compare columns of type jsonb. All fields are combined with logical 'AND'. */
export type Jsonb_Comparison_Exp = {
  /** is the column contained in the given json value */
  _contained_in?: Maybe<Scalars['jsonb']>;
  /** does the column contain the given json value at the top level */
  _contains?: Maybe<Scalars['jsonb']>;
  _eq?: Maybe<Scalars['jsonb']>;
  _gt?: Maybe<Scalars['jsonb']>;
  _gte?: Maybe<Scalars['jsonb']>;
  /** does the string exist as a top-level key in the column */
  _has_key?: Maybe<Scalars['String']>;
  /** do all of these strings exist as top-level keys in the column */
  _has_keys_all?: Maybe<Array<Scalars['String']>>;
  /** do any of these strings exist as top-level keys in the column */
  _has_keys_any?: Maybe<Array<Scalars['String']>>;
  _in?: Maybe<Array<Scalars['jsonb']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['jsonb']>;
  _lte?: Maybe<Scalars['jsonb']>;
  _neq?: Maybe<Scalars['jsonb']>;
  _nin?: Maybe<Array<Scalars['jsonb']>>;
};

/** columns and relationships of "members" */
export type Members = {
  created_at: Scalars['timestamptz'];
  /** An object relationship */
  group: Groups;
  group_id: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An array relationship */
  payments: Array<Payments>;
  /** An aggregated array relationship */
  payments_aggregate: Payments_Aggregate;
  updated_at: Scalars['timestamptz'];
  /** An object relationship */
  user: Users;
  user_id: Scalars['uuid'];
};


/** columns and relationships of "members" */
export type MembersPaymentsArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};


/** columns and relationships of "members" */
export type MembersPayments_AggregateArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};

/** aggregated selection of "members" */
export type Members_Aggregate = {
  aggregate?: Maybe<Members_Aggregate_Fields>;
  nodes: Array<Members>;
};

/** aggregate fields of "members" */
export type Members_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Members_Max_Fields>;
  min?: Maybe<Members_Min_Fields>;
};


/** aggregate fields of "members" */
export type Members_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Members_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "members" */
export type Members_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Members_Max_Order_By>;
  min?: Maybe<Members_Min_Order_By>;
};

/** input type for inserting array relation for remote table "members" */
export type Members_Arr_Rel_Insert_Input = {
  data: Array<Members_Insert_Input>;
  on_conflict?: Maybe<Members_On_Conflict>;
};

/** Boolean expression to filter rows from the table "members". All fields are combined with a logical 'AND'. */
export type Members_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Members_Bool_Exp>>>;
  _not?: Maybe<Members_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Members_Bool_Exp>>>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  group?: Maybe<Groups_Bool_Exp>;
  group_id?: Maybe<Uuid_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  payments?: Maybe<Payments_Bool_Exp>;
  updated_at?: Maybe<Timestamptz_Comparison_Exp>;
  user?: Maybe<Users_Bool_Exp>;
  user_id?: Maybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "members" */
export enum Members_Constraint {
  /** unique or primary key constraint */
  MembersGroupIdUserIdKey = 'members_group_id_user_id_key',
  /** unique or primary key constraint */
  MembersPkey = 'members_pkey'
}

/** input type for inserting data into table "members" */
export type Members_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group?: Maybe<Groups_Obj_Rel_Insert_Input>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  payments?: Maybe<Payments_Arr_Rel_Insert_Input>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  user?: Maybe<Users_Obj_Rel_Insert_Input>;
  user_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Members_Max_Fields = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "members" */
export type Members_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  group_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Members_Min_Fields = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "members" */
export type Members_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  group_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  user_id?: Maybe<Order_By>;
};

/** response of any mutation on the table "members" */
export type Members_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Members>;
};

/** input type for inserting object relation for remote table "members" */
export type Members_Obj_Rel_Insert_Input = {
  data: Members_Insert_Input;
  on_conflict?: Maybe<Members_On_Conflict>;
};

/** on conflict condition type for table "members" */
export type Members_On_Conflict = {
  constraint: Members_Constraint;
  update_columns: Array<Members_Update_Column>;
  where?: Maybe<Members_Bool_Exp>;
};

/** ordering options when selecting data from "members" */
export type Members_Order_By = {
  created_at?: Maybe<Order_By>;
  group?: Maybe<Groups_Order_By>;
  group_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  payments_aggregate?: Maybe<Payments_Aggregate_Order_By>;
  updated_at?: Maybe<Order_By>;
  user?: Maybe<Users_Order_By>;
  user_id?: Maybe<Order_By>;
};

/** primary key columns input for table: "members" */
export type Members_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "members" */
export enum Members_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  GroupId = 'group_id',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "members" */
export type Members_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
  user_id?: Maybe<Scalars['uuid']>;
};

/** update columns of table "members" */
export enum Members_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  GroupId = 'group_id',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** mutation root */
export type Mutation_Root = {
  /** perform the action: "action_create_group" */
  action_create_group?: Maybe<ActionCreateGroupOutput>;
  /** perform the action: "action_request_payment" */
  action_request_payment?: Maybe<ActionRequestPaymentOutput>;
  /** perform the action: "action_withdraw_payment" */
  action_withdraw_payment?: Maybe<ActionWithdrawPaymentOutput>;
  /** delete data from the table: "auth.account_providers" */
  delete_auth_account_providers?: Maybe<Auth_Account_Providers_Mutation_Response>;
  /** delete single row from the table: "auth.account_providers" */
  delete_auth_account_providers_by_pk?: Maybe<Auth_Account_Providers>;
  /** delete data from the table: "auth.account_roles" */
  delete_auth_account_roles?: Maybe<Auth_Account_Roles_Mutation_Response>;
  /** delete single row from the table: "auth.account_roles" */
  delete_auth_account_roles_by_pk?: Maybe<Auth_Account_Roles>;
  /** delete data from the table: "auth.accounts" */
  delete_auth_accounts?: Maybe<Auth_Accounts_Mutation_Response>;
  /** delete single row from the table: "auth.accounts" */
  delete_auth_accounts_by_pk?: Maybe<Auth_Accounts>;
  /** delete data from the table: "auth.providers" */
  delete_auth_providers?: Maybe<Auth_Providers_Mutation_Response>;
  /** delete single row from the table: "auth.providers" */
  delete_auth_providers_by_pk?: Maybe<Auth_Providers>;
  /** delete data from the table: "auth.refresh_tokens" */
  delete_auth_refresh_tokens?: Maybe<Auth_Refresh_Tokens_Mutation_Response>;
  /** delete single row from the table: "auth.refresh_tokens" */
  delete_auth_refresh_tokens_by_pk?: Maybe<Auth_Refresh_Tokens>;
  /** delete data from the table: "auth.roles" */
  delete_auth_roles?: Maybe<Auth_Roles_Mutation_Response>;
  /** delete single row from the table: "auth.roles" */
  delete_auth_roles_by_pk?: Maybe<Auth_Roles>;
  /** delete data from the table: "contribution_types" */
  delete_contribution_types?: Maybe<Contribution_Types_Mutation_Response>;
  /** delete single row from the table: "contribution_types" */
  delete_contribution_types_by_pk?: Maybe<Contribution_Types>;
  /** delete data from the table: "group_cycles" */
  delete_group_cycles?: Maybe<Group_Cycles_Mutation_Response>;
  /** delete single row from the table: "group_cycles" */
  delete_group_cycles_by_pk?: Maybe<Group_Cycles>;
  /** delete data from the table: "group_recurrencies" */
  delete_group_recurrencies?: Maybe<Group_Recurrencies_Mutation_Response>;
  /** delete single row from the table: "group_recurrencies" */
  delete_group_recurrencies_by_pk?: Maybe<Group_Recurrencies>;
  /** delete data from the table: "groups" */
  delete_groups?: Maybe<Groups_Mutation_Response>;
  /** delete single row from the table: "groups" */
  delete_groups_by_pk?: Maybe<Groups>;
  /** delete data from the table: "members" */
  delete_members?: Maybe<Members_Mutation_Response>;
  /** delete single row from the table: "members" */
  delete_members_by_pk?: Maybe<Members>;
  /** delete data from the table: "payment_statuses" */
  delete_payment_statuses?: Maybe<Payment_Statuses_Mutation_Response>;
  /** delete single row from the table: "payment_statuses" */
  delete_payment_statuses_by_pk?: Maybe<Payment_Statuses>;
  /** delete data from the table: "payment_types" */
  delete_payment_types?: Maybe<Payment_Types_Mutation_Response>;
  /** delete single row from the table: "payment_types" */
  delete_payment_types_by_pk?: Maybe<Payment_Types>;
  /** delete data from the table: "payments" */
  delete_payments?: Maybe<Payments_Mutation_Response>;
  /** delete single row from the table: "payments" */
  delete_payments_by_pk?: Maybe<Payments>;
  /** delete data from the table: "periods" */
  delete_periods?: Maybe<Periods_Mutation_Response>;
  /** delete single row from the table: "periods" */
  delete_periods_by_pk?: Maybe<Periods>;
  /** delete data from the table: "users" */
  delete_users?: Maybe<Users_Mutation_Response>;
  /** delete single row from the table: "users" */
  delete_users_by_pk?: Maybe<Users>;
  /** insert data into the table: "auth.account_providers" */
  insert_auth_account_providers?: Maybe<Auth_Account_Providers_Mutation_Response>;
  /** insert a single row into the table: "auth.account_providers" */
  insert_auth_account_providers_one?: Maybe<Auth_Account_Providers>;
  /** insert data into the table: "auth.account_roles" */
  insert_auth_account_roles?: Maybe<Auth_Account_Roles_Mutation_Response>;
  /** insert a single row into the table: "auth.account_roles" */
  insert_auth_account_roles_one?: Maybe<Auth_Account_Roles>;
  /** insert data into the table: "auth.accounts" */
  insert_auth_accounts?: Maybe<Auth_Accounts_Mutation_Response>;
  /** insert a single row into the table: "auth.accounts" */
  insert_auth_accounts_one?: Maybe<Auth_Accounts>;
  /** insert data into the table: "auth.providers" */
  insert_auth_providers?: Maybe<Auth_Providers_Mutation_Response>;
  /** insert a single row into the table: "auth.providers" */
  insert_auth_providers_one?: Maybe<Auth_Providers>;
  /** insert data into the table: "auth.refresh_tokens" */
  insert_auth_refresh_tokens?: Maybe<Auth_Refresh_Tokens_Mutation_Response>;
  /** insert a single row into the table: "auth.refresh_tokens" */
  insert_auth_refresh_tokens_one?: Maybe<Auth_Refresh_Tokens>;
  /** insert data into the table: "auth.roles" */
  insert_auth_roles?: Maybe<Auth_Roles_Mutation_Response>;
  /** insert a single row into the table: "auth.roles" */
  insert_auth_roles_one?: Maybe<Auth_Roles>;
  /** insert data into the table: "contribution_types" */
  insert_contribution_types?: Maybe<Contribution_Types_Mutation_Response>;
  /** insert a single row into the table: "contribution_types" */
  insert_contribution_types_one?: Maybe<Contribution_Types>;
  /** insert data into the table: "group_cycles" */
  insert_group_cycles?: Maybe<Group_Cycles_Mutation_Response>;
  /** insert a single row into the table: "group_cycles" */
  insert_group_cycles_one?: Maybe<Group_Cycles>;
  /** insert data into the table: "group_recurrencies" */
  insert_group_recurrencies?: Maybe<Group_Recurrencies_Mutation_Response>;
  /** insert a single row into the table: "group_recurrencies" */
  insert_group_recurrencies_one?: Maybe<Group_Recurrencies>;
  /** insert data into the table: "groups" */
  insert_groups?: Maybe<Groups_Mutation_Response>;
  /** insert a single row into the table: "groups" */
  insert_groups_one?: Maybe<Groups>;
  /** insert data into the table: "members" */
  insert_members?: Maybe<Members_Mutation_Response>;
  /** insert a single row into the table: "members" */
  insert_members_one?: Maybe<Members>;
  /** insert data into the table: "payment_statuses" */
  insert_payment_statuses?: Maybe<Payment_Statuses_Mutation_Response>;
  /** insert a single row into the table: "payment_statuses" */
  insert_payment_statuses_one?: Maybe<Payment_Statuses>;
  /** insert data into the table: "payment_types" */
  insert_payment_types?: Maybe<Payment_Types_Mutation_Response>;
  /** insert a single row into the table: "payment_types" */
  insert_payment_types_one?: Maybe<Payment_Types>;
  /** insert data into the table: "payments" */
  insert_payments?: Maybe<Payments_Mutation_Response>;
  /** insert a single row into the table: "payments" */
  insert_payments_one?: Maybe<Payments>;
  /** insert data into the table: "periods" */
  insert_periods?: Maybe<Periods_Mutation_Response>;
  /** insert a single row into the table: "periods" */
  insert_periods_one?: Maybe<Periods>;
  /** insert data into the table: "users" */
  insert_users?: Maybe<Users_Mutation_Response>;
  /** insert a single row into the table: "users" */
  insert_users_one?: Maybe<Users>;
  /** update data of the table: "auth.account_providers" */
  update_auth_account_providers?: Maybe<Auth_Account_Providers_Mutation_Response>;
  /** update single row of the table: "auth.account_providers" */
  update_auth_account_providers_by_pk?: Maybe<Auth_Account_Providers>;
  /** update data of the table: "auth.account_roles" */
  update_auth_account_roles?: Maybe<Auth_Account_Roles_Mutation_Response>;
  /** update single row of the table: "auth.account_roles" */
  update_auth_account_roles_by_pk?: Maybe<Auth_Account_Roles>;
  /** update data of the table: "auth.accounts" */
  update_auth_accounts?: Maybe<Auth_Accounts_Mutation_Response>;
  /** update single row of the table: "auth.accounts" */
  update_auth_accounts_by_pk?: Maybe<Auth_Accounts>;
  /** update data of the table: "auth.providers" */
  update_auth_providers?: Maybe<Auth_Providers_Mutation_Response>;
  /** update single row of the table: "auth.providers" */
  update_auth_providers_by_pk?: Maybe<Auth_Providers>;
  /** update data of the table: "auth.refresh_tokens" */
  update_auth_refresh_tokens?: Maybe<Auth_Refresh_Tokens_Mutation_Response>;
  /** update single row of the table: "auth.refresh_tokens" */
  update_auth_refresh_tokens_by_pk?: Maybe<Auth_Refresh_Tokens>;
  /** update data of the table: "auth.roles" */
  update_auth_roles?: Maybe<Auth_Roles_Mutation_Response>;
  /** update single row of the table: "auth.roles" */
  update_auth_roles_by_pk?: Maybe<Auth_Roles>;
  /** update data of the table: "contribution_types" */
  update_contribution_types?: Maybe<Contribution_Types_Mutation_Response>;
  /** update single row of the table: "contribution_types" */
  update_contribution_types_by_pk?: Maybe<Contribution_Types>;
  /** update data of the table: "group_cycles" */
  update_group_cycles?: Maybe<Group_Cycles_Mutation_Response>;
  /** update single row of the table: "group_cycles" */
  update_group_cycles_by_pk?: Maybe<Group_Cycles>;
  /** update data of the table: "group_recurrencies" */
  update_group_recurrencies?: Maybe<Group_Recurrencies_Mutation_Response>;
  /** update single row of the table: "group_recurrencies" */
  update_group_recurrencies_by_pk?: Maybe<Group_Recurrencies>;
  /** update data of the table: "groups" */
  update_groups?: Maybe<Groups_Mutation_Response>;
  /** update single row of the table: "groups" */
  update_groups_by_pk?: Maybe<Groups>;
  /** update data of the table: "members" */
  update_members?: Maybe<Members_Mutation_Response>;
  /** update single row of the table: "members" */
  update_members_by_pk?: Maybe<Members>;
  /** update data of the table: "payment_statuses" */
  update_payment_statuses?: Maybe<Payment_Statuses_Mutation_Response>;
  /** update single row of the table: "payment_statuses" */
  update_payment_statuses_by_pk?: Maybe<Payment_Statuses>;
  /** update data of the table: "payment_types" */
  update_payment_types?: Maybe<Payment_Types_Mutation_Response>;
  /** update single row of the table: "payment_types" */
  update_payment_types_by_pk?: Maybe<Payment_Types>;
  /** update data of the table: "payments" */
  update_payments?: Maybe<Payments_Mutation_Response>;
  /** update single row of the table: "payments" */
  update_payments_by_pk?: Maybe<Payments>;
  /** update data of the table: "periods" */
  update_periods?: Maybe<Periods_Mutation_Response>;
  /** update single row of the table: "periods" */
  update_periods_by_pk?: Maybe<Periods>;
  /** update data of the table: "users" */
  update_users?: Maybe<Users_Mutation_Response>;
  /** update single row of the table: "users" */
  update_users_by_pk?: Maybe<Users>;
};


/** mutation root */
export type Mutation_RootAction_Create_GroupArgs = {
  arg: ActionCreateGroupInput;
};


/** mutation root */
export type Mutation_RootAction_Request_PaymentArgs = {
  arg: ActionRequestPaymentInput;
};


/** mutation root */
export type Mutation_RootAction_Withdraw_PaymentArgs = {
  args: ActionWithdrawPaymentInput;
};


/** mutation root */
export type Mutation_RootDelete_Auth_Account_ProvidersArgs = {
  where: Auth_Account_Providers_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Auth_Account_Providers_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Auth_Account_RolesArgs = {
  where: Auth_Account_Roles_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Auth_Account_Roles_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Auth_AccountsArgs = {
  where: Auth_Accounts_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Auth_Accounts_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Auth_ProvidersArgs = {
  where: Auth_Providers_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Auth_Providers_By_PkArgs = {
  provider: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_Auth_Refresh_TokensArgs = {
  where: Auth_Refresh_Tokens_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Auth_Refresh_Tokens_By_PkArgs = {
  refresh_token: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Auth_RolesArgs = {
  where: Auth_Roles_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Auth_Roles_By_PkArgs = {
  role: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_Contribution_TypesArgs = {
  where: Contribution_Types_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Contribution_Types_By_PkArgs = {
  value: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_Group_CyclesArgs = {
  where: Group_Cycles_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Group_Cycles_By_PkArgs = {
  value: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_Group_RecurrenciesArgs = {
  where: Group_Recurrencies_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Group_Recurrencies_By_PkArgs = {
  value: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_GroupsArgs = {
  where: Groups_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Groups_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_MembersArgs = {
  where: Members_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Members_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Payment_StatusesArgs = {
  where: Payment_Statuses_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Payment_Statuses_By_PkArgs = {
  value: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_Payment_TypesArgs = {
  where: Payment_Types_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Payment_Types_By_PkArgs = {
  value: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_PaymentsArgs = {
  where: Payments_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Payments_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_PeriodsArgs = {
  where: Periods_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Periods_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_UsersArgs = {
  where: Users_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Users_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootInsert_Auth_Account_ProvidersArgs = {
  objects: Array<Auth_Account_Providers_Insert_Input>;
  on_conflict?: Maybe<Auth_Account_Providers_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_Account_Providers_OneArgs = {
  object: Auth_Account_Providers_Insert_Input;
  on_conflict?: Maybe<Auth_Account_Providers_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_Account_RolesArgs = {
  objects: Array<Auth_Account_Roles_Insert_Input>;
  on_conflict?: Maybe<Auth_Account_Roles_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_Account_Roles_OneArgs = {
  object: Auth_Account_Roles_Insert_Input;
  on_conflict?: Maybe<Auth_Account_Roles_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_AccountsArgs = {
  objects: Array<Auth_Accounts_Insert_Input>;
  on_conflict?: Maybe<Auth_Accounts_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_Accounts_OneArgs = {
  object: Auth_Accounts_Insert_Input;
  on_conflict?: Maybe<Auth_Accounts_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_ProvidersArgs = {
  objects: Array<Auth_Providers_Insert_Input>;
  on_conflict?: Maybe<Auth_Providers_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_Providers_OneArgs = {
  object: Auth_Providers_Insert_Input;
  on_conflict?: Maybe<Auth_Providers_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_Refresh_TokensArgs = {
  objects: Array<Auth_Refresh_Tokens_Insert_Input>;
  on_conflict?: Maybe<Auth_Refresh_Tokens_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_Refresh_Tokens_OneArgs = {
  object: Auth_Refresh_Tokens_Insert_Input;
  on_conflict?: Maybe<Auth_Refresh_Tokens_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_RolesArgs = {
  objects: Array<Auth_Roles_Insert_Input>;
  on_conflict?: Maybe<Auth_Roles_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Auth_Roles_OneArgs = {
  object: Auth_Roles_Insert_Input;
  on_conflict?: Maybe<Auth_Roles_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Contribution_TypesArgs = {
  objects: Array<Contribution_Types_Insert_Input>;
  on_conflict?: Maybe<Contribution_Types_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Contribution_Types_OneArgs = {
  object: Contribution_Types_Insert_Input;
  on_conflict?: Maybe<Contribution_Types_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Group_CyclesArgs = {
  objects: Array<Group_Cycles_Insert_Input>;
  on_conflict?: Maybe<Group_Cycles_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Group_Cycles_OneArgs = {
  object: Group_Cycles_Insert_Input;
  on_conflict?: Maybe<Group_Cycles_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Group_RecurrenciesArgs = {
  objects: Array<Group_Recurrencies_Insert_Input>;
  on_conflict?: Maybe<Group_Recurrencies_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Group_Recurrencies_OneArgs = {
  object: Group_Recurrencies_Insert_Input;
  on_conflict?: Maybe<Group_Recurrencies_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_GroupsArgs = {
  objects: Array<Groups_Insert_Input>;
  on_conflict?: Maybe<Groups_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Groups_OneArgs = {
  object: Groups_Insert_Input;
  on_conflict?: Maybe<Groups_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_MembersArgs = {
  objects: Array<Members_Insert_Input>;
  on_conflict?: Maybe<Members_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Members_OneArgs = {
  object: Members_Insert_Input;
  on_conflict?: Maybe<Members_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Payment_StatusesArgs = {
  objects: Array<Payment_Statuses_Insert_Input>;
  on_conflict?: Maybe<Payment_Statuses_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Payment_Statuses_OneArgs = {
  object: Payment_Statuses_Insert_Input;
  on_conflict?: Maybe<Payment_Statuses_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Payment_TypesArgs = {
  objects: Array<Payment_Types_Insert_Input>;
  on_conflict?: Maybe<Payment_Types_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Payment_Types_OneArgs = {
  object: Payment_Types_Insert_Input;
  on_conflict?: Maybe<Payment_Types_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_PaymentsArgs = {
  objects: Array<Payments_Insert_Input>;
  on_conflict?: Maybe<Payments_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Payments_OneArgs = {
  object: Payments_Insert_Input;
  on_conflict?: Maybe<Payments_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_PeriodsArgs = {
  objects: Array<Periods_Insert_Input>;
  on_conflict?: Maybe<Periods_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Periods_OneArgs = {
  object: Periods_Insert_Input;
  on_conflict?: Maybe<Periods_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_UsersArgs = {
  objects: Array<Users_Insert_Input>;
  on_conflict?: Maybe<Users_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Users_OneArgs = {
  object: Users_Insert_Input;
  on_conflict?: Maybe<Users_On_Conflict>;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_Account_ProvidersArgs = {
  _set?: Maybe<Auth_Account_Providers_Set_Input>;
  where: Auth_Account_Providers_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_Account_Providers_By_PkArgs = {
  _set?: Maybe<Auth_Account_Providers_Set_Input>;
  pk_columns: Auth_Account_Providers_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_Account_RolesArgs = {
  _set?: Maybe<Auth_Account_Roles_Set_Input>;
  where: Auth_Account_Roles_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_Account_Roles_By_PkArgs = {
  _set?: Maybe<Auth_Account_Roles_Set_Input>;
  pk_columns: Auth_Account_Roles_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_AccountsArgs = {
  _append?: Maybe<Auth_Accounts_Append_Input>;
  _delete_at_path?: Maybe<Auth_Accounts_Delete_At_Path_Input>;
  _delete_elem?: Maybe<Auth_Accounts_Delete_Elem_Input>;
  _delete_key?: Maybe<Auth_Accounts_Delete_Key_Input>;
  _prepend?: Maybe<Auth_Accounts_Prepend_Input>;
  _set?: Maybe<Auth_Accounts_Set_Input>;
  where: Auth_Accounts_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_Accounts_By_PkArgs = {
  _append?: Maybe<Auth_Accounts_Append_Input>;
  _delete_at_path?: Maybe<Auth_Accounts_Delete_At_Path_Input>;
  _delete_elem?: Maybe<Auth_Accounts_Delete_Elem_Input>;
  _delete_key?: Maybe<Auth_Accounts_Delete_Key_Input>;
  _prepend?: Maybe<Auth_Accounts_Prepend_Input>;
  _set?: Maybe<Auth_Accounts_Set_Input>;
  pk_columns: Auth_Accounts_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_ProvidersArgs = {
  _set?: Maybe<Auth_Providers_Set_Input>;
  where: Auth_Providers_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_Providers_By_PkArgs = {
  _set?: Maybe<Auth_Providers_Set_Input>;
  pk_columns: Auth_Providers_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_Refresh_TokensArgs = {
  _set?: Maybe<Auth_Refresh_Tokens_Set_Input>;
  where: Auth_Refresh_Tokens_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_Refresh_Tokens_By_PkArgs = {
  _set?: Maybe<Auth_Refresh_Tokens_Set_Input>;
  pk_columns: Auth_Refresh_Tokens_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_RolesArgs = {
  _set?: Maybe<Auth_Roles_Set_Input>;
  where: Auth_Roles_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Auth_Roles_By_PkArgs = {
  _set?: Maybe<Auth_Roles_Set_Input>;
  pk_columns: Auth_Roles_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Contribution_TypesArgs = {
  _set?: Maybe<Contribution_Types_Set_Input>;
  where: Contribution_Types_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Contribution_Types_By_PkArgs = {
  _set?: Maybe<Contribution_Types_Set_Input>;
  pk_columns: Contribution_Types_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Group_CyclesArgs = {
  _set?: Maybe<Group_Cycles_Set_Input>;
  where: Group_Cycles_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Group_Cycles_By_PkArgs = {
  _set?: Maybe<Group_Cycles_Set_Input>;
  pk_columns: Group_Cycles_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Group_RecurrenciesArgs = {
  _set?: Maybe<Group_Recurrencies_Set_Input>;
  where: Group_Recurrencies_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Group_Recurrencies_By_PkArgs = {
  _set?: Maybe<Group_Recurrencies_Set_Input>;
  pk_columns: Group_Recurrencies_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_GroupsArgs = {
  _inc?: Maybe<Groups_Inc_Input>;
  _set?: Maybe<Groups_Set_Input>;
  where: Groups_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Groups_By_PkArgs = {
  _inc?: Maybe<Groups_Inc_Input>;
  _set?: Maybe<Groups_Set_Input>;
  pk_columns: Groups_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_MembersArgs = {
  _set?: Maybe<Members_Set_Input>;
  where: Members_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Members_By_PkArgs = {
  _set?: Maybe<Members_Set_Input>;
  pk_columns: Members_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Payment_StatusesArgs = {
  _set?: Maybe<Payment_Statuses_Set_Input>;
  where: Payment_Statuses_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Payment_Statuses_By_PkArgs = {
  _set?: Maybe<Payment_Statuses_Set_Input>;
  pk_columns: Payment_Statuses_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Payment_TypesArgs = {
  _set?: Maybe<Payment_Types_Set_Input>;
  where: Payment_Types_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Payment_Types_By_PkArgs = {
  _set?: Maybe<Payment_Types_Set_Input>;
  pk_columns: Payment_Types_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_PaymentsArgs = {
  _inc?: Maybe<Payments_Inc_Input>;
  _set?: Maybe<Payments_Set_Input>;
  where: Payments_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Payments_By_PkArgs = {
  _inc?: Maybe<Payments_Inc_Input>;
  _set?: Maybe<Payments_Set_Input>;
  pk_columns: Payments_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_PeriodsArgs = {
  _inc?: Maybe<Periods_Inc_Input>;
  _set?: Maybe<Periods_Set_Input>;
  where: Periods_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Periods_By_PkArgs = {
  _inc?: Maybe<Periods_Inc_Input>;
  _set?: Maybe<Periods_Set_Input>;
  pk_columns: Periods_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_UsersArgs = {
  _set?: Maybe<Users_Set_Input>;
  where: Users_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Users_By_PkArgs = {
  _set?: Maybe<Users_Set_Input>;
  pk_columns: Users_Pk_Columns_Input;
};


/** expression to compare columns of type numeric. All fields are combined with logical 'AND'. */
export type Numeric_Comparison_Exp = {
  _eq?: Maybe<Scalars['numeric']>;
  _gt?: Maybe<Scalars['numeric']>;
  _gte?: Maybe<Scalars['numeric']>;
  _in?: Maybe<Array<Scalars['numeric']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['numeric']>;
  _lte?: Maybe<Scalars['numeric']>;
  _neq?: Maybe<Scalars['numeric']>;
  _nin?: Maybe<Array<Scalars['numeric']>>;
};

/** column ordering options */
export enum Order_By {
  /** in the ascending order, nulls last */
  Asc = 'asc',
  /** in the ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in the ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in the descending order, nulls first */
  Desc = 'desc',
  /** in the descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in the descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** columns and relationships of "payment_statuses" */
export type Payment_Statuses = {
  description: Scalars['String'];
  /** An array relationship */
  payments: Array<Payments>;
  /** An aggregated array relationship */
  payments_aggregate: Payments_Aggregate;
  value: Scalars['String'];
};


/** columns and relationships of "payment_statuses" */
export type Payment_StatusesPaymentsArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};


/** columns and relationships of "payment_statuses" */
export type Payment_StatusesPayments_AggregateArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};

/** aggregated selection of "payment_statuses" */
export type Payment_Statuses_Aggregate = {
  aggregate?: Maybe<Payment_Statuses_Aggregate_Fields>;
  nodes: Array<Payment_Statuses>;
};

/** aggregate fields of "payment_statuses" */
export type Payment_Statuses_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Payment_Statuses_Max_Fields>;
  min?: Maybe<Payment_Statuses_Min_Fields>;
};


/** aggregate fields of "payment_statuses" */
export type Payment_Statuses_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Payment_Statuses_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "payment_statuses" */
export type Payment_Statuses_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Payment_Statuses_Max_Order_By>;
  min?: Maybe<Payment_Statuses_Min_Order_By>;
};

/** input type for inserting array relation for remote table "payment_statuses" */
export type Payment_Statuses_Arr_Rel_Insert_Input = {
  data: Array<Payment_Statuses_Insert_Input>;
  on_conflict?: Maybe<Payment_Statuses_On_Conflict>;
};

/** Boolean expression to filter rows from the table "payment_statuses". All fields are combined with a logical 'AND'. */
export type Payment_Statuses_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Payment_Statuses_Bool_Exp>>>;
  _not?: Maybe<Payment_Statuses_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Payment_Statuses_Bool_Exp>>>;
  description?: Maybe<String_Comparison_Exp>;
  payments?: Maybe<Payments_Bool_Exp>;
  value?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "payment_statuses" */
export enum Payment_Statuses_Constraint {
  /** unique or primary key constraint */
  PaymentStatusPkey = 'payment_status_pkey'
}

export enum Payment_Statuses_Enum {
  /** Cancelled */
  Cancelled = 'CANCELLED',
  /** Completed */
  Completed = 'COMPLETED',
  /** Failed */
  Failed = 'FAILED',
  /** Pending */
  Pending = 'PENDING'
}

/** expression to compare columns of type payment_statuses_enum. All fields are combined with logical 'AND'. */
export type Payment_Statuses_Enum_Comparison_Exp = {
  _eq?: Maybe<Payment_Statuses_Enum>;
  _in?: Maybe<Array<Payment_Statuses_Enum>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _neq?: Maybe<Payment_Statuses_Enum>;
  _nin?: Maybe<Array<Payment_Statuses_Enum>>;
};

/** input type for inserting data into table "payment_statuses" */
export type Payment_Statuses_Insert_Input = {
  description?: Maybe<Scalars['String']>;
  payments?: Maybe<Payments_Arr_Rel_Insert_Input>;
  value?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Payment_Statuses_Max_Fields = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "payment_statuses" */
export type Payment_Statuses_Max_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Payment_Statuses_Min_Fields = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "payment_statuses" */
export type Payment_Statuses_Min_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** response of any mutation on the table "payment_statuses" */
export type Payment_Statuses_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Payment_Statuses>;
};

/** input type for inserting object relation for remote table "payment_statuses" */
export type Payment_Statuses_Obj_Rel_Insert_Input = {
  data: Payment_Statuses_Insert_Input;
  on_conflict?: Maybe<Payment_Statuses_On_Conflict>;
};

/** on conflict condition type for table "payment_statuses" */
export type Payment_Statuses_On_Conflict = {
  constraint: Payment_Statuses_Constraint;
  update_columns: Array<Payment_Statuses_Update_Column>;
  where?: Maybe<Payment_Statuses_Bool_Exp>;
};

/** ordering options when selecting data from "payment_statuses" */
export type Payment_Statuses_Order_By = {
  description?: Maybe<Order_By>;
  payments_aggregate?: Maybe<Payments_Aggregate_Order_By>;
  value?: Maybe<Order_By>;
};

/** primary key columns input for table: "payment_statuses" */
export type Payment_Statuses_Pk_Columns_Input = {
  value: Scalars['String'];
};

/** select columns of table "payment_statuses" */
export enum Payment_Statuses_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "payment_statuses" */
export type Payment_Statuses_Set_Input = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** update columns of table "payment_statuses" */
export enum Payment_Statuses_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** columns and relationships of "payment_types" */
export type Payment_Types = {
  description: Scalars['String'];
  value: Scalars['String'];
};

/** aggregated selection of "payment_types" */
export type Payment_Types_Aggregate = {
  aggregate?: Maybe<Payment_Types_Aggregate_Fields>;
  nodes: Array<Payment_Types>;
};

/** aggregate fields of "payment_types" */
export type Payment_Types_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Payment_Types_Max_Fields>;
  min?: Maybe<Payment_Types_Min_Fields>;
};


/** aggregate fields of "payment_types" */
export type Payment_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Payment_Types_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "payment_types" */
export type Payment_Types_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Payment_Types_Max_Order_By>;
  min?: Maybe<Payment_Types_Min_Order_By>;
};

/** input type for inserting array relation for remote table "payment_types" */
export type Payment_Types_Arr_Rel_Insert_Input = {
  data: Array<Payment_Types_Insert_Input>;
  on_conflict?: Maybe<Payment_Types_On_Conflict>;
};

/** Boolean expression to filter rows from the table "payment_types". All fields are combined with a logical 'AND'. */
export type Payment_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Payment_Types_Bool_Exp>>>;
  _not?: Maybe<Payment_Types_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Payment_Types_Bool_Exp>>>;
  description?: Maybe<String_Comparison_Exp>;
  value?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "payment_types" */
export enum Payment_Types_Constraint {
  /** unique or primary key constraint */
  PaymentTypesPkey = 'payment_types_pkey'
}

export enum Payment_Types_Enum {
  /** Money in */
  MoneyIn = 'MoneyIn',
  /** Money out */
  MoneyOut = 'MoneyOut'
}

/** expression to compare columns of type payment_types_enum. All fields are combined with logical 'AND'. */
export type Payment_Types_Enum_Comparison_Exp = {
  _eq?: Maybe<Payment_Types_Enum>;
  _in?: Maybe<Array<Payment_Types_Enum>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _neq?: Maybe<Payment_Types_Enum>;
  _nin?: Maybe<Array<Payment_Types_Enum>>;
};

/** input type for inserting data into table "payment_types" */
export type Payment_Types_Insert_Input = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Payment_Types_Max_Fields = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "payment_types" */
export type Payment_Types_Max_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Payment_Types_Min_Fields = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "payment_types" */
export type Payment_Types_Min_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** response of any mutation on the table "payment_types" */
export type Payment_Types_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Payment_Types>;
};

/** input type for inserting object relation for remote table "payment_types" */
export type Payment_Types_Obj_Rel_Insert_Input = {
  data: Payment_Types_Insert_Input;
  on_conflict?: Maybe<Payment_Types_On_Conflict>;
};

/** on conflict condition type for table "payment_types" */
export type Payment_Types_On_Conflict = {
  constraint: Payment_Types_Constraint;
  update_columns: Array<Payment_Types_Update_Column>;
  where?: Maybe<Payment_Types_Bool_Exp>;
};

/** ordering options when selecting data from "payment_types" */
export type Payment_Types_Order_By = {
  description?: Maybe<Order_By>;
  value?: Maybe<Order_By>;
};

/** primary key columns input for table: "payment_types" */
export type Payment_Types_Pk_Columns_Input = {
  value: Scalars['String'];
};

/** select columns of table "payment_types" */
export enum Payment_Types_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "payment_types" */
export type Payment_Types_Set_Input = {
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** update columns of table "payment_types" */
export enum Payment_Types_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** columns and relationships of "payments" */
export type Payments = {
  created_at: Scalars['timestamptz'];
  /** An object relationship */
  group: Groups;
  group_id: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An object relationship */
  member: Members;
  member_id: Scalars['uuid'];
  /** An object relationship */
  paymentStatusByPaymentStatus: Payment_Statuses;
  payment_amount: Scalars['numeric'];
  payment_status: Payment_Statuses_Enum;
  payment_type: Payment_Types_Enum;
  /** An object relationship */
  period?: Maybe<Periods>;
  period_id?: Maybe<Scalars['uuid']>;
  updated_at: Scalars['timestamptz'];
};

/** aggregated selection of "payments" */
export type Payments_Aggregate = {
  aggregate?: Maybe<Payments_Aggregate_Fields>;
  nodes: Array<Payments>;
};

/** aggregate fields of "payments" */
export type Payments_Aggregate_Fields = {
  avg?: Maybe<Payments_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Payments_Max_Fields>;
  min?: Maybe<Payments_Min_Fields>;
  stddev?: Maybe<Payments_Stddev_Fields>;
  stddev_pop?: Maybe<Payments_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Payments_Stddev_Samp_Fields>;
  sum?: Maybe<Payments_Sum_Fields>;
  var_pop?: Maybe<Payments_Var_Pop_Fields>;
  var_samp?: Maybe<Payments_Var_Samp_Fields>;
  variance?: Maybe<Payments_Variance_Fields>;
};


/** aggregate fields of "payments" */
export type Payments_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Payments_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "payments" */
export type Payments_Aggregate_Order_By = {
  avg?: Maybe<Payments_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Payments_Max_Order_By>;
  min?: Maybe<Payments_Min_Order_By>;
  stddev?: Maybe<Payments_Stddev_Order_By>;
  stddev_pop?: Maybe<Payments_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Payments_Stddev_Samp_Order_By>;
  sum?: Maybe<Payments_Sum_Order_By>;
  var_pop?: Maybe<Payments_Var_Pop_Order_By>;
  var_samp?: Maybe<Payments_Var_Samp_Order_By>;
  variance?: Maybe<Payments_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "payments" */
export type Payments_Arr_Rel_Insert_Input = {
  data: Array<Payments_Insert_Input>;
  on_conflict?: Maybe<Payments_On_Conflict>;
};

/** aggregate avg on columns */
export type Payments_Avg_Fields = {
  payment_amount?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "payments" */
export type Payments_Avg_Order_By = {
  payment_amount?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "payments". All fields are combined with a logical 'AND'. */
export type Payments_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Payments_Bool_Exp>>>;
  _not?: Maybe<Payments_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Payments_Bool_Exp>>>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  group?: Maybe<Groups_Bool_Exp>;
  group_id?: Maybe<Uuid_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  member?: Maybe<Members_Bool_Exp>;
  member_id?: Maybe<Uuid_Comparison_Exp>;
  paymentStatusByPaymentStatus?: Maybe<Payment_Statuses_Bool_Exp>;
  payment_amount?: Maybe<Numeric_Comparison_Exp>;
  payment_status?: Maybe<Payment_Statuses_Enum_Comparison_Exp>;
  payment_type?: Maybe<Payment_Types_Enum_Comparison_Exp>;
  period?: Maybe<Periods_Bool_Exp>;
  period_id?: Maybe<Uuid_Comparison_Exp>;
  updated_at?: Maybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "payments" */
export enum Payments_Constraint {
  /** unique or primary key constraint */
  PaymentsPkey = 'payments_pkey'
}

/** input type for incrementing integer column in table "payments" */
export type Payments_Inc_Input = {
  payment_amount?: Maybe<Scalars['numeric']>;
};

/** input type for inserting data into table "payments" */
export type Payments_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group?: Maybe<Groups_Obj_Rel_Insert_Input>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  member?: Maybe<Members_Obj_Rel_Insert_Input>;
  member_id?: Maybe<Scalars['uuid']>;
  paymentStatusByPaymentStatus?: Maybe<Payment_Statuses_Obj_Rel_Insert_Input>;
  payment_amount?: Maybe<Scalars['numeric']>;
  payment_status?: Maybe<Payment_Statuses_Enum>;
  payment_type?: Maybe<Payment_Types_Enum>;
  period?: Maybe<Periods_Obj_Rel_Insert_Input>;
  period_id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type Payments_Max_Fields = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  member_id?: Maybe<Scalars['uuid']>;
  payment_amount?: Maybe<Scalars['numeric']>;
  period_id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "payments" */
export type Payments_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  group_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  member_id?: Maybe<Order_By>;
  payment_amount?: Maybe<Order_By>;
  period_id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Payments_Min_Fields = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  member_id?: Maybe<Scalars['uuid']>;
  payment_amount?: Maybe<Scalars['numeric']>;
  period_id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "payments" */
export type Payments_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  group_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  member_id?: Maybe<Order_By>;
  payment_amount?: Maybe<Order_By>;
  period_id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** response of any mutation on the table "payments" */
export type Payments_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Payments>;
};

/** input type for inserting object relation for remote table "payments" */
export type Payments_Obj_Rel_Insert_Input = {
  data: Payments_Insert_Input;
  on_conflict?: Maybe<Payments_On_Conflict>;
};

/** on conflict condition type for table "payments" */
export type Payments_On_Conflict = {
  constraint: Payments_Constraint;
  update_columns: Array<Payments_Update_Column>;
  where?: Maybe<Payments_Bool_Exp>;
};

/** ordering options when selecting data from "payments" */
export type Payments_Order_By = {
  created_at?: Maybe<Order_By>;
  group?: Maybe<Groups_Order_By>;
  group_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  member?: Maybe<Members_Order_By>;
  member_id?: Maybe<Order_By>;
  paymentStatusByPaymentStatus?: Maybe<Payment_Statuses_Order_By>;
  payment_amount?: Maybe<Order_By>;
  payment_status?: Maybe<Order_By>;
  payment_type?: Maybe<Order_By>;
  period?: Maybe<Periods_Order_By>;
  period_id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** primary key columns input for table: "payments" */
export type Payments_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "payments" */
export enum Payments_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  GroupId = 'group_id',
  /** column name */
  Id = 'id',
  /** column name */
  MemberId = 'member_id',
  /** column name */
  PaymentAmount = 'payment_amount',
  /** column name */
  PaymentStatus = 'payment_status',
  /** column name */
  PaymentType = 'payment_type',
  /** column name */
  PeriodId = 'period_id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "payments" */
export type Payments_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  member_id?: Maybe<Scalars['uuid']>;
  payment_amount?: Maybe<Scalars['numeric']>;
  payment_status?: Maybe<Payment_Statuses_Enum>;
  payment_type?: Maybe<Payment_Types_Enum>;
  period_id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate stddev on columns */
export type Payments_Stddev_Fields = {
  payment_amount?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "payments" */
export type Payments_Stddev_Order_By = {
  payment_amount?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Payments_Stddev_Pop_Fields = {
  payment_amount?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "payments" */
export type Payments_Stddev_Pop_Order_By = {
  payment_amount?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Payments_Stddev_Samp_Fields = {
  payment_amount?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "payments" */
export type Payments_Stddev_Samp_Order_By = {
  payment_amount?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Payments_Sum_Fields = {
  payment_amount?: Maybe<Scalars['numeric']>;
};

/** order by sum() on columns of table "payments" */
export type Payments_Sum_Order_By = {
  payment_amount?: Maybe<Order_By>;
};

/** update columns of table "payments" */
export enum Payments_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  GroupId = 'group_id',
  /** column name */
  Id = 'id',
  /** column name */
  MemberId = 'member_id',
  /** column name */
  PaymentAmount = 'payment_amount',
  /** column name */
  PaymentStatus = 'payment_status',
  /** column name */
  PaymentType = 'payment_type',
  /** column name */
  PeriodId = 'period_id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate var_pop on columns */
export type Payments_Var_Pop_Fields = {
  payment_amount?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "payments" */
export type Payments_Var_Pop_Order_By = {
  payment_amount?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Payments_Var_Samp_Fields = {
  payment_amount?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "payments" */
export type Payments_Var_Samp_Order_By = {
  payment_amount?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Payments_Variance_Fields = {
  payment_amount?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "payments" */
export type Payments_Variance_Order_By = {
  payment_amount?: Maybe<Order_By>;
};

/** columns and relationships of "periods" */
export type Periods = {
  created_at: Scalars['timestamptz'];
  /** An object relationship */
  group: Groups;
  group_id: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An array relationship */
  payments: Array<Payments>;
  /** An aggregated array relationship */
  payments_aggregate: Payments_Aggregate;
  period_active: Scalars['Boolean'];
  period_completed_at: Scalars['timestamp'];
  period_index: Scalars['Int'];
  period_progression: Scalars['numeric'];
  updated_at: Scalars['timestamptz'];
};


/** columns and relationships of "periods" */
export type PeriodsPaymentsArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};


/** columns and relationships of "periods" */
export type PeriodsPayments_AggregateArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};

/** aggregated selection of "periods" */
export type Periods_Aggregate = {
  aggregate?: Maybe<Periods_Aggregate_Fields>;
  nodes: Array<Periods>;
};

/** aggregate fields of "periods" */
export type Periods_Aggregate_Fields = {
  avg?: Maybe<Periods_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Periods_Max_Fields>;
  min?: Maybe<Periods_Min_Fields>;
  stddev?: Maybe<Periods_Stddev_Fields>;
  stddev_pop?: Maybe<Periods_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Periods_Stddev_Samp_Fields>;
  sum?: Maybe<Periods_Sum_Fields>;
  var_pop?: Maybe<Periods_Var_Pop_Fields>;
  var_samp?: Maybe<Periods_Var_Samp_Fields>;
  variance?: Maybe<Periods_Variance_Fields>;
};


/** aggregate fields of "periods" */
export type Periods_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Periods_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "periods" */
export type Periods_Aggregate_Order_By = {
  avg?: Maybe<Periods_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Periods_Max_Order_By>;
  min?: Maybe<Periods_Min_Order_By>;
  stddev?: Maybe<Periods_Stddev_Order_By>;
  stddev_pop?: Maybe<Periods_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Periods_Stddev_Samp_Order_By>;
  sum?: Maybe<Periods_Sum_Order_By>;
  var_pop?: Maybe<Periods_Var_Pop_Order_By>;
  var_samp?: Maybe<Periods_Var_Samp_Order_By>;
  variance?: Maybe<Periods_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "periods" */
export type Periods_Arr_Rel_Insert_Input = {
  data: Array<Periods_Insert_Input>;
  on_conflict?: Maybe<Periods_On_Conflict>;
};

/** aggregate avg on columns */
export type Periods_Avg_Fields = {
  period_index?: Maybe<Scalars['Float']>;
  period_progression?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "periods" */
export type Periods_Avg_Order_By = {
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "periods". All fields are combined with a logical 'AND'. */
export type Periods_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Periods_Bool_Exp>>>;
  _not?: Maybe<Periods_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Periods_Bool_Exp>>>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  group?: Maybe<Groups_Bool_Exp>;
  group_id?: Maybe<Uuid_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  payments?: Maybe<Payments_Bool_Exp>;
  period_active?: Maybe<Boolean_Comparison_Exp>;
  period_completed_at?: Maybe<Timestamp_Comparison_Exp>;
  period_index?: Maybe<Int_Comparison_Exp>;
  period_progression?: Maybe<Numeric_Comparison_Exp>;
  updated_at?: Maybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "periods" */
export enum Periods_Constraint {
  /** unique or primary key constraint */
  PeriodsGroupIdPeriodIndexKey = 'periods_group_id_period_index_key',
  /** unique or primary key constraint */
  PeriodsPkey = 'periods_pkey'
}

/** input type for incrementing integer column in table "periods" */
export type Periods_Inc_Input = {
  period_index?: Maybe<Scalars['Int']>;
  period_progression?: Maybe<Scalars['numeric']>;
};

/** input type for inserting data into table "periods" */
export type Periods_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group?: Maybe<Groups_Obj_Rel_Insert_Input>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  payments?: Maybe<Payments_Arr_Rel_Insert_Input>;
  period_active?: Maybe<Scalars['Boolean']>;
  period_completed_at?: Maybe<Scalars['timestamp']>;
  period_index?: Maybe<Scalars['Int']>;
  period_progression?: Maybe<Scalars['numeric']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type Periods_Max_Fields = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  period_completed_at?: Maybe<Scalars['timestamp']>;
  period_index?: Maybe<Scalars['Int']>;
  period_progression?: Maybe<Scalars['numeric']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "periods" */
export type Periods_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  group_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  period_completed_at?: Maybe<Order_By>;
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Periods_Min_Fields = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  period_completed_at?: Maybe<Scalars['timestamp']>;
  period_index?: Maybe<Scalars['Int']>;
  period_progression?: Maybe<Scalars['numeric']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "periods" */
export type Periods_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  group_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  period_completed_at?: Maybe<Order_By>;
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** response of any mutation on the table "periods" */
export type Periods_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Periods>;
};

/** input type for inserting object relation for remote table "periods" */
export type Periods_Obj_Rel_Insert_Input = {
  data: Periods_Insert_Input;
  on_conflict?: Maybe<Periods_On_Conflict>;
};

/** on conflict condition type for table "periods" */
export type Periods_On_Conflict = {
  constraint: Periods_Constraint;
  update_columns: Array<Periods_Update_Column>;
  where?: Maybe<Periods_Bool_Exp>;
};

/** ordering options when selecting data from "periods" */
export type Periods_Order_By = {
  created_at?: Maybe<Order_By>;
  group?: Maybe<Groups_Order_By>;
  group_id?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  payments_aggregate?: Maybe<Payments_Aggregate_Order_By>;
  period_active?: Maybe<Order_By>;
  period_completed_at?: Maybe<Order_By>;
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** primary key columns input for table: "periods" */
export type Periods_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "periods" */
export enum Periods_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  GroupId = 'group_id',
  /** column name */
  Id = 'id',
  /** column name */
  PeriodActive = 'period_active',
  /** column name */
  PeriodCompletedAt = 'period_completed_at',
  /** column name */
  PeriodIndex = 'period_index',
  /** column name */
  PeriodProgression = 'period_progression',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "periods" */
export type Periods_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>;
  group_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  period_active?: Maybe<Scalars['Boolean']>;
  period_completed_at?: Maybe<Scalars['timestamp']>;
  period_index?: Maybe<Scalars['Int']>;
  period_progression?: Maybe<Scalars['numeric']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate stddev on columns */
export type Periods_Stddev_Fields = {
  period_index?: Maybe<Scalars['Float']>;
  period_progression?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "periods" */
export type Periods_Stddev_Order_By = {
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Periods_Stddev_Pop_Fields = {
  period_index?: Maybe<Scalars['Float']>;
  period_progression?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "periods" */
export type Periods_Stddev_Pop_Order_By = {
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Periods_Stddev_Samp_Fields = {
  period_index?: Maybe<Scalars['Float']>;
  period_progression?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "periods" */
export type Periods_Stddev_Samp_Order_By = {
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Periods_Sum_Fields = {
  period_index?: Maybe<Scalars['Int']>;
  period_progression?: Maybe<Scalars['numeric']>;
};

/** order by sum() on columns of table "periods" */
export type Periods_Sum_Order_By = {
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
};

/** update columns of table "periods" */
export enum Periods_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  GroupId = 'group_id',
  /** column name */
  Id = 'id',
  /** column name */
  PeriodActive = 'period_active',
  /** column name */
  PeriodCompletedAt = 'period_completed_at',
  /** column name */
  PeriodIndex = 'period_index',
  /** column name */
  PeriodProgression = 'period_progression',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate var_pop on columns */
export type Periods_Var_Pop_Fields = {
  period_index?: Maybe<Scalars['Float']>;
  period_progression?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "periods" */
export type Periods_Var_Pop_Order_By = {
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Periods_Var_Samp_Fields = {
  period_index?: Maybe<Scalars['Float']>;
  period_progression?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "periods" */
export type Periods_Var_Samp_Order_By = {
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Periods_Variance_Fields = {
  period_index?: Maybe<Scalars['Float']>;
  period_progression?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "periods" */
export type Periods_Variance_Order_By = {
  period_index?: Maybe<Order_By>;
  period_progression?: Maybe<Order_By>;
};

/** query root */
export type Query_Root = {
  /** fetch data from the table: "auth.account_providers" */
  auth_account_providers: Array<Auth_Account_Providers>;
  /** fetch aggregated fields from the table: "auth.account_providers" */
  auth_account_providers_aggregate: Auth_Account_Providers_Aggregate;
  /** fetch data from the table: "auth.account_providers" using primary key columns */
  auth_account_providers_by_pk?: Maybe<Auth_Account_Providers>;
  /** fetch data from the table: "auth.account_roles" */
  auth_account_roles: Array<Auth_Account_Roles>;
  /** fetch aggregated fields from the table: "auth.account_roles" */
  auth_account_roles_aggregate: Auth_Account_Roles_Aggregate;
  /** fetch data from the table: "auth.account_roles" using primary key columns */
  auth_account_roles_by_pk?: Maybe<Auth_Account_Roles>;
  /** fetch data from the table: "auth.accounts" */
  auth_accounts: Array<Auth_Accounts>;
  /** fetch aggregated fields from the table: "auth.accounts" */
  auth_accounts_aggregate: Auth_Accounts_Aggregate;
  /** fetch data from the table: "auth.accounts" using primary key columns */
  auth_accounts_by_pk?: Maybe<Auth_Accounts>;
  /** fetch data from the table: "auth.providers" */
  auth_providers: Array<Auth_Providers>;
  /** fetch aggregated fields from the table: "auth.providers" */
  auth_providers_aggregate: Auth_Providers_Aggregate;
  /** fetch data from the table: "auth.providers" using primary key columns */
  auth_providers_by_pk?: Maybe<Auth_Providers>;
  /** fetch data from the table: "auth.refresh_tokens" */
  auth_refresh_tokens: Array<Auth_Refresh_Tokens>;
  /** fetch aggregated fields from the table: "auth.refresh_tokens" */
  auth_refresh_tokens_aggregate: Auth_Refresh_Tokens_Aggregate;
  /** fetch data from the table: "auth.refresh_tokens" using primary key columns */
  auth_refresh_tokens_by_pk?: Maybe<Auth_Refresh_Tokens>;
  /** fetch data from the table: "auth.roles" */
  auth_roles: Array<Auth_Roles>;
  /** fetch aggregated fields from the table: "auth.roles" */
  auth_roles_aggregate: Auth_Roles_Aggregate;
  /** fetch data from the table: "auth.roles" using primary key columns */
  auth_roles_by_pk?: Maybe<Auth_Roles>;
  /** fetch data from the table: "contribution_types" */
  contribution_types: Array<Contribution_Types>;
  /** fetch aggregated fields from the table: "contribution_types" */
  contribution_types_aggregate: Contribution_Types_Aggregate;
  /** fetch data from the table: "contribution_types" using primary key columns */
  contribution_types_by_pk?: Maybe<Contribution_Types>;
  /** fetch data from the table: "group_cycles" */
  group_cycles: Array<Group_Cycles>;
  /** fetch aggregated fields from the table: "group_cycles" */
  group_cycles_aggregate: Group_Cycles_Aggregate;
  /** fetch data from the table: "group_cycles" using primary key columns */
  group_cycles_by_pk?: Maybe<Group_Cycles>;
  /** fetch data from the table: "group_recurrencies" */
  group_recurrencies: Array<Group_Recurrencies>;
  /** fetch aggregated fields from the table: "group_recurrencies" */
  group_recurrencies_aggregate: Group_Recurrencies_Aggregate;
  /** fetch data from the table: "group_recurrencies" using primary key columns */
  group_recurrencies_by_pk?: Maybe<Group_Recurrencies>;
  /** fetch data from the table: "groups" */
  groups: Array<Groups>;
  /** fetch aggregated fields from the table: "groups" */
  groups_aggregate: Groups_Aggregate;
  /** fetch data from the table: "groups" using primary key columns */
  groups_by_pk?: Maybe<Groups>;
  /** fetch data from the table: "members" */
  members: Array<Members>;
  /** fetch aggregated fields from the table: "members" */
  members_aggregate: Members_Aggregate;
  /** fetch data from the table: "members" using primary key columns */
  members_by_pk?: Maybe<Members>;
  /** fetch data from the table: "payment_statuses" */
  payment_statuses: Array<Payment_Statuses>;
  /** fetch aggregated fields from the table: "payment_statuses" */
  payment_statuses_aggregate: Payment_Statuses_Aggregate;
  /** fetch data from the table: "payment_statuses" using primary key columns */
  payment_statuses_by_pk?: Maybe<Payment_Statuses>;
  /** fetch data from the table: "payment_types" */
  payment_types: Array<Payment_Types>;
  /** fetch aggregated fields from the table: "payment_types" */
  payment_types_aggregate: Payment_Types_Aggregate;
  /** fetch data from the table: "payment_types" using primary key columns */
  payment_types_by_pk?: Maybe<Payment_Types>;
  /** fetch data from the table: "payments" */
  payments: Array<Payments>;
  /** fetch aggregated fields from the table: "payments" */
  payments_aggregate: Payments_Aggregate;
  /** fetch data from the table: "payments" using primary key columns */
  payments_by_pk?: Maybe<Payments>;
  /** fetch data from the table: "periods" */
  periods: Array<Periods>;
  /** fetch aggregated fields from the table: "periods" */
  periods_aggregate: Periods_Aggregate;
  /** fetch data from the table: "periods" using primary key columns */
  periods_by_pk?: Maybe<Periods>;
  /** fetch data from the table: "users" */
  users: Array<Users>;
  /** fetch aggregated fields from the table: "users" */
  users_aggregate: Users_Aggregate;
  /** fetch data from the table: "users" using primary key columns */
  users_by_pk?: Maybe<Users>;
};


/** query root */
export type Query_RootAuth_Account_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>;
  where?: Maybe<Auth_Account_Providers_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Account_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>;
  where?: Maybe<Auth_Account_Providers_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Account_Providers_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootAuth_Account_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>;
  where?: Maybe<Auth_Account_Roles_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Account_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>;
  where?: Maybe<Auth_Account_Roles_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Account_Roles_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootAuth_AccountsArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>;
  where?: Maybe<Auth_Accounts_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Accounts_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>;
  where?: Maybe<Auth_Accounts_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Accounts_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootAuth_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Providers_Order_By>>;
  where?: Maybe<Auth_Providers_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Providers_Order_By>>;
  where?: Maybe<Auth_Providers_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Providers_By_PkArgs = {
  provider: Scalars['String'];
};


/** query root */
export type Query_RootAuth_Refresh_TokensArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>;
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Refresh_Tokens_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>;
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Refresh_Tokens_By_PkArgs = {
  refresh_token: Scalars['uuid'];
};


/** query root */
export type Query_RootAuth_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Roles_Order_By>>;
  where?: Maybe<Auth_Roles_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Roles_Order_By>>;
  where?: Maybe<Auth_Roles_Bool_Exp>;
};


/** query root */
export type Query_RootAuth_Roles_By_PkArgs = {
  role: Scalars['String'];
};


/** query root */
export type Query_RootContribution_TypesArgs = {
  distinct_on?: Maybe<Array<Contribution_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Contribution_Types_Order_By>>;
  where?: Maybe<Contribution_Types_Bool_Exp>;
};


/** query root */
export type Query_RootContribution_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Contribution_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Contribution_Types_Order_By>>;
  where?: Maybe<Contribution_Types_Bool_Exp>;
};


/** query root */
export type Query_RootContribution_Types_By_PkArgs = {
  value: Scalars['String'];
};


/** query root */
export type Query_RootGroup_CyclesArgs = {
  distinct_on?: Maybe<Array<Group_Cycles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Group_Cycles_Order_By>>;
  where?: Maybe<Group_Cycles_Bool_Exp>;
};


/** query root */
export type Query_RootGroup_Cycles_AggregateArgs = {
  distinct_on?: Maybe<Array<Group_Cycles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Group_Cycles_Order_By>>;
  where?: Maybe<Group_Cycles_Bool_Exp>;
};


/** query root */
export type Query_RootGroup_Cycles_By_PkArgs = {
  value: Scalars['String'];
};


/** query root */
export type Query_RootGroup_RecurrenciesArgs = {
  distinct_on?: Maybe<Array<Group_Recurrencies_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Group_Recurrencies_Order_By>>;
  where?: Maybe<Group_Recurrencies_Bool_Exp>;
};


/** query root */
export type Query_RootGroup_Recurrencies_AggregateArgs = {
  distinct_on?: Maybe<Array<Group_Recurrencies_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Group_Recurrencies_Order_By>>;
  where?: Maybe<Group_Recurrencies_Bool_Exp>;
};


/** query root */
export type Query_RootGroup_Recurrencies_By_PkArgs = {
  value: Scalars['String'];
};


/** query root */
export type Query_RootGroupsArgs = {
  distinct_on?: Maybe<Array<Groups_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Groups_Order_By>>;
  where?: Maybe<Groups_Bool_Exp>;
};


/** query root */
export type Query_RootGroups_AggregateArgs = {
  distinct_on?: Maybe<Array<Groups_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Groups_Order_By>>;
  where?: Maybe<Groups_Bool_Exp>;
};


/** query root */
export type Query_RootGroups_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootMembersArgs = {
  distinct_on?: Maybe<Array<Members_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Members_Order_By>>;
  where?: Maybe<Members_Bool_Exp>;
};


/** query root */
export type Query_RootMembers_AggregateArgs = {
  distinct_on?: Maybe<Array<Members_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Members_Order_By>>;
  where?: Maybe<Members_Bool_Exp>;
};


/** query root */
export type Query_RootMembers_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootPayment_StatusesArgs = {
  distinct_on?: Maybe<Array<Payment_Statuses_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payment_Statuses_Order_By>>;
  where?: Maybe<Payment_Statuses_Bool_Exp>;
};


/** query root */
export type Query_RootPayment_Statuses_AggregateArgs = {
  distinct_on?: Maybe<Array<Payment_Statuses_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payment_Statuses_Order_By>>;
  where?: Maybe<Payment_Statuses_Bool_Exp>;
};


/** query root */
export type Query_RootPayment_Statuses_By_PkArgs = {
  value: Scalars['String'];
};


/** query root */
export type Query_RootPayment_TypesArgs = {
  distinct_on?: Maybe<Array<Payment_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payment_Types_Order_By>>;
  where?: Maybe<Payment_Types_Bool_Exp>;
};


/** query root */
export type Query_RootPayment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Payment_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payment_Types_Order_By>>;
  where?: Maybe<Payment_Types_Bool_Exp>;
};


/** query root */
export type Query_RootPayment_Types_By_PkArgs = {
  value: Scalars['String'];
};


/** query root */
export type Query_RootPaymentsArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};


/** query root */
export type Query_RootPayments_AggregateArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};


/** query root */
export type Query_RootPayments_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootPeriodsArgs = {
  distinct_on?: Maybe<Array<Periods_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Periods_Order_By>>;
  where?: Maybe<Periods_Bool_Exp>;
};


/** query root */
export type Query_RootPeriods_AggregateArgs = {
  distinct_on?: Maybe<Array<Periods_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Periods_Order_By>>;
  where?: Maybe<Periods_Bool_Exp>;
};


/** query root */
export type Query_RootPeriods_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootUsersArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Users_Order_By>>;
  where?: Maybe<Users_Bool_Exp>;
};


/** query root */
export type Query_RootUsers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Users_Order_By>>;
  where?: Maybe<Users_Bool_Exp>;
};


/** query root */
export type Query_RootUsers_By_PkArgs = {
  id: Scalars['uuid'];
};

/** subscription root */
export type Subscription_Root = {
  /** fetch data from the table: "auth.account_providers" */
  auth_account_providers: Array<Auth_Account_Providers>;
  /** fetch aggregated fields from the table: "auth.account_providers" */
  auth_account_providers_aggregate: Auth_Account_Providers_Aggregate;
  /** fetch data from the table: "auth.account_providers" using primary key columns */
  auth_account_providers_by_pk?: Maybe<Auth_Account_Providers>;
  /** fetch data from the table: "auth.account_roles" */
  auth_account_roles: Array<Auth_Account_Roles>;
  /** fetch aggregated fields from the table: "auth.account_roles" */
  auth_account_roles_aggregate: Auth_Account_Roles_Aggregate;
  /** fetch data from the table: "auth.account_roles" using primary key columns */
  auth_account_roles_by_pk?: Maybe<Auth_Account_Roles>;
  /** fetch data from the table: "auth.accounts" */
  auth_accounts: Array<Auth_Accounts>;
  /** fetch aggregated fields from the table: "auth.accounts" */
  auth_accounts_aggregate: Auth_Accounts_Aggregate;
  /** fetch data from the table: "auth.accounts" using primary key columns */
  auth_accounts_by_pk?: Maybe<Auth_Accounts>;
  /** fetch data from the table: "auth.providers" */
  auth_providers: Array<Auth_Providers>;
  /** fetch aggregated fields from the table: "auth.providers" */
  auth_providers_aggregate: Auth_Providers_Aggregate;
  /** fetch data from the table: "auth.providers" using primary key columns */
  auth_providers_by_pk?: Maybe<Auth_Providers>;
  /** fetch data from the table: "auth.refresh_tokens" */
  auth_refresh_tokens: Array<Auth_Refresh_Tokens>;
  /** fetch aggregated fields from the table: "auth.refresh_tokens" */
  auth_refresh_tokens_aggregate: Auth_Refresh_Tokens_Aggregate;
  /** fetch data from the table: "auth.refresh_tokens" using primary key columns */
  auth_refresh_tokens_by_pk?: Maybe<Auth_Refresh_Tokens>;
  /** fetch data from the table: "auth.roles" */
  auth_roles: Array<Auth_Roles>;
  /** fetch aggregated fields from the table: "auth.roles" */
  auth_roles_aggregate: Auth_Roles_Aggregate;
  /** fetch data from the table: "auth.roles" using primary key columns */
  auth_roles_by_pk?: Maybe<Auth_Roles>;
  /** fetch data from the table: "contribution_types" */
  contribution_types: Array<Contribution_Types>;
  /** fetch aggregated fields from the table: "contribution_types" */
  contribution_types_aggregate: Contribution_Types_Aggregate;
  /** fetch data from the table: "contribution_types" using primary key columns */
  contribution_types_by_pk?: Maybe<Contribution_Types>;
  /** fetch data from the table: "group_cycles" */
  group_cycles: Array<Group_Cycles>;
  /** fetch aggregated fields from the table: "group_cycles" */
  group_cycles_aggregate: Group_Cycles_Aggregate;
  /** fetch data from the table: "group_cycles" using primary key columns */
  group_cycles_by_pk?: Maybe<Group_Cycles>;
  /** fetch data from the table: "group_recurrencies" */
  group_recurrencies: Array<Group_Recurrencies>;
  /** fetch aggregated fields from the table: "group_recurrencies" */
  group_recurrencies_aggregate: Group_Recurrencies_Aggregate;
  /** fetch data from the table: "group_recurrencies" using primary key columns */
  group_recurrencies_by_pk?: Maybe<Group_Recurrencies>;
  /** fetch data from the table: "groups" */
  groups: Array<Groups>;
  /** fetch aggregated fields from the table: "groups" */
  groups_aggregate: Groups_Aggregate;
  /** fetch data from the table: "groups" using primary key columns */
  groups_by_pk?: Maybe<Groups>;
  /** fetch data from the table: "members" */
  members: Array<Members>;
  /** fetch aggregated fields from the table: "members" */
  members_aggregate: Members_Aggregate;
  /** fetch data from the table: "members" using primary key columns */
  members_by_pk?: Maybe<Members>;
  /** fetch data from the table: "payment_statuses" */
  payment_statuses: Array<Payment_Statuses>;
  /** fetch aggregated fields from the table: "payment_statuses" */
  payment_statuses_aggregate: Payment_Statuses_Aggregate;
  /** fetch data from the table: "payment_statuses" using primary key columns */
  payment_statuses_by_pk?: Maybe<Payment_Statuses>;
  /** fetch data from the table: "payment_types" */
  payment_types: Array<Payment_Types>;
  /** fetch aggregated fields from the table: "payment_types" */
  payment_types_aggregate: Payment_Types_Aggregate;
  /** fetch data from the table: "payment_types" using primary key columns */
  payment_types_by_pk?: Maybe<Payment_Types>;
  /** fetch data from the table: "payments" */
  payments: Array<Payments>;
  /** fetch aggregated fields from the table: "payments" */
  payments_aggregate: Payments_Aggregate;
  /** fetch data from the table: "payments" using primary key columns */
  payments_by_pk?: Maybe<Payments>;
  /** fetch data from the table: "periods" */
  periods: Array<Periods>;
  /** fetch aggregated fields from the table: "periods" */
  periods_aggregate: Periods_Aggregate;
  /** fetch data from the table: "periods" using primary key columns */
  periods_by_pk?: Maybe<Periods>;
  /** fetch data from the table: "users" */
  users: Array<Users>;
  /** fetch aggregated fields from the table: "users" */
  users_aggregate: Users_Aggregate;
  /** fetch data from the table: "users" using primary key columns */
  users_by_pk?: Maybe<Users>;
};


/** subscription root */
export type Subscription_RootAuth_Account_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>;
  where?: Maybe<Auth_Account_Providers_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Account_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>;
  where?: Maybe<Auth_Account_Providers_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Account_Providers_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootAuth_Account_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>;
  where?: Maybe<Auth_Account_Roles_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Account_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>;
  where?: Maybe<Auth_Account_Roles_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Account_Roles_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootAuth_AccountsArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>;
  where?: Maybe<Auth_Accounts_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Accounts_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>;
  where?: Maybe<Auth_Accounts_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Accounts_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootAuth_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Providers_Order_By>>;
  where?: Maybe<Auth_Providers_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Providers_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Providers_Order_By>>;
  where?: Maybe<Auth_Providers_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Providers_By_PkArgs = {
  provider: Scalars['String'];
};


/** subscription root */
export type Subscription_RootAuth_Refresh_TokensArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>;
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Refresh_Tokens_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>;
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Refresh_Tokens_By_PkArgs = {
  refresh_token: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootAuth_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Roles_Order_By>>;
  where?: Maybe<Auth_Roles_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Roles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Auth_Roles_Order_By>>;
  where?: Maybe<Auth_Roles_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAuth_Roles_By_PkArgs = {
  role: Scalars['String'];
};


/** subscription root */
export type Subscription_RootContribution_TypesArgs = {
  distinct_on?: Maybe<Array<Contribution_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Contribution_Types_Order_By>>;
  where?: Maybe<Contribution_Types_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootContribution_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Contribution_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Contribution_Types_Order_By>>;
  where?: Maybe<Contribution_Types_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootContribution_Types_By_PkArgs = {
  value: Scalars['String'];
};


/** subscription root */
export type Subscription_RootGroup_CyclesArgs = {
  distinct_on?: Maybe<Array<Group_Cycles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Group_Cycles_Order_By>>;
  where?: Maybe<Group_Cycles_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGroup_Cycles_AggregateArgs = {
  distinct_on?: Maybe<Array<Group_Cycles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Group_Cycles_Order_By>>;
  where?: Maybe<Group_Cycles_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGroup_Cycles_By_PkArgs = {
  value: Scalars['String'];
};


/** subscription root */
export type Subscription_RootGroup_RecurrenciesArgs = {
  distinct_on?: Maybe<Array<Group_Recurrencies_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Group_Recurrencies_Order_By>>;
  where?: Maybe<Group_Recurrencies_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGroup_Recurrencies_AggregateArgs = {
  distinct_on?: Maybe<Array<Group_Recurrencies_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Group_Recurrencies_Order_By>>;
  where?: Maybe<Group_Recurrencies_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGroup_Recurrencies_By_PkArgs = {
  value: Scalars['String'];
};


/** subscription root */
export type Subscription_RootGroupsArgs = {
  distinct_on?: Maybe<Array<Groups_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Groups_Order_By>>;
  where?: Maybe<Groups_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGroups_AggregateArgs = {
  distinct_on?: Maybe<Array<Groups_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Groups_Order_By>>;
  where?: Maybe<Groups_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootGroups_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootMembersArgs = {
  distinct_on?: Maybe<Array<Members_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Members_Order_By>>;
  where?: Maybe<Members_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootMembers_AggregateArgs = {
  distinct_on?: Maybe<Array<Members_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Members_Order_By>>;
  where?: Maybe<Members_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootMembers_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootPayment_StatusesArgs = {
  distinct_on?: Maybe<Array<Payment_Statuses_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payment_Statuses_Order_By>>;
  where?: Maybe<Payment_Statuses_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootPayment_Statuses_AggregateArgs = {
  distinct_on?: Maybe<Array<Payment_Statuses_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payment_Statuses_Order_By>>;
  where?: Maybe<Payment_Statuses_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootPayment_Statuses_By_PkArgs = {
  value: Scalars['String'];
};


/** subscription root */
export type Subscription_RootPayment_TypesArgs = {
  distinct_on?: Maybe<Array<Payment_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payment_Types_Order_By>>;
  where?: Maybe<Payment_Types_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootPayment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Payment_Types_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payment_Types_Order_By>>;
  where?: Maybe<Payment_Types_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootPayment_Types_By_PkArgs = {
  value: Scalars['String'];
};


/** subscription root */
export type Subscription_RootPaymentsArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootPayments_AggregateArgs = {
  distinct_on?: Maybe<Array<Payments_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Payments_Order_By>>;
  where?: Maybe<Payments_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootPayments_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootPeriodsArgs = {
  distinct_on?: Maybe<Array<Periods_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Periods_Order_By>>;
  where?: Maybe<Periods_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootPeriods_AggregateArgs = {
  distinct_on?: Maybe<Array<Periods_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Periods_Order_By>>;
  where?: Maybe<Periods_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootPeriods_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootUsersArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Users_Order_By>>;
  where?: Maybe<Users_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootUsers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Users_Order_By>>;
  where?: Maybe<Users_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootUsers_By_PkArgs = {
  id: Scalars['uuid'];
};


/** expression to compare columns of type timestamp. All fields are combined with logical 'AND'. */
export type Timestamp_Comparison_Exp = {
  _eq?: Maybe<Scalars['timestamp']>;
  _gt?: Maybe<Scalars['timestamp']>;
  _gte?: Maybe<Scalars['timestamp']>;
  _in?: Maybe<Array<Scalars['timestamp']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['timestamp']>;
  _lte?: Maybe<Scalars['timestamp']>;
  _neq?: Maybe<Scalars['timestamp']>;
  _nin?: Maybe<Array<Scalars['timestamp']>>;
};


/** expression to compare columns of type timestamptz. All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _eq?: Maybe<Scalars['timestamptz']>;
  _gt?: Maybe<Scalars['timestamptz']>;
  _gte?: Maybe<Scalars['timestamptz']>;
  _in?: Maybe<Array<Scalars['timestamptz']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['timestamptz']>;
  _lte?: Maybe<Scalars['timestamptz']>;
  _neq?: Maybe<Scalars['timestamptz']>;
  _nin?: Maybe<Array<Scalars['timestamptz']>>;
};

/** columns and relationships of "users" */
export type Users = {
  /** An object relationship */
  account?: Maybe<Auth_Accounts>;
  avatar_url?: Maybe<Scalars['String']>;
  created_at: Scalars['timestamptz'];
  display_name?: Maybe<Scalars['String']>;
  expo_push_token?: Maybe<Scalars['String']>;
  /** An array relationship */
  groups: Array<Groups>;
  /** An aggregated array relationship */
  groups_aggregate: Groups_Aggregate;
  id: Scalars['uuid'];
  /** An array relationship */
  members: Array<Members>;
  /** An aggregated array relationship */
  members_aggregate: Members_Aggregate;
  updated_at: Scalars['timestamptz'];
};


/** columns and relationships of "users" */
export type UsersGroupsArgs = {
  distinct_on?: Maybe<Array<Groups_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Groups_Order_By>>;
  where?: Maybe<Groups_Bool_Exp>;
};


/** columns and relationships of "users" */
export type UsersGroups_AggregateArgs = {
  distinct_on?: Maybe<Array<Groups_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Groups_Order_By>>;
  where?: Maybe<Groups_Bool_Exp>;
};


/** columns and relationships of "users" */
export type UsersMembersArgs = {
  distinct_on?: Maybe<Array<Members_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Members_Order_By>>;
  where?: Maybe<Members_Bool_Exp>;
};


/** columns and relationships of "users" */
export type UsersMembers_AggregateArgs = {
  distinct_on?: Maybe<Array<Members_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Members_Order_By>>;
  where?: Maybe<Members_Bool_Exp>;
};

/** aggregated selection of "users" */
export type Users_Aggregate = {
  aggregate?: Maybe<Users_Aggregate_Fields>;
  nodes: Array<Users>;
};

/** aggregate fields of "users" */
export type Users_Aggregate_Fields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Users_Max_Fields>;
  min?: Maybe<Users_Min_Fields>;
};


/** aggregate fields of "users" */
export type Users_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "users" */
export type Users_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Users_Max_Order_By>;
  min?: Maybe<Users_Min_Order_By>;
};

/** input type for inserting array relation for remote table "users" */
export type Users_Arr_Rel_Insert_Input = {
  data: Array<Users_Insert_Input>;
  on_conflict?: Maybe<Users_On_Conflict>;
};

/** Boolean expression to filter rows from the table "users". All fields are combined with a logical 'AND'. */
export type Users_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Users_Bool_Exp>>>;
  _not?: Maybe<Users_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Users_Bool_Exp>>>;
  account?: Maybe<Auth_Accounts_Bool_Exp>;
  avatar_url?: Maybe<String_Comparison_Exp>;
  created_at?: Maybe<Timestamptz_Comparison_Exp>;
  display_name?: Maybe<String_Comparison_Exp>;
  expo_push_token?: Maybe<String_Comparison_Exp>;
  groups?: Maybe<Groups_Bool_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  members?: Maybe<Members_Bool_Exp>;
  updated_at?: Maybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "users" */
export enum Users_Constraint {
  /** unique or primary key constraint */
  UsersPkey = 'users_pkey'
}

/** input type for inserting data into table "users" */
export type Users_Insert_Input = {
  account?: Maybe<Auth_Accounts_Obj_Rel_Insert_Input>;
  avatar_url?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  display_name?: Maybe<Scalars['String']>;
  expo_push_token?: Maybe<Scalars['String']>;
  groups?: Maybe<Groups_Arr_Rel_Insert_Input>;
  id?: Maybe<Scalars['uuid']>;
  members?: Maybe<Members_Arr_Rel_Insert_Input>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type Users_Max_Fields = {
  avatar_url?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  display_name?: Maybe<Scalars['String']>;
  expo_push_token?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "users" */
export type Users_Max_Order_By = {
  avatar_url?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  display_name?: Maybe<Order_By>;
  expo_push_token?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Users_Min_Fields = {
  avatar_url?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  display_name?: Maybe<Scalars['String']>;
  expo_push_token?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "users" */
export type Users_Min_Order_By = {
  avatar_url?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  display_name?: Maybe<Order_By>;
  expo_push_token?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** response of any mutation on the table "users" */
export type Users_Mutation_Response = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Users>;
};

/** input type for inserting object relation for remote table "users" */
export type Users_Obj_Rel_Insert_Input = {
  data: Users_Insert_Input;
  on_conflict?: Maybe<Users_On_Conflict>;
};

/** on conflict condition type for table "users" */
export type Users_On_Conflict = {
  constraint: Users_Constraint;
  update_columns: Array<Users_Update_Column>;
  where?: Maybe<Users_Bool_Exp>;
};

/** ordering options when selecting data from "users" */
export type Users_Order_By = {
  account?: Maybe<Auth_Accounts_Order_By>;
  avatar_url?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  display_name?: Maybe<Order_By>;
  expo_push_token?: Maybe<Order_By>;
  groups_aggregate?: Maybe<Groups_Aggregate_Order_By>;
  id?: Maybe<Order_By>;
  members_aggregate?: Maybe<Members_Aggregate_Order_By>;
  updated_at?: Maybe<Order_By>;
};

/** primary key columns input for table: "users" */
export type Users_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "users" */
export enum Users_Select_Column {
  /** column name */
  AvatarUrl = 'avatar_url',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DisplayName = 'display_name',
  /** column name */
  ExpoPushToken = 'expo_push_token',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "users" */
export type Users_Set_Input = {
  avatar_url?: Maybe<Scalars['String']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  display_name?: Maybe<Scalars['String']>;
  expo_push_token?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** update columns of table "users" */
export enum Users_Update_Column {
  /** column name */
  AvatarUrl = 'avatar_url',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DisplayName = 'display_name',
  /** column name */
  ExpoPushToken = 'expo_push_token',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at'
}


/** expression to compare columns of type uuid. All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
  _eq?: Maybe<Scalars['uuid']>;
  _gt?: Maybe<Scalars['uuid']>;
  _gte?: Maybe<Scalars['uuid']>;
  _in?: Maybe<Array<Scalars['uuid']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['uuid']>;
  _lte?: Maybe<Scalars['uuid']>;
  _neq?: Maybe<Scalars['uuid']>;
  _nin?: Maybe<Array<Scalars['uuid']>>;
};

export type UserDtoFragment = Pick<Users, 'id' | 'display_name' | 'avatar_url' | 'created_at'>;

export type MemberDtoFragment = (
  Pick<Members, 'id' | 'created_at'>
  & { user: UserDtoFragment }
);

export type PaymentDtoFragment = Pick<Payments, 'id' | 'member_id' | 'created_at' | 'payment_amount'>;

export type PeriodDtoFragment = (
  Pick<Periods, 'id' | 'created_at' | 'period_progression' | 'period_completed_at'>
  & { payments: Array<PaymentDtoFragment> }
);

export type GroupDtoFragment = (
  Pick<Groups, 'id' | 'created_at' | 'group_balance' | 'group_code' | 'group_contribution_amount' | 'group_contribution_type' | 'group_name' | 'group_recurrency_amount' | 'group_recurrency_day' | 'group_recurrency_type'>
  & { creator: UserDtoFragment, members: Array<MemberDtoFragment>, periods: Array<PeriodDtoFragment>, payments: Array<PaymentDtoFragment> }
);

export type GroupsSubscriptionVariables = Exact<{
  user_id: Scalars['uuid'];
}>;


export type GroupsSubscriptionResult = { groups: Array<GroupDtoFragment> };

export type Users_By_PkQueryVariables = Exact<{
  user_id: Scalars['uuid'];
}>;


export type Users_By_PkQueryResult = { users_by_pk?: Maybe<Pick<Users, 'created_at' | 'id'>> };

export type Action_Create_GroupMutationVariables = Exact<{
  group_name: Scalars['String'];
  creator_id: Scalars['uuid'];
  group_contribution_type: Scalars['String'];
  group_recurrency_type: Scalars['String'];
  group_recurrency_day?: Maybe<Scalars['numeric']>;
  group_contribution_amount?: Maybe<Scalars['numeric']>;
}>;


export type Action_Create_GroupMutationResult = { action_create_group?: Maybe<Pick<ActionCreateGroupOutput, 'ok'>> };

export type Set_User_Expo_Push_TokenMutationVariables = Exact<{
  userid: Scalars['uuid'];
  token: Scalars['String'];
}>;


export type Set_User_Expo_Push_TokenMutationResult = { update_users_by_pk?: Maybe<Pick<Users, 'id'>> };

export type Insert_Members_OneMutationVariables = Exact<{
  group_id: Scalars['uuid'];
  user_id: Scalars['uuid'];
}>;


export type Insert_Members_OneMutationResult = { insert_members_one?: Maybe<Pick<Members, 'id'>> };

export type Action_Request_PaymentMutationVariables = Exact<{
  args: ActionRequestPaymentInput;
}>;


export type Action_Request_PaymentMutationResult = { action_request_payment?: Maybe<Pick<ActionRequestPaymentOutput, 'url' | 'id'>> };

export type Action_Withdraw_PaymentMutationVariables = Exact<{
  args: ActionWithdrawPaymentInput;
}>;


export type Action_Withdraw_PaymentMutationResult = { action_withdraw_payment?: Maybe<Pick<ActionWithdrawPaymentOutput, 'id'>> };

export const UserDtoFragmentDoc = gql`
    fragment UserDto on users {
  id
  display_name
  avatar_url
  created_at
}
    `;
export const MemberDtoFragmentDoc = gql`
    fragment MemberDto on members {
  id
  created_at
  user {
    ...UserDto
  }
}
    ${UserDtoFragmentDoc}`;
export const PaymentDtoFragmentDoc = gql`
    fragment PaymentDto on payments {
  id
  member_id
  created_at
  payment_amount
}
    `;
export const PeriodDtoFragmentDoc = gql`
    fragment PeriodDto on periods {
  id
  created_at
  period_progression
  period_completed_at
  payments(
    limit: 1
    order_by: {created_at: desc}
    where: {payment_status: {_eq: COMPLETED}}
  ) {
    ...PaymentDto
  }
}
    ${PaymentDtoFragmentDoc}`;
export const GroupDtoFragmentDoc = gql`
    fragment GroupDto on groups {
  id
  created_at
  group_balance
  group_code
  group_contribution_amount
  group_contribution_type
  group_name
  group_recurrency_amount
  group_recurrency_day
  group_recurrency_type
  creator {
    ...UserDto
  }
  members {
    ...MemberDto
  }
  periods(
    limit: 1
    order_by: {created_at: desc}
    where: {period_active: {_eq: true}}
  ) {
    ...PeriodDto
  }
  payments(
    limit: 1
    order_by: {created_at: desc}
    where: {payment_status: {_eq: COMPLETED}}
  ) {
    ...PaymentDto
  }
}
    ${UserDtoFragmentDoc}
${MemberDtoFragmentDoc}
${PeriodDtoFragmentDoc}
${PaymentDtoFragmentDoc}`;
export const GroupsDocument = gql`
    subscription groups($user_id: uuid!) {
  groups(
    where: {members: {user_id: {_in: [$user_id]}}}
    order_by: {group_name: asc}
  ) {
    ...GroupDto
  }
}
    ${GroupDtoFragmentDoc}`;
export const Users_By_PkDocument = gql`
    query users_by_pk($user_id: uuid!) {
  users_by_pk(id: $user_id) {
    created_at
    id
  }
}
    `;
export const Action_Create_GroupDocument = gql`
    mutation action_create_group($group_name: String!, $creator_id: uuid!, $group_contribution_type: String!, $group_recurrency_type: String!, $group_recurrency_day: numeric, $group_contribution_amount: numeric) {
  action_create_group(
    arg: {creator_id: $creator_id, group_contribution_type: $group_contribution_type, group_recurrency_type: $group_recurrency_type, group_name: $group_name, group_recurrency_day: $group_recurrency_day, group_contribution_amount: $group_contribution_amount}
  ) {
    ok
  }
}
    `;
export const Set_User_Expo_Push_TokenDocument = gql`
    mutation set_user_expo_push_token($userid: uuid!, $token: String!) {
  update_users_by_pk(pk_columns: {id: $userid}, _set: {expo_push_token: $token}) {
    id
  }
}
    `;
export const Insert_Members_OneDocument = gql`
    mutation insert_members_one($group_id: uuid!, $user_id: uuid!) {
  insert_members_one(object: {group_id: $group_id, user_id: $user_id}) {
    id
  }
}
    `;
export const Action_Request_PaymentDocument = gql`
    mutation action_request_payment($args: ActionRequestPaymentInput!) {
  action_request_payment(arg: $args) {
    url
    id
  }
}
    `;
export const Action_Withdraw_PaymentDocument = gql`
    mutation action_withdraw_payment($args: ActionWithdrawPaymentInput!) {
  action_withdraw_payment(args: $args) {
    id
  }
}
    `;

export type SdkFunctionWrapper = <T>(action: (requestHeaders?:Record<string, string>) => Promise<T>, operationName: string) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = (action, _operationName) => action();

export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    groups(variables: GroupsSubscriptionVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GroupsSubscriptionResult> {
      return withWrapper((wrappedRequestHeaders) => client.request<GroupsSubscriptionResult>(GroupsDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'groups');
    },
    users_by_pk(variables: Users_By_PkQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<Users_By_PkQueryResult> {
      return withWrapper((wrappedRequestHeaders) => client.request<Users_By_PkQueryResult>(Users_By_PkDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'users_by_pk');
    },
    action_create_group(variables: Action_Create_GroupMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<Action_Create_GroupMutationResult> {
      return withWrapper((wrappedRequestHeaders) => client.request<Action_Create_GroupMutationResult>(Action_Create_GroupDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'action_create_group');
    },
    set_user_expo_push_token(variables: Set_User_Expo_Push_TokenMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<Set_User_Expo_Push_TokenMutationResult> {
      return withWrapper((wrappedRequestHeaders) => client.request<Set_User_Expo_Push_TokenMutationResult>(Set_User_Expo_Push_TokenDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'set_user_expo_push_token');
    },
    insert_members_one(variables: Insert_Members_OneMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<Insert_Members_OneMutationResult> {
      return withWrapper((wrappedRequestHeaders) => client.request<Insert_Members_OneMutationResult>(Insert_Members_OneDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'insert_members_one');
    },
    action_request_payment(variables: Action_Request_PaymentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<Action_Request_PaymentMutationResult> {
      return withWrapper((wrappedRequestHeaders) => client.request<Action_Request_PaymentMutationResult>(Action_Request_PaymentDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'action_request_payment');
    },
    action_withdraw_payment(variables: Action_Withdraw_PaymentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<Action_Withdraw_PaymentMutationResult> {
      return withWrapper((wrappedRequestHeaders) => client.request<Action_Withdraw_PaymentMutationResult>(Action_Withdraw_PaymentDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'action_withdraw_payment');
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;