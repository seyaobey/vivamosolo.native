import get from 'lodash/get';

export const getApiError = (error: unknown) =>
    get(error, 'response.errors[0].message') as string;
