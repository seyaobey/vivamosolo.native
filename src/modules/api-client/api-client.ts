import { GraphQLClient } from 'graphql-request';

import { getEnvVariables } from 'env-variables';

import { getSdk } from './api-sdk-generated';

export const graphClient = new GraphQLClient(
    getEnvVariables().ENV_GRAPHQL_URL,
    {
        headers: {
            'X-Hasura-Admin-Secret': getEnvVariables().ENV_HASURA_ADMIN,
            'X-Hasura-Role': 'admin',
        } as never,
    }
);

export const clientApi = getSdk(graphClient);
