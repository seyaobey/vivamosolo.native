type ExtractValuePart<T, K extends keyof T> = T[K];

export type MakeEvent<T, Payload = unknown> = {
    type: T;
} & Payload;

type ExpandEvents<T> = {
    [k in keyof T]: {
        type: `${string & k}`;
    } & T[k];
};

export type MakeEvents<T> = ExtractValuePart<ExpandEvents<T>, keyof T>;

type ExpandStates<Context, T> = {
    [p in keyof T]: {
        value: `${string & p}`;
        context: Context;
    } & T[p];
};

export type MakeContextTypeState<Context, T> = ExtractValuePart<
    ExpandStates<Context, T>,
    keyof T
>;

// export type EventExample = CombineEvents<{
//     is_authenticated: { id: string };
//     is_unauthenticated: unknown;
// }>;

type CombineStates<T> = ExtractValuePart<
    {
        [k in keyof T]: {
            value: `${string & k}`;
        };
    },
    keyof T
>;

export type MakeTypeState<T, C> = CombineStates<T> & {
    context: C;
};
