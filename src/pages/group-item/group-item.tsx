import React from 'react';

import { VStack, HStack } from 'native-base';

import { Page } from 'components/page';
import { PanelBox } from 'components/panel';
import { ProgressStatus } from 'components/progress-status';
import { Scrollbox } from 'components/scrollbox';
import { usePaymentMachine } from 'machines/payment';
import { progress } from 'machines/progress';
import { withdraw } from 'machines/withdraw';
import { getApiError } from 'modules/api-client';
import { useApplicationMachine } from 'modules/application-machine';
import { Title } from 'modules/compounds/title';
import { ItemCard } from 'pages/group-listing/listing-page/item-card/item-card';
import { PaymentSwipeable } from 'pages/payment/payment-swipeable';
import { WithdrawSwipeable } from 'pages/payment/withdraw-swipeable';
import { WithdrawSwipeableContacts } from 'pages/payment/withdraw-swipeable-contacts';

import { GroupMembers } from './group-members';
import { GroupPaymentButtons } from './group-payment-actions';

type IGroupItemProps = {
    ctx: ReturnType<typeof useGroupItem>;
};

const useGroupItem = () => {
    const {
        app: { state, send },
    } = useApplicationMachine();
    const goBack = () => send('go_back');
    const { active } = usePaymentMachine();

    const onTitle = () => {
        const { state: progrState } = progress;
        const {
            context: { error },
        } = withdraw.state;

        if (progrState.matches('failed') && error) {
            return {
                onTitle: () => getApiError(error),
            };
        }
        return {};
    };

    return {
        goBack,
        onTitle,
        activePayment: active,
        group: state.context.groups.find(
            (g) => g.id === state.context.groupId
        )!,
    };
};

const GroupItem: React.FC<IGroupItemProps> = ({
    ctx = useGroupItem(),
}: IGroupItemProps) => (
    <Page>
        <Scrollbox>
            <Page.AppBar onGoBack={ctx.goBack} />
            <Page.Content>
                <VStack>
                    <Title mb={2}>Group activity</Title>
                    <ItemCard item={ctx.group} />
                </VStack>

                <HStack mt={4}>
                    <GroupPaymentButtons />
                </HStack>

                <PanelBox mt={8}>
                    <GroupMembers group={ctx.group} />
                </PanelBox>
            </Page.Content>
        </Scrollbox>

        <PaymentSwipeable />
        <WithdrawSwipeable />
        <WithdrawSwipeableContacts />
        <ProgressStatus
            {...ctx.onTitle()}
            onComplete={() => {
                progress.send('RESET');
            }}
        />
    </Page>
);

export { GroupItem };
