/* eslint-disable react/no-unused-prop-types */
import React from 'react';

import { FlatList, Divider } from 'native-base';

import { GroupDtoFragment } from 'modules/api-graphql/api-apollo';

import { renderMember } from './group-member';

type IGroupMembersProps = {
    group: GroupDtoFragment;
};

const GroupMembers: React.FC<IGroupMembersProps> = ({ group }) => (
    <FlatList
        scrollEnabled={false}
        data={group.members ?? []}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        ItemSeparatorComponent={() => <Divider my={4} />}
        keyExtractor={(item) => `${item.id}`}
        renderItem={renderMember(group)}
    />
);

export { GroupMembers };
