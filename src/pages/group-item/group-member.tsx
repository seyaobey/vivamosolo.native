/* eslint-disable react/no-unused-prop-types */
import React from 'react';

import { AntDesign } from '@expo/vector-icons';
import { VStack, HStack, Text, Box, Icon } from 'native-base';

import { CurrencyText } from 'components/currency-text';
import { TextStack } from 'components/text-stack';
import {
    GroupDtoFragment,
    MemberDtoFragment,
} from 'modules/api-graphql/api-apollo';
import { MemberIcon } from 'pages/group-listing/listing-page/members';

type MemberProps = {
    item: MemberDtoFragment;
    group: GroupDtoFragment;
};
const Member = ({ group, item }: MemberProps) => (
    <HStack flex={1} alignItems="center" justifyContent="space-between">
        <HStack alignItems="center">
            <Box mr={2}>
                <MemberIcon member={item} fallback="icon" />
            </Box>
            <Text>{item.user.display_name}</Text>
        </HStack>

        <VStack>
            <Contribution group={group} item={item} />
        </VStack>
    </HStack>
);

type ContributionProps = MemberProps;
const Contribution = ({ group }: ContributionProps) => {
    const payment = group.payments?.[0]?.payment_amount ?? 0;
    return (
        <TextStack title="contributions">
            <HStack alignItems="center" justifyContent="flex-end">
                {payment ? (
                    <Icon
                        name="arrowup"
                        size={4}
                        color="green.600"
                        as={AntDesign}
                    />
                ) : (
                    <></>
                )}
                <CurrencyText
                    value={payment}
                    render={(text) => (
                        <Text color="green.500" fontSize="lg">
                            {text}
                        </Text>
                    )}
                />
            </HStack>
        </TextStack>
    );
};

export const renderMember =
    (group: GroupDtoFragment) =>
    ({ item }: Pick<MemberProps, 'item'>) =>
        <Member group={group} item={item} />;
