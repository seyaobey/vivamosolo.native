import React from 'react';

import { FormControl, Input, VStack, Box, Text } from 'native-base';
import { useForm, useController } from 'react-hook-form';
import NumberFormat from 'react-number-format';

import { ButtonAction } from 'components/button-action';
import { PanelBox } from 'components/panel';
import { Swipeable } from 'components/swipeable';
import { Group_Recurrencies_Enum } from 'modules/api-graphql/api-react-query';
import { auth } from 'modules/application';
import { useApplicationMachine } from 'modules/application-machine';

export type PaymentFormProps = {
    ctx?: ReturnType<typeof usePaymentForm>;
};

type PaymentAmountValue = {
    strAmount: string;
    numAmount: number;
};

const SwipeablePaymentForm = ({ ctx = usePaymentForm() }: PaymentFormProps) => (
    <Swipeable isOpen={ctx.isOpen} onClosed={ctx.onClose}>
        <VStack mx={4} space={8}>
            <PanelBox mt={8} mb={2}>
                <Text fontSize="2xl" fontWeight="light">
                    Add money
                </Text>
            </PanelBox>

            <PanelBox>
                <FormControl
                    isRequired
                    isInvalid={!!ctx.errors?.strAmount?.message}>
                    <FormControl.Label
                        _text={{
                            fontWeight: 'light',
                        }}>
                        Enter the payment amount
                    </FormControl.Label>
                    <NumberFormat
                        value={ctx.value}
                        displayType="text"
                        thousandSeparator
                        prefix="$"
                        allowNegative={false}
                        onValueChange={({ floatValue }) => {
                            ctx.setValue('numAmount', Number(floatValue));
                        }}
                        renderText={getAmountField({ onChange: ctx.onChange })}
                    />
                    <FormControl.ErrorMessage>
                        {ctx.errors?.strAmount?.message}
                    </FormControl.ErrorMessage>
                </FormControl>
            </PanelBox>

            <Box my={6} h={100}>
                <ButtonAction
                    onPress={ctx.makePayment}
                    isLoading={ctx.processing}>
                    Make payment
                </ButtonAction>
            </Box>
        </VStack>
    </Swipeable>
);

type AmountFieldProps = {
    onChange: (text: string) => void;
};

const getAmountField =
    ({ onChange }: AmountFieldProps) =>
    (value: string) =>
        (
            <Input
                variant="filled"
                textAlign="right"
                value={value}
                color="gray.500"
                keyboardType="decimal-pad"
                onChangeText={onChange}
                placeholder="$0"
            />
        );

const usePaymentForm = () => {
    const {
        app: { state, send },
    } = useApplicationMachine();

    const {
        control,
        setValue,
        handleSubmit,
        formState: { errors },
    } = useForm<PaymentAmountValue>({
        defaultValues: { numAmount: 0 },
    });

    const {
        field: { value, onChange },
    } = useController<PaymentAmountValue>({
        name: 'strAmount',
        control,
        rules: {
            required: {
                value: true,
                message: 'Payment amount is required',
            },
            min: {
                value: 1,
                message: 'Payment amount is not valid',
            },
        },
    });

    const isOpen = state.matches({
        authenticated: { listing_item: 'payment_preparing' },
    });

    const onClose = () => send({ type: 'cancel_payment' });

    const makePayment = handleSubmit((data: PaymentAmountValue) => {
        const group = state.context.groups.find(
            (g) => g.id === state.context.groupId
        )!;

        const memberId = group.members.find(
            (m) => m.user.id === auth.user()?.id
        )!.id;

        const period =
            group.group_recurrency_type !== Group_Recurrencies_Enum.None
                ? group.periods[0]
                : undefined;

        send({
            type: 'request_payment_url',
            value: {
                args: {
                    amount: data.numAmount,
                    group_id: state.context.groupId,
                    member_id: memberId,
                    period_id: period?.id,
                },
            },
        });
    });

    return {
        value,
        control,
        isOpen,
        errors,
        processing: false,
        onChange,
        onClose,
        setValue,
        makePayment,
    };
};

export { SwipeablePaymentForm };
