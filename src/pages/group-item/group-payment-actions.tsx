import React from 'react';

import { Entypo, Ionicons } from '@expo/vector-icons';
import { Box, Icon } from 'native-base';

import { ButtonAction } from 'components/button-action';
import { usePaymentMachine } from 'machines/payment';
import { withdraw } from 'machines/withdraw';
import { useApplicationMachine } from 'modules/application-machine';

export type IGroupPaymentActionsProps = {};

const AddMoneyIcon = <Icon name="wallet" size={7} as={Entypo} />;
const WithdorawIcon = <Icon name="arrow-forward" size={7} as={Ionicons} />;

const GroupPaymentActions: React.FC<IGroupPaymentActionsProps> = ({}) => {
    const { send } = usePaymentMachine();
    const { app } = useApplicationMachine();
    return (
        <>
            <ButtonAction
                flex={1}
                startIcon={AddMoneyIcon}
                onPress={() => send({ type: 'START_PAYMENT' })}>
                Add money
            </ButtonAction>
            <Box w={6} />
            <ButtonAction
                flex={1}
                startIcon={WithdorawIcon}
                onPress={() => {
                    const group = app.state.context.groups.find(
                        (g) => g.id === app.state.context.groupId
                    )!;
                    withdraw.send({ type: 'START_WITHDRAW', group });
                }}>
                Send
            </ButtonAction>
        </>
    );
};

export { GroupPaymentActions as GroupPaymentButtons };
