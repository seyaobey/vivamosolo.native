/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { FontAwesome5 as FA } from '@expo/vector-icons';
import dateFormat from 'dateformat';
import Constants from 'expo-constants';
import * as updates from 'expo-updates';
import { Icon, Center, Text, Column } from 'native-base';
import { DataTable } from 'react-native-paper';
import SkeletonContent from 'react-native-skeleton-content';

import { Page } from 'components/page';
import { Panel } from 'components/panel';
import { Scrollbox } from 'components/scrollbox';
import { auth } from 'modules/application';
import { useApplicationMachine } from 'modules/application-machine';

export type IMessagesProps = {};

const Profile: React.FC<IMessagesProps> = ({}) => {
    const {
        app: { state },
    } = useApplicationMachine();

    const fetching = state.matches({
        authenticated: { profile: 'fetching' },
    });

    return (
        <Page bg="white">
            <Scrollbox mt={4}>
                {fetching ? (
                    <Skeleton loading>
                        <></>
                    </Skeleton>
                ) : (
                    <Page.Content bg="white">
                        <Center>
                            <Icon
                                size={20}
                                mb={4}
                                color="main"
                                fontWeight="light"
                                as={<FA name="user-circle" />}
                            />
                            <Page.Title>{auth.user()?.display_name}</Page.Title>
                            {state.context?.profile?.created_at && (
                                <Text fontSize="sm">
                                    Joined on{' '}
                                    {dateFormat(
                                        state.context.profile!.created_at,
                                        'ddd, mmm dd yyyy'
                                    )}
                                </Text>
                            )}
                        </Center>

                        <Column flex={1} justifyContent="flex-end">
                            <Panel mb={10} p={4}>
                                <DataTable>
                                    <DataTable.Row>
                                        <DataTable.Cell>
                                            <Text fontWeight="semibold">
                                                Version
                                            </Text>
                                        </DataTable.Cell>
                                        <DataTable.Cell numeric>
                                            <Text>
                                                {(updates.manifest as any)
                                                    ?.version ??
                                                    Constants.manifest
                                                        ?.version ??
                                                    'development'}
                                            </Text>
                                        </DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>
                                            <Text fontWeight="semibold">
                                                Channel
                                            </Text>
                                        </DataTable.Cell>
                                        <DataTable.Cell numeric>
                                            <Text>
                                                {(updates.manifest as any)
                                                    ?.releaseChannel ??
                                                    Constants.manifest
                                                        ?.releaseChannel ??
                                                    'default'}
                                            </Text>
                                        </DataTable.Cell>
                                    </DataTable.Row>
                                    <DataTable.Row>
                                        <DataTable.Cell>
                                            <Text fontWeight="semibold">
                                                Release
                                            </Text>
                                        </DataTable.Cell>
                                        <DataTable.Cell numeric>
                                            <Text>
                                                {(updates.manifest as any)
                                                    ?.revisionId ?? 'NA'}
                                            </Text>
                                        </DataTable.Cell>
                                    </DataTable.Row>
                                </DataTable>
                            </Panel>
                        </Column>
                    </Page.Content>
                )}
            </Scrollbox>
        </Page>
    );
};

// releaseId
// const retrieveReleaseId = (id: string | undefined) => id;

type SkeletonProps = {
    loading: boolean;
    children: React.ReactNode;
};
const Skeleton = ({ loading, children }: SkeletonProps) => (
    <Center mx={4}>
        <SkeletonContent
            containerStyle={{ flex: 1 }}
            isLoading={loading}
            animationType="pulse"
            layout={[
                {
                    key: 'profile-icon',
                    width: 80,
                    height: 80,
                    marginBottom: 30,
                    borderRadius: 50,
                    alignSelf: 'center',
                },
                { key: 'line-1', width: 200, height: 30, marginBottom: 6 },
            ]}>
            {children}
        </SkeletonContent>
    </Center>
);

export { Profile };
