import React, { createContext, useContext } from 'react';

import { useNavigation } from '@react-navigation/native';
import { useMachine } from '@xstate/react';
import { useToast } from 'native-base';

import { graphClient, getApiError } from 'modules/api-client';
import {
    Contribution_Types_Enum,
    Group_Recurrencies_Enum,
    useAction_Create_GroupMutation,
} from 'modules/api-graphql/api-react-query';
import { auth } from 'modules/application';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';

import { stepsMachine } from './group-steps-master-machine';

const useMasterStepsMachine = () => {
    const [master, setMaster] = React.useState<{
        send: (event: string) => unknown;
    }>();
    const { navigate, goBack } = useNavigation<NavigationPagesStack>();
    const toast = useToast();

    const { mutate } = useAction_Create_GroupMutation(graphClient, {
        onError: (err: unknown) => {
            toast.show({
                title: 'Oups! An error has occured',
                description: getApiError(err),
                status: 'error',
            });
            master!.send('submission_failed');
        },
        onSuccess: () => {
            master!.send('submission_succeeded');
        },
    });

    const [state, send, service] = useMachine(stepsMachine, {
        actions: {
            go_to_step_name_screen: () => navigate('GroupStep01NameScreen'),

            go_to_step_participants_screen: () =>
                navigate('GroupStep02Contacts'),

            go_to_step_contributions_screen: () =>
                navigate('GroupStep03ContribScreen'),

            go_to_step_recurring_cycle_screen: () =>
                navigate('GroupStep04CycleScreen'),

            submit_values: (ctx: typeof stepsMachine['context']) => {
                const payload = {
                    creator_id: auth.user()?.id,
                    group_name: ctx.data.group_name!,
                    group_contribution_type:
                        ctx.data.contrib_type ??
                        Contribution_Types_Enum.AnyAmount,
                    group_contribution_amount: ctx.data.contrib_amount ?? 0,
                    group_recurrency_type:
                        ctx.data.recurrent_type ?? Group_Recurrencies_Enum.None,
                    group_recurrency_day: ctx.data.recurrent_day ?? 0,
                };

                mutate(payload);
            },

            exit: () => navigate('Groups'),

            go_back: goBack,
        },
    });

    React.useEffect(() => {
        setMaster({
            send: send as never,
        });
    }, []);

    return { state, send, service };
};

const context = createContext<ReturnType<typeof useMasterStepsMachine>>(
    {} as never
);

type GroupStepsProviderProps = {
    children: React.ReactNode;
};

export const GroupStepsMachineProvider = ({
    children,
}: GroupStepsProviderProps) => (
    <context.Provider value={useMasterStepsMachine()}>
        {children}
    </context.Provider>
);

export const useGroupStepsMachine = () => useContext(context);
