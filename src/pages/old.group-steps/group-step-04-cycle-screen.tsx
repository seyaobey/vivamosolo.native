import React from 'react';

import { useMachine } from '@xstate/react';
import { VStack, Text, HStack, Center } from 'native-base';
import { useController, useForm } from 'react-hook-form';
import { createMachine } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { ButtonAction } from 'components/button-action';
import { ButtonChange } from 'components/button-change';
import { Calendar, CalendarMode, LeadingDay } from 'components/calendar';
import { SelectField } from 'components/form/select-field/select-field';
import { Page } from 'components/page';
import { Panel, PanelBox } from 'components/panel';
import { recurrencyLabels } from 'components/payments';
import { Scrollbox } from 'components/scrollbox';
import { Swipeable } from 'components/swipeable';
import { Eventify } from 'helpers/types';
import { Group_Recurrencies_Enum } from 'modules/api-graphql/api-react-query';
import { Title } from 'modules/compounds/title';
import { hasReccurency } from 'pages/old.group-create/utils';

import { useGroupStepsMachine } from './group-steps-master-provider';

export const GroupStep04CycleScreen = () => {
    const { state, master, masterSend, send, editing, onSubmitChanges } =
        useGroupStepCycleScreen();

    return (
        <Page>
            <Scrollbox>
                <>
                    <Page.AppBar onGoBack={() => masterSend('go_back')} />
                    <Page.Content>
                        <VStack flex={1}>
                            <VStack>
                                <Title>When should</Title>
                                <Title>each member pay?</Title>
                                <Text color="gray.500" fontSize="md" mt={4}>
                                    You can set a{' '}
                                    <Text color="gray.600" fontWeight="bold">
                                        recurrent payment deadline
                                    </Text>{' '}
                                    by which every member of this group should
                                    have paid his contribution or allow each one
                                    to setup his own recurrent deadline
                                </Text>
                            </VStack>
                            <CycleField
                                payload={state.context.data}
                                changeValues={() => send('enter_editing')}
                            />
                            <ButtonAction
                                mt={20}
                                isLoading={master.matches('step_submit_values')}
                                onPress={() => masterSend('go_next_step')}>
                                Finish
                            </ButtonAction>
                        </VStack>
                    </Page.Content>
                </>
            </Scrollbox>
            <Swipeable
                isOpen={editing}
                onClosed={() => {
                    send('exit_editing');
                }}>
                <GroupCycleFieldSwiper
                    payload={state.context.data}
                    onSubmit={onSubmitChanges}
                />
            </Swipeable>
        </Page>
    );
};

type CyclePayload = {
    recurrent_type?: Group_Recurrencies_Enum;
    recurrent_day?: number;
};
type CycleContext = {
    data: CyclePayload;
};
type TypeState = { value: 'editing' };

type CycleFieldProps = {
    payload?: CyclePayload;
    changeValues: () => void;
};
const CycleField = ({ payload, changeValues }: CycleFieldProps) => {
    const isRecurrentWeekMonth = hasReccurency(
        payload?.recurrent_type ?? Group_Recurrencies_Enum.None,
        Group_Recurrencies_Enum.Daily
    );
    return (
        <Panel mt={8} py={4}>
            <HStack justifyContent="space-between" alignItems="center">
                <HStack>
                    <VStack space={1}>
                        <Text>
                            {
                                recurrencyLabels[
                                    payload?.recurrent_type ??
                                        Group_Recurrencies_Enum.None
                                ]
                            }
                        </Text>
                        {isRecurrentWeekMonth && (
                            <LeadingDay
                                activeDay={payload!.recurrent_day!}
                                mode={payload!.recurrent_type! as CalendarMode}
                            />
                        )}
                    </VStack>
                </HStack>
                <ButtonChange onPress={changeValues}>change</ButtonChange>
            </HStack>
        </Panel>
    );
};

type GroupCycleFieldSwiperProps = {
    payload?: CyclePayload;
    onSubmit: (values: CyclePayload) => void;
};
const GroupCycleFieldSwiper = ({
    payload,
    onSubmit,
}: GroupCycleFieldSwiperProps) => {
    const recurrency = payload?.recurrent_type ?? Group_Recurrencies_Enum.None;

    const { control, setValue, handleSubmit } = useForm<CyclePayload>();

    const {
        field: { value: recurrentType, onChange: onChangeRecurrentType },
    } = useController<CyclePayload, 'recurrent_type'>({
        control,
        name: 'recurrent_type',
        defaultValue: recurrency,
    });

    const {
        field: { value: recurentDay, onChange: onChangeRecurrentDay },
    } = useController<CyclePayload, 'recurrent_day'>({
        control,
        name: 'recurrent_day',
        defaultValue: payload?.recurrent_day ?? 0,
    });

    const displayCalendar = React.useMemo(
        () =>
            (
                [
                    Group_Recurrencies_Enum.Weekly,
                    Group_Recurrencies_Enum.Monthly,
                ] as Group_Recurrencies_Enum[]
            ).includes(recurrentType!),
        [recurrentType]
    );

    React.useEffect(() => {
        if (recurrentType !== Group_Recurrencies_Enum.Monthly) {
            if ((recurentDay ?? 0) > 7) {
                setValue('recurrent_day', 0);
            }
        }
    }, [recurrentType]);

    return (
        <VStack flex={1} space={10} px={4} pt={6}>
            <PanelBox>
                <Center>
                    <Text fontSize="lg">When should participants pay.</Text>
                </Center>
            </PanelBox>

            <PanelBox>
                <Text ml={4} fontSize="xs" color="gray.400">
                    Recurring payment cycle
                </Text>

                <SelectField<Group_Recurrencies_Enum>
                    items={recurrencyLabels}
                    value={recurrentType!}
                    onChange={onChangeRecurrentType}
                    dropdownIcon={<Text color="blue.500">change</Text>}
                />
            </PanelBox>

            {displayCalendar && (
                <PanelBox>
                    <Calendar
                        value={recurentDay!}
                        onChange={onChangeRecurrentDay}
                        mode={
                            recurrentType === Group_Recurrencies_Enum.Weekly
                                ? Group_Recurrencies_Enum.Weekly
                                : Group_Recurrencies_Enum.Monthly
                        }
                    />
                </PanelBox>
            )}

            <ButtonAction mb={10} mt={3} onPress={handleSubmit(onSubmit)}>
                Save
            </ButtonAction>
        </VStack>
    );
};

export const model = createModel({} as CycleContext, {
    events: {
        idle: () => ({}),
        enter_editing: () => ({}),
        exit_editing: () => ({}),
        update_cycle_values: (payload: CyclePayload) => ({ payload }),
    },
});

const cycleMachine = createMachine<
    typeof model,
    CycleContext,
    Eventify<typeof model.events, keyof typeof model.events>,
    TypeState & { context: CycleContext }
>(
    {
        context: model.initialContext,
        initial: 'ready',
        states: {
            ready: {
                on: {
                    enter_editing: {
                        target: 'editing',
                    },
                },
            },
            editing: {
                on: {
                    update_cycle_values: {
                        target: 'ready',
                        actions: 'update_cycle_values',
                    },
                    exit_editing: {
                        target: 'ready',
                    },
                },
            },
        },
    },
    {
        actions: {
            update_cycle_values: model.assign(
                {
                    data: (_, e) => e.payload,
                },
                'update_cycle_values'
            ) as never,
        },
    }
);

const useGroupStepCycleScreen = () => {
    const { state: master, send: masterSend } = useGroupStepsMachine();

    const [state, send] = useMachine(
        cycleMachine.withContext({
            ...cycleMachine.context,
            data: {
                recurrent_day: master.context.data?.recurrent_day,
                recurrent_type: master.context.data?.recurrent_type,
            },
        })
    );

    const onSubmitChanges = (payload: CyclePayload) => {
        send('update_cycle_values', { payload });
        masterSend('update_payload', {
            data: payload,
        });
    };

    return {
        state,
        master,
        send,
        masterSend,
        onSubmitChanges,
        editing: state.matches('editing'),
    };
};
