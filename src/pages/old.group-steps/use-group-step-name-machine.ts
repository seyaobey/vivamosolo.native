import React from 'react';

import { useMachine } from '@xstate/react';

import { buildFormMachine } from 'modules/form-machine';

import { useGroupStepsMachine } from './group-steps-master-provider';

export const useGroupStepName = () => {
    const { state, send: sendMaster } = useGroupStepsMachine();

    const [machine, send] = useMachine(
        buildFormMachine({
            model: {
                values: {
                    group_name: state.context.data?.group_name,
                },
            },
            validate: (values) => {
                const isValid = values?.group_name !== undefined;
                return {
                    group_name: isValid ? undefined : 'Group name is required',
                };
            },
        }),
        {
            services: {
                submit: () => {
                    sendMaster('go_next_step', {
                        data: machine.context.values,
                    });
                    return Promise.resolve();
                },
            },
        }
    );

    const onSubmit = () => {
        send('request_validation');
    };

    const onChange = (text: string) =>
        send({ type: 'read_input', values: { group_name: text } });

    const isInvalid = React.useMemo(
        () => machine.matches({ ready: 'input_is_invalid' }),
        [machine.value]
    );

    return {
        machine,
        isInvalid,
        onSubmit,
        onChange,
    };
};
