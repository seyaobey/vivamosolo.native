import React from 'react';

import { Center, VStack, Text, Fade, FormControl, Input } from 'native-base';
import { useController, useForm } from 'react-hook-form';
import NumberFormat from 'react-number-format';

import { ButtonAction } from 'components/button-action';
import { SelectField } from 'components/form/select-field/select-field';
import { PanelBox } from 'components/panel';
import { Contribution_Types_Enum } from 'modules/api-graphql/api-react-query';
import { contribTypes } from 'pages/old.group-create/create-types';

import {
    ContribContext,
    ContribPayload,
} from './use-group-step-contrib-machine';

type Payload = ContribContext['data'];
type Props = {
    initial?: Payload;
    onSubmitChanges: (payload: Payload) => void;
};

export const GroupStepContribScreenSwiper = ({
    initial,
    onSubmitChanges,
}: Props) => {
    const {
        errors,
        contribType,
        contribAmount,
        setContribNum,
        onContribTypeChange,
        onContribAmountChange,
        requestSubmitChanges,
    } = useGroupStepContribScreenSwiper({ initial, onSubmitChanges });

    return (
        <VStack flex={1} space={10} px={4} pt={6}>
            <PanelBox>
                <Center>
                    <Text fontSize="lg">
                        <Text color="gray.500" fontWeight="700">
                            How much
                        </Text>{' '}
                        participants should pay.
                    </Text>
                </Center>
            </PanelBox>

            <PanelBox>
                <Text ml={4} fontSize="xs" color="gray.400">
                    Select
                </Text>
                <SelectField<Contribution_Types_Enum>
                    items={contribTypes}
                    value={contribType!}
                    onChange={onContribTypeChange}
                    dropdownIcon={<Text color="blue.500">change</Text>}
                />
            </PanelBox>

            <Fade
                in={[
                    Contribution_Types_Enum.MinimumAmount,
                    Contribution_Types_Enum.FixAmount,
                ].includes(contribType!)}>
                <PanelBox px={7} mb={8}>
                    <FormControl
                        isRequired
                        isInvalid={!!errors?.contrib_amount?.message}>
                        <FormControl.Label>Enter amount</FormControl.Label>
                        <NumberFormat
                            value={contribAmount}
                            displayType="text"
                            thousandSeparator
                            prefix="$"
                            allowNegative={false}
                            onValueChange={({ floatValue: value }) => {
                                setContribNum(value);
                            }}
                            renderText={React.useCallback(
                                InputField({
                                    onChangeText: onContribAmountChange,
                                }),
                                []
                            )}
                        />
                        <FormControl.ErrorMessage>
                            {errors?.contrib_amount?.message}
                        </FormControl.ErrorMessage>
                    </FormControl>
                </PanelBox>
            </Fade>

            <ButtonAction mb={6} onPress={requestSubmitChanges}>
                Save
            </ButtonAction>
        </VStack>
    );
};

type InputFieldProps = {
    onChangeText: (text: string) => void;
};
const InputField =
    ({ onChangeText }: InputFieldProps) =>
    (value: string) =>
        (
            <Input
                variant="filled"
                textAlign="right"
                value={value}
                color="gray.500"
                keyboardType="decimal-pad"
                onChangeText={onChangeText}
                placeholder="$0"
                size="xl"
            />
        );

type GroupStepContribScreenSwiperProps = {
    initial?: Payload;
    onSubmitChanges: (payload: Payload) => void;
};
const useGroupStepContribScreenSwiper = ({
    initial,
    onSubmitChanges,
}: GroupStepContribScreenSwiperProps) => {
    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm<Payload>();

    const [contribNum, setContribNum] = React.useState<number>();

    const {
        field: { value: contribAmount, onChange: onContribAmountChange },
    } = useController<Payload, 'contrib_amount'>({
        name: 'contrib_amount',
        defaultValue: initial?.contrib_amount ?? 0,
        rules: {
            required: 'Contribution amount is required',
            min: {
                value: 1,
                message: 'Contribution amount is not valid',
            },
        },
        control,
    });

    const {
        field: { value: contribType, onChange: onContribTypeChange },
    } = useController<Payload, 'contrib_type'>({
        name: 'contrib_type',
        defaultValue:
            initial?.contrib_type || Contribution_Types_Enum.AnyAmount,
        control,
    });

    const handleSubmitChange = (payload: ContribPayload) =>
        onSubmitChanges({
            contrib_amount: contribNum,
            contrib_type: payload.contrib_type,
        });

    const requestSubmitChanges = handleSubmit(handleSubmitChange);

    return {
        errors,
        control,
        contribAmount,
        contribType,
        setContribNum,
        onContribAmountChange,
        onContribTypeChange,
        requestSubmitChanges,
    };
};
