import React from 'react';

import { VStack, Text, HStack } from 'native-base';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NumberFormat from 'react-number-format';

import { ButtonAction } from 'components/button-action';
import { ButtonChange } from 'components/button-change';
import { Page } from 'components/page';
import { Panel } from 'components/panel';
import { Scrollbox } from 'components/scrollbox';
import { Swipeable } from 'components/swipeable';
import { Contribution_Types_Enum } from 'modules/api-graphql/api-react-query';
import { Title } from 'modules/compounds/title';
import { contribTypes } from 'pages/old.group-create/create-types';

import { GroupStepContribScreenSwiper } from './group-step-03-contrib-screen-swiper';
import {
    ContribPayload,
    useStepContribMachine,
} from './use-group-step-contrib-machine';

export const GroupStep03ContribScreen = () => {
    const { masterSend, state, send, nextStep, onSubmitChanges } =
        useStepContribMachine();

    return (
        <Page>
            <>
                <Scrollbox>
                    <>
                        <Page.AppBar onGoBack={() => masterSend('go_back')} />
                        <Page.Content>
                            <VStack flex={1}>
                                <VStack space={hp('0.5%')}>
                                    <VStack>
                                        <Title>How much should</Title>
                                        <Title>each member pay?</Title>
                                        <Text
                                            color="gray.500"
                                            fontSize="md"
                                            mt={4}>
                                            You can set a{' '}
                                            <Text
                                                color="gray.600"
                                                fontWeight="bold">
                                                minimum or fix amount
                                            </Text>{' '}
                                            for all members of this group or
                                            allow each one to{' '}
                                            <Text
                                                color="gray.600"
                                                fontWeight="bold">
                                                freely
                                            </Text>{' '}
                                            set his/her own contribution.
                                        </Text>
                                    </VStack>
                                    <ContribField
                                        payload={state.context.data}
                                        openSwiper={() => send('enter_editing')}
                                    />
                                </VStack>
                                <ButtonAction mt={20} onPress={nextStep}>
                                    Continue
                                </ButtonAction>
                            </VStack>
                        </Page.Content>
                    </>
                </Scrollbox>

                <Swipeable
                    isOpen={state.matches('editing')}
                    onClosed={() => send('exit_editing')}>
                    <GroupStepContribScreenSwiper
                        initial={state.context.data}
                        onSubmitChanges={onSubmitChanges}
                    />
                </Swipeable>
            </>
        </Page>
    );
};

type ContribFieldProps = {
    payload?: ContribPayload;
    openSwiper: () => void;
};
const ContribField = ({ payload, openSwiper }: ContribFieldProps) => {
    const type = payload?.contrib_type ?? Contribution_Types_Enum.AnyAmount;
    const displayNumField = type !== Contribution_Types_Enum.AnyAmount;

    return (
        <Panel mt={8} py={4}>
            <HStack justifyContent="space-between" alignItems="center">
                <VStack space={2}>
                    <Text>{contribTypes[type]}</Text>
                    {displayNumField && (
                        <NumberFormat
                            value={payload?.contrib_amount}
                            displayType="text"
                            thousandSeparator
                            decimalSeparator="."
                            decimalScale={2}
                            fixedDecimalScale
                            prefix="$"
                            renderText={(value) => <Text>{value}</Text>}
                        />
                    )}
                </VStack>
                <ButtonChange onPress={openSwiper}>change</ButtonChange>
            </HStack>
        </Panel>
    );
};
