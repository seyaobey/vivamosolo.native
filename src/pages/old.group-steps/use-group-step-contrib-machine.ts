import { useMachine } from '@xstate/react';
import { createMachine } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { Eventify } from 'helpers/types';

import { StepsPayload } from './group-steps-master-machine';
import { useGroupStepsMachine } from './group-steps-master-provider';

export type ContribPayload = Pick<
    StepsPayload,
    'contrib_amount' | 'contrib_type'
>;

export type ContribContext = {
    data: ContribPayload;
};
type TypeState = { value: 'editing' };

export const model = createModel({} as ContribContext, {
    events: {
        idle: () => ({}),
        enter_editing: () => ({}),
        exit_editing: () => ({}),
        finish_editing: (payload: ContribPayload) => ({ payload }),
    },
});

const contribMachine = createMachine<
    typeof model,
    ContribContext,
    Eventify<typeof model.events, keyof typeof model.events>,
    TypeState & { context: ContribContext }
>(
    {
        context: model.initialContext,
        initial: 'ready',
        states: {
            ready: {
                on: {
                    enter_editing: {
                        target: 'editing',
                    },
                },
            },
            editing: {
                on: {
                    finish_editing: {
                        target: 'ready',
                        actions: 'finish_editing',
                    },
                    exit_editing: {
                        target: 'ready',
                    },
                },
            },
        },
    },
    {
        actions: {
            finish_editing: model.assign(
                {
                    data: (ctx, e) => e.payload,
                },
                'finish_editing'
            ) as never,
        },
    }
);

export const useStepContribMachine = () => {
    const { state: master, send: masterSend } = useGroupStepsMachine();

    const [state, send] = useMachine(
        contribMachine.withContext({
            ...contribMachine.context,
            data: {
                contrib_amount: master.context.data?.contrib_amount,
                contrib_type: master.context.data?.contrib_type,
            },
        })
    );

    const onSubmitChanges = (payload: ContribPayload) => {
        send('finish_editing', { payload });
    };

    const nextStep = () => {
        masterSend('go_next_step', { data: state.context.data });
    };

    return {
        state,
        send,
        nextStep,
        masterSend,
        onSubmitChanges,
    };
};
