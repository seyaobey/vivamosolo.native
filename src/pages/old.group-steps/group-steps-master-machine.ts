import { createMachine } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { Eventify } from 'helpers/types';
import {
    Contribution_Types_Enum,
    Group_Recurrencies_Enum,
} from 'modules/api-graphql/api-react-query';

export type StepsPayload = {
    group_name?: string;
    contrib_amount?: number;
    contrib_type?: Contribution_Types_Enum;
    recurrent_type?: Group_Recurrencies_Enum;
    recurrent_day?: number;
};

type Context = {
    data: StepsPayload;
};

type TypeState = { value: 'step_submit_values' };

const model = createModel(
    {
        data: {
            contrib_amount: 0,
            contrib_type: Contribution_Types_Enum.AnyAmount,
            recurrent_day: 0,
            recurrent_type: Group_Recurrencies_Enum.None,
        } as StepsPayload,
    },
    {
        events: {
            start: () => ({}),
            go_back: () => ({}),
            go_next_step: (data: StepsPayload) => ({ data }),
            update_payload: (data: Partial<StepsPayload>) => ({ data }),
            submission_failed: () => ({}),
            submission_succeeded: () => ({}),
        },
    }
);

export const stepsMachine = createMachine<
    typeof model,
    Context,
    Eventify<typeof model.events, keyof typeof model.events>,
    TypeState & { context: Context }
>(
    {
        id: 'steps-machine',
        context: model.initialContext,
        initial: 'ready',
        states: {
            ready: {
                entry: 'reset_context',
                on: {
                    start: {
                        target: 'step_name',
                    },
                },
            },

            step_name: {
                entry: 'go_to_step_name_screen', // call navigator
                on: {
                    go_back: {
                        target: 'ready',
                        actions: ['reset_context', 'go_back'],
                    },

                    go_next_step: {
                        target: 'step_participants',
                        actions: 'update_context',
                    },
                },
            },

            step_participants: {
                entry: 'go_to_step_participants_screen',
                on: {
                    go_back: {
                        target: 'step_name',
                        actions: 'go_back',
                    },

                    go_next_step: {
                        target: 'step_contributions',
                    },
                },
            },

            step_contributions: {
                entry: 'go_to_step_contributions_screen',
                on: {
                    go_back: {
                        target: 'step_participants',
                        actions: 'go_back',
                    },
                    update_payload: {
                        actions: 'update_context',
                    },
                    go_next_step: {
                        target: 'step_recurring_cycle',
                        actions: 'update_context',
                    },
                },
            },

            step_recurring_cycle: {
                entry: 'go_to_step_recurring_cycle_screen',
                on: {
                    go_back: {
                        target: 'step_contributions',
                        actions: 'go_back',
                    },
                    update_payload: {
                        actions: 'update_context',
                    },
                    go_next_step: {
                        target: 'step_submit_values',
                    },
                },
            },

            step_submit_values: {
                entry: 'submit_values',
                on: {
                    submission_failed: {
                        target: 'step_recurring_cycle',
                    },
                    submission_succeeded: {
                        target: 'step_exit',
                    },
                },
            },

            step_exit: {
                type: 'final',
                entry: ['reset_context', 'exit'],
            },
        },
    },
    {
        actions: {
            update_context: model.assign(
                {
                    data: (ctx, e) => ({
                        ...ctx.data,
                        ...e.data,
                    }),
                },
                'go_next_step'
            ) as never,

            reset_context: model.assign({
                data: () => ({}),
            }) as never,
        },
    }
);
