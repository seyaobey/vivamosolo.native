export type GroupStepNameStates = {
    states: {
        ready: {
            states: {
                input_reading: {};
                input_validation: {};
                invalid_input: {};
            };
        };
        cancel: {};
        done: {};
    };
};

export type GroupStepNameEvents =
    | { type: 'goBack' }
    | { type: 'readInput'; value: string }
    | { type: 'validateInput' }
    | { type: 'inputIsValid' }
    | { type: 'inputIsInvalid' };

export type GroupStepNameContext = {
    group_name: string;
};
