import React from 'react';

import { FormControl, Input, VStack, Column } from 'native-base';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { ButtonAction } from 'components/button-action';
import { Page } from 'components/page';
import { Scrollbox } from 'components/scrollbox';
import { Title } from 'modules/compounds/title';

import { useGroupStepsMachine } from './group-steps-master-provider';
import { useGroupStepName } from './use-group-step-name-machine';

export const GroupStep01NameScreen = () => {
    const { send } = useGroupStepsMachine();
    return (
        <Page>
            <Scrollbox>
                <>
                    <Page.AppBar onGoBack={() => send('go_back')} />
                    <Page.Content>
                        <Column flex={1} justifyContent="space-between">
                            <VStack space={hp('0.5%')}>
                                <VStack>
                                    <Title>Enter the</Title>
                                    <Title>new group name</Title>
                                </VStack>

                                <FormValues />
                            </VStack>
                        </Column>
                    </Page.Content>
                </>
            </Scrollbox>
        </Page>
    );
};

const FormValues = React.memo(() => {
    const { machine, onChange, onSubmit, isInvalid } = useGroupStepName();

    return (
        <VStack>
            <FormControl mt={6} isRequired isInvalid={isInvalid}>
                <FormControl.Label>group name</FormControl.Label>
                <Input
                    value={machine.context?.values?.group_name ?? ''}
                    variant="filled"
                    isInvalid={isInvalid}
                    placeholder="Name your group"
                    onChangeText={onChange}
                />
                <FormControl.ErrorMessage>
                    {machine.context?.errors?.group_name}
                </FormControl.ErrorMessage>
            </FormControl>

            <ButtonAction mt={20} onPress={onSubmit}>
                Continue
            </ButtonAction>
        </VStack>
    );
});
