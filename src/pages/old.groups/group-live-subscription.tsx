/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect } from 'react';

import { pagingStore } from 'components/pagination';
import { useGroupsSubscription } from 'modules/api-graphql/api-apollo';
import { auth } from 'modules/application';
import { useApplicationMachine } from 'modules/application-machine.old';

export const GroupLiveSubscription = () => {
    const [_, send] = useApplicationMachine();
    const { data = { groups: [] }, loading } = useGroupsSubscription({
        variables: { user_id: auth.user()?.id },
    });

    const { groups } = data;

    useEffect(() => {
        if (!loading) {
            pagingStore.init(groups.length);
            if (groups.length) {
                send('update_groups', { groups });
            } else {
                send('no_group_found');
            }
        }
    }, [groups, loading]);

    return <></>;
};
