import React from 'react';

import { Feather } from '@expo/vector-icons';
import { view } from '@risingstack/react-easy-state';
import dateFormat from 'dateformat';
import { Icon, Row, VStack, Text, Column, Progress } from 'native-base';
import NumberFormat from 'react-number-format';

import { CurrencyText } from 'components/currency-text';
import { Panel } from 'components/panel';
import { recurrencyLabels } from 'components/payments';
import { TextStack } from 'components/text-stack';
import {
    GroupDtoFragment,
    Group_Recurrencies_Enum,
} from 'modules/api-graphql/api-react-query';

export type IGroupPageCardProps = {
    group: GroupDtoFragment;
};

const GroupPageCard: React.FC<IGroupPageCardProps> = view(({ group }) => (
    <VStack space={6}>
        <Panel bg="main" shadow={3}>
            <Row justifyContent="space-between" alignItems="center">
                <WhiteText fontSize="2xl">{group?.group_name}</WhiteText>
                <Icon size={5} color="white" as={<Feather name="settings" />} />
            </Row>

            <Column mt={3}>
                <Row justifyContent="space-between">
                    <TextStack title="balance">
                        <CurrencyText
                            value={group.group_balance}
                            render={(text) => (
                                <WhiteText fontSize="xl">{text}</WhiteText>
                            )}
                        />
                    </TextStack>

                    <TextStack title="members" alignItems="center">
                        <Row justifyContent="center">
                            <WhiteText fontSize="xl" mr={1}>
                                {group.members.length}
                            </WhiteText>
                            <Icon
                                size={5}
                                color="white"
                                as={
                                    <Feather
                                        name={
                                            group.members.length > 1
                                                ? 'users'
                                                : 'user'
                                        }
                                    />
                                }
                            />
                        </Row>
                    </TextStack>

                    <TextStack title="created" alignItems="flex-end">
                        <WhiteText>
                            {dateFormat(
                                new Date(group.created_at),
                                'ddd, mmm dd'
                            ).toLowerCase()}
                        </WhiteText>
                    </TextStack>
                </Row>
            </Column>
        </Panel>

        {group.group_recurrency_type !== Group_Recurrencies_Enum.None && (
            <GroupProgress group={group} />
        )}
    </VStack>
));

const WhiteText = ({
    color = 'white',
    children,
    ...rest
}: React.ComponentProps<typeof Text>) => (
    <Text color={color} {...rest}>
        {children}
    </Text>
);

type GroupProgressProps = {
    group: GroupDtoFragment;
};
export const GroupProgress = ({ group }: GroupProgressProps) => {
    const target =
        group.members.length * Number(group.group_contribution_amount);
    const progressValue =
        group.periods[0].period_progression >= target
            ? 1
            : group.periods[0].period_progression / target;

    return (
        <Panel>
            <Row justifyContent="space-between">
                <TextStack title="collected">
                    <CurrencyText
                        value={group.periods[0].period_progression}
                        render={(text) => <Text color="green.600">{text}</Text>}
                    />
                </TextStack>

                <TextStack
                    title={`${recurrencyLabels[
                        group.group_recurrency_type
                    ].toLowerCase()} target`}
                    alignItems="center">
                    <CurrencyText
                        value={target}
                        render={(text) => (
                            <Text color="lightBlue.800">{text}</Text>
                        )}
                    />
                </TextStack>

                {group.group_recurrency_type !==
                    Group_Recurrencies_Enum.None && (
                    <TextStack title="deadline" alignItems="flex-end">
                        <Text color="lightBlue.800">
                            {dateFormat(
                                new Date(group.periods[0].period_completed_at),
                                'ddd, mmm dd'
                            ).toLowerCase()}
                        </Text>
                    </TextStack>
                )}
            </Row>

            <VStack space={1} mt={2}>
                <NumberFormat
                    value={progressValue * 100}
                    displayType="text"
                    thousandSeparator
                    decimalSeparator="."
                    decimalScale={2}
                    fixedDecimalScale
                    suffix="%"
                    renderText={(text) => (
                        <Text color="gray.400" fontSize="xs" textAlign="center">
                            progress: {text}
                        </Text>
                    )}
                />
                <Progress value={progressValue * 100} width="100%" />
            </VStack>
        </Panel>
    );
};

export { GroupPageCard };
