import React, { useRef, useEffect } from 'react';

import { view, store } from '@risingstack/react-easy-state';
import { Box } from 'native-base';
import {
    ScrollView,
    Dimensions,
    StyleSheet,
    NativeSyntheticEvent,
    NativeScrollEvent,
} from 'react-native';

import { pagingStore } from 'components/pagination';

const PAGEWIDTH_OFFSET = 33;
const pageWidth = Dimensions.get('window').width - PAGEWIDTH_OFFSET;

type PageGroupsViewerProps = {
    children: React.ReactChild | React.ReactChild[];
};

const Viewer = view(({ children }: PageGroupsViewerProps) => {
    const viewer = store({
        currentPos: 0,
        nextPos: 0,
    });

    const ref = useRef<ScrollView>(null);
    const scrollToIndex = (index: number) => {
        const next = Number(index) * pageWidth;
        ref.current?.scrollTo({
            x: next,
            y: 0,
            animated: true,
        });
    };

    const handleOnScroll = (e: NativeSyntheticEvent<NativeScrollEvent>) => {
        if (!pagingStore.fromUser) {
            viewer.nextPos = e.nativeEvent.contentOffset.x;

            if (Math.abs(viewer.nextPos - viewer.currentPos) > 100) {
                const isRight = viewer.nextPos >= viewer.currentPos;

                const nextIndex = isRight
                    ? Math.ceil(viewer.nextPos / pageWidth)
                    : Math.floor(viewer.nextPos / pageWidth);

                viewer.currentPos = viewer.nextPos;

                const diff = Math.abs(pagingStore.index - nextIndex);

                if (diff !== 0 && pagingStore.inRange(nextIndex)) {
                    pagingStore.updateIndex({
                        index: nextIndex,
                        fromUser: false,
                    });
                }
            }
        }
    };

    useEffect(() => {
        if (pagingStore.fromUser) {
            scrollToIndex(pagingStore.index);
        }
    }, [pagingStore.index, pagingStore.fromUser]);

    return (
        <ScrollView
            ref={ref}
            horizontal
            pagingEnabled
            nestedScrollEnabled
            scrollEventThrottle={16}
            showsHorizontalScrollIndicator={false}
            alwaysBounceHorizontal
            onScroll={handleOnScroll}
            onMomentumScrollEnd={() => {
                if (pagingStore.fromUser) {
                    pagingStore.fromUser = false;
                }
            }}
            style={styles.viewer}>
            {children}
        </ScrollView>
    );
});
export const GroupsViewer = (props: PageGroupsViewerProps) => (
    <Viewer {...props} />
);

type PageViewerContentProps = {
    children: React.ReactNode;
};
const PageViewerItem = ({ children }: PageViewerContentProps) => (
    <Box flex={1} py={2} px={2} w={pageWidth}>
        {children}
    </Box>
);

GroupsViewer.Page = PageViewerItem;

const styles = StyleSheet.create({
    viewer: {
        flex: 1,
    },
});
