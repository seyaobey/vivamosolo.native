import React, { useState } from 'react';

import { useNavigation } from '@react-navigation/native';
import { view } from '@risingstack/react-easy-state';
import { VStack, Box, HStack, Pressable } from 'native-base';

import { Panel } from 'components/panel';
import { Title } from 'modules/compounds/title';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';
import { useGroupStepsMachine } from 'pages/old.group-steps/group-steps-master-provider';

import SvgJoinGroup from '../../../assets/svgs/business-and-finance.svg';
import SvgCreateGroup from '../../../assets/svgs/crowdfunding.svg';
import { GroupLiveSubscription } from './group-live-subscription';

export const GroupsWelcome = view(() => {
    const { navigate } = useNavigation<NavigationPagesStack>();
    const { send, service } = useGroupStepsMachine();

    return (
        <VStack>
            <Box>
                <Title>Welcome</Title>
                <Title>to Viva Mosolo</Title>
            </Box>

            <GroupLiveSubscription />

            <VStack space={4}>
                <PressablePanel
                    mt={10}
                    h={24}
                    justifyContent="center"
                    onPress={() => {
                        service.start();
                        send('start');
                    }}>
                    <HStack alignItems="center">
                        <SvgCreateGroup width={50} height={50} />
                        <Title ml={6} flex={1} fontSize="lg">
                            Create a new group, invite friends and family and
                            start saving now.
                        </Title>
                    </HStack>
                </PressablePanel>

                <PressablePanel
                    mt={10}
                    p={6}
                    onPress={() => navigate('GroupJoin')}>
                    <HStack alignItems="center">
                        <SvgJoinGroup width={50} height={50} />
                        <Title ml={6} flex={1} fontSize="lg">
                            Join an existing group and start your journey from
                            here.
                        </Title>
                    </HStack>
                </PressablePanel>
            </VStack>
        </VStack>
    );
});

const PressablePanel = ({
    children,
    onPress,
    bg,
    pressBg = 'gray.200',
    ...rest
}: React.ComponentProps<typeof Panel> & {
    pressBg?: string;
    onPress: () => void;
}) => {
    const [pressing, setPressing] = useState(false);

    const handlePressIn = () => setPressing(true);
    const handlePressOut = () => setPressing(false);

    return (
        <Panel {...rest} bg={pressing ? pressBg : bg}>
            <Pressable
                onPress={onPress}
                onPressIn={handlePressIn}
                onPressOut={handlePressOut}>
                {children}
            </Pressable>
        </Panel>
    );
};
