import React, { useState } from 'react';

import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { FAB } from 'react-native-paper';

// import { defaultTheme } from 'modules/application-theme/default-theme';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';
import { useGroupStepsMachine } from 'pages/old.group-steps/group-steps-master-provider';
// import { createStore } from 'pages/group-create/create-store';

type PageGroupFabProps = {
    children: React.ReactNode;
};

export const GroupsFAB = ({ children }: PageGroupFabProps) => {
    const [state, setState] = useState({ open: false });
    const { send, service } = useGroupStepsMachine();

    const { navigate } = useNavigation<NavigationPagesStack>();
    const onStateChange = ({ open }: { open: boolean }) => setState({ open });
    const [t] = useTranslation();
    const { open } = state;

    return (
        <>
            {children}
            <FAB.Group
                visible
                open={open}
                color="white"
                icon={open ? 'close' : 'plus'}
                fabStyle={{
                    // backgroundColor: defaultTheme.colors.amber,
                    opacity: 0.8,
                }}
                actions={[
                    {
                        icon: 'account-multiple-plus',
                        label: t('create-group'),
                        onPress: () => {
                            service.start();
                            send('start');
                        },
                    },
                    {
                        icon: 'account-plus',
                        label: 'Join group',
                        onPress: () => {
                            navigate('GroupJoin');
                        },
                    },
                ]}
                onStateChange={onStateChange}
            />
        </>
    );
};
