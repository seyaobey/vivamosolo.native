import React from 'react';

import { view } from '@risingstack/react-easy-state';
import { Box, Row, Factory } from 'native-base';
import { useTranslation } from 'react-i18next';
import { ScrollView } from 'react-native';

import { Page } from 'components/page';
import { Pagination } from 'components/pagination';
import { useApplicationMachine } from 'modules/application-machine.old';
import { Title } from 'modules/compounds/title';

import { GroupLiveSubscription } from './group-live-subscription';
import { GroupsPages } from './group-pages';
import { GroupsFAB } from './groups-fab';

const ScrollViewBox = Factory(ScrollView);

export const Groups = view(() => {
    const [t] = useTranslation();
    const [machine] = useApplicationMachine();
    const displayHeader = machine.matches('ready');

    return (
        <GroupsFAB>
            <Page>
                <ScrollViewBox
                    flex={1}
                    alwaysBounceVertical
                    showsVerticalScrollIndicator={false}
                    horizontal={false}
                    nestedScrollEnabled
                    contentContainerStyle={{
                        flexGrow: 1,
                    }}>
                    <Page.Content>
                        <Row
                            mt={4}
                            pl={2}
                            justifyContent="space-between"
                            alignItems="center">
                            {displayHeader && (
                                <>
                                    <Title>{t('my-pools')}</Title>
                                    <Pagination />
                                </>
                            )}
                        </Row>

                        <Box mt={2} flex={1}>
                            <GroupsPages />
                        </Box>
                    </Page.Content>
                </ScrollViewBox>
                <GroupLiveSubscription />
            </Page>
        </GroupsFAB>
    );
});
