/* eslint-disable no-mixed-spaces-and-tabs */
import React from 'react';

import { Text, Row, VStack } from 'native-base';
import { useTranslation } from 'react-i18next';

import { Panel } from 'components/panel';
import { GroupDtoFragment } from 'modules/api-graphql/api-react-query';

import { GroupMembers } from './group-members';
import { GroupPageCard } from './group-page-card';
import { PaymentButtons } from './group-payment-buttons';

type GroupPageProps = {
    group: GroupDtoFragment;
};
export const GroupPage = ({ group }: GroupPageProps) => {
    const [t] = useTranslation();

    return (
        <VStack space={6}>
            <GroupPageCard group={group} />

            <PaymentButtons />

            <VStack space={2}>
                <Row justifyContent="space-between">
                    <Text fontSize="sm">
                        {t('members-count', {
                            count: group.members.length,
                        })}
                    </Text>
                    <Text fontSize="sm" color="main">
                        Add member
                    </Text>
                </Row>
                <Panel>
                    <GroupMembers group={group} />
                </Panel>
            </VStack>
        </VStack>
    );
};
