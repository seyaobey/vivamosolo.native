import React from 'react';

import { useMachine } from '@xstate/react';
import { createMachine } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { Eventify } from 'helpers/types';
import { GroupDtoFragment } from 'modules/api-graphql/api-react-query';

type Context = {
    groups: GroupDtoFragment[];
};

type Events = Eventify<typeof model.events, keyof typeof model.events>;

type TypeState = { value: 'ready' };

const model = createModel(
    {
        groups: [] as GroupDtoFragment[],
    },
    {
        events: {
            started: () => ({}),
        },
    }
);

export const groupsMachine = createMachine<
    typeof model,
    Context,
    Events,
    TypeState & { context: Context }
>({
    context: model.initialContext,
    initial: 'starting',
    states: {
        starting: {
            on: {
                started: {
                    target: 'ready',
                },
            },
        },
        ready: {
            initial: 'idle',
            states: {
                idle: {},
            },
        },
    },
});

const useBaseGroupsMachine = () => {
    const [state, send] = useMachine(groupsMachine);

    return {
        state,
        send,
        groups: state.context.groups,
    };
};

export const context = React.createContext<
    ReturnType<typeof useBaseGroupsMachine>
>({} as never);

type GroupsMachineProviderProps = {
    children: React.ReactNode;
};
export const GroupsMachineProvider = ({
    children,
}: GroupsMachineProviderProps) => <>{children}</>;
