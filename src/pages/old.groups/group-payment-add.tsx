import React from 'react';

import { Text, Button, Modal, useDisclose } from 'native-base';

export const PaymentButton = ({
    ...rest
}: Omit<React.ComponentProps<typeof Button>, 'onPress'>) => {
    const { isOpen, onToggle } = useDisclose();
    return (
        <>
            <PaymentModal isOpen={isOpen} onClose={onToggle} />
            <Button
                flex={1}
                shadow={4}
                bg="blue.100"
                _pressed={{
                    bg: 'blue.300',
                }}
                _text={{
                    color: 'blue.700',
                }}
                onPress={onToggle}
                {...rest}
            />
        </>
    );
};

type PaymentModalProps = {
    isOpen: boolean;
    onClose: () => void;
};
const PaymentModal = ({ isOpen, onClose }: PaymentModalProps) => (
    <Modal isOpen={isOpen} onClose={onClose} size="full" avoidKeyboard>
        <Modal.Content h={600} style={{ marginBottom: 0, paddingBottom: 0 }}>
            <Modal.Header _text={{ fontFamily: 'body' }}>Payment</Modal.Header>
            <Modal.Body>
                <Text>Payment content</Text>
            </Modal.Body>
        </Modal.Content>
    </Modal>
);
