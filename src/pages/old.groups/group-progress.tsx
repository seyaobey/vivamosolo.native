import React, { memo, forwardRef } from 'react';

import { Box, Text } from 'native-base';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const ProgressFn = () => (
    <Box bg="blue.500">
        <Text>progress</Text>
    </Box>
);

export const Progress = memo(forwardRef(ProgressFn));
