/* eslint-disable react/no-unused-prop-types */
import React from 'react';

import { Feather, AntDesign } from '@expo/vector-icons';
import dateFormat from 'dateformat';
import {
    Avatar,
    Box,
    Row,
    Text,
    Icon,
    VStack,
    HStack,
    Divider,
    Circle,
} from 'native-base';
import { FlatList } from 'react-native';

import { CurrencyText } from 'components/currency-text';
import {
    GroupDtoFragment,
    MemberDtoFragment,
} from 'modules/api-graphql/api-react-query';

type MemberItemProps = {
    item: MemberDtoFragment;
};

type GroupMembersProps = {
    group: GroupDtoFragment;
};
export const GroupMembers = ({ group }: GroupMembersProps) => (
    <FlatList
        data={group.members ?? []}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        ItemSeparatorComponent={() => <Divider my={4} />}
        keyExtractor={(item) => `${item.id}`}
        scrollEnabled={false}
        renderItem={({ item }: MemberItemProps) => (
            <PageGroupMember group={group} member={item} />
        )}
    />
);
type PageGroupMemberProps = GroupMembersProps & {
    member: MemberDtoFragment;
};
const PageGroupMember = ({ group, member }: PageGroupMemberProps) => (
    <VStack mr={2}>
        <Row flex={1} justifyContent="space-between" alignItems="flex-start">
            <HStack>
                <Box>
                    {member.user.avatar_url ? (
                        <Avatar
                            mr={3}
                            source={{
                                uri: member.user.avatar_url!,
                            }}>
                            {member.user.display_name}
                        </Avatar>
                    ) : (
                        <Circle mr={3} p={2} bg="warmGray.300">
                            <Icon
                                size={8}
                                color="white"
                                as={<Feather name="user" />}
                            />
                        </Circle>
                    )}
                </Box>

                <VStack space={1}>
                    <Text fontSize="lg" color="gray.600">
                        {member.user.display_name!}
                    </Text>
                    <ActivityDate group={group} member={member} />
                </VStack>
            </HStack>
            <ActivityValue group={group} member={member} />
        </Row>
    </VStack>
);

type ActivityProps = GroupMembersProps & {
    member: MemberDtoFragment;
};
const ActivityDate = ({ group, member }: ActivityProps) => {
    const latest = group.payments?.[0]?.created_at ?? member.created_at;
    return (
        <HStack alignItems="center">
            <Icon
                mr={1}
                size={3}
                color="gray.400"
                as={<Feather name="clock" />}
            />
            <Text fontSize="xs">
                {dateFormat(latest, 'ddd, mmm dd, HH:MM')}
            </Text>
        </HStack>
    );
};

const ActivityValue = ({ group }: ActivityProps) => {
    const payment = group.payments?.[0]?.payment_amount;
    return payment ? (
        <HStack alignItems="center">
            <Icon name="arrowup" size={4} color="green.600" as={AntDesign} />
            <CurrencyText
                value={payment}
                render={(text) => (
                    <Text color="green.500" fontSize="lg">
                        {text}
                    </Text>
                )}
            />
        </HStack>
    ) : (
        <></>
    );
};
