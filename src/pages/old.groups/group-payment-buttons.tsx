/* eslint-disable no-mixed-spaces-and-tabs */
import React from 'react';

import { Entypo, Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { view, store } from '@risingstack/react-easy-state';
import { Button, Icon, Row, Box } from 'native-base';
import { Modalize } from 'react-native-modalize';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { Title } from 'modules/compounds/title';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';

enum PaymentOperation {
    none,
    payment,
    withdraw,
}

const PaymentButton = ({ ...rest }: React.ComponentProps<typeof Button>) => (
    <>
        <Button
            flex={1}
            shadow={1}
            bg="blue.100"
            _pressed={{
                bg: 'blue.300',
            }}
            _text={{
                color: 'blue.700',
            }}
            {...rest}
        />
    </>
);

export const paymentButtonsStore = store({
    operation: PaymentOperation.none,
});

export const PaymentButtons = view(() => {
    const { navigate } = useNavigation<NavigationPagesStack>();
    return (
        <Row justifyContent="space-between">
            <PaymentButton
                mr={3}
                onPress={() => {
                    navigate('Payment');
                }}
                startIcon={<Icon size={7} as={<Entypo name="wallet" />} />}>
                Add money
            </PaymentButton>

            <PaymentButton
                onPress={() => navigate('Payment')}
                startIcon={
                    <Icon size={7} as={<Ionicons name="arrow-forward" />} />
                }>
                Withdraw
            </PaymentButton>
        </Row>
    );
});

export const PaymentModal = view(() => {
    const modal = React.useRef<Modalize>(null);

    React.useEffect(() => {
        if (paymentButtonsStore.operation !== PaymentOperation.none) {
            modal.current?.open();
        } else {
            modal.current?.close();
        }
    }, [paymentButtonsStore.operation]);

    return (
        <Modalize
            ref={modal}
            handlePosition="inside"
            panGestureEnabled={false}
            // adjustToContentHeight
            closeOnOverlayTap={false}
            modalTopOffset={hp(4)}
            scrollViewProps={{
                showsVerticalScrollIndicator: false,
            }}>
            <Box flex={1} p={4}>
                <Title pt={8}>Add money</Title>
                <Button
                    mt={10}
                    onPress={() => {
                        paymentButtonsStore.operation = PaymentOperation.none;
                    }}>
                    Close
                </Button>
            </Box>
        </Modalize>
    );
});
