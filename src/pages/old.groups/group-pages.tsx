/* eslint-disable no-nested-ternary */
import React from 'react';

import { Center, Spinner } from 'native-base';

import { useApplicationMachine } from 'modules/application-machine.old';

import { GroupPage } from './group-page';
import { GroupsViewer } from './group-viewer';
import { GroupsWelcome } from './groups-welcome';

export const GroupsPages = () => {
    const [machine] = useApplicationMachine();

    if (machine.matches('starting')) {
        return (
            <Center flex={1}>
                <Spinner />
            </Center>
        );
    }

    if (machine.matches('welcome')) {
        return <GroupsWelcome />;
    }

    if (machine.matches('ready')) {
        return (
            <GroupsViewer>
                {machine.context.groups.map((gr) => (
                    <GroupsViewer.Page key={gr.id}>
                        <GroupPage group={gr} />
                    </GroupsViewer.Page>
                ))}
            </GroupsViewer>
        );
    }

    return <></>;
};
