import React from 'react';

import { BaseAuth } from './base-auth';
import { useBasePageAuth } from './use-base-auth';

export const Login = () => (
    <BaseAuth
        {...useBasePageAuth({
            mode: 'login',
        })}
    />
);
