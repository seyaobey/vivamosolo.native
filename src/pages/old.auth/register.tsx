import React from 'react';

import { BaseAuth } from './base-auth';
import { useBasePageAuth } from './use-base-auth';

export const Register = () => (
    <BaseAuth
        {...useBasePageAuth({
            mode: 'register',
        })}
    />
);
