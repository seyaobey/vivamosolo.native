/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useState } from 'react';

import { useAuth } from '@nhost/react-auth';
import { useNavigation } from '@react-navigation/native';
import * as ExpoNotifications from 'expo-notifications';
import { GraphQLClient } from 'graphql-request';
import { useToast } from 'native-base';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useMutation } from 'react-query';

import { getEnvVariables } from 'env-variables';
import { graphClient } from 'modules/api-client';
import { useSet_User_Expo_Push_TokenMutation } from 'modules/api-graphql/api-react-query';
import { auth } from 'modules/application';
import { applicationStore } from 'modules/application/application-store';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';
import { Credentials, LoginResults, RegisterResults } from 'types';

type AuthMode = 'login' | 'register';

type UseBasePageAuth = {
    mode: AuthMode;
};

export const useBasePageAuth = ({ mode }: UseBasePageAuth) => {
    const { control, handleSubmit } = useForm<Credentials>();
    const { signedIn } = useAuth();
    const [userLoggedIn, setUserLoggedIn] = useState<boolean>();
    const { navigate } = useNavigation<NavigationPagesStack>();
    const [t] = useTranslation();
    const toast = useToast();

    const navigateTo = (_mode: AuthMode) => () =>
        navigate(_mode === 'login' ? 'Register' : 'Login');

    const { mutate: setExpoPushToken } = useSet_User_Expo_Push_TokenMutation(
        graphClient as GraphQLClient
    );

    const { mutate, isLoading } = useMutation(
        async ({ email, password, name, surname }: Credentials) => {
            let res: LoginResults | RegisterResults;

            if (mode === 'login') {
                res = await auth.login({ email, password });
            } else {
                res = await auth.register({
                    email,
                    password,
                    options: {
                        userData: {
                            display_name: `${name} ${surname}`,
                        },
                    },
                });
            }

            // todo: save expo-push-token (will be removed) !!!!
            const expoPushToken = (
                await ExpoNotifications.getExpoPushTokenAsync()
            ).data;

            await setExpoPushToken({
                userid: res.user?.id,
                token: expoPushToken,
            });
        },
        {
            onSuccess: () => {
                applicationStore.loggedIn = true;
            },

            onError: (err: unknown) => {
                toast.show({
                    title: 'Error',
                    description: (err as any).response.data.message,
                    status: 'error',
                });
            },
        }
    );

    const onSubmit = (values: Credentials) => mutate(values);

    useEffect(() => {
        if (signedIn) {
            applicationStore.loggedIn = getEnvVariables().ENV_AUTO_LOGIN!;
            setUserLoggedIn(applicationStore.loggedIn);
        }
    }, [signedIn]);

    return {
        t,
        mode,
        control,
        handleSubmit,
        isLoading,
        onSubmit,
        navigate: navigateTo,
        signedIn: userLoggedIn,
    };
};
