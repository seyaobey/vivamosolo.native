import React from 'react';

import { MaterialIcons, Feather } from '@expo/vector-icons';
import { Box, Text, Center, Spinner, Button, VStack } from 'native-base';

import { ButtonAction } from 'components/button-action';
import { InputField } from 'components/form/input-field';
import { Page } from 'components/page';
import { Scrollbox } from 'components/scrollbox';
import { Title } from 'modules/compounds/title';

import { useBasePageAuth } from './use-base-auth';

export const BaseAuth = ({
    t,
    mode,
    control,
    isLoading,
    signedIn,
    onSubmit,
    navigate,
    handleSubmit,
}: ReturnType<typeof useBasePageAuth>) => {
    if (signedIn === null) {
        return (
            <Center flex={1}>
                <Spinner accessibilityLabel="Authenticating" />
            </Center>
        );
    }

    return signedIn === true ? (
        <></>
    ) : (
        <Page>
            <Page.Content>
                <Scrollbox>
                    <Box flex={1} mt={4}>
                        <Title color="blue.700" fontSize="2xl">
                            Welcome to
                        </Title>
                        <Title color="blue.700" fontSize="4xl">
                            Viva Mosolo
                        </Title>

                        <Title mt={8}>
                            {t(mode === 'login' ? 'login' : 'register')}
                        </Title>

                        <VStack mt={8} space={8}>
                            {mode === 'register' && (
                                <VStack space={8}>
                                    <InputField
                                        name="name"
                                        placeholder="name"
                                        variant="underlined"
                                        control={control}
                                        rules={{ required: 'name is required' }}
                                        leftIcon={<Feather name="user" />}
                                    />
                                    <InputField
                                        name="surname"
                                        placeholder="Surname"
                                        variant="underlined"
                                        control={control}
                                        rules={{
                                            required: 'surname is required',
                                        }}
                                        leftIcon={<Feather name="user" />}
                                    />
                                </VStack>
                            )}

                            <InputField
                                name="email"
                                placeholder="Email"
                                variant="underlined"
                                control={control}
                                keyboardType="email-address"
                                rules={{ required: 'email is required' }}
                                leftIcon={
                                    <MaterialIcons name="alternate-email" />
                                }
                            />
                            <InputField
                                name="password"
                                placeholder="Password"
                                variant="underlined"
                                control={control}
                                password
                                rules={{ required: 'password is required' }}
                                leftIcon={<MaterialIcons name="lock" />}
                            />
                        </VStack>

                        <ButtonAction
                            mt={16}
                            isLoading={isLoading}
                            onPress={handleSubmit(onSubmit)}>
                            {t(mode === 'login' ? 'login' : 'register')}
                        </ButtonAction>

                        <Center mt={4}>
                            <Text>
                                {t(
                                    mode === 'login'
                                        ? 'registerInstead'
                                        : 'loginInstead'
                                )}
                            </Text>
                            <Button
                                variant="ghost"
                                onPress={navigate(mode)}
                                _text={{
                                    color: 'main',
                                    fontWeight: 400,
                                }}
                                _pressed={{
                                    bg: 'gray.200',
                                }}>
                                {t(mode === 'login' ? 'register' : 'login')}
                            </Button>
                        </Center>
                    </Box>
                </Scrollbox>
            </Page.Content>
        </Page>
    );
};
