import React from 'react';

import { AntDesign } from '@expo/vector-icons';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { Icon } from 'native-base';
import { WebView } from 'react-native-webview';

import { Page } from 'components/page';
import { Title } from 'modules/compounds/title';
import { NavigationPages } from 'modules/navigation/navigation-routes';

type PaymentWebviewRoute = RouteProp<NavigationPages, 'PaymentWebView'>;

export const PaymentWebView = () => {
    const { goBack } = useNavigation();

    const {
        params: { url },
    } = useRoute<PaymentWebviewRoute>() || {};

    return (
        <Page>
            <Page.AppBar
                hideBackBtn
                actions={[
                    <Page.AppBarAction key={1} onPress={goBack}>
                        <Icon color="main" as={<AntDesign name="close" />} />
                    </Page.AppBarAction>,
                ]}
            />
            <Page.Content>
                <Title mb={10}>Payment transaction</Title>
                <WebView
                    source={{ uri: url }}
                    scalesPageToFit
                    style={{ flex: 1 }}
                />
            </Page.Content>
        </Page>
    );
};
