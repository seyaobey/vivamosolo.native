import { store } from '@risingstack/react-easy-state';

export enum PaymentProgressStatus {
    Idle,
    Progressing,
    Completed,
    Failed,
}

export const paymentRequestStore = store({
    state: PaymentProgressStatus.Idle,
    paymentUrl: '',
});
