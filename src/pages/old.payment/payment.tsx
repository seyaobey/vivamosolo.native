import React, { useCallback } from 'react';

import { view } from '@risingstack/react-easy-state';
import { VStack, Column, FormControl, Input } from 'native-base';
// import { groupsStore } from 'pages/groups/group-store';
import { KeyboardAvoidingView, Platform } from 'react-native';
import NumberFormat from 'react-number-format';

import { ButtonAction } from 'components/button-action';
import { Page } from 'components/page';
import { pagingStore } from 'components/pagination';
import { PanelBox } from 'components/panel';
import { Scrollbox } from 'components/scrollbox';
// import { pagingStore } from 'compounds/pagination';
import { GroupDtoFragment } from 'modules/api-graphql/api-react-query';
import { useApplicationMachine } from 'modules/application-machine.old';
import { Title } from 'modules/compounds/title';
import { GroupPageCard } from 'pages/old.groups/group-page-card';

// import { PaymentProgressStatus, paymentRequestStore } from './payment-store';
import { usePaymentRequest } from './use-payment';

const Payment: React.FC = view(() => {
    const [machine] = useApplicationMachine();
    const group = machine.context.groups[pagingStore.index];
    return (
        <KeyboardAvoidingView
            style={{ flex: 1 }}
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
            <Page>
                <Scrollbox>
                    <>
                        <Page.AppBar />
                        <Page.Content>
                            <PaymentModalContent group={group} />
                        </Page.Content>
                    </>
                </Scrollbox>
            </Page>
        </KeyboardAvoidingView>
    );
});

type PaymentModalContentProps = {
    group: GroupDtoFragment;
};
const PaymentModalContent = view(({ group }: PaymentModalContentProps) => {
    const {
        t,
        errors,
        isLoading,
        strAmount,
        setValue,
        onChange,
        handleSubmit,
    } = usePaymentRequest({ group });

    const renderText = useCallback(
        InputField({
            onChangeText: onChange,
        }),
        []
    );

    return (
        <>
            <VStack space={8}>
                <Title>{t('addMoney')}</Title>
                <GroupPageCard group={group} />
                <PanelBox>
                    <FormControl
                        isRequired
                        isInvalid={!!errors?.strAmount?.message}>
                        <FormControl.Label>
                            {t('enterAmount')}
                        </FormControl.Label>

                        <NumberFormat
                            value={strAmount}
                            displayType="text"
                            thousandSeparator
                            prefix="$"
                            allowNegative={false}
                            onValueChange={({ floatValue }) => {
                                setValue('numAmount', Number(floatValue));
                            }}
                            renderText={renderText}
                        />

                        <FormControl.ErrorMessage>
                            {errors?.strAmount?.message}
                        </FormControl.ErrorMessage>
                    </FormControl>
                </PanelBox>
            </VStack>

            <Column flex={1} justifyContent="flex-end" mb={16}>
                <ButtonAction onPress={handleSubmit} isLoading={isLoading}>
                    {t('makePayment')}
                </ButtonAction>
            </Column>
        </>
    );
});

type InputFieldProps = {
    onChangeText: (text: string) => void;
};
const InputField =
    ({ onChangeText }: InputFieldProps) =>
    (value: string) =>
        (
            <Input
                variant="filled"
                textAlign="right"
                value={value}
                color="gray.500"
                keyboardType="decimal-pad"
                onChangeText={onChangeText}
                placeholder="$0"
            />
        );

export { Payment };
