import { useNavigation } from '@react-navigation/native';
import { useToast } from 'native-base';
import { useForm, useController } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { graphClient } from 'modules/api-client';
import { getApiError } from 'modules/api-client/api-utils';
import {
    GroupDtoFragment,
    Group_Recurrencies_Enum,
    useAction_Request_PaymentMutation,
} from 'modules/api-graphql/api-react-query';
import { auth } from 'modules/application';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';

import { PaymentProgressStatus, paymentRequestStore } from './payment-store';

export type Amount = {
    strAmount: string;
    numAmount: number;
};

export type UsePaymentRequestProps = {
    group: GroupDtoFragment;
};
export const usePaymentRequest = ({ group }: UsePaymentRequestProps) => {
    const [t] = useTranslation();
    const toast = useToast();
    const { navigate } = useNavigation<NavigationPagesStack>();

    const {
        control,
        setValue,
        handleSubmit,
        formState: { errors },
    } = useForm<Amount>({ defaultValues: { numAmount: 0 } });

    const { mutate, isLoading, isSuccess, isError } =
        useAction_Request_PaymentMutation(graphClient, {
            onSuccess: ({ action_request_payment: res }) => {
                navigate('PaymentWebView', { url: res!.url! });
            },
            onError: (err: unknown) => {
                toast.show({
                    title: 'Oups! An error has occured',
                    description: getApiError(err),
                    status: 'error',
                });
            },
        });

    const {
        field: { value: strAmount, onChange },
    } = useController<Amount>({
        name: 'strAmount',
        control,
        rules: {
            required: {
                value: true,
                message: 'Payment amount is required',
            },
            min: {
                value: 1,
                message: 'Payment amount is not valid',
            },
        },
    });

    const submitPaymentRequest = (values: Amount) => {
        const memberId = group.members.find(
            (m) => m.user.id === auth.user()?.id
        )!.id;
        const period =
            group.group_recurrency_type !== Group_Recurrencies_Enum.None
                ? group.periods[0]
                : undefined;

        mutate({
            args: {
                amount: values.numAmount,
                group_id: group.id,
                member_id: memberId,
                period_id: period?.id,
            },
        });

        paymentRequestStore.state = PaymentProgressStatus.Progressing;
        // paymentRequestStore.transition(PaymentProgressStatus.Progressing);
    };

    // useEffect(() => {
    //     progressStatusStore.init();
    // }, []);

    // useEffect(() => {
    //     progressStatusStore.watch(isLoading, isSuccess, isError);
    // }, [isLoading, isSuccess, isError]);

    return {
        t,
        group,
        isLoading,
        errors,
        strAmount,
        onChange,
        setValue,
        handleSubmit: handleSubmit(submitPaymentRequest),
    };
};
