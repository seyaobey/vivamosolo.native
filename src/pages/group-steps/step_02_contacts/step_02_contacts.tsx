import React from 'react';

import { Feather } from '@expo/vector-icons';
import { Icon, Input, Row, Text } from 'native-base';

import { Page } from 'components/page';
import { Panel } from 'components/panel';
import { Scrollbox } from 'components/scrollbox';
import { TextLarge, TextLargeBold } from 'components/text-large';
import { useApplicationMachine } from 'modules/application-machine';
import { Title } from 'modules/compounds/title';

export type IStep02ContactsProps = {};

const Step02Contacts: React.FC<IStep02ContactsProps> = ({}) => {
    const { steps } = useApplicationMachine();

    return (
        <Page>
            <Scrollbox>
                <>
                    <Page.AppBar
                        onGoBack={() => steps.send('go_back')}
                        actions={[
                            <Page.AppBarAction
                                key={1}
                                onPress={() => steps.send('go_next_step')}>
                                Skip
                            </Page.AppBarAction>,
                        ]}
                    />

                    <Page.Content>
                        <Title>Select</Title>
                        <Title>participants</Title>

                        <TextLarge mt={2}>
                            Select{' '}
                            <TextLargeBold>participants</TextLargeBold> from
                            your contacts list and start saving together
                        </TextLarge>

                        <Input
                            mt={7}
                            variant="filled"
                            placeholder="Search"
                            InputLeftElement={
                                <Icon
                                    size={6}
                                    ml={1}
                                    color="gray.400"
                                    as={<Feather name="search" />}
                                />
                            }
                        />

                        <Panel mt={12}>
                            <Row>
                                <Icon
                                    size={6}
                                    ml={1}
                                    color="amber.400"
                                    as={<Feather name="alert-triangle" />}
                                />
                                <Text flex={1} ml={3} color="gray.500">
                                    This functionality is still under
                                    constructor. Click &apos;Skip&apos;
                                </Text>
                            </Row>
                        </Panel>
                    </Page.Content>
                </>
            </Scrollbox>
        </Page>
    );
};

export { Step02Contacts };
