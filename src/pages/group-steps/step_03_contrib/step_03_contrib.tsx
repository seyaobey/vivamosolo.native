import React from 'react';

import { Text, HStack, VStack } from 'native-base';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NumberFormat from 'react-number-format';

import { ButtonAction } from 'components/button-action';
import { ButtonChange } from 'components/button-change';
import { Page } from 'components/page';
import { Panel } from 'components/panel';
import { Scrollbox } from 'components/scrollbox';
import { Swipeable } from 'components/swipeable';
import { TextLarge, TextLargeBold } from 'components/text-large';
import { Contribution_Types_Enum } from 'modules/api-graphql/api-apollo';
import { Title } from 'modules/compounds/title';
import { contribTypes } from 'pages/old.group-create/create-types';

import { ContribSwiper } from './contrib-swiper';
import { ContribPayload, useContribMachine } from './use-contrib-machine';

export type IStep03ContribProps = {};

const Step03Contrib: React.FC<IStep03ContribProps> = ({}) => {
    const { state, nextStep, send, masterSend, onSubmitChanges } =
        useContribMachine();

    return (
        <Page>
            <>
                <Scrollbox>
                    <>
                        <Page.AppBar onGoBack={() => masterSend('go_back')} />
                        <Page.Content>
                            <VStack flex={1}>
                                <VStack space={hp('0.5%')}>
                                    <VStack>
                                        <Title>How much</Title>
                                        <Title>should each member pay?</Title>
                                        <TextLarge mt={4}>
                                            You can set a{' '}
                                            <TextLargeBold>
                                                minimum
                                            </TextLargeBold>{' '}
                                            or{' '}
                                            <TextLargeBold>
                                                fix amount
                                            </TextLargeBold>{' '}
                                            for all members of this group. Or
                                            you can allow each participant to{' '}
                                            <TextLargeBold>
                                                freely
                                            </TextLargeBold>{' '}
                                            set his/her own contribution.
                                        </TextLarge>
                                    </VStack>
                                    <ContribField
                                        payload={state.context.data}
                                        openSwiper={() => send('enter_editing')}
                                    />
                                </VStack>
                                <ButtonAction mt={20} onPress={nextStep}>
                                    Continue
                                </ButtonAction>
                            </VStack>
                        </Page.Content>
                    </>
                </Scrollbox>

                <Swipeable
                    closable
                    isOpen={state.matches('editing')}
                    onClose={() => send('exit_editing')}
                    onClosed={() => send('exit_editing')}>
                    <ContribSwiper
                        initial={state.context.data}
                        onSubmitChanges={onSubmitChanges}
                    />
                </Swipeable>
            </>
        </Page>
    );
};

export { Step03Contrib };

type ContribFieldProps = {
    payload?: ContribPayload;
    openSwiper: () => void;
};
const ContribField = ({ payload, openSwiper }: ContribFieldProps) => {
    const type = payload?.contrib_type ?? Contribution_Types_Enum.AnyAmount;
    const displayNumField = type !== Contribution_Types_Enum.AnyAmount;

    return (
        <Panel mt={8} py={4}>
            <HStack justifyContent="space-between" alignItems="center">
                <VStack space={2}>
                    <TextLarge>{contribTypes[type]}</TextLarge>
                    {displayNumField && (
                        <NumberFormat
                            value={payload?.contrib_amount}
                            displayType="text"
                            thousandSeparator
                            decimalSeparator="."
                            decimalScale={2}
                            fixedDecimalScale
                            prefix="$"
                            renderText={(value) => <Text>{value}</Text>}
                        />
                    )}
                </VStack>
                <ButtonChange onPress={openSwiper}>change</ButtonChange>
            </HStack>
        </Panel>
    );
};
