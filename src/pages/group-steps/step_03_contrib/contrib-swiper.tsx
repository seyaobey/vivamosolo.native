import React from 'react';

import { VStack, Fade, FormControl, Input } from 'native-base';
import { useController, useForm } from 'react-hook-form';
import NumberFormat from 'react-number-format';

import { ButtonAction } from 'components/button-action';
import { PanelBox } from 'components/panel';
import { SelectButton } from 'components/select-button';
import { TextLarge, TextLargeBold } from 'components/text-large';
import { Contribution_Types_Enum } from 'modules/api-graphql/api-react-query';
import { contribTypes } from 'pages/old.group-create/create-types';

import { ContribContext, ContribPayload } from './use-contrib-machine';

type Payload = ContribContext['data'];
type Props = {
    initial?: Payload;
    onSubmitChanges: (payload: Payload) => void;
};

export const ContribSwiper = ({ initial, onSubmitChanges }: Props) => {
    const {
        errors,
        contribType,
        contribAmount,
        setContribNum,
        onContribTypeChange,
        onContribAmountChange,
        requestSubmitChanges,
    } = useGroupStepContribScreenSwiper({ initial, onSubmitChanges });

    return (
        <VStack flex={1} space={10} px={4}>
            <PanelBox>
                <TextLarge>
                    <TextLargeBold>How much</TextLargeBold> participants should
                    pay.
                </TextLarge>
            </PanelBox>

            <SelectButton<Contribution_Types_Enum>
                caption="Select"
                items={contribTypes}
                value={contribType!}
                onChange={onContribTypeChange}
            />

            <Fade
                in={[
                    Contribution_Types_Enum.MinimumAmount,
                    Contribution_Types_Enum.FixAmount,
                ].includes(contribType!)}>
                <PanelBox px={4} mb={8}>
                    <FormControl
                        isRequired
                        isInvalid={!!errors?.contrib_amount?.message}>
                        <FormControl.Label
                            _text={{
                                fontWeight: 'light',
                                fontSize: 'lg',
                            }}>
                            Enter amount
                        </FormControl.Label>
                        <NumberFormat
                            value={contribAmount}
                            displayType="text"
                            thousandSeparator
                            prefix="$"
                            allowNegative={false}
                            onValueChange={({ floatValue: value }) => {
                                setContribNum(value);
                            }}
                            renderText={React.useCallback(
                                InputField({
                                    onChangeText: onContribAmountChange,
                                }),
                                []
                            )}
                        />
                        <FormControl.ErrorMessage>
                            {errors?.contrib_amount?.message}
                        </FormControl.ErrorMessage>
                    </FormControl>
                </PanelBox>
            </Fade>

            <ButtonAction mb={6} onPress={requestSubmitChanges}>
                Save
            </ButtonAction>
        </VStack>
    );
};

type InputFieldProps = {
    onChangeText: (text: string) => void;
};
const InputField =
    ({ onChangeText }: InputFieldProps) =>
    (value: string) =>
        (
            <Input
                variant="filled"
                textAlign="right"
                value={value}
                color="gray.500"
                keyboardType="decimal-pad"
                onChangeText={onChangeText}
                placeholder="$0"
                size="xl"
            />
        );

type GroupStepContribScreenSwiperProps = {
    initial?: Payload;
    onSubmitChanges: (payload: Payload) => void;
};
const useGroupStepContribScreenSwiper = ({
    initial,
    onSubmitChanges,
}: GroupStepContribScreenSwiperProps) => {
    const {
        control,
        handleSubmit,
        getValues,
        formState: { errors },
    } = useForm<Payload>();

    const [contribNum, setContribNum] = React.useState<number>();

    const {
        field: { value: contribAmount, onChange: onContribAmountChange },
    } = useController<Payload, 'contrib_amount'>({
        name: 'contrib_amount',
        defaultValue: initial?.contrib_amount ?? undefined,
        rules: {
            validate: (value: number | undefined) => {
                if (
                    getValues().contrib_type !==
                    Contribution_Types_Enum.AnyAmount
                ) {
                    return !value || value <= 1
                        ? 'Contribution amount is not valid'
                        : undefined;
                }
                return undefined;
            },
        },
        control,
    });

    const {
        field: { value: contribType, onChange: onContribTypeChange },
    } = useController<Payload, 'contrib_type'>({
        name: 'contrib_type',
        defaultValue:
            initial?.contrib_type || Contribution_Types_Enum.AnyAmount,
        control,
    });

    const handleSubmitChange = (payload: ContribPayload) =>
        onSubmitChanges({
            contrib_amount: contribNum,
            contrib_type: payload.contrib_type,
        });

    const requestSubmitChanges = handleSubmit(handleSubmitChange);

    return {
        errors,
        control,
        contribAmount,
        contribType,
        setContribNum,
        onContribAmountChange,
        onContribTypeChange,
        requestSubmitChanges,
    };
};
