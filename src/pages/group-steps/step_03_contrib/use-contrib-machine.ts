import { useMachine } from '@xstate/react';
import { createMachine } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { useApplicationMachine } from 'modules/application-machine';
import { StepsPayload } from 'modules/application-machine/machines/steps-machine';

export type ContribPayload = Pick<
    StepsPayload,
    'contrib_amount' | 'contrib_type'
>;

export type ContribContext = {
    data: ContribPayload;
};

type TypeState = { value: 'editing' };

type Events =
    | { type: 'enter_editing' }
    | { type: 'exit_editing' }
    | {
          type: 'finish_editing';
          value: {
              payload: ContribPayload;
          };
      };

const model = createModel({} as ContribContext, {
    events: {
        enter_editing: () => ({}),
        exit_editing: () => ({}),
        finish_editing: (payload: ContribPayload) => ({ payload }),
    },
});

const contribMachine = createMachine<
    typeof model,
    ContribContext,
    Events,
    TypeState & { context: ContribContext }
>(
    {
        context: model.initialContext,
        initial: 'ready',
        states: {
            ready: {
                on: {
                    enter_editing: {
                        target: 'editing',
                    },
                },
            },
            editing: {
                on: {
                    finish_editing: {
                        target: 'ready',
                        actions: 'finish_editing',
                    },
                    exit_editing: {
                        target: 'ready',
                    },
                },
            },
        },
    },
    {
        actions: {
            finish_editing: model.assign(
                {
                    data: (ctx, e) => e.payload,
                },
                'finish_editing'
            ) as never,
        },
    }
);

export const useContribMachine = () => {
    const { steps } = useApplicationMachine();
    const [state, send] = useMachine(
        contribMachine.withContext({
            ...contribMachine.context,
            data: {
                contrib_amount: steps.state.context.data?.contrib_amount,
                contrib_type: steps.state.context.data?.contrib_type,
            },
        })
    );

    const onSubmitChanges = (payload: ContribPayload) => {
        send('finish_editing', { payload });
    };

    const nextStep = () => {
        steps.send({
            type: 'go_next_step',
            value: { payload: state.context.data },
        });
    };

    return {
        state,
        send,
        nextStep,
        masterSend: steps.send,
        onSubmitChanges,
    };
};
