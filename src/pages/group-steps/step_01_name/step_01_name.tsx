import React from 'react';

import { Column, VStack } from 'native-base';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { Page } from 'components/page';
import { Scrollbox } from 'components/scrollbox';
import { useApplicationMachine } from 'modules/application-machine';
import { Title } from 'modules/compounds/title';

import { Step01NameForm } from './step_01_name-form';

export type IStep01NameProps = {};

const Step01Name: React.FC<IStep01NameProps> = ({}) => {
    const { steps } = useApplicationMachine();
    return (
        <Page>
            <Scrollbox>
                <>
                    <Page.AppBar onGoBack={() => steps.send('go_back')} />
                    <Page.Content>
                        <Column flex={1} justifyContent="space-between">
                            <VStack space={hp('0.5%')}>
                                <VStack>
                                    <Title>Enter the</Title>
                                    <Title>new group name</Title>
                                </VStack>

                                <Step01NameForm />
                            </VStack>
                        </Column>
                    </Page.Content>
                </>
            </Scrollbox>
        </Page>
    );
};

export { Step01Name };
