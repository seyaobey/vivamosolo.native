import { useMemo } from 'react';

import { useMachine } from '@xstate/react';

import { useApplicationMachine } from 'modules/application-machine';
import { buildFormMachine } from 'modules/form-machine';

export const useFormMachine = () => {
    const { steps } = useApplicationMachine();

    const [form, send] = useMachine(
        buildFormMachine({
            model: {
                values: {
                    group_name: steps.state.context?.data?.group_name,
                },
            },
            validate: (values) => {
                const isValid =
                    values?.group_name?.length &&
                    values?.group_name?.trim() !== undefined;
                return {
                    group_name: isValid ? undefined : 'Group name is required',
                };
            },
        }),
        {
            services: {
                submit: () => {
                    send('reset');
                    steps.send({
                        type: 'go_next_step',
                        value: { payload: form.context.values },
                    });
                    return Promise.resolve();
                },
            },
        }
    );

    const onSubmit = () => {
        send('request_validation');
    };

    const onChange = (text: string) =>
        send({ type: 'read_input', values: { group_name: text } });

    const isInvalid = useMemo(
        () => form.matches({ ready: 'input_is_invalid' }),
        [form.value]
    );

    return {
        form,
        onSubmit,
        onChange,
        isInvalid,
    };
};
