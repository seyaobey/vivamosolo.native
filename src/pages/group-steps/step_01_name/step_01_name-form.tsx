import React from 'react';

import { FormControl, Input, VStack } from 'native-base';

import { ButtonAction } from 'components/button-action';

import { useFormMachine } from './use-form-machine';

export type IStep01NameFormProps = {};

const Step01NameForm: React.FC<IStep01NameFormProps> = React.memo(({}) => {
    const { form, isInvalid, onChange, onSubmit } = useFormMachine();

    return (
        <VStack>
            <FormControl mt={6} isRequired isInvalid={isInvalid}>
                <FormControl.Label
                    _text={{
                        fontWeight: 'light',
                        fontSize: 'lg',
                    }}>
                    group name
                </FormControl.Label>
                <Input
                    value={form.context?.values?.group_name ?? ''}
                    variant="filled"
                    isInvalid={isInvalid}
                    placeholder="Name your new group"
                    onChangeText={onChange}
                />
                <FormControl.ErrorMessage>
                    {form.context?.errors?.group_name}
                </FormControl.ErrorMessage>
            </FormControl>

            <ButtonAction mt={20} onPress={onSubmit}>
                Continue
            </ButtonAction>
        </VStack>
    );
});

export { Step01NameForm };
