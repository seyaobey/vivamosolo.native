import { useMachine } from '@xstate/react';
import { createMachine } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { useApplicationMachine } from 'modules/application-machine';
import { StepsPayload } from 'modules/application-machine/machines/steps-machine';

export type RecurrencyPayload = Pick<
    StepsPayload,
    'recurrent_day' | 'recurrent_type'
>;

type RecurrencyContext = {
    data: RecurrencyPayload;
};

type TypeState = { value: 'editing' };

type Events =
    | { type: 'enter_editing' }
    | { type: 'exit_editing' }
    | {
          type: 'update_cycle_values';
          value: {
              payload: RecurrencyPayload;
          };
      };

const model = createModel({} as unknown as RecurrencyContext, {
    events: {
        enter_editing: () => ({}),
        exit_editing: () => ({}),
        update_cycle_values: (payload: RecurrencyPayload) => ({
            value: { payload },
        }),
    },
});

const recurrencyMachine = createMachine<
    typeof model,
    RecurrencyContext,
    Events,
    TypeState & { context: RecurrencyContext }
>(
    {
        context: model.initialContext,
        initial: 'ready',
        states: {
            ready: {
                on: {
                    enter_editing: {
                        target: 'editing',
                    },
                },
            },
            editing: {
                on: {
                    update_cycle_values: {
                        target: 'ready',
                        actions: 'update_cycle_values',
                    },
                    exit_editing: {
                        target: 'ready',
                    },
                },
            },
        },
    },
    {
        actions: {
            update_cycle_values: model.assign(
                {
                    data: (_, e) => e.value.payload,
                },
                'update_cycle_values'
            ) as never,
        },
    }
);

export const useRecurrencyMachine = () => {
    const { steps } = useApplicationMachine();

    const [state, send] = useMachine(
        recurrencyMachine.withContext({
            ...recurrencyMachine.context,
            data: {
                recurrent_day: steps.state.context.data?.recurrent_day,
                recurrent_type: steps.state.context.data?.recurrent_type,
            },
        })
    );

    const onSubmitChanges = (payload: RecurrencyPayload) => {
        send({ type: 'update_cycle_values', value: { payload } });
        steps.send({ type: 'update_payload', value: { payload } });
    };

    return {
        state,
        master: steps.state,
        send,
        masterSend: steps.send,
        onSubmitChanges,
        editing: state.matches('editing'),
    };
};
