import React from 'react';

import { VStack } from 'native-base';

import { ButtonAction } from 'components/button-action';
import { Page } from 'components/page';
import { Scrollbox } from 'components/scrollbox';
import { Swipeable } from 'components/swipeable';
import { TextLarge, TextLargeBold } from 'components/text-large';
import { Title } from 'modules/compounds/title';

import { CycleField, CycleFieldSwiper } from './components';
import { useRecurrencyMachine } from './use-recurrency-machine';

export type IStep04RecurrencyProps = {};

const Step04Recurrency: React.FC<IStep04RecurrencyProps> = ({}) => {
    const { masterSend, send, master, state, editing, onSubmitChanges } =
        useRecurrencyMachine();

    return (
        <Page>
            <Scrollbox>
                <>
                    <Page.AppBar onGoBack={() => masterSend('go_back')} />
                    <Page.Content>
                        <VStack flex={1}>
                            <VStack>
                                <Title>When should</Title>
                                <Title>each member pay?</Title>

                                <TextLarge mt={4}>
                                    You can set a{' '}
                                    <TextLargeBold>
                                        recurrent payment deadline
                                    </TextLargeBold>{' '}
                                    by which every member of this group should
                                    have paid his contribution or allow each one
                                    to setup his own recurrent deadline
                                </TextLarge>
                            </VStack>
                            <CycleField
                                payload={state.context.data}
                                changeValues={() => send('enter_editing')}
                            />
                            <ButtonAction
                                mt={20}
                                isLoading={master.matches('step_submit_values')}
                                onPress={() => masterSend('go_next_step')}>
                                Finish
                            </ButtonAction>
                        </VStack>
                    </Page.Content>
                </>
            </Scrollbox>

            <Swipeable
                closable
                isOpen={editing}
                onClose={() => send('exit_editing')}
                onClosed={() => {
                    send('exit_editing');
                }}>
                <CycleFieldSwiper
                    payload={state.context.data}
                    onSubmit={onSubmitChanges}
                />
            </Swipeable>
        </Page>
    );
};

export { Step04Recurrency };
