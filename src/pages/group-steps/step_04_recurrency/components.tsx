import React from 'react';

import { HStack, VStack } from 'native-base';
import { useController, useForm } from 'react-hook-form';

import { ButtonAction } from 'components/button-action';
import { ButtonChange } from 'components/button-change';
import { Calendar, CalendarMode, LeadingDay } from 'components/calendar';
import { Panel, PanelBox } from 'components/panel';
import { recurrencyLabels } from 'components/payments';
import { SelectButton } from 'components/select-button';
import { TextLarge } from 'components/text-large';
import { Group_Recurrencies_Enum } from 'modules/api-graphql/api-apollo';

import { RecurrencyPayload } from './use-recurrency-machine';
import { hasReccurency } from './utils';

type CycleFieldProps = {
    payload?: RecurrencyPayload;
    changeValues: () => void;
};
export const CycleField = ({ payload, changeValues }: CycleFieldProps) => {
    const isRecurrentWeekMonth = hasReccurency(
        payload?.recurrent_type ?? Group_Recurrencies_Enum.None,
        Group_Recurrencies_Enum.Daily
    );
    return (
        <Panel mt={8} py={4}>
            <HStack justifyContent="space-between" alignItems="center">
                <HStack>
                    <VStack space={1}>
                        <TextLarge>
                            {
                                recurrencyLabels[
                                    payload?.recurrent_type ??
                                        Group_Recurrencies_Enum.None
                                ]
                            }
                        </TextLarge>
                        {isRecurrentWeekMonth && (
                            <LeadingDay
                                activeDay={payload!.recurrent_day!}
                                mode={payload!.recurrent_type! as CalendarMode}
                            />
                        )}
                    </VStack>
                </HStack>
                <ButtonChange onPress={changeValues}>change</ButtonChange>
            </HStack>
        </Panel>
    );
};

type CycleFieldSwiperProps = {
    payload?: RecurrencyPayload;
    onSubmit: (values: RecurrencyPayload) => void;
};
export const CycleFieldSwiper = ({
    payload,
    onSubmit,
}: CycleFieldSwiperProps) => {
    const recurrency = payload?.recurrent_type ?? Group_Recurrencies_Enum.None;

    const { control, setValue, handleSubmit } = useForm<RecurrencyPayload>();

    const {
        field: { value: recurrentType, onChange: onChangeRecurrentType },
    } = useController<RecurrencyPayload, 'recurrent_type'>({
        control,
        name: 'recurrent_type',
        defaultValue: recurrency,
    });

    const {
        field: { value: recurentDay, onChange: onChangeRecurrentDay },
    } = useController<RecurrencyPayload, 'recurrent_day'>({
        control,
        name: 'recurrent_day',
        defaultValue: payload?.recurrent_day ?? 0,
    });

    const displayCalendar = React.useMemo(
        () =>
            (
                [
                    Group_Recurrencies_Enum.Weekly,
                    Group_Recurrencies_Enum.Monthly,
                ] as Group_Recurrencies_Enum[]
            ).includes(recurrentType!),
        [recurrentType]
    );

    React.useEffect(() => {
        if (recurrentType !== Group_Recurrencies_Enum.Monthly) {
            if ((recurentDay ?? 0) > 7) {
                setValue('recurrent_day', 0);
            }
        }
    }, [recurrentType]);

    return (
        <VStack flex={1} space={10} px={4}>
            <PanelBox>
                <TextLarge>When should participants pay.</TextLarge>
            </PanelBox>

            <SelectButton<Group_Recurrencies_Enum>
                caption="Payment cycle"
                items={recurrencyLabels}
                value={recurrentType!}
                onChange={onChangeRecurrentType}
            />

            {displayCalendar && (
                <PanelBox>
                    <Calendar
                        value={recurentDay!}
                        onChange={onChangeRecurrentDay}
                        mode={
                            recurrentType === Group_Recurrencies_Enum.Weekly
                                ? Group_Recurrencies_Enum.Weekly
                                : Group_Recurrencies_Enum.Monthly
                        }
                    />
                </PanelBox>
            )}

            <ButtonAction mb={10} mt={3} onPress={handleSubmit(onSubmit)}>
                Save
            </ButtonAction>
        </VStack>
    );
};
