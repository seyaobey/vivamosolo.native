import { store } from '@risingstack/react-easy-state';

import {
    Contribution_Types_Enum,
    Group_Recurrencies_Enum,
} from 'modules/api-graphql/api-react-query';

const initialValues = {
    name: '',
    recurrency: Group_Recurrencies_Enum.None,
    recurrencyDay: 0,
    contributionType: Contribution_Types_Enum.AnyAmount,
    contributionAmount: undefined as unknown as number,
};

export const createStore = store({
    values: {
        ...initialValues,
    },
    init: () => {
        createStore.values = {
            ...initialValues,
        };
    },
});
