import React, { useCallback } from 'react';

import { Feather } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { view } from '@risingstack/react-easy-state';
import {
    Input,
    FormControl,
    Box,
    Text,
    VStack,
    Row,
    useDisclose,
    Icon,
} from 'native-base';
import { useForm, Controller } from 'react-hook-form';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NumberFormat from 'react-number-format';

import { ButtonChange } from 'components/button-change';
import { Page } from 'components/page';
import { Panel } from 'components/panel';
import { Scrollbox } from 'components/scrollbox';
import { Swipeable } from 'components/swipeable';
import { Contribution_Types_Enum } from 'modules/api-graphql/api-react-query';
import { Title } from 'modules/compounds/title';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';

import { CreateGroupCustomContribs } from './create-customize-contribs';
import { createStore } from './create-store';
import { contribTypes, CreateGroup } from './create-types';
import { hasContribValue, resolveContribColor } from './utils';

export const PageGroupCreateCustomize = view(() => {
    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm<CreateGroup>();

    const { navigate } = useNavigation<NavigationPagesStack>();
    const { isOpen, onToggle } = useDisclose();

    const onSubmit = (values: CreateGroup) => {
        createStore.values.name = values.name;
        navigate('CreateGroupPayments');
    };
    const submit = useCallback(handleSubmit(onSubmit), []);

    return (
        <>
            <Page>
                <Scrollbox>
                    <>
                        <Page.AppBar
                            actions={[
                                <Page.AppBarAction key={1} onPress={submit}>
                                    Next
                                </Page.AppBarAction>,
                            ]}
                        />

                        <Page.Content flex={1}>
                            <Title>Customize</Title>

                            <VStack space={hp('6%')}>
                                <Controller<CreateGroup>
                                    control={control}
                                    name="name"
                                    rules={{
                                        required: 'Group name is required',
                                    }}
                                    render={({
                                        field: { onChange, value, ref },
                                    }) => (
                                        <FormControl
                                            mt={6}
                                            isRequired
                                            isInvalid={!!errors.name}>
                                            <FormControl.Label>
                                                Group name
                                            </FormControl.Label>
                                            <Input
                                                ref={ref}
                                                value={value as string}
                                                variant="filled"
                                                isInvalid={!!errors.name}
                                                placeholder="Name your group"
                                                onChangeText={(text: string) =>
                                                    onChange(text)
                                                }
                                            />
                                            <FormControl.ErrorMessage>
                                                {errors?.name?.message}
                                            </FormControl.ErrorMessage>
                                        </FormControl>
                                    )}
                                />

                                <Box>
                                    <Text
                                        ml={1}
                                        mr={2}
                                        color="gray.500"
                                        fontSize="lg">
                                        How much can{' '}
                                        <Text fontSize="lg" fontWeight="500">
                                            participants
                                        </Text>{' '}
                                        pay?
                                    </Text>

                                    <Text fontSize="sm" color="gray.400">
                                        {' '}
                                        You can change it later.
                                    </Text>
                                </Box>

                                <Panel>
                                    <Row
                                        justifyContent="space-between"
                                        alignItems="center">
                                        <VStack space={1}>
                                            <Text
                                                color={resolveContribColor(
                                                    createStore.values
                                                        .contributionType
                                                )}
                                                fontWeight={
                                                    hasContribValue(
                                                        createStore.values
                                                            .contributionType
                                                    )
                                                        ? '700'
                                                        : '400'
                                                }>
                                                {
                                                    contribTypes[
                                                        createStore.values
                                                            .contributionType
                                                    ]
                                                }
                                            </Text>
                                            {createStore.values
                                                .contributionType !==
                                                Contribution_Types_Enum.AnyAmount && (
                                                <NumberFormat
                                                    value={
                                                        createStore.values
                                                            .contributionAmount ??
                                                        0
                                                    }
                                                    displayType="text"
                                                    thousandSeparator
                                                    decimalSeparator="."
                                                    decimalScale={2}
                                                    fixedDecimalScale
                                                    prefix="$"
                                                    renderText={(value) => (
                                                        <Text
                                                            color={resolveContribColor(
                                                                createStore
                                                                    .values
                                                                    .contributionType,
                                                                '600'
                                                            )}>
                                                            {value}
                                                        </Text>
                                                    )}
                                                />
                                            )}
                                        </VStack>
                                        <ButtonChange onPress={onToggle}>
                                            change
                                        </ButtonChange>
                                    </Row>
                                </Panel>

                                {createStore.values.contributionType ===
                                    Contribution_Types_Enum.AnyAmount && (
                                    <Panel py={6}>
                                        <Row>
                                            <Icon
                                                size={6}
                                                color="amber.500"
                                                as={
                                                    <Feather name="alert-triangle" />
                                                }
                                            />
                                            <Text
                                                ml={4}
                                                mr={4}
                                                fontSize="sm"
                                                color="gray.400"
                                                flexWrap="wrap"
                                                noOfLines={4}>
                                                <Text
                                                    fontSize="sm"
                                                    color="amber.400">
                                                    Any amount
                                                </Text>{' '}
                                                means each participant may
                                                freely decide how much he/she
                                                can contribute.
                                            </Text>
                                        </Row>
                                    </Panel>
                                )}
                            </VStack>
                        </Page.Content>
                    </>
                </Scrollbox>

                <Swipeable isOpen={isOpen}>
                    <CreateGroupCustomContribs onClose={onToggle} />
                </Swipeable>
            </Page>
        </>
    );
});
