/* eslint-disable no-nested-ternary */
/* eslint-disable indent */
/* eslint-disable react/jsx-closing-bracket-location */
import { useEffect } from 'react';

import { useNavigation } from '@react-navigation/native';
import { useToast } from 'native-base';

import { progressStatusStore } from 'components/modal-progress';
import { graphClient } from 'modules/api-client';
import { getApiError } from 'modules/api-client/api-utils';
import { useAction_Create_GroupMutation } from 'modules/api-graphql/api-react-query';
import { auth } from 'modules/application';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';

import { createStore } from './create-store';

export const useGroupCreateFinalize = () => {
    const toast = useToast();
    const { navigate } = useNavigation<NavigationPagesStack>();

    const navigateToGroups = () => navigate('Groups');

    const { mutate, isLoading, isSuccess, isError } =
        useAction_Create_GroupMutation(graphClient, {
            onError: (err: unknown) => {
                toast.show({
                    title: 'Oups! An error has occured',
                    description: getApiError(err),
                    status: 'error',
                });
            },
        });

    useEffect(() => {
        progressStatusStore.init();
    }, []);

    useEffect(() => {
        progressStatusStore.watch(isLoading, isSuccess, isError);
    }, [isLoading, isSuccess, isError]);

    const submitValues = async () => {
        const {
            contributionAmount,
            contributionType,
            name,
            recurrency,
            recurrencyDay,
        } = createStore.values;

        const payload = {
            group_name: name,
            creator_id: auth.user()?.id,
            group_contribution_type: contributionType,
            group_contribution_amount: contributionAmount || 0,
            group_recurrency_type: recurrency,
            group_recurrency_day: recurrencyDay,
        };

        return mutate(payload);
    };

    return {
        createGroup: submitValues,
        navigateToGroups,
    };
};
