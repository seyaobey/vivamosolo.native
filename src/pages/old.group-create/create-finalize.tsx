/* eslint-disable indent */
/* eslint-disable react/jsx-closing-bracket-location */
import React from 'react';

import { Feather } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { view } from '@risingstack/react-easy-state';
import {
    Text,
    Icon,
    Factory,
    Row,
    Column,
    useDisclose,
    VStack,
} from 'native-base';
import { useTranslation } from 'react-i18next';
import { ScrollView as ScrollViewBase } from 'react-native';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { ButtonAction } from 'components/button-action';
import { ButtonChange } from 'components/button-change';
import { CalendarMode, LeadingDay } from 'components/calendar';
import { Page } from 'components/page';
import { Panel } from 'components/panel';
import { PaymentRecurrency, recurrencyLabels } from 'components/payments';
import { ProgressStatus } from 'components/progress-status';
import { Group_Recurrencies_Enum } from 'modules/api-graphql/api-react-query';
import { Title } from 'modules/compounds/title';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';

import { createStore } from './create-store';
import { useGroupCreateFinalize } from './use-create-finalize';
import { hasReccurency, resolveRecurrencyColor } from './utils';

const ScrollView = Factory(ScrollViewBase);

export const PageGroupCreateFinalize = view(() => {
    const { isOpen, onToggle } = useDisclose();
    const { createGroup } = useGroupCreateFinalize();
    const { navigate } = useNavigation<NavigationPagesStack>();
    const [t] = useTranslation();

    const goToPageGroups = () => navigate('Groups');

    return (
        <>
            <Page>
                <ScrollView
                    flex={1}
                    alwaysBounceVertical
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ flexGrow: 1 }}>
                    <Page.AppBar />

                    <Page.Content>
                        <Title>Recurrent deadline</Title>
                        <Text mt={2} color="gray.400" fontSize="sm">
                            Set a recurrent deadline by which all participants
                            must have paid their due contribution.
                        </Text>

                        <Text color="gray.400" fontSize="xs">
                            You can always change this setting later.
                        </Text>

                        <VStack flex={1} mt={10} space={hp('6%')}>
                            <Panel>
                                <Row
                                    justifyContent="space-between"
                                    alignItems="center">
                                    <VStack>
                                        <Row alignItems="center">
                                            {hasReccurency(
                                                createStore.values.recurrency
                                            ) && (
                                                <Icon
                                                    size={4}
                                                    color={resolveRecurrencyColor(
                                                        createStore.values
                                                            .recurrency
                                                    )}
                                                    as={
                                                        <Feather name="calendar" />
                                                    }
                                                />
                                            )}

                                            <Text
                                                ml={2}
                                                color={resolveRecurrencyColor(
                                                    createStore.values
                                                        .recurrency
                                                )}
                                                fontWeight={
                                                    hasReccurency(
                                                        createStore.values
                                                            .recurrency
                                                    )
                                                        ? '700'
                                                        : '400'
                                                }>
                                                {
                                                    recurrencyLabels[
                                                        createStore.values
                                                            .recurrency
                                                    ]
                                                }
                                            </Text>
                                        </Row>
                                        {hasReccurency(
                                            createStore.values.recurrency,
                                            Group_Recurrencies_Enum.Daily
                                        ) && (
                                            <LeadingDay
                                                activeDay={
                                                    createStore.values
                                                        .recurrencyDay
                                                }
                                                color={resolveRecurrencyColor(
                                                    createStore.values
                                                        .recurrency
                                                )}
                                                mode={
                                                    createStore.values
                                                        .recurrency as CalendarMode
                                                }
                                            />
                                        )}
                                    </VStack>

                                    <ButtonChange onPress={onToggle}>
                                        {t('change')}
                                    </ButtonChange>
                                </Row>
                            </Panel>

                            {createStore.values.recurrency ===
                                Group_Recurrencies_Enum.None && (
                                <Panel py={6}>
                                    <Row>
                                        <Icon
                                            size={6}
                                            color="amber.500"
                                            as={
                                                <Feather name="alert-triangle" />
                                            }
                                        />
                                        <Text
                                            ml={4}
                                            mr={4}
                                            fontSize="sm"
                                            color="gray.400"
                                            flexWrap="wrap"
                                            noOfLines={4}>
                                            <Text
                                                fontSize="sm"
                                                color="amber.400">
                                                No deadline
                                            </Text>{' '}
                                            means each participant may freely
                                            determine how often he/she can
                                            contribute.
                                        </Text>
                                    </Row>
                                </Panel>
                            )}

                            <Column justifyContent="flex-end">
                                <ButtonAction onPress={createGroup}>
                                    {t('create-group')}
                                </ButtonAction>
                            </Column>
                        </VStack>
                    </Page.Content>
                </ScrollView>
            </Page>

            <PaymentRecurrency isOpen={isOpen} onClose={onToggle} />

            <ProgressStatus
                onComplete={goToPageGroups}
                onTitle={(status) => {
                    if (status === 'success') {
                        return t('createCreatedSuccessfully');
                    }
                    return undefined;
                }}
            />
        </>
    );
});
