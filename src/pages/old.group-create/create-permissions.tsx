import React from 'react';

import { Feather } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { Text, Icon, Row } from 'native-base';

import { Page } from 'components/page';
import { Panel } from 'components/panel';
import { Title } from 'modules/compounds/title';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';

export const PageGroupCreatePermission = () => {
    const { navigate } = useNavigation<NavigationPagesStack>();
    return (
        <Page>
            <Page.AppBar
                actions={[
                    <Page.AppBarAction
                        key={1}
                        onPress={() => navigate('CreateGroupCustomize')}>
                        Skip
                    </Page.AppBarAction>,
                ]}
            />

            <Page.Content>
                <Title>Permissions</Title>
                <Text mt={1} color="gray.400" fontSize="sm">
                    Set who can only deposit and who can both deposit and
                    withdraw. You can always change this later
                </Text>

                <Panel mt={12}>
                    <Row>
                        <Icon
                            size={6}
                            ml={1}
                            color="amber.400"
                            as={<Feather name="alert-triangle" />}
                        />
                        <Text ml={3} color="gray.500">
                            This functionality is still under construction.
                            Click &apos;Skip&apos;
                        </Text>
                    </Row>
                </Panel>
            </Page.Content>
        </Page>
    );
};
