import React, { useCallback, useState } from 'react';

import { view } from '@risingstack/react-easy-state';
import { Input, FormControl, Text, VStack, Fade, Center } from 'native-base';
import { useController, useForm } from 'react-hook-form';
import NumberFormat from 'react-number-format';

import { ButtonAction } from 'components/button-action';
import { SelectField } from 'components/form/select-field/select-field';
import { PanelBox } from 'components/panel';
import { Contribution_Types_Enum } from 'modules/api-graphql/api-react-query';

import { createStore } from './create-store';
import { contribTypes, CreateGroup } from './create-types';

type CreateGroupCustomContribProps = {
    onClose: () => void;
};
export const CreateGroupCustomContribs = view(
    ({ onClose }: CreateGroupCustomContribProps) => {
        const {
            control,
            setError,
            clearErrors,
            formState: { errors },
        } = useForm<CreateGroup>();

        const [contribNum, setContribNum] = useState<number>();

        const {
            field: { value: contribType, onChange: onContribTypeChange },
        } = useController<CreateGroup, 'contribType'>({
            name: 'contribType',
            defaultValue: createStore.values.contributionType,
            control,
        });

        const {
            field: { value: contribAmount, onChange: onContribValueChange },
        } = useController<CreateGroup, 'contribAmount'>({
            name: 'contribAmount',
            defaultValue: createStore.values.contributionAmount,
            control,
        });

        const isInvalidAmount = () =>
            contribType !== Contribution_Types_Enum.AnyAmount &&
            (contribAmount === undefined ||
                Number.isNaN(contribAmount) ||
                Number(contribAmount) <= 0);

        const handleOnChangeContribValue = (text: string) => {
            if (text && isInvalidAmount) {
                clearErrors();
            }
            onContribValueChange(text);
        };

        const submitValues = () => {
            setError('contribAmount', {
                message: isInvalidAmount()
                    ? 'Contribution must be greater than zero'
                    : undefined,
            });

            if (!isInvalidAmount()) {
                createStore.values.contributionType = contribType;
                createStore.values.contributionAmount = contribNum!;
                onClose();
            }
        };

        return (
            <VStack flex={1} space={10} px={4} pt={6}>
                <PanelBox>
                    <Center>
                        <Text fontSize="sm" color="gray.400">
                            <Text color="gray.500" fontWeight="700">
                                Determine
                            </Text>{' '}
                            how much participants to this group can pay.
                        </Text>
                    </Center>
                </PanelBox>

                <PanelBox>
                    <Text ml={4} fontSize="xs" color="gray.400">
                        Select
                    </Text>
                    <SelectField<Contribution_Types_Enum>
                        items={contribTypes}
                        value={contribType}
                        onChange={(val) => onContribTypeChange(val)}
                        dropdownIcon={<Text color="blue.500">change</Text>}
                    />
                </PanelBox>

                <Fade
                    in={[
                        Contribution_Types_Enum.MinimumAmount,
                        Contribution_Types_Enum.FixAmount,
                    ].includes(contribType)}>
                    <PanelBox>
                        <FormControl
                            isRequired
                            isInvalid={!!errors?.contribAmount?.message}>
                            <FormControl.Label>Enter amount</FormControl.Label>
                            <NumberFormat
                                value={contribAmount}
                                displayType="text"
                                thousandSeparator
                                prefix="$"
                                allowNegative={false}
                                onValueChange={({ floatValue }) => {
                                    setContribNum(floatValue);
                                }}
                                renderText={useCallback(
                                    InputField({
                                        onChangeText:
                                            handleOnChangeContribValue,
                                    }),
                                    []
                                )}
                            />
                            <FormControl.ErrorMessage>
                                {errors?.contribAmount?.message}
                            </FormControl.ErrorMessage>
                        </FormControl>
                    </PanelBox>
                </Fade>

                <ButtonAction mb={6} onPress={submitValues}>
                    Save
                </ButtonAction>
            </VStack>
        );
    }
);

type InputFieldProps = {
    onChangeText: (text: string) => void;
};
const InputField =
    ({ onChangeText }: InputFieldProps) =>
    (value: string) =>
        (
            <Input
                variant="filled"
                textAlign="right"
                value={value}
                color="gray.500"
                keyboardType="decimal-pad"
                onChangeText={onChangeText}
                placeholder="$0"
            />
        );
