import { Contribution_Types_Enum } from 'modules/api-graphql/api-react-query';

export type ContributionTypeKeys = `${Contribution_Types_Enum}`;

export type CreateGroup = {
    name: string;
    contribType: Contribution_Types_Enum;
    contribAmount: number;
};

export const contribTypes: {
    [name in ContributionTypeKeys]: string;
} = {
    AnyAmount: 'Any amount',
    MinimumAmount: 'Minimum amount',
    FixAmount: 'Fix amount',
};
