import {
    Contribution_Types_Enum,
    Group_Recurrencies_Enum,
} from 'modules/api-graphql/api-react-query';

export const hasContribValue = (value: Contribution_Types_Enum) =>
    value !== Contribution_Types_Enum.AnyAmount;

export const hasReccurency = (
    value: Group_Recurrencies_Enum,
    except?: Group_Recurrencies_Enum
) =>
    (
        [
            Group_Recurrencies_Enum.Weekly,
            Group_Recurrencies_Enum.Monthly,
            Group_Recurrencies_Enum.Daily,
        ] as Group_Recurrencies_Enum[]
    )
        .filter((v) => (except ? v !== except : true))
        .includes(value);

const resolveValueColor =
    <T>(resolver: (value: T) => boolean) =>
    (value: T, variant: '500' | '600' = '500') =>
        resolver(value) ? `orange.${variant}` : 'gray.400';

export const resolveContribColor = resolveValueColor(hasContribValue);

export const resolveRecurrencyColor = resolveValueColor(hasReccurency);
