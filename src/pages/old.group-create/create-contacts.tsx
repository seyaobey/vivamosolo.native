import React from 'react';

import { Feather } from '@expo/vector-icons';
import { Input, Text, Icon, Row } from 'native-base';

import { Page } from 'components/page';
import { Panel } from 'components/panel';
import { Scrollbox } from 'components/scrollbox';
import { Title } from 'modules/compounds/title';
import { useGroupStepsMachine } from 'pages/old.group-steps/group-steps-master-provider';

export const GroupContacts = () => {
    const { send } = useGroupStepsMachine();
    return (
        <Page>
            <Scrollbox>
                <>
                    <Page.AppBar
                        onGoBack={() => send('go_back')}
                        actions={[
                            <Page.AppBarAction
                                key={1}
                                onPress={() => send('go_next_step')}>
                                Skip
                            </Page.AppBarAction>,
                        ]}
                    />

                    <Page.Content>
                        <Title>Select participants</Title>

                        <Text color="gray.400" fontSize="sm">
                            Select <Text color="gray.500">participants</Text>{' '}
                            from your contacts list and start saving together
                        </Text>

                        <Input
                            mt={7}
                            variant="filled"
                            placeholder="Search"
                            InputLeftElement={
                                <Icon
                                    size={6}
                                    ml={1}
                                    color="gray.400"
                                    as={<Feather name="search" />}
                                />
                            }
                        />

                        <Panel mt={12}>
                            <Row>
                                <Icon
                                    size={6}
                                    ml={1}
                                    color="amber.400"
                                    as={<Feather name="alert-triangle" />}
                                />
                                <Text flex={1} ml={3} color="gray.500">
                                    This functionality is still under
                                    constructor. Click &apos;Skip&apos;
                                </Text>
                            </Row>
                        </Panel>
                    </Page.Content>
                </>
            </Scrollbox>
        </Page>
    );
};
