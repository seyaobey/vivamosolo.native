import { useMemo } from 'react';

import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { useAuthenticationMachine } from 'machines/authentication';
import { Credentials } from 'types';

type AuthMode = 'login' | 'register';

export const useAuthentication = () => {
    const [t] = useTranslation();
    const { state, send } = useAuthenticationMachine();

    const { control, handleSubmit } = useForm<Credentials>({
        defaultValues: {},
    });

    const mode = useMemo<AuthMode>(
        () =>
            state.value === 'mounting' ||
            state.value === 'login' ||
            state.value === 'logging_user'
                ? 'login'
                : 'register',
        [state.value]
    );

    const processing = useMemo(
        () =>
            state.value === 'logging_user' ||
            state.value === 'registering_user',
        [state.value]
    );

    const authenticate = handleSubmit((data) => {
        send({
            type: mode === 'login' ? 'login_user_event' : 'register_user_event',
            data,
        });
    });

    const switchPage = () => {
        send(mode === 'login' ? 'switch_to_register' : 'switch_to_login');
    };

    return {
        t,
        mode,
        state,
        control,
        processing,
        switchPage,
        handleSubmit,
        authenticate,
    };
};
