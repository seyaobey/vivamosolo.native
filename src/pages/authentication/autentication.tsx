import React from 'react';

import { Feather, MaterialIcons } from '@expo/vector-icons';
import {
    Button,
    Box,
    Center,
    Column,
    KeyboardAvoidingView,
    VStack,
    Text,
} from 'native-base';
import { Control } from 'react-hook-form';
import { Platform } from 'react-native';

import { ButtonAction } from 'components/button-action';
import { InputField } from 'components/form/input-field';
import { Page } from 'components/page';
import { Scrollbox } from 'components/scrollbox';
import { Title } from 'modules/compounds/title';
import { Credentials } from 'types';

import { useAuthentication } from './use-authentication';

export const Authentication = () => {
    const { control, mode, processing, t, authenticate, switchPage } =
        useAuthentication();
    const OuterWrap = getOuterWrap()!;
    const InnerWrap = getInnerWrap()!;
    return (
        <OuterWrap>
            <Page>
                <Page.Content mx={6}>
                    <Scrollbox>
                        <Box mt={10} mb={10}>
                            <Welcome />
                        </Box>
                        <InnerWrap>
                            <VStack space={8}>
                                {mode === 'register' && (
                                    <RegisterCredentials control={control} />
                                )}
                                <LoginCredentials control={control} />
                            </VStack>
                            <VStack mt={16}>
                                <AuthenticationButton
                                    caption={
                                        mode === 'login' ? 'Login' : 'Register'
                                    }
                                    isLoading={processing}
                                    onPress={authenticate}
                                />
                                <ButtonSwitchMode
                                    caption={t(
                                        mode === 'login' ? 'register' : 'login'
                                    )}
                                    message={t(
                                        mode === 'login'
                                            ? 'registerInstead'
                                            : 'loginInstead'
                                    )}
                                    onPress={switchPage}
                                />
                            </VStack>
                        </InnerWrap>
                    </Scrollbox>
                </Page.Content>
            </Page>
        </OuterWrap>
    );
};

type CredentialsProps = {
    control: Control<Credentials>;
};
const LoginCredentials = ({ control }: CredentialsProps) => (
    <VStack space={8}>
        <InputField
            name="email"
            placeholder="Email"
            variant="filled"
            control={control}
            keyboardType="email-address"
            rules={{ required: 'email is required' }}
            leftIcon={<MaterialIcons name="alternate-email" />}
        />
        <InputField
            name="password"
            placeholder="Password"
            variant="filled"
            control={control}
            password
            rules={{ required: 'password is required' }}
            leftIcon={<MaterialIcons name="lock" />}
        />
    </VStack>
);

const RegisterCredentials = ({ control }: CredentialsProps) => (
    <VStack space={8}>
        <InputField
            name="name"
            placeholder="name"
            variant="filled"
            control={control}
            rules={{ required: 'name is required' }}
            leftIcon={<Feather name="edit-3" />}
        />
        <InputField
            name="surname"
            placeholder="Surname"
            variant="filled"
            control={control}
            rules={{
                required: 'surname is required',
            }}
            leftIcon={<Feather name="edit-3" />}
        />
    </VStack>
);

type AuthenticationButtonProps = {
    caption: string;
    isLoading: boolean;
    onPress: () => void;
};
const AuthenticationButton = ({
    caption,
    isLoading,
    onPress,
}: AuthenticationButtonProps) => (
    <ButtonAction size="lg" isLoading={isLoading} onPress={onPress}>
        {caption}
    </ButtonAction>
);

type ButtonSwitchModeProps = {
    caption: string;
    message: string;
    onPress: () => void;
};
const ButtonSwitchMode = ({
    caption,
    message,
    onPress,
}: ButtonSwitchModeProps) => (
    <Center mt={4}>
        <Text>{message}</Text>
        <Button
            variant="ghost"
            onPress={onPress}
            _text={{
                color: 'main',
                fontWeight: 400,
            }}
            _pressed={{
                bg: 'gray.200',
            }}>
            {caption}
        </Button>
    </Center>
);

const Welcome = () => (
    <VStack>
        <Title fontSize="2xl">Welcome to</Title>
        <Title fontSize="4xl" color="main_dark">
            Viva Mosolo
        </Title>
    </VStack>
);

type OuterProps = {
    children: React.ReactNode;
};
const getOuterWrap = () =>
    Platform.select({
        ios: ({ children }: OuterProps) => (
            <KeyboardAvoidingView flex={1} behavior="padding">
                {children}
            </KeyboardAvoidingView>
        ),
        android: ({ children }: OuterProps) => <>{children}</>,
    });

const getInnerWrap = () =>
    Platform.select({
        ios: ({ children }: OuterProps) => (
            <Column mb={8} justifyContent="center">
                {children}
            </Column>
        ),

        android: ({ children }: OuterProps) => (
            <KeyboardAvoidingView
                behavior="padding"
                mb={8}
                justifyContent="center">
                {children}
            </KeyboardAvoidingView>
        ),
    });
