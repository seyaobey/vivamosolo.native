import React from 'react';

import { Page } from 'components/page';
import { Scrollbox } from 'components/scrollbox';

export type IMessagesProps = {};

const Messages: React.FC<IMessagesProps> = ({}) => (
    <Page bg="white">
        <Scrollbox mt={4}>
            <Page.Content bg="white">
                <Page.Title>Messages</Page.Title>
            </Page.Content>
        </Scrollbox>
    </Page>
);

export { Messages };
