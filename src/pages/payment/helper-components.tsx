import React from 'react';

import { Input } from 'native-base';

type AmountFieldProps = {
    onChange: (text: string) => void;
};

export const getAmountField =
    ({ onChange }: AmountFieldProps) =>
    (value: string) =>
        (
            <Input
                variant="filled"
                textAlign="right"
                value={value}
                keyboardType="decimal-pad"
                onChangeText={onChange}
                placeholder="$0"
            />
        );
