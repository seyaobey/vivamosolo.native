import React, { useMemo } from 'react';

import {
    FormControl,
    HStack,
    Input,
    Pressable,
    VStack,
    Text,
    Box,
} from 'native-base';
import { useForm, useController } from 'react-hook-form';
import NumberFormat from 'react-number-format';

import { ButtonAction } from 'components/button-action';
import { PanelBox } from 'components/panel';
import { useXMachine } from 'machines/use-xmachine';
import { withdraw } from 'machines/withdraw';
import { useApplicationMachine } from 'modules/application-machine';

import { getAmountField } from './helper-components';

type WithdrawPaymentData = {
    strAmount: string;
    numAmount?: number;
    phoneNumber?: string;
};

type WithdrawSwipeableFormProps = {
    ctx?: ReturnType<typeof useWithdrawSwipeableForm>;
};

export const WithdrawSwipeableForm = ({
    ctx = useWithdrawSwipeableForm(),
}: WithdrawSwipeableFormProps) => (
    <VStack mx={4} space={8}>
        <PanelBox>
            <Text fontSize="2xl" fontWeight="light">
                Send money
            </Text>
        </PanelBox>
        <PanelBox>
            <FormControl
                isRequired
                isInvalid={!!ctx.errors?.strAmount?.message}>
                <FormControl.Label
                    _text={{
                        fontWeight: 'light',
                    }}>
                    Amount
                </FormControl.Label>
                <NumberFormat
                    value={ctx.amount}
                    displayType="text"
                    thousandSeparator
                    prefix="$"
                    allowNegative={false}
                    onValueChange={({ floatValue }) => {
                        ctx.setValue('numAmount', Number(floatValue));
                    }}
                    renderText={getAmountField({
                        onChange: ctx.onAmountChange,
                    })}
                />
                <FormControl.ErrorMessage>
                    {ctx.errors?.strAmount?.message}
                </FormControl.ErrorMessage>
            </FormControl>
        </PanelBox>
        <PanelBox>
            <FormControl isRequired>
                <FormControl.Label
                    _text={{
                        fontWeight: 'light',
                    }}>
                    Recipient phone number
                </FormControl.Label>
                <Input
                    variant="filled"
                    value={ctx.phoneNumber as string}
                    keyboardType="phone-pad"
                    isInvalid={!!ctx.errors?.phoneNumber?.message}
                    placeholder="Enter the recipient phone number"
                    onChangeText={ctx.onPhoneNumberChange}
                />
            </FormControl>
            <HStack mt={4} justifyContent="flex-end">
                <Pressable
                    onPress={() =>
                        withdraw.send({
                            type: 'OPEN_CONTACTS',
                            amount: ctx.amount! as unknown as number,
                        })
                    }>
                    <Text color="blue.500">Open contact list</Text>
                </Pressable>
            </HStack>
        </PanelBox>
        <Box my={6} h={100}>
            <ButtonAction onPress={ctx.makePayment} isLoading={ctx.loading}>
                Send
            </ButtonAction>
        </Box>
    </VStack>
);

const useWithdrawSwipeableForm = () => {
    const { state, send } = useXMachine(withdraw);
    const { app } = useApplicationMachine();

    const loading = useMemo(() => state.matches('processing'), [state.value]);

    const {
        control,
        setValue,
        handleSubmit,
        formState: { errors },
    } = useForm<WithdrawPaymentData>({
        defaultValues: {
            strAmount: state.context.amount
                ? state.context.amount.toString()
                : (undefined as unknown as string),
            numAmount: state.context.amount,
            phoneNumber: state.context.recipientPhone,
        },
    });

    const {
        field: { value: amount, onChange: onAmountChange },
    } = useController<WithdrawPaymentData>({
        name: 'strAmount',
        control,
        rules: {
            required: {
                value: true,
                message: 'Payment amount is required',
            },
            min: {
                value: 1,
                message: 'Payment amount is not valid',
            },
        },
    });

    const {
        field: { value: phoneNumber, onChange: onPhoneNumberChange },
    } = useController<WithdrawPaymentData>({
        name: 'phoneNumber',
        control,
        rules: {
            required: {
                value: true,
                message: 'Recipient phone number is required',
            },
        },
    });

    const makePayment = handleSubmit((data: WithdrawPaymentData) => {
        const group = app.state.context.groups.find(
            (g) => g.id === app.state.context.groupId
        )!;
        send({ type: 'MAKE_PAYMENT', group, amount: data.numAmount! });
    });

    return {
        amount,
        errors,
        loading,
        phoneNumber,
        setValue,
        makePayment,
        onAmountChange,
        onPhoneNumberChange,
    };
};
