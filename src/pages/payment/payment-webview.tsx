import React from 'react';

import { AntDesign } from '@expo/vector-icons';
import { Icon } from 'native-base';
import { WebView } from 'react-native-webview';

import { Page } from 'components/page';
import { usePaymentMachine } from 'machines/payment';
import { Title } from 'modules/compounds/title';

export const PaymentWebView = () => {
    const { state, send } = usePaymentMachine();
    return (
        <Page>
            <Page.AppBar
                hideBackBtn
                actions={[
                    <Page.AppBarAction
                        key={1}
                        onPress={() => {
                            send('EXIT_PAYMENT_WEBVIEW');
                        }}>
                        <Icon color="main" as={<AntDesign name="close" />} />
                    </Page.AppBarAction>,
                ]}
            />
            <Page.Content>
                <Title mb={10}>Add money</Title>
                <WebView
                    source={{ uri: state.context.url }}
                    scalesPageToFit
                    style={{ flex: 1 }}
                />
            </Page.Content>
        </Page>
    );
};
