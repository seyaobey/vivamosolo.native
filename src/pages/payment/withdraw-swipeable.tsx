import React, { useMemo } from 'react';

import { Swipeable } from 'components/swipeable';
import { useXMachine } from 'machines/use-xmachine';
import { withdraw } from 'machines/withdraw';

import { WithdrawSwipeableForm } from './withdraw-swipeable-form';

type WithdrawSwipeableProps = {
    ctx?: ReturnType<typeof useWithdrawSwipeable>;
};

export const WithdrawSwipeable = ({
    ctx = useWithdrawSwipeable(),
}: WithdrawSwipeableProps) => (
    <Swipeable
        closable
        isOpen={ctx.open}
        onClose={ctx.onClose}
        onClosed={ctx.onClose}>
        {ctx.open ? <WithdrawSwipeableForm /> : <></>}
    </Swipeable>
);

const useWithdrawSwipeable = () => {
    const { state } = useXMachine(withdraw);
    const open = useMemo(
        () => state.matches('active') || state.matches('processing'),
        [state.value]
    );

    const onClose = () => {
        withdraw.send('RESET_WITHDRAW');
    };

    return {
        open,
        onClose,
    };
};
