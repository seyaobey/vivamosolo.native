import { useEffect, useMemo } from 'react';

import { useController, useForm } from 'react-hook-form';

import { usePaymentMachine } from 'machines/payment';
import { Group_Recurrencies_Enum } from 'modules/api-graphql/api-react-query';
import { auth } from 'modules/application';
import { useApplicationMachine } from 'modules/application-machine';

type PaymentAmountValue = {
    strAmount: string;
    numAmount: number;
};

export const usePaymentSwipeable = () => {
    const { state, send, active } = usePaymentMachine();
    const { app } = useApplicationMachine();

    const {
        control,
        reset,
        setValue,
        handleSubmit,
        formState: { errors },
    } = useForm<PaymentAmountValue>({
        defaultValues: { numAmount: 0 },
    });

    const {
        field: { value, onChange },
    } = useController<PaymentAmountValue>({
        name: 'strAmount',
        control,
        rules: {
            required: {
                value: true,
                message: 'Payment amount is required',
            },
            min: {
                value: 1,
                message: 'Payment amount is not valid',
            },
        },
    });

    const processing = useMemo(
        () => state.matches('payment_url_requesting'),
        [state.value]
    );

    const onClose = () => send('EXIT_PAYMENT');

    const requestUrl = handleSubmit((data: PaymentAmountValue) => {
        const group = app.state.context.groups.find(
            (g) => g.id === app.state.context.groupId
        )!;

        const memberId = group.members.find(
            (m) => m.user.id === auth.user()?.id
        )!.id;

        const period =
            group.group_recurrency_type !== Group_Recurrencies_Enum.None
                ? group.periods[0]
                : undefined;

        send({
            type: 'REQUEST_URL',
            data: {
                amount: data.numAmount,
                group_id: app.state.context.groupId,
                member_id: memberId,
                period_id: period?.id,
            },
        });
    });

    useEffect(() => {
        if (!active) {
            reset();
        }
    }, [active]);

    return {
        open: active,
        value,
        errors,
        onClose,
        onChange,
        setValue,
        requestUrl,
        processing,
    };
};
