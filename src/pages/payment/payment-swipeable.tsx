import React from 'react';

import { FormControl, VStack, Box, Text } from 'native-base';
import NumberFormat from 'react-number-format';

import { ButtonAction } from 'components/button-action';
import { PanelBox } from 'components/panel';
import { Swipeable } from 'components/swipeable';

import { getAmountField } from './helper-components';
import { usePaymentSwipeable } from './use-payment-swipeable';

export type IPaymentSwipeableProps = {
    ctx?: ReturnType<typeof usePaymentSwipeable>;
};

const PaymentSwipeable: React.FC<IPaymentSwipeableProps> = ({
    ctx = usePaymentSwipeable(),
}) => (
    <Swipeable
        isOpen={ctx.open}
        closable
        onClose={ctx.onClose}
        onClosed={ctx.onClose}>
        <VStack mx={4} space={8}>
            <PanelBox mb={2}>
                <Text fontSize="2xl" fontWeight="light">
                    Add money
                </Text>
            </PanelBox>

            <PanelBox>
                <FormControl
                    isRequired
                    isInvalid={!!ctx.errors?.strAmount?.message}>
                    <FormControl.Label
                        _text={{
                            fontWeight: 'light',
                        }}>
                        Enter the payment amount
                    </FormControl.Label>
                    <NumberFormat
                        value={ctx.value}
                        displayType="text"
                        thousandSeparator
                        prefix="$"
                        allowNegative={false}
                        onValueChange={({ floatValue }) => {
                            ctx.setValue('numAmount', Number(floatValue));
                        }}
                        renderText={getAmountField({ onChange: ctx.onChange })}
                    />
                    <FormControl.ErrorMessage>
                        {ctx.errors?.strAmount?.message}
                    </FormControl.ErrorMessage>
                </FormControl>
            </PanelBox>

            <Box my={6} h={100}>
                <ButtonAction
                    onPress={ctx.requestUrl}
                    isLoading={ctx.processing}>
                    Make payment
                </ButtonAction>
            </Box>
        </VStack>
    </Swipeable>
);

export { PaymentSwipeable };
