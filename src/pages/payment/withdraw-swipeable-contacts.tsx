import React, { useMemo } from 'react';

import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import {
    FindContacts,
    SelectContact,
} from 'components/find-contacts/find-contacts';
import { Swipeable } from 'components/swipeable';
import { contacts } from 'machines/contacts';
import { useXMachine } from 'machines/use-xmachine';
import { withdraw } from 'machines/withdraw';

type WithdrawSwipeableContactsProps = {
    ctx?: ReturnType<typeof useWithdrawContacts>;
};
export const WithdrawSwipeableContacts = ({
    ctx = useWithdrawContacts(),
}: WithdrawSwipeableContactsProps) => (
    <Swipeable
        topOffset={hp(2)}
        isOpen={ctx.open}
        closable
        onClose={ctx.onClosed}
        onClosed={ctx.onClosed}>
        {ctx.open && (
            <FindContacts title="Select a contact" onSelect={ctx.onSelect} />
        )}
    </Swipeable>
);

const useWithdrawContacts = () => {
    const { state } = useXMachine(withdraw);

    const open = useMemo(() => state.matches('contacts'), [state.value]);

    const onClosed = () => {
        withdraw.send('CLOSE_CONTACTS');
        contacts.send('RESET_CONTACTS');
    };

    const onSelect: SelectContact = (recipient) =>
        withdraw.send({
            type: 'RECIPIENT_PICKED',
            recipientPhone: recipient.phoneNumbers![0].number!,
            recipientName: recipient.name,
        });

    return {
        open,
        state,
        onClosed,
        onSelect,
    };
};
