import React from 'react';

import { MaterialCommunityIcons } from '@expo/vector-icons';
import {
    Box,
    Text,
    IconButton,
    Stagger,
    useDisclose,
    // ZStack,
    HStack,
    Flex,
} from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';

import { useApplicationMachine } from 'modules/application-machine';

export type IListingActionsProps = {};

const ListingActions: React.FC<IListingActionsProps> = ({}) => {
    const { isOpen, onToggle } = useDisclose();
    const { steps } = useApplicationMachine();

    const startSteps = () => {
        onToggle();
        steps.startSteps();
    };

    return (
        <Box>
            <HStack justifyContent="flex-end">
                <IconButton
                    variant="solid"
                    rounded="full"
                    size="lg"
                    shadow={2}
                    bg="main_dark"
                    onPress={onToggle}
                    _pressed={{
                        bg: 'main_dark',
                    }}
                    icon={
                        <MaterialCommunityIcons
                            color="white"
                            size={24}
                            name="plus"
                        />
                    }
                />
            </HStack>

            <Box mr={145} pos="relative">
                <>
                    <Flex
                        alignItems="flex-end"
                        pos="absolute"
                        mt={4}
                        minH={220}
                        zIndex={1000}>
                        <Stagger
                            visible={isOpen}
                            initial={{
                                opacity: 0,
                                scale: 0,
                                translateY: -34,
                            }}
                            animate={{
                                translateY: 0,
                                scale: 1,
                                opacity: 1,
                                transition: {
                                    type: 'spring',
                                    mass: 0.8,
                                    stagger: {
                                        offset: 30,
                                        reverse: false,
                                    },
                                },
                            }}
                            exit={{
                                translateY: 34,
                                scale: 0.5,
                                opacity: 0,
                                transition: {
                                    duration: 100,
                                    stagger: {
                                        offset: 30,
                                        reverse: false,
                                    },
                                },
                            }}>
                            <ActionItem
                                icon="account-multiple-plus"
                                title="New group"
                                onPress={startSteps}
                            />
                            <ActionItem
                                icon="account-arrow-right"
                                title="Join group"
                                onPress={onToggle}
                            />
                        </Stagger>
                    </Flex>
                </>
            </Box>
        </Box>
    );
};

type ActionItemProps = {
    title: string;
    bg?: string;
    bgPressed?: string;
    onPress: () => void;
    icon: React.ComponentProps<typeof MaterialCommunityIcons>['name'];
};
const ActionItem = ({
    icon,
    title,
    bg = 'main',
    bgPressed = 'main_dark',
    onPress,
}: ActionItemProps) => (
    <TouchableOpacity onPress={onPress} activeOpacity={0.9}>
        <HStack alignItems="center">
            <Box
                bg="white"
                justifyContent="center"
                alignItems="center"
                mb={5}
                mr={2}
                py={1}
                px={2}
                h={36}
                shadow={4}
                borderRadius={6}>
                <Text color="accent" fontSize="md">
                    {title}
                </Text>
            </Box>
            <IconButton
                mb={4}
                shadow={6}
                variant="solid"
                rounded="full"
                bg={bg}
                _pressed={{
                    bg: bgPressed,
                }}
                icon={
                    <MaterialCommunityIcons
                        size={24}
                        name={icon}
                        color="white"
                    />
                }
            />
        </HStack>
    </TouchableOpacity>
);

export { ListingActions };
