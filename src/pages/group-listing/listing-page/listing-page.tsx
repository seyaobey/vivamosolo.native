/* eslint-disable no-nested-ternary */
import React, { useRef } from 'react';

import { HStack, Divider } from 'native-base';
import { Animated } from 'react-native';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { Page } from 'components/page';
import { appTheme } from 'modules/application-theme/app-theme';

import { ListingActions } from '../listing-actions/listing-actions';
import { ListingSubscription } from '../listing-subscription';
import { ListingItems } from './list-items';

export type IGroupListingPageProps = {};

const GroupListingPage: React.FC<IGroupListingPageProps> = ({}) => {
    const scrolling = useRef(new Animated.Value(50)).current;

    const headerAnim = scrolling.interpolate({
        inputRange: [60, 90, 120],
        outputRange: [hp(5), hp(5) - 7, hp(5) - 14],
        extrapolate: 'clamp',
    });

    return (
        <Page>
            <AnimatedHeader animation={headerAnim}>
                <HStack
                    m={4}
                    px={1}
                    alignItems="center"
                    justifyContent="space-between">
                    <Page.Title>My groups</Page.Title>
                    <ListingActions />
                </HStack>
                <Divider size={1} bg="animated_header.divider_bg" />
            </AnimatedHeader>

            <AnimatedScrollView scrolling={scrolling}>
                <Page.Content mx={4}>
                    <HStack
                        zIndex={appTheme.space.animated_scrollview.zIndex}
                        mb="animated_scrollview.mb">
                        <ListingItems />
                    </HStack>
                </Page.Content>
            </AnimatedScrollView>
            <ListingSubscription />
        </Page>
    );
};

type AnimatedHeaderProps = {
    animation: Animated.AnimatedInterpolation;
    children: React.ReactNode;
};
const AnimatedHeader = ({ animation, children }: AnimatedHeaderProps) => (
    <Animated.View
        style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            zIndex: 10,
            backgroundColor: appTheme.colors.animated_scrollview.bg,
            transform: [{ translateY: animation }],
        }}>
        <>{children}</>
    </Animated.View>
);

type AnimatedScrollViewProps = {
    scrolling: Animated.Value;
    children: React.ReactNode;
};
const AnimatedScrollView = ({
    scrolling,
    children,
}: AnimatedScrollViewProps) => (
    <Animated.ScrollView
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
            [
                {
                    nativeEvent: {
                        contentOffset: {
                            y: scrolling,
                        },
                    },
                },
            ],
            { useNativeDriver: true }
        )}
        style={[
            {
                paddingTop: 75,
                backgroundColor: appTheme.colors.animated_scrollview.bg,
            },
        ]}>
        {children}
    </Animated.ScrollView>
);
export { GroupListingPage };
