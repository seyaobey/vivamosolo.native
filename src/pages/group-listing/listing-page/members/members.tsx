import React from 'react';

import { Feather } from '@expo/vector-icons';
import { Text, HStack, Icon } from 'native-base';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import { TextStack } from 'components/text-stack';
import { GroupDtoFragment } from 'modules/api-graphql/api-apollo';

import { MemberIcon } from './member-icon';

type MembersProps = {
    item: GroupDtoFragment;
};
export const Members = ({ item }: MembersProps) => (
    <HStack alignItems="center">
        <TextStack title="members">
            <HStack>
                <ContentText fontWeight="normal" mr={1}>
                    {item.members.length}
                </ContentText>
                <ContentIcon
                    name={item.members.length > 1 ? 'users' : 'user'}
                />
            </HStack>
        </TextStack>
        <HStack ml={2}>
            {item.members.map((m) => (
                <MemberIcon member={m} key={m.id} />
            ))}
        </HStack>
    </HStack>
);

type ContentTextProps = React.ComponentProps<typeof Text>;
export const ContentText = ({ fontSize = 'sm', ...rest }: ContentTextProps) => (
    <Text fontSize={fontSize} {...rest} />
);

type ContentIconProps = {
    name: React.ComponentProps<typeof Feather>['name'];
};
export const ContentIcon = ({ name }: ContentIconProps) => (
    <Icon size={wp(4)} as={<Feather name={name} />} />
);
