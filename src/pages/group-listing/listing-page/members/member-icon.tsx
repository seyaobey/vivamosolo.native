import React from 'react';

import { FontAwesome5 } from '@expo/vector-icons';
import { Text, Avatar, Circle, Icon } from 'native-base';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import { MemberDtoFragment } from 'modules/api-graphql/api-apollo';

type MemberProps = {
    fallback?: 'text' | 'icon';
    member: MemberDtoFragment;
};
export const MemberIcon = ({ member, fallback = 'text' }: MemberProps) => {
    const circleSize = wp(12);
    const textified = fallback === 'text';

    return member.user.avatar_url ? (
        <Avatar mr={1} size={10} source={{ uri: member.user.avatar_url! }} />
    ) : (
        <Circle
            mr={1}
            p={1}
            bg={textified ? '' : 'gray.200'}
            borderWidth={textified ? 1 : 0}
            borderColor={textified ? 'main' : ''}
            size={circleSize}>
            {textified ? (
                <Text noOfLines={1} ellipsizeMode="clip">
                    {getInitials(member.user.display_name!)}
                </Text>
            ) : (
                <Icon
                    size={6}
                    color="gray.500"
                    alignItems="center"
                    justifyContent="center"
                    name="user"
                    as={FontAwesome5}
                />
            )}
        </Circle>
    );
};

const getInitials = (str: string) =>
    str
        .split(/\s/)
        .filter((s, i) => i < 1)
        .toString();
