import React, { useCallback } from 'react';

import { Pressable } from 'native-base';
import { Platform } from 'react-native';

import { GroupDtoFragment } from 'modules/api-graphql/api-apollo';
import { useApplicationMachine } from 'modules/application-machine';

import { PressableCard } from '../item-card/item-card';

type ListinItemProps = {
    item: GroupDtoFragment;
};

const ListingItem: React.FC<ListinItemProps> = ({ item }) => (
    <ListingItemContent item={item} />
);

const ListingItemContent = ({ item }: ListinItemProps) => {
    const {
        app: { send },
    } = useApplicationMachine();

    const goToItem = useCallback(() => {
        send({ type: 'listing_item', value: { groupId: item.id } });
    }, []);

    return (
        <Pressable
            mt={Platform.select({
                android: 6,
                ios: 0,
            })}
            onPress={goToItem}>
            {PressableCard({ item })}
        </Pressable>
    );
};

export { ListingItem };
