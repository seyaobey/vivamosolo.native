import React from 'react';

import { MaterialIcons } from '@expo/vector-icons';
import dateFormat from 'dateformat';
import { Box, Divider, HStack, Icon, Text } from 'native-base';
import { Platform } from 'react-native';

import { CurrencyText } from 'components/currency-text';
import { PanelBox } from 'components/panel';
import { TextStack } from 'components/text-stack';
import {
    GroupDtoFragment,
    Group_Recurrencies_Enum,
} from 'modules/api-graphql/api-apollo';
import { appTheme } from 'modules/application-theme/app-theme';

import { ContentText, Members } from '../members';
import { ItemProgress } from '../progress';

type ItemCardProps = {
    isPressed?: boolean;
    pressable?: boolean;
    item: GroupDtoFragment;
};
export const ItemCard = ({ item, isPressed, pressable }: ItemCardProps) => {
    const {
        fontSizes: {
            listing_item: {
                group_name: groupNameFontSize,
                balance: balanceFontSize,
            },
        },
    } = appTheme;

    return (
        <PanelBox
            px={4}
            my={Platform.select({
                android: 0,
                ios: 3,
            })}
            opacity={isPressed ? 0.4 : 1}>
            <HStack justifyContent="space-between">
                <TextStack title="group name">
                    <Text fontSize={groupNameFontSize}>{item.group_name}</Text>
                    <Text fontSize="xs">
                        created on{' '}
                        {dateFormat(
                            new Date(item.created_at),
                            'ddd, mmm dd'
                        ).toLowerCase()}
                    </Text>
                </TextStack>

                <HStack justifyContent="space-between">
                    <TextStack
                        title="balance"
                        {...(pressable ? {} : { alignItems: 'flex-end' })}>
                        <CurrencyText
                            value={item.group_balance}
                            render={(text) => (
                                <ContentText
                                    fontSize={balanceFontSize}
                                    color="listing_item.balance">
                                    {text}
                                </ContentText>
                            )}
                        />
                    </TextStack>
                    {pressable && (
                        <Icon
                            ml={2}
                            size={4}
                            color="gray.400"
                            name="arrow-forward-ios"
                            as={MaterialIcons}
                        />
                    )}
                </HStack>
            </HStack>

            <Box mr={pressable ? 6 : 0}>
                {item.group_recurrency_type !==
                    Group_Recurrencies_Enum.None && (
                    <>
                        <Divider my={4} />
                        <ItemProgress group={item} />
                    </>
                )}

                {pressable && (
                    <>
                        <Divider my={4} />
                        <Members item={item} />
                    </>
                )}
            </Box>
        </PanelBox>
    );
};

type PressableCardProps = {
    item: GroupDtoFragment;
};
export const PressableCard =
    ({ item }: PressableCardProps) =>
    ({ isPressed }: Pick<ItemCardProps, 'isPressed'>) =>
        <ItemCard item={item} isPressed={isPressed} pressable />;
