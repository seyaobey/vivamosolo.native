/* eslint-disable react/no-unused-prop-types */
import React from 'react';

import { FlatList, Box } from 'native-base';

import { GroupDtoFragment } from 'modules/api-graphql/api-apollo';
import { useApplicationMachine } from 'modules/application-machine';

import { ListingItem } from '../list-item';

export type IListingItemsProps = {};

const ItemSeparator = () => <Box my="listing.divider_my" />;

const ListingItems: React.FC<IListingItemsProps> = ({}) => {
    const { app } = useApplicationMachine();

    return (
        <Box flex={1}>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={app.state.context.groups}
                keyExtractor={(item: GroupDtoFragment) => item.id}
                renderItem={ListingItem}
                alwaysBounceVertical
                ItemSeparatorComponent={ItemSeparator}
            />
        </Box>
    );
};

export { ListingItems };
