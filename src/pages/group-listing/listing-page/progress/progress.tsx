/* eslint-disable no-nested-ternary */
import React from 'react';

import dateFormat from 'dateformat';
import { Box, Progress, Row, Text, VStack } from 'native-base';
import NumberFormat from 'react-number-format';

import { CurrencyText } from 'components/currency-text';
import { recurrencyLabels } from 'components/payments';
import { TextStack } from 'components/text-stack';
import {
    GroupDtoFragment,
    Group_Recurrencies_Enum,
} from 'modules/api-graphql/api-apollo';

type ItemProgressProps = {
    group: GroupDtoFragment;
};
export const ItemProgress = ({ group }: ItemProgressProps) => {
    const target =
        group.members.length * Number(group.group_contribution_amount);

    const progressValue = !target
        ? 0
        : group.periods[0].period_progression >= target && target > 0
        ? 1
        : group.periods[0].period_progression / target;

    const recurrency_type_str =
        recurrencyLabels[group.group_recurrency_type].toLowerCase();

    return (
        <Box>
            <Row justifyContent="space-between">
                <TextStack title={`${recurrency_type_str} target`}>
                    <CurrencyText
                        value={target}
                        render={(text) => (
                            <Text color="lightBlue.600" fontWeight="semibold">
                                {text}
                            </Text>
                        )}
                    />
                </TextStack>

                <TextStack title="collected" alignItems="center">
                    <CurrencyText
                        value={group.periods[0].period_progression}
                        render={(text) => (
                            <Text color="green.600" fontWeight="semibold">
                                {text}
                            </Text>
                        )}
                    />
                </TextStack>

                {group.group_recurrency_type !==
                    Group_Recurrencies_Enum.None && (
                    <TextStack title="deadline" alignItems="flex-end">
                        <Text color="lightBlue.600" fontWeight="semibold">
                            {dateFormat(
                                new Date(group.periods[0].period_completed_at),
                                'ddd, mmm dd'
                            ).toLowerCase()}
                        </Text>
                    </TextStack>
                )}
            </Row>

            <VStack space={1} mt={2}>
                <NumberFormat
                    value={progressValue * 100}
                    displayType="text"
                    thousandSeparator
                    decimalSeparator="."
                    decimalScale={2}
                    fixedDecimalScale
                    suffix="%"
                    renderText={(text) => (
                        <Text color="gray.400" fontSize="xs" textAlign="center">
                            {recurrency_type_str} progress: {text}
                        </Text>
                    )}
                />
                <Progress value={progressValue * 100} width="100%" />
            </VStack>
        </Box>
    );
};
