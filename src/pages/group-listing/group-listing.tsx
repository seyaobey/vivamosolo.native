import React from 'react';

import { GroupListingPage } from './listing-page';

export type IGroupListingProps = {};

const GroupListing: React.FC<IGroupListingProps> = ({}) => <GroupListingPage />;

export { GroupListing };
