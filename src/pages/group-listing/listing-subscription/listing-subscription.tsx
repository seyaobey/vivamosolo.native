import React from 'react';

import { useGroupsSubscription } from 'modules/api-graphql/api-apollo';
import { auth } from 'modules/application';
import { useApplicationMachine } from 'modules/application-machine';

export type IListingSubscriptionProps = {};

const ListingSubscription: React.FC<IListingSubscriptionProps> = ({}) => {
    const {
        app: { send },
    } = useApplicationMachine();

    useGroupsSubscription({
        variables: { user_id: auth.user()?.id },
        onSubscriptionData: ({ subscriptionData: res }) => {
            send({
                type: 'update_groups',
                value: {
                    groups: res.data!.groups,
                },
            });
        },
    });

    return <></>;
};

export { ListingSubscription };
