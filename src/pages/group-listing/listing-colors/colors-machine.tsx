/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable prettier/prettier */
import React, { createContext, useContext, useEffect } from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { useMachine } from '@xstate/react';
import { theme } from 'native-base';
import { createMachine } from 'xstate';
import { createModel } from 'xstate/lib/model';

import { TypeStatify } from 'helpers/types';

export type ColorValue = {
    id: string;
    color: string;
};
type Context = {
    colors: ColorValue[];
};

type Events =
    | { type: 'MOUNTED'; value: { colors: ColorValue[] } }
    | { type: 'INITIALIZED'; value: { colors: ColorValue[] } }
    | { type: 'READY' };

type TypeState =
    | { value: 'unmount' }
    | { value: 'mounted' }
    | { value: 'initializing' }
    | { value: 'ready' };

const model = createModel(
    {
        colors: [] as ColorValue[],
    },
    {
        events: {
            MOUNTED: (colors: ColorValue[]) => ({ value: { colors } }),
            INITIALIZED: (colors: ColorValue[]) => ({ value: { colors } }),
            READY: () => ({}),
        },
    }
);

export const colorMachine = createMachine<
    typeof model,
    Context,
    Events,
    TypeStatify<TypeState, Context>
>(
    {
        context: {
            colors: [],
        },
        initial: 'mounting',
        states: {
            mounting: {
                invoke: {
                    src: () => async (cb) => {
                        const str = await AsyncStorage.getItem(
                            '@border-colors'
                        );
                        const colors = !str ? JSON.parse(str!) : [];
                        cb({
                            type: 'MOUNTED',
                            value: {
                                colors,
                            },
                        });
                    },
                },
                on: {
                    MOUNTED: {
                        target: 'mounted',
                        actions: 'update_colors',
                    },
                },
            },

            mounted: {
                on: {
                    INITIALIZED: {
                        target: 'ready',
                        actions: 'update_colors',
                    },
                },
            },

            ready: {
                entry: (ctx) => {
                        AsyncStorage.setItem('@border-colors', JSON.stringify(ctx.colors))
                },
                type: 'final',
            },
        },
    },
    {
        actions: {
            update_colors: model.assign(
                {
                    colors: (_, e) => e.value.colors,
                },
                'INITIALIZED'
            ) as never,
        },
    }
);

const defaultColors = [
    theme.colors.indigo[300],
    theme.colors.red[300],
    theme.colors.teal[300],
    theme.colors.green[300],
    // theme.colors.yellow[300],
    theme.colors.orange[300],
    theme.colors.blue[300],
];

const randomColor = () =>
    defaultColors[Math.floor(Math.random() * defaultColors.length)];

type ColorMachineProps = {
    ids: string[];
};
const useBaseColorMachine = ({ ids }: ColorMachineProps) => {
    const [state, send] = useMachine(colorMachine);

    const initializeColors = () => {
        const { colors } = state.context;
        const newColors = ids.reduce<ColorValue[]>((acc, id) => {
            const color = colors.find((b) => b.id === id);
            return [...acc, color ?? { id, color: randomColor() }];
        }, []);

        send({ type: 'INITIALIZED', value: { colors: newColors } });
    };

    useEffect(() => {
        if (ids.length && state.matches('mounted')) {
            initializeColors();
        }
    }, [state.value, ids]);

    const getColor = (id: string) => state.context.colors.find((b) => b.id === id);

    return {
        state,
        send,
        getColor,
    };
};

const context = createContext<ReturnType<typeof useBaseColorMachine>>({} as any);

type ColorValuesProps = {
    ids: string[];
    children: React.ReactNode;
}
export const ColorValues = ({ ids, children }: ColorValuesProps) => (
    <context.Provider value={useBaseColorMachine({ ids })} >{children}</context.Provider>
)

export const useColorValues = () => useContext(context);