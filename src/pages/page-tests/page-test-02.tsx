import React from 'react';

import { useNavigation } from '@react-navigation/native';
import { Button, Center } from 'native-base';

import { Page } from 'components/page';
import { Scrollbox } from 'components/scrollbox';
import { Title } from 'modules/compounds/title';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';

export type IPageTest02Props = {};

const PageTest02: React.FC<IPageTest02Props> = ({}) => {
    const { navigate } = useNavigation<NavigationPagesStack>();
    return (
        <Page>
            <Page.AppBar />
            <Scrollbox>
                <Page.Content>
                    <Title>Test page 2</Title>
                    <Center>
                        <Button onPress={() => navigate('PageTest01')}>
                            Go to Page 1
                        </Button>
                    </Center>
                </Page.Content>
            </Scrollbox>
        </Page>
    );
};

export { PageTest02 };
