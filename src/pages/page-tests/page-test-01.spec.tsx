import React from 'react';

import { render } from 'testing-setup';

import { PageTest01 } from './page-test-01';

describe('PageTest01', () => {
    test('it should render', () => {
        const { getByTestId } = render(<PageTest01 />);
        expect(getByTestId('page')).toBeDefined();
    });
});
