import React from 'react';

import { useNavigation } from '@react-navigation/native';
import { Button, Center } from 'native-base';

import { Page } from 'components/page';
import { Scrollbox } from 'components/scrollbox';
import { Title } from 'modules/compounds/title';
import { NavigationPagesStack } from 'modules/navigation/navigation-routes';

export type IPageTest01Props = {};

const PageTest01: React.FC<IPageTest01Props> = ({}) => {
    const { navigate } = useNavigation<NavigationPagesStack>();
    return (
        <Page testID="page">
            <Page.AppBar />
            <Scrollbox>
                <Page.Content>
                    <Title>Test page 1</Title>
                    <Center flex={1}>
                        <Button onPress={() => navigate('PageTest02')}>
                            Go to Page 3
                        </Button>
                    </Center>
                </Page.Content>
            </Scrollbox>
        </Page>
    );
};

export { PageTest01 };
