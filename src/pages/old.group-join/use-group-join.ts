// import { groupsStore } from 'pages/groups/group-store';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { graphClient } from 'modules/api-client';
import { useInsert_Members_OneMutation } from 'modules/api-graphql/api-react-query';
import { auth } from 'modules/application';
import { useApplicationMachine } from 'modules/application-machine.old';

import { GroupJoinFormValues } from './types';

export const useGroupJoin = () => {
    const [t] = useTranslation();
    const {
        control,
        handleSubmit,
        setError,
        formState: { errors },
    } = useForm<GroupJoinFormValues>();

    const [machine] = useApplicationMachine();

    const { mutate } = useInsert_Members_OneMutation(graphClient);

    const joinGroup = handleSubmit((values: GroupJoinFormValues) => {
        const { code } = values;
        const user_id = auth.user()?.id;
        const group = machine.context.groups.find((g) => g.group_code === code);
        if (!group) {
            setError('code', {
                type: 'validate',
                message: 'group code not found',
            });
        } else if (group.members.find((m) => m.user.id === user_id)) {
            setError('code', {
                type: 'validate',
                message: 'You are already member of this group',
            });
        } else {
            mutate({
                group_id: group.id,
                user_id,
            });
        }
    });

    return { t, control, errors, joinGroup };
};
