import React from 'react';

import { Text, FormControl, Input, VStack } from 'native-base';
import { Controller } from 'react-hook-form';

import { ButtonAction } from 'components/button-action';
import { Page } from 'components/page';
import { Scrollbox } from 'components/scrollbox';
import { Title } from 'modules/compounds/title';

import { GroupJoinFormValues } from './types';
import { useGroupJoin } from './use-group-join';

const GroupJoin: React.FC = () => {
    const { t, control, errors, joinGroup } = useGroupJoin();

    return (
        <Page>
            <Page.AppBar />
            <Scrollbox>
                <Page.Content>
                    <Title>{t('join-group')}</Title>
                    <Text color="gray.400" fontSize="sm">
                        To join a group, enter the{' '}
                        <Text color="gray.500">code</Text> you have been
                        provided with.
                    </Text>
                    <VStack space={12}>
                        <Controller<GroupJoinFormValues>
                            control={control}
                            name="code"
                            rules={{ required: 'Code is required' }}
                            render={({ field: { onChange, value, ref } }) => (
                                <FormControl
                                    mt={6}
                                    isRequired
                                    isInvalid={!!errors.code}>
                                    <FormControl.Label>
                                        Group code
                                    </FormControl.Label>
                                    <Input
                                        ref={ref}
                                        value={value as string}
                                        variant="filled"
                                        isInvalid={!!errors.code}
                                        placeholder="Enter the group code"
                                        onChangeText={(text: string) =>
                                            onChange(text)
                                        }
                                    />
                                    <FormControl.ErrorMessage>
                                        {errors?.code?.message}
                                    </FormControl.ErrorMessage>
                                </FormControl>
                            )}
                        />
                        <ButtonAction mt={10} onPress={joinGroup}>
                            Join group
                        </ButtonAction>
                    </VStack>
                </Page.Content>
            </Scrollbox>
        </Page>
    );
};

export { GroupJoin };
