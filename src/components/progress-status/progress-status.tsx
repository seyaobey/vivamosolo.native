import React from 'react';

import { Box, Button, Spinner } from 'native-base';

import { ModalProgress } from 'components/modal-progress';
import { ProgressStatus, useProgress } from 'machines/progress';

export type IProgressStatusProps = Omit<
    React.ComponentProps<typeof ModalProgress>,
    'children'
> & {
    onComplete: () => void;
};

const ProgressStatusComponent = ({
    onIcon,
    onTitle,
    onComplete,
}: IProgressStatusProps) => {
    const { visible, reset } = useProgress();
    return (
        <>
            {visible ? (
                <ModalProgress onTitle={onTitle} onIcon={onIcon}>
                    {(status: ProgressStatus) => (
                        <Box px={20}>
                            {status === 'processing' ? (
                                <Spinner mt={6} size="lg" />
                            ) : (
                                <FinalizeButton
                                    status={status}
                                    onPress={
                                        status === 'success'
                                            ? onComplete
                                            : reset
                                    }
                                />
                            )}
                        </Box>
                    )}
                </ModalProgress>
            ) : (
                <></>
            )}
        </>
    );
};
type FinalizeButtonProps = {
    status: ProgressStatus;
    onPress: () => void;
};
const FinalizeButton = ({ status, onPress }: FinalizeButtonProps) => (
    <Button
        mt={8}
        shadow={8}
        color="white"
        _text={{
            color: 'white',
        }}
        _pressed={{
            bg: status === 'success' ? 'cyan.600' : 'orange.600',
        }}
        onPress={onPress}
        colorScheme={status === 'success' ? 'cyan' : 'warning'}>
        Close
    </Button>
);

export { ProgressStatusComponent as ProgressStatus };
