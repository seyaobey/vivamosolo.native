import React from 'react';

import { Text } from 'native-base';
import NumberFormat from 'react-number-format';

const TextRender = ({ ...rest }: React.ComponentProps<typeof Text>) => (
    <Text {...rest} />
);

type CurrencyTextProps = {
    value: number;
    render?: (text: string) => JSX.Element;
};
export const CurrencyText = ({ value, render }: CurrencyTextProps) => (
    <NumberFormat
        value={value}
        displayType="text"
        thousandSeparator
        decimalSeparator="."
        decimalScale={2}
        fixedDecimalScale
        prefix="$"
        renderText={(text) =>
            render ? render(text) : <TextRender>{text}</TextRender>
        }
    />
);
