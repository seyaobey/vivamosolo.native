/* eslint-disable no-return-assign */
import React, { memo } from 'react';

import { store as baseStore, view } from '@risingstack/react-easy-state';
import { Center, Text, VStack } from 'native-base';

import { ButtonAction } from 'components/button-action';
import { Calendar } from 'components/calendar';
import { SelectField } from 'components/form/select-field/select-field';
import { PanelBox } from 'components/panel';
import { Swipeable } from 'components/swipeable';
import { Group_Recurrencies_Enum } from 'modules/api-graphql/api-react-query';
import { createStore } from 'pages/old.group-create/create-store';

import { recurrencyLabels } from './payments-constants';

type PaymentRecurrencyProps = {
    isOpen: boolean;
    onClose: () => void;
};

export const PaymentRecurrency = memo(
    view(({ isOpen, onClose }: PaymentRecurrencyProps) => (
        <Swipeable isOpen={isOpen}>
            <PaymentRecurrencyContent onClose={onClose} />
        </Swipeable>
    ))
);

type PaymentRecurrencyContentProps = {
    onClose: () => void;
};
const PaymentRecurrencyContent = memo(
    view(({ onClose }: PaymentRecurrencyContentProps) => {
        const store = baseStore({
            recurrency: createStore.values.recurrency,
            recurrencyDay: createStore.values.recurrencyDay,
            sheetOpen: false,
        });
        const submitValues = () => {
            createStore.values.recurrency = store.recurrency;
            createStore.values.recurrencyDay = store.recurrencyDay;
            onClose();
        };
        return (
            <VStack px={4} mt={8} space={8}>
                <PanelBox>
                    <Center>
                        <Text fontSize="sm" color="gray.400">
                            <Text color="gray.600">Each participant</Text> is
                            expected to have paid the amount due by this day
                            every week or every month.
                        </Text>
                    </Center>
                </PanelBox>

                <PanelBox>
                    <Text ml={4} fontSize="xs" color="gray.400">
                        Repeat
                    </Text>

                    <SelectField<Group_Recurrencies_Enum>
                        items={recurrencyLabels}
                        value={store.recurrency}
                        onChange={(val) => (store.recurrency = val)}
                        dropdownIcon={<Text color="blue.500">change</Text>}
                    />
                </PanelBox>

                {(
                    [
                        Group_Recurrencies_Enum.Weekly,
                        Group_Recurrencies_Enum.Monthly,
                    ] as Group_Recurrencies_Enum[]
                ).includes(store.recurrency) && (
                    <PanelBox>
                        <Calendar
                            value={store.recurrencyDay}
                            onChange={(day) => (store.recurrencyDay = day)}
                            mode={
                                store.recurrency ===
                                Group_Recurrencies_Enum.Weekly
                                    ? Group_Recurrencies_Enum.Weekly
                                    : Group_Recurrencies_Enum.Monthly
                            }
                        />
                    </PanelBox>
                )}

                <ButtonAction mb={6} mt={3} onPress={submitValues}>
                    Save
                </ButtonAction>
            </VStack>
        );
    })
);
