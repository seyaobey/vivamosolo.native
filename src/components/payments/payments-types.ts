import { Group_Recurrencies_Enum } from 'modules/api-graphql/api-react-query';

export type Recurrencies = `${Group_Recurrencies_Enum}`;
export type RecurrencyLabels = {
    [name in Recurrencies]: string;
};
