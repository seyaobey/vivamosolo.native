import { RecurrencyLabels } from './payments-types';

export const recurrencyLabels: RecurrencyLabels = {
    Daily: 'Daily',
    Weekly: 'Weekly',
    Monthly: 'Monthly',
    None: 'No payment deadline',
};
