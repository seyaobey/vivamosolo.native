import React from 'react';

import { FormControl, Icon, Input } from 'native-base';
import { FieldValues, useController } from 'react-hook-form';

import { InputFieldProps } from '../types';

export function InputField<T extends FieldValues>({
    name,
    control,
    leftIcon: LeftIcon,
    keyboardType,
    password,
    variant,
    placeholder,
    rules,
    label,
}: InputFieldProps<T>) {
    const {
        field: { onChange, value },
        fieldState: { invalid, error },
    } = useController({
        name,
        rules,
        control,
    });

    return (
        <FormControl isInvalid={invalid}>
            {label && (
                <FormControl.Label
                    _text={{
                        fontWeight: 'light',
                        fontSize: 'lg',
                    }}>
                    {label}
                </FormControl.Label>
            )}
            <Input
                px={2}
                size="lg"
                testID={`input-${name}`}
                color="gray.600"
                fontFamily="Roboto-Regular"
                isInvalid={invalid}
                variant={variant}
                type={password ? 'password' : 'text'}
                placeholder={placeholder}
                value={value as string}
                onChangeText={(text) => onChange(text)}
                keyboardType={keyboardType}
                InputLeftElement={
                    LeftIcon ? (
                        <Icon ml={2} size={6} color="gray.400" as={LeftIcon} />
                    ) : (
                        <></>
                    )
                }
            />
            {invalid && error?.message && (
                <FormControl.ErrorMessage>
                    {error?.message}
                </FormControl.ErrorMessage>
            )}
        </FormControl>
    );
}
