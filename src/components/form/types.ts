import { IInputProps } from 'native-base';
import { FieldValues, UseControllerProps } from 'react-hook-form';
import { KeyboardTypeOptions } from 'react-native';

export type FieldProps<T extends FieldValues> = UseControllerProps<T>;
export type InputFieldProps<T extends FieldValues> = FieldProps<T> & {
    label?: string;
    keyboardType?: KeyboardTypeOptions;
    leftIcon?: React.ReactNode;
    password?: boolean;
    variant?: IInputProps['variant'];
    placeholder?: string;
};
