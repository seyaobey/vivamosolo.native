import React, { useCallback } from 'react';

import { MaterialIcons } from '@expo/vector-icons';
import { Icon, Select } from 'native-base';

export type SelectFieldProps<T extends string> = React.ComponentProps<
    typeof Select
> & {
    value: T;
    onChange: (value: T) => void;
    items: {
        [name in T]: string;
    };
};

export function SelectField<T extends string>({
    items,
    value,
    variant = 'unstyled',
    onChange,
    ...rest
}: SelectFieldProps<T>) {
    return (
        <Select
            pl={0}
            variant={variant}
            selectedValue={value}
            _actionSheetContent={{
                pb: 10,
            }}
            _item={{
                mb: 2,
                _text: {
                    fontSize: 'lg',
                },
                borderWidth: 1,
                borderColor: 'gray.200',
            }}
            _selectedItem={{
                borderColor: 'blue.500',
                _text: {
                    color: 'blue.500',
                    fontWeight: '700',
                },
                endIcon: (
                    <Icon
                        size={7}
                        color="blue.500"
                        as={<MaterialIcons name="check" />}
                    />
                ),
            }}
            onValueChange={useCallback((val) => onChange?.(val as T), [])}
            {...rest}>
            {Object.entries(items).map((k) => (
                <Select.Item key={k[0]} value={k[0]} label={k[1] as string} />
            ))}
        </Select>
    );
}
