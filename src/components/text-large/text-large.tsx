import React from 'react';

import { Text } from 'native-base';

type Props = React.ComponentProps<typeof Text>;

export const TextLarge = ({
    fontSize = 'lg',
    fontWeight = 'light',
    ...rest
}: Props) => <Text fontSize={fontSize} fontWeight={fontWeight} {...rest} />;

export const TextLargeBold = ({ fontWeight = 'normal', ...rest }: Props) => (
    <TextLarge fontWeight={fontWeight} {...rest} />
);
