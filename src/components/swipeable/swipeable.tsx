import React, { useEffect, useRef } from 'react';

import { Feather } from '@expo/vector-icons';
import { IconButton, Icon, Row } from 'native-base';
import { Modalize } from 'react-native-modalize';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { appTheme } from 'modules/application-theme/app-theme';

type SwipeablePanelProps = {
    isOpen: boolean;
    topOffset?: number;
    adjustHeight?: boolean;
    onClose?: () => void;
    onClosed?: () => void;
    closable?: boolean;
    children: React.ReactNode;
};
export const Swipeable = ({
    isOpen,
    children,
    topOffset = hp(10),
    adjustHeight,
    closable,
    onClose,
    onClosed,
}: SwipeablePanelProps) => {
    const modal = useRef<Modalize>(null);
    const handleOpen = () => {
        modal.current?.open();
    };
    const handleClose = () => {
        modal.current?.close();
    };

    useEffect(() => {
        if (isOpen) {
            handleOpen();
        } else {
            handleClose();
        }
    }, [isOpen]);

    return (
        <Modalize
            adjustToContentHeight={adjustHeight}
            modalTopOffset={topOffset}
            modalStyle={{
                paddingHorizontal: 4,
                backgroundColor: appTheme.colors.modalize_bg,
            }}
            scrollViewProps={{
                showsVerticalScrollIndicator: false,
                contentContainerStyle: {
                    flexGrow: 1,
                },
                style: {
                    flex: 1,
                },
            }}
            onClosed={onClosed}
            ref={modal}>
            <>
                {closable && (
                    <Row justifyContent="flex-end" my={2} mr={3}>
                        <IconButton
                            variant="ghost"
                            onPress={onClose}
                            icon={
                                <Icon
                                    size={6}
                                    color="gray.600"
                                    as={<Feather name="x" />}
                                />
                            }
                        />
                    </Row>
                )}
                {children}
            </>
        </Modalize>
    );
};
