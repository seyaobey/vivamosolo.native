import React, { useState, useEffect } from 'react';

import { Feather } from '@expo/vector-icons';
import { Actionsheet, Icon } from 'native-base';

export type ActionSheetItem<T> = {
    value: T;
    label?: string;
};
export type ActionSheetProps<T> = {
    items: Array<ActionSheetItem<T>>;
    isOpen?: boolean;
    value: T;
    onChange: (value: T) => void;
    onClose: () => void;
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function ActionSheet<T>({
    items,
    isOpen,
    value,
    onChange,
    onClose,
}: ActionSheetProps<T>) {
    const [open, setOpen] = useState(isOpen);

    useEffect(() => {
        setOpen(isOpen);
    }, [isOpen]);

    return (
        <Actionsheet isOpen={open} onClose={onClose}>
            <Actionsheet.Content>
                {items.map((v) => (
                    <Actionsheet.Item
                        // eslint-disable-next-line @typescript-eslint/no-explicit-any
                        key={v.value as any}
                        onPress={() => onChange(v.value)}
                        endIcon={
                            v.value === value ? (
                                <Icon
                                    size={6}
                                    color="green.500"
                                    as={<Feather name="check" />}
                                />
                            ) : (
                                <></>
                            )
                        }>
                        {v.label ?? v.value}
                    </Actionsheet.Item>
                ))}
            </Actionsheet.Content>
        </Actionsheet>
    );
}
