import React from 'react';

import { Button } from 'native-base';

export type IButtonChangeProps = React.ComponentProps<typeof Button>;

const ButtonChange: React.FC<IButtonChangeProps> = ({
    variant = 'ghost',
    _pressed = { bg: 'btn_change_pressed' },
    _text = { color: 'main', fontSize: 'lg' },
    ...rest
}) => <Button variant={variant} _text={_text} _pressed={_pressed} {...rest} />;

export { ButtonChange };
