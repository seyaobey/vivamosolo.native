import React from 'react';

import { Feather } from '@expo/vector-icons';

import { ProgressStatus } from 'machines/progress';

export type ProgressStatusProps = {
    icon: React.ComponentProps<typeof Feather>['name'];
    title: string;
    status: ProgressStatus;
    children: React.ReactNode;
};

export type ModalProgressProps = {
    onIcon?: (
        status: ProgressStatus
    ) => React.ComponentProps<typeof Feather>['name'] | undefined;
    onTitle?: (status: ProgressStatus) => string | undefined;
    children: (status: ProgressStatus) => JSX.Element;
    onClose?: () => void;
};
