import React from 'react';

import { Feather } from '@expo/vector-icons';

import { ProgressStatus } from 'machines/progress';

export const defaultOnIcon = (
    status: ProgressStatus
): React.ComponentProps<typeof Feather>['name'] => {
    switch (status) {
        case 'failed':
            return 'x-circle';
        case 'success':
            return 'check-circle';
        default:
            return 'clock';
    }
};

export const defaultTitle = (status: ProgressStatus): string => {
    switch (status) {
        case 'failed':
            return 'Error';
        case 'success':
            return 'Completed';
        default:
            return 'Processing...';
    }
};
