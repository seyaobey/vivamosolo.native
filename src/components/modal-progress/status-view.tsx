import React from 'react';

import { Feather } from '@expo/vector-icons';
import { view } from '@risingstack/react-easy-state';
import { Box, Center, Icon, Heading } from 'native-base';

import { ScaleFade } from 'components/scale-fade';

import { ProgressStatusProps } from './types';

export const StatusView = view(
    ({ icon, status, title, children }: ProgressStatusProps) => (
        <ScaleFade>
            <Box flex={1}>
                <Center>
                    <Icon
                        size={16}
                        color={status === 'failed' ? 'orange.500' : 'cyan.500'}
                        as={<Feather name={icon} />}
                    />
                </Center>
                <Center>
                    <Heading
                        size="lg"
                        color={status === 'failed' ? 'orange.500' : 'gray.500'}
                        fontFamily="body">
                        {title}
                    </Heading>
                </Center>
                {children}
            </Box>
        </ScaleFade>
    )
);
