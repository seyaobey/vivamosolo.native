import React, { useEffect, useRef } from 'react';

import { view } from '@risingstack/react-easy-state';
import { Column } from 'native-base';
import { Modalize } from 'react-native-modalize';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { useProgress } from 'machines/progress';

import { defaultOnIcon, defaultTitle } from './defaults';
import { StatusView } from './status-view';
import { ModalProgressProps } from './types';

export const ModalProgress = view(
    ({
        onIcon = defaultOnIcon,
        onTitle = defaultTitle,
        children,
        onClose,
    }: ModalProgressProps) => {
        const { status } = useProgress();
        const modal = useRef<Modalize>(null);

        useEffect(() => {
            modal.current?.open();
        }, []);

        return (
            <Modalize
                ref={modal}
                handlePosition="inside"
                panGestureEnabled={false}
                adjustToContentHeight
                closeOnOverlayTap={false}
                onClose={onClose}
                scrollViewProps={{
                    showsVerticalScrollIndicator: false,
                }}>
                <Column
                    h={hp(32)}
                    flex={1}
                    pt={6}
                    bg="white"
                    borderTopRadius={20}>
                    {status !== 'idle' ? (
                        <StatusView
                            status={status}
                            icon={onIcon(status)!}
                            title={onTitle(status)!}>
                            {children(status)}
                        </StatusView>
                    ) : (
                        <></>
                    )}
                </Column>
            </Modalize>
        );
    }
);
