import React from 'react';

import { Button } from 'native-base';

export type IButtonActionProps = React.ComponentProps<typeof Button>;

const ButtonAction: React.FC<IButtonActionProps> = ({
    bg = 'main',
    shadow = 6,
    _pressed = {
        bg: 'main_dark',
    },
    _text = {
        color: 'white',
        fontWeight: 'bold',
    },
    ...rest
}) => (
    <Button
        size="lg"
        bg={bg}
        shadow={shadow}
        _pressed={_pressed}
        _text={_text}
        {...rest}
    />
);

export { ButtonAction };
