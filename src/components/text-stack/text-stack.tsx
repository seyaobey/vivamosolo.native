import React from 'react';

import { Text, VStack } from 'native-base';

type StackProps = {
    title: string;
    alignItems?: React.ComponentProps<typeof VStack>['alignItems'];
    children: React.ReactNode;
};
export const TextStack = ({ title, alignItems, children }: StackProps) => (
    <VStack alignItems={alignItems}>
        <Text fontSize="sm" fontWeight="light" mb={1}>
            {title}
        </Text>
        {children}
    </VStack>
);
