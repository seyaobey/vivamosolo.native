import React from 'react';

import { ScrollView } from 'native-base';

export type IScrollboxProps = React.ComponentProps<typeof ScrollView>;

export const Scrollbox = ({
    flex = 1,
    alwaysBounceVertical = true,
    nestedScrollEnabled = true,
    showsVerticalScrollIndicator = false,
    _contentContainerStyle = {
        flexGrow: 1,
    },
    children,
    ...rest
}: IScrollboxProps) => (
    <ScrollView
        flex={flex}
        alwaysBounceVertical={alwaysBounceVertical}
        nestedScrollEnabled={nestedScrollEnabled}
        showsVerticalScrollIndicator={showsVerticalScrollIndicator}
        _contentContainerStyle={{
            ..._contentContainerStyle,
        }}
        {...rest}>
        {children}
    </ScrollView>
);
