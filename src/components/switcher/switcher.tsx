import React from 'react';

type SwitcherProps = {
    visible: boolean;
    children: React.ReactNode;
    fallback?: React.ReactNode;
};
export const Switcher = ({
    visible,
    fallback: Fallback,
    children,
}: SwitcherProps) => (
    <>
        <>{visible && children}</>
        <>
            {!visible &&
                Fallback &&
                (typeof Fallback === 'function' ? <Fallback /> : Fallback)}
        </>
    </>
);
