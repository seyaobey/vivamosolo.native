import React from 'react';

import { Box } from 'native-base';

export const PanelBox = ({
    p = 4,
    bg = 'white',
    borderRadius = 8,
    ...rest
}: React.ComponentProps<typeof Box>) => (
    <Box p={p} bg={bg} borderRadius={borderRadius} {...rest} />
);
