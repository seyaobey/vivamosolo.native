import React from 'react';

import { Box } from 'native-base';

export const Panel = ({
    px = 3,
    py = 3,
    bg = 'white',
    borderRadius = 8,
    shadow = 0,
    children,
    ...rest
}: React.ComponentProps<typeof Box>) => (
    <Box
        px={px}
        py={py}
        bg={bg}
        shadow={shadow}
        borderRadius={borderRadius}
        {...rest}>
        {children}
    </Box>
);
