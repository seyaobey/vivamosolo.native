/* eslint-disable react/style-prop-object */
import React from 'react';

import { StatusBar } from 'expo-status-bar';
import { Box, Factory } from 'native-base';

import { Title } from 'modules/compounds/title';

import { AppbarAction, PageAppBar } from './page-appbar';

export const Page = ({
    flex = 1,
    safeAreaTop = true,
    bg = 'page_bg',
    children,
    ...rest
}: React.ComponentProps<typeof Box>) => (
    <Box flex={flex} safeAreaTop={safeAreaTop} bg={bg} {...rest}>
        <StatusBarBase style="dark" bg={bg as string} />
        {children}
    </Box>
);

const StatusBarBase = Factory(StatusBar);

const PageContent = ({
    mx = 4,
    flex = 1,
    safeAreaBottom = true,
    ...rest
}: React.ComponentProps<typeof Box>) => (
    <Box mx={mx} flex={flex} safeAreaBottom={safeAreaBottom} {...rest} />
);

Page.Content = PageContent;
Page.AppBar = PageAppBar;
Page.AppBarAction = AppbarAction;
Page.Title = Title;
