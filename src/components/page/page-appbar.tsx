import React from 'react';

import { Feather } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { Row, Icon, Button, Box } from 'native-base';

type AppbarActionProps = React.ComponentProps<typeof Button>;

type PageAppBarProps = {
    hideBackBtn?: boolean;
    actions?: Array<React.ReactElement<AppbarActionProps>>;
    onGoBack?: () => void;
};
export const PageAppBar = ({
    hideBackBtn,
    actions = [],
    onGoBack,
}: PageAppBarProps) => {
    const { canGoBack, goBack } = useNavigation();
    const displayBackBtn = !hideBackBtn && canGoBack();

    return (
        <Row mx={2} justifyContent="space-between" alignItems="center">
            {displayBackBtn ? (
                <AppbarAction
                    onPress={onGoBack ?? goBack}
                    startIcon={<Icon name="arrow-left" as={Feather} />}
                />
            ) : (
                <Box />
            )}

            <Row>{actions}</Row>
        </Row>
    );
};

export const AppbarAction = ({
    pl = 0,
    variant = 'ghost',
    _text,
    _pressed,
    ...rest
}: AppbarActionProps) => (
    <Button
        variant={variant}
        pl={pl}
        _pressed={{
            bg: 'gray.100',
            ..._pressed,
        }}
        _text={{
            color: 'main',
            fontSize: 'lg',
            ..._text,
        }}
        {...rest}
    />
);
