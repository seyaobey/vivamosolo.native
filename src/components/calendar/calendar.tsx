/* eslint-disable no-plusplus */
import React from 'react';

import dnames from 'date-names';
import { Row, Text, VStack, Factory, Center } from 'native-base';
import { TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import { Group_Recurrencies_Enum } from 'modules/api-graphql/api-react-query';

const WEEK_LENGTH = 7;

export type CalendarMode =
    | Group_Recurrencies_Enum.Weekly
    | Group_Recurrencies_Enum.Monthly;

const TouchableOpacityBox = Factory(TouchableOpacity);

type CalendarProps = {
    value: number;
    onChange: (value: number) => void;
    mode: CalendarMode;
};
export const Calendar = React.memo(
    ({ mode, value, onChange }: CalendarProps) => (
        <VStack>
            <LeadingDay mode={mode} activeDay={value} />
            {getWeeksOfMonth(mode).map((w) => (
                <RowWeek
                    key={w}
                    mode={mode}
                    activeDay={value}
                    week={w}
                    onClick={onChange}
                />
            ))}
        </VStack>
    )
);

Calendar.displayName = 'Calendar';

type LeadingDayProps = {
    mode: CalendarMode;
    activeDay: number;
    color?: string;
};
export const LeadingDay = ({ mode, activeDay, color }: LeadingDayProps) => (
    <Text fontSize="lg" fontWeight="light" color={color ?? 'gray.500'}>
        Each{' '}
        <Text fontSize="lg" fontWeight="light" color={color ?? 'gray.500'}>
            {mode === Group_Recurrencies_Enum.Monthly
                ? activeDay + 1
                : (
                      (getDayName(activeDay, false) ?? '') as string
                  ).toLowerCase()}
        </Text>
    </Text>
);

type RowWeekProps = {
    week: number;
    mode: CalendarMode;
    activeDay: number;
    onClick: (day: number) => void;
};
const RowWeek = ({ week, mode, activeDay, onClick }: RowWeekProps) => {
    const daysOfWeek = getDaysOfWeek(week);
    return (
        <Row
            mt={2}
            justifyContent={
                daysOfWeek.length === 7 ? 'space-between' : 'flex-start'
            }
            flexWrap="nowrap">
            {daysOfWeek.map((d) => {
                const dayNum: number = week * 7 + d;
                return (
                    <TouchableOpacityBox
                        key={d}
                        mr={2}
                        mb={3}
                        onPress={() => onClick(dayNum)}>
                        <CellBox bg={activeDay === dayNum ? 'main' : 'white'}>
                            <Text
                                {...formatActiveDayText(activeDay, dayNum)}
                                fontWeight={
                                    activeDay === dayNum ? 'semibold' : 'light'
                                }
                                fontSize={
                                    mode === Group_Recurrencies_Enum.Weekly
                                        ? 'xs'
                                        : 'md'
                                }>
                                {mode === Group_Recurrencies_Enum.Weekly
                                    ? getDayName(dayNum)
                                    : dayNum + 1}
                            </Text>
                        </CellBox>
                    </TouchableOpacityBox>
                );
            })}
        </Row>
    );
};

type CellBoxProps = Pick<
    React.ComponentProps<typeof VStack>,
    'bg' | 'children'
>;
const CellBox = ({ bg = 'white', children }: CellBoxProps) => (
    <Center
        bg={bg}
        width={`${wp(10)}px`}
        height={`${wp('8.9')}px`}
        borderRadius={10}>
        {children}
    </Center>
);

const endOfMounth = () => 31;
const getDaysOfWeek = (week: number) => [
    ...Array(week < 4 ? 7 : endOfMounth() - week * 7).keys(),
];
const getWeeksOfMonth = (mode: CalendarMode) => {
    const numOfWeeks =
        mode === Group_Recurrencies_Enum.Weekly
            ? 1
            : Math.ceil(endOfMounth() / WEEK_LENGTH);

    let weeks: number[] = [];
    for (let i = 0; i < numOfWeeks; i++) {
        weeks = [...weeks, i];
    }
    return weeks;
};

export const getDayName = (day: number, abbr = true) =>
    dnames[abbr ? 'abbreviated_days' : 'days'][day];

const formatActiveDayText = (
    active: number,
    day: number
): Partial<Pick<React.ComponentProps<typeof Text>, 'color'>> =>
    active === day
        ? {
              color: 'white',
          }
        : {};
