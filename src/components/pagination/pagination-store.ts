import { store, batch } from '@risingstack/react-easy-state';

export const pagingStore = store({
    count: 0,
    index: 0,
    fromUser: false,
    init: (count: number) => {
        pagingStore.count = count;
    },
    inRange: (value: number) => value >= 0 && value < pagingStore.count,
    updateIndex: (props: { index: number; fromUser: boolean }) => {
        batch(() => {
            pagingStore.index = props.index;
            pagingStore.fromUser = props.fromUser;
        });
    },
});
