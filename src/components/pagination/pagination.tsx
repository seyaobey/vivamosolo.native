import React from 'react';

import { MaterialIcons } from '@expo/vector-icons';
import { view } from '@risingstack/react-easy-state';
import { HStack, IconButton, Icon, Text } from 'native-base';

import { pagingStore } from './pagination-store';

export const Pagination = view(() => {
    const changeIndex = (next: number) => {
        if (pagingStore.inRange(next)) {
            pagingStore.updateIndex({ index: next, fromUser: true });
        }
    };
    const next = () => changeIndex(pagingStore.index + 1);
    const prev = () => changeIndex(pagingStore.index - 1);

    return (
        <>
            {pagingStore.count ? (
                <HStack space={2} alignItems="center">
                    <ArrowButton dir="left" onPress={prev} />

                    <Text fontSize={22} color="main">
                        {pagingStore.index + 1} / {pagingStore.count}
                    </Text>

                    <ArrowButton dir="right" onPress={next} />
                </HStack>
            ) : (
                <></>
            )}
        </>
    );
});

type ArrowButtonProps = {
    dir: 'left' | 'right';
    onPress: () => void;
};
const ArrowButton = ({ dir, onPress }: ArrowButtonProps) => (
    <IconButton
        p={0}
        variant="ghost"
        onPress={onPress}
        icon={
            <Icon
                size="lg"
                color="main"
                as={<MaterialIcons name={`arrow-${dir}` as never} />}
            />
        }
    />
);
