import React, { useEffect, useMemo } from 'react';

import { MaterialIcons, Feather } from '@expo/vector-icons';
import * as Contacts from 'expo-contacts';
import {
    VStack,
    ZStack,
    Row,
    FlatList,
    Divider,
    Text,
    Icon,
    Pressable,
    FormControl,
    Input,
} from 'native-base';

import { ButtonAction } from 'components/button-action';
import { PanelBox } from 'components/panel';
import { ListSkeleton } from 'components/skeleton';
import { TextLarge } from 'components/text-large';
import { contacts } from 'machines/contacts';
import { useXMachine } from 'machines/use-xmachine';

export type SelectContact = (contact: Contacts.Contact) => void;

type FindContactsProps = {
    title: string;
    onSelect: SelectContact;
    multiselect?: boolean;
    ctx?: ReturnType<typeof useFindContacts>;
};
export const FindContacts = ({
    title,
    multiselect,
    onSelect,
    ctx = useFindContacts(),
}: FindContactsProps) => (
    <VStack mx={4} space={4} flex={1}>
        <PanelBox>
            <TextLarge fontSize="2xl">{title}</TextLarge>
        </PanelBox>
        <ZStack flex={1}>
            <PanelBox w="100%" alignItems="center" h="95%">
                <Row w="100%">
                    <SearchInput
                        term={ctx.searchTerm}
                        onChange={ctx.onSearch}
                    />
                </Row>
                <Divider mt={4} />
                <ListSkeleton loading={ctx.loading}>
                    <Row w="100%">
                        <FlatList
                            data={ctx.getDataset()}
                            showsVerticalScrollIndicator
                            showsHorizontalScrollIndicator={false}
                            ItemSeparatorComponent={() => <Divider />}
                            keyExtractor={(item: Contacts.Contact) => item.id}
                            renderItem={renderContact(onSelect)}
                            onEndReachedThreshold={0.3}
                            onEndReached={() => contacts.send('FETCH_MORE')}
                        />
                    </Row>
                </ListSkeleton>
                {multiselect && (
                    <Row
                        justifyContent="center"
                        pos="absolute"
                        top="93%"
                        shadow={8}
                        w="100%">
                        <ButtonAction w="100%" opacity={0}>
                            Select
                        </ButtonAction>
                    </Row>
                )}
            </PanelBox>
        </ZStack>
    </VStack>
);

const renderContact =
    (onSelect: SelectContact) =>
    ({ item }: ContactProps) =>
        <ContactItem item={item} onSelect={onSelect} />;

type ContactProps = {
    item: Contacts.Contact;
    // eslint-disable-next-line react/no-unused-prop-types
    onSelect?: SelectContact;
};
const ContactItem = React.memo(({ item, onSelect }: ContactProps) => (
    <Pressable onPress={() => onSelect?.(item)}>
        {({ isPressed }) => (
            <VStack
                py={4}
                opacity={isPressed ? 0.5 : 1}
                bg={isPressed ? 'gray.200' : undefined}>
                <VStack>
                    <Row justifyContent="space-between">
                        <TextLarge>{item.name}</TextLarge>
                        <Icon
                            ml={2}
                            mr={4}
                            opacity={0}
                            size={4}
                            color="gray.400"
                            name="arrow-forward-ios"
                            as={MaterialIcons}
                        />
                    </Row>
                    {item.phoneNumbers?.length ? (
                        <Row mt={2}>
                            <Text color="gray.600">
                                {item.phoneNumbers[0].number}
                            </Text>
                        </Row>
                    ) : (
                        <></>
                    )}
                </VStack>
            </VStack>
        )}
    </Pressable>
));

type SearchInputProps = {
    term: string;
    onChange: (value: string) => void;
};
const SearchInput = ({ term, onChange }: SearchInputProps) => (
    <FormControl>
        <Input
            value={term}
            variant="filled"
            placeholder="Search"
            onChangeText={onChange}
            InputLeftElement={
                <Icon
                    ml={2}
                    color="gray.500"
                    size={6}
                    as={<Feather name="search" />}
                />
            }
        />
    </FormControl>
);

const useFindContacts = () => {
    const { state, send } = useXMachine(contacts);
    const data = useMemo(() => state.context.contacts, [state.value]);
    const loading = useMemo(
        () => state.matches({ acquired: 'fetching_contacts' }),
        [state.value]
    );
    const getDataset = () => {
        const { search } = state.context;
        let dataset = data;
        if (search) {
            dataset = data.filter((c) => {
                const found =
                    c.name
                        ?.toLocaleLowerCase()
                        ?.indexOf(search.toLocaleLowerCase()) >= 0;
                return found;
            });
        }
        return dataset.filter((c, i) => i < state.context.cursor);
    };
    const onSearch = (term: string) =>
        contacts.send({ type: 'SAVE_SEARCH_TERM', term });

    useEffect(() => {
        send('FETCH_CONTACTS');
    }, []);

    return {
        loading,
        searchTerm: contacts.state.context.search,
        getDataset,
        onSearch,
    };
};
