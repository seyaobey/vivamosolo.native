import React from 'react';

import { PresenceTransition } from 'native-base';

type ScaleFadeProps = {
    children: React.ReactNode;
};
export const ScaleFade = ({ children }: ScaleFadeProps) => (
    <PresenceTransition
        visible
        style={{ flex: 1 }}
        initial={{
            opacity: 0,
            scale: 0,
        }}
        animate={{
            opacity: 1,
            scale: 1,
            transition: {
                duration: 250,
            },
        }}>
        {children}
    </PresenceTransition>
);
