import React from 'react';

import { Dimensions } from 'react-native';
import RnSkeleton from 'react-native-skeleton-content';

const windowWidth = Dimensions.get('window').width;

type ListSkeletonProps = {
    loading: boolean;
    lines?: number;
    children: React.ReactNode;
};
export const ListSkeleton = ({
    loading,
    lines = 7,
    children,
}: ListSkeletonProps) => (
    <RnSkeleton
        containerStyle={{ flex: 1 }}
        isLoading={loading}
        animationType="pulse"
        layout={Array.from({ length: lines }, (_, i) => i).map((i) => ({
            key: i,
            width: windowWidth - 65,
            height: 36,
            marginBottom: 20,
            alignSelf: 'flex-start',
        }))}>
        {children}
    </RnSkeleton>
);
