/* eslint-disable indent */
/* eslint-disable react/jsx-closing-bracket-location */
import React from 'react';

import { Text, Box, Center, Spinner } from 'native-base';
import { StyleSheet } from 'react-native';

type OverlayProps = {
    message?: string;
};
export const Overlay = ({ message = 'Processing...' }: OverlayProps) => (
    <Box style={[StyleSheet.absoluteFill, styles.cover]}>
        <Center flex={1}>
            <Center size={20} w={200}>
                <Spinner color="white" accessibilityLabel="Processing" />
                <Text color="white">{message}</Text>
            </Center>
        </Center>
    </Box>
);

const styles = StyleSheet.create({
    cover: {
        backgroundColor: 'rgba(0,0,0,.5)',
    },
});
