import React from 'react';

import {
    SelectField,
    SelectFieldProps,
} from 'components/form/select-field/select-field';
import { PanelBox } from 'components/panel';
import { TextLarge, TextLargeBold } from 'components/text-large';

export type ISelectButtonProps<T extends string> = SelectFieldProps<T> & {
    caption: string;
    action?: string;
};

function SelectButton<T extends string>({
    caption,
    action = 'Change',
    ...rest
}: ISelectButtonProps<T>) {
    return (
        <PanelBox>
            <TextLarge>{caption}</TextLarge>
            <SelectField<T>
                {...rest}
                dropdownIcon={
                    <TextLargeBold color="main">{action}</TextLargeBold>
                }
            />
        </PanelBox>
    );
}

export { SelectButton };
