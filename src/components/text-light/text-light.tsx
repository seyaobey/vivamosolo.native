import React from 'react';

import { Text } from 'native-base';

type TextProps = React.ComponentProps<typeof Text>;

export const TextLight = ({ fontWeight = 'light', ...rest }: TextProps) => (
    <Text fontWeight={fontWeight} {...rest} />
);
