export const stringToBool = (val: unknown) =>
    typeof val === 'string'
        ? (val as string).toLowerCase() === 'true'
        : (val as boolean);
