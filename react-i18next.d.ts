/* eslint-disable @typescript-eslint/no-empty-interface */
import { resources, defaultNS } from './src/modules/localization';

declare module 'react-i18next' {
    interface CustomTypeOptions {
        defaultNS: typeof defaultNS;
        resources: typeof resources['en'];
    }
    // type DefaultResources = typeof resources['en'];
    // interface Resources extends DefaultResources {}
}
