type ZEUS_INTERFACES = never;
type ZEUS_UNIONS = never;

export type ValueTypes = {
    ['ActionCreateGroupInput']: {
        creator_id: ValueTypes['uuid'];
        group_contribution_amount?: ValueTypes['numeric'];
        group_contribution_type: string;
        group_name: string;
        group_recurrency_day?: ValueTypes['numeric'];
        group_recurrency_type: string;
    };
    ['ActionCreateGroupOutpu']: AliasType<{
        ok?: true;
        __typename?: true;
    }>;
    ['ActionCreateGroupOutput']: AliasType<{
        ok?: true;
        __typename?: true;
    }>;
    ['ActionRequestPaymentInput']: {
        amount: ValueTypes['numeric'];
        group_id: ValueTypes['uuid'];
        member_id: ValueTypes['uuid'];
        period_id: ValueTypes['uuid'];
    };
    ['ActionRequestPaymentOutput']: AliasType<{
        id?: true;
        url?: true;
        __typename?: true;
    }>;
    ['ActionWithdrawPaymentInput']: {
        amount: ValueTypes['numeric'];
        group_id: ValueTypes['uuid'];
        member_id: ValueTypes['uuid'];
        period_id: ValueTypes['uuid'];
        senderName: string;
        senderPhone: string;
    };
    ['ActionWithdrawPaymentOutput']: AliasType<{
        id?: true;
        __typename?: true;
    }>;
    /** expression to compare columns of type Boolean. All fields are combined with logical 'AND'. */
    ['Boolean_comparison_exp']: {
        _eq?: boolean;
        _gt?: boolean;
        _gte?: boolean;
        _in?: boolean[];
        _is_null?: boolean;
        _lt?: boolean;
        _lte?: boolean;
        _neq?: boolean;
        _nin?: boolean[];
    };
    ['InsertCreateInput']: {
        amount: ValueTypes['numeric'];
        group_id: ValueTypes['uuid'];
        member_id: ValueTypes['uuid'];
        period_id: ValueTypes['uuid'];
    };
    ['InsertPaymentOneActionInput']: {
        amount: ValueTypes['numeric'];
        group_id: ValueTypes['uuid'];
        period_id: ValueTypes['uuid'];
        user_id: ValueTypes['uuid'];
    };
    ['InsertPaymentOneActionOutput']: AliasType<{
        url?: true;
        __typename?: true;
    }>;
    ['InsertPaymentOutput']: AliasType<{
        id?: true;
        url?: true;
        __typename?: true;
    }>;
    /** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
    ['Int_comparison_exp']: {
        _eq?: number;
        _gt?: number;
        _gte?: number;
        _in?: number[];
        _is_null?: boolean;
        _lt?: number;
        _lte?: number;
        _neq?: number;
        _nin?: number[];
    };
    ['PaymentCreateInput']: {
        amount: ValueTypes['numeric'];
        group_id: ValueTypes['uuid'];
        period_id: ValueTypes['uuid'];
        user_id: ValueTypes['uuid'];
    };
    ['PaymentCreatedOutput']: AliasType<{
        url?: true;
        __typename?: true;
    }>;
    /** expression to compare columns of type String. All fields are combined with logical 'AND'. */
    ['String_comparison_exp']: {
        _eq?: string;
        _gt?: string;
        _gte?: string;
        _ilike?: string;
        _in?: string[];
        _is_null?: boolean;
        _like?: string;
        _lt?: string;
        _lte?: string;
        _neq?: string;
        _nilike?: string;
        _nin?: string[];
        _nlike?: string;
        _nsimilar?: string;
        _similar?: string;
    };
    /** columns and relationships of "auth.account_providers" */
    ['auth_account_providers']: AliasType<{
        /** An object relationship */
        account?: ValueTypes['auth_accounts'];
        account_id?: true;
        auth_provider?: true;
        auth_provider_unique_id?: true;
        created_at?: true;
        id?: true;
        /** An object relationship */
        provider?: ValueTypes['auth_providers'];
        updated_at?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "auth.account_providers" */
    ['auth_account_providers_aggregate']: AliasType<{
        aggregate?: ValueTypes['auth_account_providers_aggregate_fields'];
        nodes?: ValueTypes['auth_account_providers'];
        __typename?: true;
    }>;
    /** aggregate fields of "auth.account_providers" */
    ['auth_account_providers_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['auth_account_providers_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['auth_account_providers_max_fields'];
        min?: ValueTypes['auth_account_providers_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "auth.account_providers" */
    ['auth_account_providers_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['auth_account_providers_max_order_by'];
        min?: ValueTypes['auth_account_providers_min_order_by'];
    };
    /** input type for inserting array relation for remote table "auth.account_providers" */
    ['auth_account_providers_arr_rel_insert_input']: {
        data: ValueTypes['auth_account_providers_insert_input'][];
        on_conflict?: ValueTypes['auth_account_providers_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.account_providers". All fields are combined with a logical 'AND'. */
    ['auth_account_providers_bool_exp']: {
        _and?: (ValueTypes['auth_account_providers_bool_exp'] | undefined)[];
        _not?: ValueTypes['auth_account_providers_bool_exp'];
        _or?: (ValueTypes['auth_account_providers_bool_exp'] | undefined)[];
        account?: ValueTypes['auth_accounts_bool_exp'];
        account_id?: ValueTypes['uuid_comparison_exp'];
        auth_provider?: ValueTypes['String_comparison_exp'];
        auth_provider_unique_id?: ValueTypes['String_comparison_exp'];
        created_at?: ValueTypes['timestamptz_comparison_exp'];
        id?: ValueTypes['uuid_comparison_exp'];
        provider?: ValueTypes['auth_providers_bool_exp'];
        updated_at?: ValueTypes['timestamptz_comparison_exp'];
    };
    /** unique or primary key constraints on table "auth.account_providers" */
    ['auth_account_providers_constraint']: auth_account_providers_constraint;
    /** input type for inserting data into table "auth.account_providers" */
    ['auth_account_providers_insert_input']: {
        account?: ValueTypes['auth_accounts_obj_rel_insert_input'];
        account_id?: ValueTypes['uuid'];
        auth_provider?: string;
        auth_provider_unique_id?: string;
        created_at?: ValueTypes['timestamptz'];
        id?: ValueTypes['uuid'];
        provider?: ValueTypes['auth_providers_obj_rel_insert_input'];
        updated_at?: ValueTypes['timestamptz'];
    };
    /** aggregate max on columns */
    ['auth_account_providers_max_fields']: AliasType<{
        account_id?: true;
        auth_provider?: true;
        auth_provider_unique_id?: true;
        created_at?: true;
        id?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "auth.account_providers" */
    ['auth_account_providers_max_order_by']: {
        account_id?: ValueTypes['order_by'];
        auth_provider?: ValueTypes['order_by'];
        auth_provider_unique_id?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_account_providers_min_fields']: AliasType<{
        account_id?: true;
        auth_provider?: true;
        auth_provider_unique_id?: true;
        created_at?: true;
        id?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "auth.account_providers" */
    ['auth_account_providers_min_order_by']: {
        account_id?: ValueTypes['order_by'];
        auth_provider?: ValueTypes['order_by'];
        auth_provider_unique_id?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "auth.account_providers" */
    ['auth_account_providers_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['auth_account_providers'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "auth.account_providers" */
    ['auth_account_providers_obj_rel_insert_input']: {
        data: ValueTypes['auth_account_providers_insert_input'];
        on_conflict?: ValueTypes['auth_account_providers_on_conflict'];
    };
    /** on conflict condition type for table "auth.account_providers" */
    ['auth_account_providers_on_conflict']: {
        constraint: ValueTypes['auth_account_providers_constraint'];
        update_columns: ValueTypes['auth_account_providers_update_column'][];
        where?: ValueTypes['auth_account_providers_bool_exp'];
    };
    /** ordering options when selecting data from "auth.account_providers" */
    ['auth_account_providers_order_by']: {
        account?: ValueTypes['auth_accounts_order_by'];
        account_id?: ValueTypes['order_by'];
        auth_provider?: ValueTypes['order_by'];
        auth_provider_unique_id?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        provider?: ValueTypes['auth_providers_order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "auth.account_providers" */
    ['auth_account_providers_pk_columns_input']: {
        id: ValueTypes['uuid'];
    };
    /** select columns of table "auth.account_providers" */
    ['auth_account_providers_select_column']: auth_account_providers_select_column;
    /** input type for updating data in table "auth.account_providers" */
    ['auth_account_providers_set_input']: {
        account_id?: ValueTypes['uuid'];
        auth_provider?: string;
        auth_provider_unique_id?: string;
        created_at?: ValueTypes['timestamptz'];
        id?: ValueTypes['uuid'];
        updated_at?: ValueTypes['timestamptz'];
    };
    /** update columns of table "auth.account_providers" */
    ['auth_account_providers_update_column']: auth_account_providers_update_column;
    /** columns and relationships of "auth.account_roles" */
    ['auth_account_roles']: AliasType<{
        /** An object relationship */
        account?: ValueTypes['auth_accounts'];
        account_id?: true;
        created_at?: true;
        id?: true;
        role?: true;
        /** An object relationship */
        roleByRole?: ValueTypes['auth_roles'];
        __typename?: true;
    }>;
    /** aggregated selection of "auth.account_roles" */
    ['auth_account_roles_aggregate']: AliasType<{
        aggregate?: ValueTypes['auth_account_roles_aggregate_fields'];
        nodes?: ValueTypes['auth_account_roles'];
        __typename?: true;
    }>;
    /** aggregate fields of "auth.account_roles" */
    ['auth_account_roles_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['auth_account_roles_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['auth_account_roles_max_fields'];
        min?: ValueTypes['auth_account_roles_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "auth.account_roles" */
    ['auth_account_roles_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['auth_account_roles_max_order_by'];
        min?: ValueTypes['auth_account_roles_min_order_by'];
    };
    /** input type for inserting array relation for remote table "auth.account_roles" */
    ['auth_account_roles_arr_rel_insert_input']: {
        data: ValueTypes['auth_account_roles_insert_input'][];
        on_conflict?: ValueTypes['auth_account_roles_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.account_roles". All fields are combined with a logical 'AND'. */
    ['auth_account_roles_bool_exp']: {
        _and?: (ValueTypes['auth_account_roles_bool_exp'] | undefined)[];
        _not?: ValueTypes['auth_account_roles_bool_exp'];
        _or?: (ValueTypes['auth_account_roles_bool_exp'] | undefined)[];
        account?: ValueTypes['auth_accounts_bool_exp'];
        account_id?: ValueTypes['uuid_comparison_exp'];
        created_at?: ValueTypes['timestamptz_comparison_exp'];
        id?: ValueTypes['uuid_comparison_exp'];
        role?: ValueTypes['String_comparison_exp'];
        roleByRole?: ValueTypes['auth_roles_bool_exp'];
    };
    /** unique or primary key constraints on table "auth.account_roles" */
    ['auth_account_roles_constraint']: auth_account_roles_constraint;
    /** input type for inserting data into table "auth.account_roles" */
    ['auth_account_roles_insert_input']: {
        account?: ValueTypes['auth_accounts_obj_rel_insert_input'];
        account_id?: ValueTypes['uuid'];
        created_at?: ValueTypes['timestamptz'];
        id?: ValueTypes['uuid'];
        role?: string;
        roleByRole?: ValueTypes['auth_roles_obj_rel_insert_input'];
    };
    /** aggregate max on columns */
    ['auth_account_roles_max_fields']: AliasType<{
        account_id?: true;
        created_at?: true;
        id?: true;
        role?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "auth.account_roles" */
    ['auth_account_roles_max_order_by']: {
        account_id?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        role?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_account_roles_min_fields']: AliasType<{
        account_id?: true;
        created_at?: true;
        id?: true;
        role?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "auth.account_roles" */
    ['auth_account_roles_min_order_by']: {
        account_id?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        role?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "auth.account_roles" */
    ['auth_account_roles_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['auth_account_roles'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "auth.account_roles" */
    ['auth_account_roles_obj_rel_insert_input']: {
        data: ValueTypes['auth_account_roles_insert_input'];
        on_conflict?: ValueTypes['auth_account_roles_on_conflict'];
    };
    /** on conflict condition type for table "auth.account_roles" */
    ['auth_account_roles_on_conflict']: {
        constraint: ValueTypes['auth_account_roles_constraint'];
        update_columns: ValueTypes['auth_account_roles_update_column'][];
        where?: ValueTypes['auth_account_roles_bool_exp'];
    };
    /** ordering options when selecting data from "auth.account_roles" */
    ['auth_account_roles_order_by']: {
        account?: ValueTypes['auth_accounts_order_by'];
        account_id?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        role?: ValueTypes['order_by'];
        roleByRole?: ValueTypes['auth_roles_order_by'];
    };
    /** primary key columns input for table: "auth.account_roles" */
    ['auth_account_roles_pk_columns_input']: {
        id: ValueTypes['uuid'];
    };
    /** select columns of table "auth.account_roles" */
    ['auth_account_roles_select_column']: auth_account_roles_select_column;
    /** input type for updating data in table "auth.account_roles" */
    ['auth_account_roles_set_input']: {
        account_id?: ValueTypes['uuid'];
        created_at?: ValueTypes['timestamptz'];
        id?: ValueTypes['uuid'];
        role?: string;
    };
    /** update columns of table "auth.account_roles" */
    ['auth_account_roles_update_column']: auth_account_roles_update_column;
    /** columns and relationships of "auth.accounts" */
    ['auth_accounts']: AliasType<{
        account_providers?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_providers_bool_exp'];
            },
            ValueTypes['auth_account_providers']
        ];
        account_providers_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_providers_bool_exp'];
            },
            ValueTypes['auth_account_providers_aggregate']
        ];
        account_roles?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_roles_bool_exp'];
            },
            ValueTypes['auth_account_roles']
        ];
        account_roles_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_roles_bool_exp'];
            },
            ValueTypes['auth_account_roles_aggregate']
        ];
        active?: true;
        created_at?: true;
        custom_register_data?: [
            {
                /** JSON select path */ path?: string;
            },
            true
        ];
        default_role?: true;
        email?: true;
        id?: true;
        is_anonymous?: true;
        mfa_enabled?: true;
        new_email?: true;
        otp_secret?: true;
        password_hash?: true;
        refresh_tokens?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_refresh_tokens_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_refresh_tokens_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_refresh_tokens_bool_exp'];
            },
            ValueTypes['auth_refresh_tokens']
        ];
        refresh_tokens_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_refresh_tokens_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_refresh_tokens_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_refresh_tokens_bool_exp'];
            },
            ValueTypes['auth_refresh_tokens_aggregate']
        ];
        /** An object relationship */
        role?: ValueTypes['auth_roles'];
        ticket?: true;
        ticket_expires_at?: true;
        updated_at?: true;
        /** An object relationship */
        user?: ValueTypes['users'];
        user_id?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "auth.accounts" */
    ['auth_accounts_aggregate']: AliasType<{
        aggregate?: ValueTypes['auth_accounts_aggregate_fields'];
        nodes?: ValueTypes['auth_accounts'];
        __typename?: true;
    }>;
    /** aggregate fields of "auth.accounts" */
    ['auth_accounts_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['auth_accounts_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['auth_accounts_max_fields'];
        min?: ValueTypes['auth_accounts_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "auth.accounts" */
    ['auth_accounts_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['auth_accounts_max_order_by'];
        min?: ValueTypes['auth_accounts_min_order_by'];
    };
    /** append existing jsonb value of filtered columns with new jsonb value */
    ['auth_accounts_append_input']: {
        custom_register_data?: ValueTypes['jsonb'];
    };
    /** input type for inserting array relation for remote table "auth.accounts" */
    ['auth_accounts_arr_rel_insert_input']: {
        data: ValueTypes['auth_accounts_insert_input'][];
        on_conflict?: ValueTypes['auth_accounts_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.accounts". All fields are combined with a logical 'AND'. */
    ['auth_accounts_bool_exp']: {
        _and?: (ValueTypes['auth_accounts_bool_exp'] | undefined)[];
        _not?: ValueTypes['auth_accounts_bool_exp'];
        _or?: (ValueTypes['auth_accounts_bool_exp'] | undefined)[];
        account_providers?: ValueTypes['auth_account_providers_bool_exp'];
        account_roles?: ValueTypes['auth_account_roles_bool_exp'];
        active?: ValueTypes['Boolean_comparison_exp'];
        created_at?: ValueTypes['timestamptz_comparison_exp'];
        custom_register_data?: ValueTypes['jsonb_comparison_exp'];
        default_role?: ValueTypes['String_comparison_exp'];
        email?: ValueTypes['citext_comparison_exp'];
        id?: ValueTypes['uuid_comparison_exp'];
        is_anonymous?: ValueTypes['Boolean_comparison_exp'];
        mfa_enabled?: ValueTypes['Boolean_comparison_exp'];
        new_email?: ValueTypes['citext_comparison_exp'];
        otp_secret?: ValueTypes['String_comparison_exp'];
        password_hash?: ValueTypes['String_comparison_exp'];
        refresh_tokens?: ValueTypes['auth_refresh_tokens_bool_exp'];
        role?: ValueTypes['auth_roles_bool_exp'];
        ticket?: ValueTypes['uuid_comparison_exp'];
        ticket_expires_at?: ValueTypes['timestamptz_comparison_exp'];
        updated_at?: ValueTypes['timestamptz_comparison_exp'];
        user?: ValueTypes['users_bool_exp'];
        user_id?: ValueTypes['uuid_comparison_exp'];
    };
    /** unique or primary key constraints on table "auth.accounts" */
    ['auth_accounts_constraint']: auth_accounts_constraint;
    /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
    ['auth_accounts_delete_at_path_input']: {
        custom_register_data?: (string | undefined)[];
    };
    /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
    ['auth_accounts_delete_elem_input']: {
        custom_register_data?: number;
    };
    /** delete key/value pair or string element. key/value pairs are matched based on their key value */
    ['auth_accounts_delete_key_input']: {
        custom_register_data?: string;
    };
    /** input type for inserting data into table "auth.accounts" */
    ['auth_accounts_insert_input']: {
        account_providers?: ValueTypes['auth_account_providers_arr_rel_insert_input'];
        account_roles?: ValueTypes['auth_account_roles_arr_rel_insert_input'];
        active?: boolean;
        created_at?: ValueTypes['timestamptz'];
        custom_register_data?: ValueTypes['jsonb'];
        default_role?: string;
        email?: ValueTypes['citext'];
        id?: ValueTypes['uuid'];
        is_anonymous?: boolean;
        mfa_enabled?: boolean;
        new_email?: ValueTypes['citext'];
        otp_secret?: string;
        password_hash?: string;
        refresh_tokens?: ValueTypes['auth_refresh_tokens_arr_rel_insert_input'];
        role?: ValueTypes['auth_roles_obj_rel_insert_input'];
        ticket?: ValueTypes['uuid'];
        ticket_expires_at?: ValueTypes['timestamptz'];
        updated_at?: ValueTypes['timestamptz'];
        user?: ValueTypes['users_obj_rel_insert_input'];
        user_id?: ValueTypes['uuid'];
    };
    /** aggregate max on columns */
    ['auth_accounts_max_fields']: AliasType<{
        created_at?: true;
        default_role?: true;
        email?: true;
        id?: true;
        new_email?: true;
        otp_secret?: true;
        password_hash?: true;
        ticket?: true;
        ticket_expires_at?: true;
        updated_at?: true;
        user_id?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "auth.accounts" */
    ['auth_accounts_max_order_by']: {
        created_at?: ValueTypes['order_by'];
        default_role?: ValueTypes['order_by'];
        email?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        new_email?: ValueTypes['order_by'];
        otp_secret?: ValueTypes['order_by'];
        password_hash?: ValueTypes['order_by'];
        ticket?: ValueTypes['order_by'];
        ticket_expires_at?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
        user_id?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_accounts_min_fields']: AliasType<{
        created_at?: true;
        default_role?: true;
        email?: true;
        id?: true;
        new_email?: true;
        otp_secret?: true;
        password_hash?: true;
        ticket?: true;
        ticket_expires_at?: true;
        updated_at?: true;
        user_id?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "auth.accounts" */
    ['auth_accounts_min_order_by']: {
        created_at?: ValueTypes['order_by'];
        default_role?: ValueTypes['order_by'];
        email?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        new_email?: ValueTypes['order_by'];
        otp_secret?: ValueTypes['order_by'];
        password_hash?: ValueTypes['order_by'];
        ticket?: ValueTypes['order_by'];
        ticket_expires_at?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
        user_id?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "auth.accounts" */
    ['auth_accounts_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['auth_accounts'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "auth.accounts" */
    ['auth_accounts_obj_rel_insert_input']: {
        data: ValueTypes['auth_accounts_insert_input'];
        on_conflict?: ValueTypes['auth_accounts_on_conflict'];
    };
    /** on conflict condition type for table "auth.accounts" */
    ['auth_accounts_on_conflict']: {
        constraint: ValueTypes['auth_accounts_constraint'];
        update_columns: ValueTypes['auth_accounts_update_column'][];
        where?: ValueTypes['auth_accounts_bool_exp'];
    };
    /** ordering options when selecting data from "auth.accounts" */
    ['auth_accounts_order_by']: {
        account_providers_aggregate?: ValueTypes['auth_account_providers_aggregate_order_by'];
        account_roles_aggregate?: ValueTypes['auth_account_roles_aggregate_order_by'];
        active?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        custom_register_data?: ValueTypes['order_by'];
        default_role?: ValueTypes['order_by'];
        email?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        is_anonymous?: ValueTypes['order_by'];
        mfa_enabled?: ValueTypes['order_by'];
        new_email?: ValueTypes['order_by'];
        otp_secret?: ValueTypes['order_by'];
        password_hash?: ValueTypes['order_by'];
        refresh_tokens_aggregate?: ValueTypes['auth_refresh_tokens_aggregate_order_by'];
        role?: ValueTypes['auth_roles_order_by'];
        ticket?: ValueTypes['order_by'];
        ticket_expires_at?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
        user?: ValueTypes['users_order_by'];
        user_id?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "auth.accounts" */
    ['auth_accounts_pk_columns_input']: {
        id: ValueTypes['uuid'];
    };
    /** prepend existing jsonb value of filtered columns with new jsonb value */
    ['auth_accounts_prepend_input']: {
        custom_register_data?: ValueTypes['jsonb'];
    };
    /** select columns of table "auth.accounts" */
    ['auth_accounts_select_column']: auth_accounts_select_column;
    /** input type for updating data in table "auth.accounts" */
    ['auth_accounts_set_input']: {
        active?: boolean;
        created_at?: ValueTypes['timestamptz'];
        custom_register_data?: ValueTypes['jsonb'];
        default_role?: string;
        email?: ValueTypes['citext'];
        id?: ValueTypes['uuid'];
        is_anonymous?: boolean;
        mfa_enabled?: boolean;
        new_email?: ValueTypes['citext'];
        otp_secret?: string;
        password_hash?: string;
        ticket?: ValueTypes['uuid'];
        ticket_expires_at?: ValueTypes['timestamptz'];
        updated_at?: ValueTypes['timestamptz'];
        user_id?: ValueTypes['uuid'];
    };
    /** update columns of table "auth.accounts" */
    ['auth_accounts_update_column']: auth_accounts_update_column;
    /** columns and relationships of "auth.providers" */
    ['auth_providers']: AliasType<{
        account_providers?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_providers_bool_exp'];
            },
            ValueTypes['auth_account_providers']
        ];
        account_providers_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_providers_bool_exp'];
            },
            ValueTypes['auth_account_providers_aggregate']
        ];
        provider?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "auth.providers" */
    ['auth_providers_aggregate']: AliasType<{
        aggregate?: ValueTypes['auth_providers_aggregate_fields'];
        nodes?: ValueTypes['auth_providers'];
        __typename?: true;
    }>;
    /** aggregate fields of "auth.providers" */
    ['auth_providers_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['auth_providers_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['auth_providers_max_fields'];
        min?: ValueTypes['auth_providers_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "auth.providers" */
    ['auth_providers_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['auth_providers_max_order_by'];
        min?: ValueTypes['auth_providers_min_order_by'];
    };
    /** input type for inserting array relation for remote table "auth.providers" */
    ['auth_providers_arr_rel_insert_input']: {
        data: ValueTypes['auth_providers_insert_input'][];
        on_conflict?: ValueTypes['auth_providers_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.providers". All fields are combined with a logical 'AND'. */
    ['auth_providers_bool_exp']: {
        _and?: (ValueTypes['auth_providers_bool_exp'] | undefined)[];
        _not?: ValueTypes['auth_providers_bool_exp'];
        _or?: (ValueTypes['auth_providers_bool_exp'] | undefined)[];
        account_providers?: ValueTypes['auth_account_providers_bool_exp'];
        provider?: ValueTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "auth.providers" */
    ['auth_providers_constraint']: auth_providers_constraint;
    /** input type for inserting data into table "auth.providers" */
    ['auth_providers_insert_input']: {
        account_providers?: ValueTypes['auth_account_providers_arr_rel_insert_input'];
        provider?: string;
    };
    /** aggregate max on columns */
    ['auth_providers_max_fields']: AliasType<{
        provider?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "auth.providers" */
    ['auth_providers_max_order_by']: {
        provider?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_providers_min_fields']: AliasType<{
        provider?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "auth.providers" */
    ['auth_providers_min_order_by']: {
        provider?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "auth.providers" */
    ['auth_providers_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['auth_providers'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "auth.providers" */
    ['auth_providers_obj_rel_insert_input']: {
        data: ValueTypes['auth_providers_insert_input'];
        on_conflict?: ValueTypes['auth_providers_on_conflict'];
    };
    /** on conflict condition type for table "auth.providers" */
    ['auth_providers_on_conflict']: {
        constraint: ValueTypes['auth_providers_constraint'];
        update_columns: ValueTypes['auth_providers_update_column'][];
        where?: ValueTypes['auth_providers_bool_exp'];
    };
    /** ordering options when selecting data from "auth.providers" */
    ['auth_providers_order_by']: {
        account_providers_aggregate?: ValueTypes['auth_account_providers_aggregate_order_by'];
        provider?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "auth.providers" */
    ['auth_providers_pk_columns_input']: {
        provider: string;
    };
    /** select columns of table "auth.providers" */
    ['auth_providers_select_column']: auth_providers_select_column;
    /** input type for updating data in table "auth.providers" */
    ['auth_providers_set_input']: {
        provider?: string;
    };
    /** update columns of table "auth.providers" */
    ['auth_providers_update_column']: auth_providers_update_column;
    /** columns and relationships of "auth.refresh_tokens" */
    ['auth_refresh_tokens']: AliasType<{
        /** An object relationship */
        account?: ValueTypes['auth_accounts'];
        account_id?: true;
        created_at?: true;
        expires_at?: true;
        refresh_token?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "auth.refresh_tokens" */
    ['auth_refresh_tokens_aggregate']: AliasType<{
        aggregate?: ValueTypes['auth_refresh_tokens_aggregate_fields'];
        nodes?: ValueTypes['auth_refresh_tokens'];
        __typename?: true;
    }>;
    /** aggregate fields of "auth.refresh_tokens" */
    ['auth_refresh_tokens_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['auth_refresh_tokens_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['auth_refresh_tokens_max_fields'];
        min?: ValueTypes['auth_refresh_tokens_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['auth_refresh_tokens_max_order_by'];
        min?: ValueTypes['auth_refresh_tokens_min_order_by'];
    };
    /** input type for inserting array relation for remote table "auth.refresh_tokens" */
    ['auth_refresh_tokens_arr_rel_insert_input']: {
        data: ValueTypes['auth_refresh_tokens_insert_input'][];
        on_conflict?: ValueTypes['auth_refresh_tokens_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.refresh_tokens". All fields are combined with a logical 'AND'. */
    ['auth_refresh_tokens_bool_exp']: {
        _and?: (ValueTypes['auth_refresh_tokens_bool_exp'] | undefined)[];
        _not?: ValueTypes['auth_refresh_tokens_bool_exp'];
        _or?: (ValueTypes['auth_refresh_tokens_bool_exp'] | undefined)[];
        account?: ValueTypes['auth_accounts_bool_exp'];
        account_id?: ValueTypes['uuid_comparison_exp'];
        created_at?: ValueTypes['timestamptz_comparison_exp'];
        expires_at?: ValueTypes['timestamptz_comparison_exp'];
        refresh_token?: ValueTypes['uuid_comparison_exp'];
    };
    /** unique or primary key constraints on table "auth.refresh_tokens" */
    ['auth_refresh_tokens_constraint']: auth_refresh_tokens_constraint;
    /** input type for inserting data into table "auth.refresh_tokens" */
    ['auth_refresh_tokens_insert_input']: {
        account?: ValueTypes['auth_accounts_obj_rel_insert_input'];
        account_id?: ValueTypes['uuid'];
        created_at?: ValueTypes['timestamptz'];
        expires_at?: ValueTypes['timestamptz'];
        refresh_token?: ValueTypes['uuid'];
    };
    /** aggregate max on columns */
    ['auth_refresh_tokens_max_fields']: AliasType<{
        account_id?: true;
        created_at?: true;
        expires_at?: true;
        refresh_token?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_max_order_by']: {
        account_id?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        expires_at?: ValueTypes['order_by'];
        refresh_token?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_refresh_tokens_min_fields']: AliasType<{
        account_id?: true;
        created_at?: true;
        expires_at?: true;
        refresh_token?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_min_order_by']: {
        account_id?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        expires_at?: ValueTypes['order_by'];
        refresh_token?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "auth.refresh_tokens" */
    ['auth_refresh_tokens_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['auth_refresh_tokens'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "auth.refresh_tokens" */
    ['auth_refresh_tokens_obj_rel_insert_input']: {
        data: ValueTypes['auth_refresh_tokens_insert_input'];
        on_conflict?: ValueTypes['auth_refresh_tokens_on_conflict'];
    };
    /** on conflict condition type for table "auth.refresh_tokens" */
    ['auth_refresh_tokens_on_conflict']: {
        constraint: ValueTypes['auth_refresh_tokens_constraint'];
        update_columns: ValueTypes['auth_refresh_tokens_update_column'][];
        where?: ValueTypes['auth_refresh_tokens_bool_exp'];
    };
    /** ordering options when selecting data from "auth.refresh_tokens" */
    ['auth_refresh_tokens_order_by']: {
        account?: ValueTypes['auth_accounts_order_by'];
        account_id?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        expires_at?: ValueTypes['order_by'];
        refresh_token?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "auth.refresh_tokens" */
    ['auth_refresh_tokens_pk_columns_input']: {
        refresh_token: ValueTypes['uuid'];
    };
    /** select columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_select_column']: auth_refresh_tokens_select_column;
    /** input type for updating data in table "auth.refresh_tokens" */
    ['auth_refresh_tokens_set_input']: {
        account_id?: ValueTypes['uuid'];
        created_at?: ValueTypes['timestamptz'];
        expires_at?: ValueTypes['timestamptz'];
        refresh_token?: ValueTypes['uuid'];
    };
    /** update columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_update_column']: auth_refresh_tokens_update_column;
    /** columns and relationships of "auth.roles" */
    ['auth_roles']: AliasType<{
        account_roles?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_roles_bool_exp'];
            },
            ValueTypes['auth_account_roles']
        ];
        account_roles_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_roles_bool_exp'];
            },
            ValueTypes['auth_account_roles_aggregate']
        ];
        accounts?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_accounts_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_accounts_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_accounts_bool_exp'];
            },
            ValueTypes['auth_accounts']
        ];
        accounts_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_accounts_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_accounts_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_accounts_bool_exp'];
            },
            ValueTypes['auth_accounts_aggregate']
        ];
        role?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "auth.roles" */
    ['auth_roles_aggregate']: AliasType<{
        aggregate?: ValueTypes['auth_roles_aggregate_fields'];
        nodes?: ValueTypes['auth_roles'];
        __typename?: true;
    }>;
    /** aggregate fields of "auth.roles" */
    ['auth_roles_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['auth_roles_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['auth_roles_max_fields'];
        min?: ValueTypes['auth_roles_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "auth.roles" */
    ['auth_roles_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['auth_roles_max_order_by'];
        min?: ValueTypes['auth_roles_min_order_by'];
    };
    /** input type for inserting array relation for remote table "auth.roles" */
    ['auth_roles_arr_rel_insert_input']: {
        data: ValueTypes['auth_roles_insert_input'][];
        on_conflict?: ValueTypes['auth_roles_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.roles". All fields are combined with a logical 'AND'. */
    ['auth_roles_bool_exp']: {
        _and?: (ValueTypes['auth_roles_bool_exp'] | undefined)[];
        _not?: ValueTypes['auth_roles_bool_exp'];
        _or?: (ValueTypes['auth_roles_bool_exp'] | undefined)[];
        account_roles?: ValueTypes['auth_account_roles_bool_exp'];
        accounts?: ValueTypes['auth_accounts_bool_exp'];
        role?: ValueTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "auth.roles" */
    ['auth_roles_constraint']: auth_roles_constraint;
    /** input type for inserting data into table "auth.roles" */
    ['auth_roles_insert_input']: {
        account_roles?: ValueTypes['auth_account_roles_arr_rel_insert_input'];
        accounts?: ValueTypes['auth_accounts_arr_rel_insert_input'];
        role?: string;
    };
    /** aggregate max on columns */
    ['auth_roles_max_fields']: AliasType<{
        role?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "auth.roles" */
    ['auth_roles_max_order_by']: {
        role?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_roles_min_fields']: AliasType<{
        role?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "auth.roles" */
    ['auth_roles_min_order_by']: {
        role?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "auth.roles" */
    ['auth_roles_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['auth_roles'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "auth.roles" */
    ['auth_roles_obj_rel_insert_input']: {
        data: ValueTypes['auth_roles_insert_input'];
        on_conflict?: ValueTypes['auth_roles_on_conflict'];
    };
    /** on conflict condition type for table "auth.roles" */
    ['auth_roles_on_conflict']: {
        constraint: ValueTypes['auth_roles_constraint'];
        update_columns: ValueTypes['auth_roles_update_column'][];
        where?: ValueTypes['auth_roles_bool_exp'];
    };
    /** ordering options when selecting data from "auth.roles" */
    ['auth_roles_order_by']: {
        account_roles_aggregate?: ValueTypes['auth_account_roles_aggregate_order_by'];
        accounts_aggregate?: ValueTypes['auth_accounts_aggregate_order_by'];
        role?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "auth.roles" */
    ['auth_roles_pk_columns_input']: {
        role: string;
    };
    /** select columns of table "auth.roles" */
    ['auth_roles_select_column']: auth_roles_select_column;
    /** input type for updating data in table "auth.roles" */
    ['auth_roles_set_input']: {
        role?: string;
    };
    /** update columns of table "auth.roles" */
    ['auth_roles_update_column']: auth_roles_update_column;
    ['citext']: unknown;
    /** expression to compare columns of type citext. All fields are combined with logical 'AND'. */
    ['citext_comparison_exp']: {
        _eq?: ValueTypes['citext'];
        _gt?: ValueTypes['citext'];
        _gte?: ValueTypes['citext'];
        _ilike?: string;
        _in?: ValueTypes['citext'][];
        _is_null?: boolean;
        _like?: string;
        _lt?: ValueTypes['citext'];
        _lte?: ValueTypes['citext'];
        _neq?: ValueTypes['citext'];
        _nilike?: string;
        _nin?: ValueTypes['citext'][];
        _nlike?: string;
        _nsimilar?: string;
        _similar?: string;
    };
    /** columns and relationships of "contribution_types" */
    ['contribution_types']: AliasType<{
        description?: true;
        groups?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['groups_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['groups_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups']
        ];
        groups_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['groups_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['groups_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups_aggregate']
        ];
        value?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "contribution_types" */
    ['contribution_types_aggregate']: AliasType<{
        aggregate?: ValueTypes['contribution_types_aggregate_fields'];
        nodes?: ValueTypes['contribution_types'];
        __typename?: true;
    }>;
    /** aggregate fields of "contribution_types" */
    ['contribution_types_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['contribution_types_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['contribution_types_max_fields'];
        min?: ValueTypes['contribution_types_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "contribution_types" */
    ['contribution_types_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['contribution_types_max_order_by'];
        min?: ValueTypes['contribution_types_min_order_by'];
    };
    /** input type for inserting array relation for remote table "contribution_types" */
    ['contribution_types_arr_rel_insert_input']: {
        data: ValueTypes['contribution_types_insert_input'][];
        on_conflict?: ValueTypes['contribution_types_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "contribution_types". All fields are combined with a logical 'AND'. */
    ['contribution_types_bool_exp']: {
        _and?: (ValueTypes['contribution_types_bool_exp'] | undefined)[];
        _not?: ValueTypes['contribution_types_bool_exp'];
        _or?: (ValueTypes['contribution_types_bool_exp'] | undefined)[];
        description?: ValueTypes['String_comparison_exp'];
        groups?: ValueTypes['groups_bool_exp'];
        value?: ValueTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "contribution_types" */
    ['contribution_types_constraint']: contribution_types_constraint;
    ['contribution_types_enum']: contribution_types_enum;
    /** expression to compare columns of type contribution_types_enum. All fields are combined with logical 'AND'. */
    ['contribution_types_enum_comparison_exp']: {
        _eq?: ValueTypes['contribution_types_enum'];
        _in?: ValueTypes['contribution_types_enum'][];
        _is_null?: boolean;
        _neq?: ValueTypes['contribution_types_enum'];
        _nin?: ValueTypes['contribution_types_enum'][];
    };
    /** input type for inserting data into table "contribution_types" */
    ['contribution_types_insert_input']: {
        description?: string;
        groups?: ValueTypes['groups_arr_rel_insert_input'];
        value?: string;
    };
    /** aggregate max on columns */
    ['contribution_types_max_fields']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "contribution_types" */
    ['contribution_types_max_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['contribution_types_min_fields']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "contribution_types" */
    ['contribution_types_min_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "contribution_types" */
    ['contribution_types_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['contribution_types'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "contribution_types" */
    ['contribution_types_obj_rel_insert_input']: {
        data: ValueTypes['contribution_types_insert_input'];
        on_conflict?: ValueTypes['contribution_types_on_conflict'];
    };
    /** on conflict condition type for table "contribution_types" */
    ['contribution_types_on_conflict']: {
        constraint: ValueTypes['contribution_types_constraint'];
        update_columns: ValueTypes['contribution_types_update_column'][];
        where?: ValueTypes['contribution_types_bool_exp'];
    };
    /** ordering options when selecting data from "contribution_types" */
    ['contribution_types_order_by']: {
        description?: ValueTypes['order_by'];
        groups_aggregate?: ValueTypes['groups_aggregate_order_by'];
        value?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "contribution_types" */
    ['contribution_types_pk_columns_input']: {
        value: string;
    };
    /** select columns of table "contribution_types" */
    ['contribution_types_select_column']: contribution_types_select_column;
    /** input type for updating data in table "contribution_types" */
    ['contribution_types_set_input']: {
        description?: string;
        value?: string;
    };
    /** update columns of table "contribution_types" */
    ['contribution_types_update_column']: contribution_types_update_column;
    /** columns and relationships of "group_cycles" */
    ['group_cycles']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "group_cycles" */
    ['group_cycles_aggregate']: AliasType<{
        aggregate?: ValueTypes['group_cycles_aggregate_fields'];
        nodes?: ValueTypes['group_cycles'];
        __typename?: true;
    }>;
    /** aggregate fields of "group_cycles" */
    ['group_cycles_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['group_cycles_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['group_cycles_max_fields'];
        min?: ValueTypes['group_cycles_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "group_cycles" */
    ['group_cycles_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['group_cycles_max_order_by'];
        min?: ValueTypes['group_cycles_min_order_by'];
    };
    /** input type for inserting array relation for remote table "group_cycles" */
    ['group_cycles_arr_rel_insert_input']: {
        data: ValueTypes['group_cycles_insert_input'][];
        on_conflict?: ValueTypes['group_cycles_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "group_cycles". All fields are combined with a logical 'AND'. */
    ['group_cycles_bool_exp']: {
        _and?: (ValueTypes['group_cycles_bool_exp'] | undefined)[];
        _not?: ValueTypes['group_cycles_bool_exp'];
        _or?: (ValueTypes['group_cycles_bool_exp'] | undefined)[];
        description?: ValueTypes['String_comparison_exp'];
        value?: ValueTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "group_cycles" */
    ['group_cycles_constraint']: group_cycles_constraint;
    /** input type for inserting data into table "group_cycles" */
    ['group_cycles_insert_input']: {
        description?: string;
        value?: string;
    };
    /** aggregate max on columns */
    ['group_cycles_max_fields']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "group_cycles" */
    ['group_cycles_max_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['group_cycles_min_fields']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "group_cycles" */
    ['group_cycles_min_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "group_cycles" */
    ['group_cycles_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['group_cycles'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "group_cycles" */
    ['group_cycles_obj_rel_insert_input']: {
        data: ValueTypes['group_cycles_insert_input'];
        on_conflict?: ValueTypes['group_cycles_on_conflict'];
    };
    /** on conflict condition type for table "group_cycles" */
    ['group_cycles_on_conflict']: {
        constraint: ValueTypes['group_cycles_constraint'];
        update_columns: ValueTypes['group_cycles_update_column'][];
        where?: ValueTypes['group_cycles_bool_exp'];
    };
    /** ordering options when selecting data from "group_cycles" */
    ['group_cycles_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "group_cycles" */
    ['group_cycles_pk_columns_input']: {
        value: string;
    };
    /** select columns of table "group_cycles" */
    ['group_cycles_select_column']: group_cycles_select_column;
    /** input type for updating data in table "group_cycles" */
    ['group_cycles_set_input']: {
        description?: string;
        value?: string;
    };
    /** update columns of table "group_cycles" */
    ['group_cycles_update_column']: group_cycles_update_column;
    /** columns and relationships of "group_recurrencies" */
    ['group_recurrencies']: AliasType<{
        description?: true;
        groups?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['groups_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['groups_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups']
        ];
        groups_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['groups_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['groups_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups_aggregate']
        ];
        value?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "group_recurrencies" */
    ['group_recurrencies_aggregate']: AliasType<{
        aggregate?: ValueTypes['group_recurrencies_aggregate_fields'];
        nodes?: ValueTypes['group_recurrencies'];
        __typename?: true;
    }>;
    /** aggregate fields of "group_recurrencies" */
    ['group_recurrencies_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['group_recurrencies_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['group_recurrencies_max_fields'];
        min?: ValueTypes['group_recurrencies_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "group_recurrencies" */
    ['group_recurrencies_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['group_recurrencies_max_order_by'];
        min?: ValueTypes['group_recurrencies_min_order_by'];
    };
    /** input type for inserting array relation for remote table "group_recurrencies" */
    ['group_recurrencies_arr_rel_insert_input']: {
        data: ValueTypes['group_recurrencies_insert_input'][];
        on_conflict?: ValueTypes['group_recurrencies_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "group_recurrencies". All fields are combined with a logical 'AND'. */
    ['group_recurrencies_bool_exp']: {
        _and?: (ValueTypes['group_recurrencies_bool_exp'] | undefined)[];
        _not?: ValueTypes['group_recurrencies_bool_exp'];
        _or?: (ValueTypes['group_recurrencies_bool_exp'] | undefined)[];
        description?: ValueTypes['String_comparison_exp'];
        groups?: ValueTypes['groups_bool_exp'];
        value?: ValueTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "group_recurrencies" */
    ['group_recurrencies_constraint']: group_recurrencies_constraint;
    ['group_recurrencies_enum']: group_recurrencies_enum;
    /** expression to compare columns of type group_recurrencies_enum. All fields are combined with logical 'AND'. */
    ['group_recurrencies_enum_comparison_exp']: {
        _eq?: ValueTypes['group_recurrencies_enum'];
        _in?: ValueTypes['group_recurrencies_enum'][];
        _is_null?: boolean;
        _neq?: ValueTypes['group_recurrencies_enum'];
        _nin?: ValueTypes['group_recurrencies_enum'][];
    };
    /** input type for inserting data into table "group_recurrencies" */
    ['group_recurrencies_insert_input']: {
        description?: string;
        groups?: ValueTypes['groups_arr_rel_insert_input'];
        value?: string;
    };
    /** aggregate max on columns */
    ['group_recurrencies_max_fields']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "group_recurrencies" */
    ['group_recurrencies_max_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['group_recurrencies_min_fields']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "group_recurrencies" */
    ['group_recurrencies_min_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "group_recurrencies" */
    ['group_recurrencies_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['group_recurrencies'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "group_recurrencies" */
    ['group_recurrencies_obj_rel_insert_input']: {
        data: ValueTypes['group_recurrencies_insert_input'];
        on_conflict?: ValueTypes['group_recurrencies_on_conflict'];
    };
    /** on conflict condition type for table "group_recurrencies" */
    ['group_recurrencies_on_conflict']: {
        constraint: ValueTypes['group_recurrencies_constraint'];
        update_columns: ValueTypes['group_recurrencies_update_column'][];
        where?: ValueTypes['group_recurrencies_bool_exp'];
    };
    /** ordering options when selecting data from "group_recurrencies" */
    ['group_recurrencies_order_by']: {
        description?: ValueTypes['order_by'];
        groups_aggregate?: ValueTypes['groups_aggregate_order_by'];
        value?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "group_recurrencies" */
    ['group_recurrencies_pk_columns_input']: {
        value: string;
    };
    /** select columns of table "group_recurrencies" */
    ['group_recurrencies_select_column']: group_recurrencies_select_column;
    /** input type for updating data in table "group_recurrencies" */
    ['group_recurrencies_set_input']: {
        description?: string;
        value?: string;
    };
    /** update columns of table "group_recurrencies" */
    ['group_recurrencies_update_column']: group_recurrencies_update_column;
    /** columns and relationships of "groups" */
    ['groups']: AliasType<{
        /** An object relationship */
        contribution_type?: ValueTypes['contribution_types'];
        created_at?: true;
        /** An object relationship */
        creator?: ValueTypes['users'];
        creator_id?: true;
        group_balance?: true;
        group_code?: true;
        group_contribution_amount?: true;
        group_contribution_type?: true;
        group_name?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        group_recurrency_type?: true;
        id?: true;
        members?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['members_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['members_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['members_bool_exp'];
            },
            ValueTypes['members']
        ];
        members_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['members_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['members_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['members_bool_exp'];
            },
            ValueTypes['members_aggregate']
        ];
        /** An object relationship */
        payment_frequency?: ValueTypes['group_recurrencies'];
        payments?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments']
        ];
        payments_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments_aggregate']
        ];
        periods?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['periods_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['periods_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['periods_bool_exp'];
            },
            ValueTypes['periods']
        ];
        periods_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['periods_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['periods_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['periods_bool_exp'];
            },
            ValueTypes['periods_aggregate']
        ];
        updated_at?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "groups" */
    ['groups_aggregate']: AliasType<{
        aggregate?: ValueTypes['groups_aggregate_fields'];
        nodes?: ValueTypes['groups'];
        __typename?: true;
    }>;
    /** aggregate fields of "groups" */
    ['groups_aggregate_fields']: AliasType<{
        avg?: ValueTypes['groups_avg_fields'];
        count?: [
            {
                columns?: ValueTypes['groups_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['groups_max_fields'];
        min?: ValueTypes['groups_min_fields'];
        stddev?: ValueTypes['groups_stddev_fields'];
        stddev_pop?: ValueTypes['groups_stddev_pop_fields'];
        stddev_samp?: ValueTypes['groups_stddev_samp_fields'];
        sum?: ValueTypes['groups_sum_fields'];
        var_pop?: ValueTypes['groups_var_pop_fields'];
        var_samp?: ValueTypes['groups_var_samp_fields'];
        variance?: ValueTypes['groups_variance_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "groups" */
    ['groups_aggregate_order_by']: {
        avg?: ValueTypes['groups_avg_order_by'];
        count?: ValueTypes['order_by'];
        max?: ValueTypes['groups_max_order_by'];
        min?: ValueTypes['groups_min_order_by'];
        stddev?: ValueTypes['groups_stddev_order_by'];
        stddev_pop?: ValueTypes['groups_stddev_pop_order_by'];
        stddev_samp?: ValueTypes['groups_stddev_samp_order_by'];
        sum?: ValueTypes['groups_sum_order_by'];
        var_pop?: ValueTypes['groups_var_pop_order_by'];
        var_samp?: ValueTypes['groups_var_samp_order_by'];
        variance?: ValueTypes['groups_variance_order_by'];
    };
    /** input type for inserting array relation for remote table "groups" */
    ['groups_arr_rel_insert_input']: {
        data: ValueTypes['groups_insert_input'][];
        on_conflict?: ValueTypes['groups_on_conflict'];
    };
    /** aggregate avg on columns */
    ['groups_avg_fields']: AliasType<{
        group_balance?: true;
        group_contribution_amount?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        __typename?: true;
    }>;
    /** order by avg() on columns of table "groups" */
    ['groups_avg_order_by']: {
        group_balance?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
    };
    /** Boolean expression to filter rows from the table "groups". All fields are combined with a logical 'AND'. */
    ['groups_bool_exp']: {
        _and?: (ValueTypes['groups_bool_exp'] | undefined)[];
        _not?: ValueTypes['groups_bool_exp'];
        _or?: (ValueTypes['groups_bool_exp'] | undefined)[];
        contribution_type?: ValueTypes['contribution_types_bool_exp'];
        created_at?: ValueTypes['timestamp_comparison_exp'];
        creator?: ValueTypes['users_bool_exp'];
        creator_id?: ValueTypes['uuid_comparison_exp'];
        group_balance?: ValueTypes['numeric_comparison_exp'];
        group_code?: ValueTypes['String_comparison_exp'];
        group_contribution_amount?: ValueTypes['numeric_comparison_exp'];
        group_contribution_type?: ValueTypes['contribution_types_enum_comparison_exp'];
        group_name?: ValueTypes['String_comparison_exp'];
        group_recurrency_amount?: ValueTypes['numeric_comparison_exp'];
        group_recurrency_day?: ValueTypes['Int_comparison_exp'];
        group_recurrency_type?: ValueTypes['group_recurrencies_enum_comparison_exp'];
        id?: ValueTypes['uuid_comparison_exp'];
        members?: ValueTypes['members_bool_exp'];
        payment_frequency?: ValueTypes['group_recurrencies_bool_exp'];
        payments?: ValueTypes['payments_bool_exp'];
        periods?: ValueTypes['periods_bool_exp'];
        updated_at?: ValueTypes['timestamptz_comparison_exp'];
    };
    /** unique or primary key constraints on table "groups" */
    ['groups_constraint']: groups_constraint;
    /** input type for incrementing integer column in table "groups" */
    ['groups_inc_input']: {
        group_balance?: ValueTypes['numeric'];
        group_contribution_amount?: ValueTypes['numeric'];
        group_recurrency_amount?: ValueTypes['numeric'];
        group_recurrency_day?: number;
    };
    /** input type for inserting data into table "groups" */
    ['groups_insert_input']: {
        contribution_type?: ValueTypes['contribution_types_obj_rel_insert_input'];
        created_at?: ValueTypes['timestamp'];
        creator?: ValueTypes['users_obj_rel_insert_input'];
        creator_id?: ValueTypes['uuid'];
        group_balance?: ValueTypes['numeric'];
        group_code?: string;
        group_contribution_amount?: ValueTypes['numeric'];
        group_contribution_type?: ValueTypes['contribution_types_enum'];
        group_name?: string;
        group_recurrency_amount?: ValueTypes['numeric'];
        group_recurrency_day?: number;
        group_recurrency_type?: ValueTypes['group_recurrencies_enum'];
        id?: ValueTypes['uuid'];
        members?: ValueTypes['members_arr_rel_insert_input'];
        payment_frequency?: ValueTypes['group_recurrencies_obj_rel_insert_input'];
        payments?: ValueTypes['payments_arr_rel_insert_input'];
        periods?: ValueTypes['periods_arr_rel_insert_input'];
        updated_at?: ValueTypes['timestamptz'];
    };
    /** aggregate max on columns */
    ['groups_max_fields']: AliasType<{
        created_at?: true;
        creator_id?: true;
        group_balance?: true;
        group_code?: true;
        group_contribution_amount?: true;
        group_name?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        id?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "groups" */
    ['groups_max_order_by']: {
        created_at?: ValueTypes['order_by'];
        creator_id?: ValueTypes['order_by'];
        group_balance?: ValueTypes['order_by'];
        group_code?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_name?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['groups_min_fields']: AliasType<{
        created_at?: true;
        creator_id?: true;
        group_balance?: true;
        group_code?: true;
        group_contribution_amount?: true;
        group_name?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        id?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "groups" */
    ['groups_min_order_by']: {
        created_at?: ValueTypes['order_by'];
        creator_id?: ValueTypes['order_by'];
        group_balance?: ValueTypes['order_by'];
        group_code?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_name?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "groups" */
    ['groups_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['groups'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "groups" */
    ['groups_obj_rel_insert_input']: {
        data: ValueTypes['groups_insert_input'];
        on_conflict?: ValueTypes['groups_on_conflict'];
    };
    /** on conflict condition type for table "groups" */
    ['groups_on_conflict']: {
        constraint: ValueTypes['groups_constraint'];
        update_columns: ValueTypes['groups_update_column'][];
        where?: ValueTypes['groups_bool_exp'];
    };
    /** ordering options when selecting data from "groups" */
    ['groups_order_by']: {
        contribution_type?: ValueTypes['contribution_types_order_by'];
        created_at?: ValueTypes['order_by'];
        creator?: ValueTypes['users_order_by'];
        creator_id?: ValueTypes['order_by'];
        group_balance?: ValueTypes['order_by'];
        group_code?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_contribution_type?: ValueTypes['order_by'];
        group_name?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
        group_recurrency_type?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        members_aggregate?: ValueTypes['members_aggregate_order_by'];
        payment_frequency?: ValueTypes['group_recurrencies_order_by'];
        payments_aggregate?: ValueTypes['payments_aggregate_order_by'];
        periods_aggregate?: ValueTypes['periods_aggregate_order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "groups" */
    ['groups_pk_columns_input']: {
        id: ValueTypes['uuid'];
    };
    /** select columns of table "groups" */
    ['groups_select_column']: groups_select_column;
    /** input type for updating data in table "groups" */
    ['groups_set_input']: {
        created_at?: ValueTypes['timestamp'];
        creator_id?: ValueTypes['uuid'];
        group_balance?: ValueTypes['numeric'];
        group_code?: string;
        group_contribution_amount?: ValueTypes['numeric'];
        group_contribution_type?: ValueTypes['contribution_types_enum'];
        group_name?: string;
        group_recurrency_amount?: ValueTypes['numeric'];
        group_recurrency_day?: number;
        group_recurrency_type?: ValueTypes['group_recurrencies_enum'];
        id?: ValueTypes['uuid'];
        updated_at?: ValueTypes['timestamptz'];
    };
    /** aggregate stddev on columns */
    ['groups_stddev_fields']: AliasType<{
        group_balance?: true;
        group_contribution_amount?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        __typename?: true;
    }>;
    /** order by stddev() on columns of table "groups" */
    ['groups_stddev_order_by']: {
        group_balance?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
    };
    /** aggregate stddev_pop on columns */
    ['groups_stddev_pop_fields']: AliasType<{
        group_balance?: true;
        group_contribution_amount?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        __typename?: true;
    }>;
    /** order by stddev_pop() on columns of table "groups" */
    ['groups_stddev_pop_order_by']: {
        group_balance?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
    };
    /** aggregate stddev_samp on columns */
    ['groups_stddev_samp_fields']: AliasType<{
        group_balance?: true;
        group_contribution_amount?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        __typename?: true;
    }>;
    /** order by stddev_samp() on columns of table "groups" */
    ['groups_stddev_samp_order_by']: {
        group_balance?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
    };
    /** aggregate sum on columns */
    ['groups_sum_fields']: AliasType<{
        group_balance?: true;
        group_contribution_amount?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        __typename?: true;
    }>;
    /** order by sum() on columns of table "groups" */
    ['groups_sum_order_by']: {
        group_balance?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
    };
    /** update columns of table "groups" */
    ['groups_update_column']: groups_update_column;
    /** aggregate var_pop on columns */
    ['groups_var_pop_fields']: AliasType<{
        group_balance?: true;
        group_contribution_amount?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        __typename?: true;
    }>;
    /** order by var_pop() on columns of table "groups" */
    ['groups_var_pop_order_by']: {
        group_balance?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
    };
    /** aggregate var_samp on columns */
    ['groups_var_samp_fields']: AliasType<{
        group_balance?: true;
        group_contribution_amount?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        __typename?: true;
    }>;
    /** order by var_samp() on columns of table "groups" */
    ['groups_var_samp_order_by']: {
        group_balance?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
    };
    /** aggregate variance on columns */
    ['groups_variance_fields']: AliasType<{
        group_balance?: true;
        group_contribution_amount?: true;
        group_recurrency_amount?: true;
        group_recurrency_day?: true;
        __typename?: true;
    }>;
    /** order by variance() on columns of table "groups" */
    ['groups_variance_order_by']: {
        group_balance?: ValueTypes['order_by'];
        group_contribution_amount?: ValueTypes['order_by'];
        group_recurrency_amount?: ValueTypes['order_by'];
        group_recurrency_day?: ValueTypes['order_by'];
    };
    ['json']: unknown;
    /** expression to compare columns of type json. All fields are combined with logical 'AND'. */
    ['json_comparison_exp']: {
        _eq?: ValueTypes['json'];
        _gt?: ValueTypes['json'];
        _gte?: ValueTypes['json'];
        _in?: ValueTypes['json'][];
        _is_null?: boolean;
        _lt?: ValueTypes['json'];
        _lte?: ValueTypes['json'];
        _neq?: ValueTypes['json'];
        _nin?: ValueTypes['json'][];
    };
    ['jsonb']: unknown;
    /** expression to compare columns of type jsonb. All fields are combined with logical 'AND'. */
    ['jsonb_comparison_exp']: {
        /** is the column contained in the given json value */
        _contained_in?: ValueTypes['jsonb'];
        /** does the column contain the given json value at the top level */
        _contains?: ValueTypes['jsonb'];
        _eq?: ValueTypes['jsonb'];
        _gt?: ValueTypes['jsonb'];
        _gte?: ValueTypes['jsonb'];
        /** does the string exist as a top-level key in the column */
        _has_key?: string;
        /** do all of these strings exist as top-level keys in the column */
        _has_keys_all?: string[];
        /** do any of these strings exist as top-level keys in the column */
        _has_keys_any?: string[];
        _in?: ValueTypes['jsonb'][];
        _is_null?: boolean;
        _lt?: ValueTypes['jsonb'];
        _lte?: ValueTypes['jsonb'];
        _neq?: ValueTypes['jsonb'];
        _nin?: ValueTypes['jsonb'][];
    };
    /** columns and relationships of "members" */
    ['members']: AliasType<{
        created_at?: true;
        /** An object relationship */
        group?: ValueTypes['groups'];
        group_id?: true;
        id?: true;
        payments?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments']
        ];
        payments_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments_aggregate']
        ];
        updated_at?: true;
        /** An object relationship */
        user?: ValueTypes['users'];
        user_id?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "members" */
    ['members_aggregate']: AliasType<{
        aggregate?: ValueTypes['members_aggregate_fields'];
        nodes?: ValueTypes['members'];
        __typename?: true;
    }>;
    /** aggregate fields of "members" */
    ['members_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['members_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['members_max_fields'];
        min?: ValueTypes['members_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "members" */
    ['members_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['members_max_order_by'];
        min?: ValueTypes['members_min_order_by'];
    };
    /** input type for inserting array relation for remote table "members" */
    ['members_arr_rel_insert_input']: {
        data: ValueTypes['members_insert_input'][];
        on_conflict?: ValueTypes['members_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "members". All fields are combined with a logical 'AND'. */
    ['members_bool_exp']: {
        _and?: (ValueTypes['members_bool_exp'] | undefined)[];
        _not?: ValueTypes['members_bool_exp'];
        _or?: (ValueTypes['members_bool_exp'] | undefined)[];
        created_at?: ValueTypes['timestamptz_comparison_exp'];
        group?: ValueTypes['groups_bool_exp'];
        group_id?: ValueTypes['uuid_comparison_exp'];
        id?: ValueTypes['uuid_comparison_exp'];
        payments?: ValueTypes['payments_bool_exp'];
        updated_at?: ValueTypes['timestamptz_comparison_exp'];
        user?: ValueTypes['users_bool_exp'];
        user_id?: ValueTypes['uuid_comparison_exp'];
    };
    /** unique or primary key constraints on table "members" */
    ['members_constraint']: members_constraint;
    /** input type for inserting data into table "members" */
    ['members_insert_input']: {
        created_at?: ValueTypes['timestamptz'];
        group?: ValueTypes['groups_obj_rel_insert_input'];
        group_id?: ValueTypes['uuid'];
        id?: ValueTypes['uuid'];
        payments?: ValueTypes['payments_arr_rel_insert_input'];
        updated_at?: ValueTypes['timestamptz'];
        user?: ValueTypes['users_obj_rel_insert_input'];
        user_id?: ValueTypes['uuid'];
    };
    /** aggregate max on columns */
    ['members_max_fields']: AliasType<{
        created_at?: true;
        group_id?: true;
        id?: true;
        updated_at?: true;
        user_id?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "members" */
    ['members_max_order_by']: {
        created_at?: ValueTypes['order_by'];
        group_id?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
        user_id?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['members_min_fields']: AliasType<{
        created_at?: true;
        group_id?: true;
        id?: true;
        updated_at?: true;
        user_id?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "members" */
    ['members_min_order_by']: {
        created_at?: ValueTypes['order_by'];
        group_id?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
        user_id?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "members" */
    ['members_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['members'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "members" */
    ['members_obj_rel_insert_input']: {
        data: ValueTypes['members_insert_input'];
        on_conflict?: ValueTypes['members_on_conflict'];
    };
    /** on conflict condition type for table "members" */
    ['members_on_conflict']: {
        constraint: ValueTypes['members_constraint'];
        update_columns: ValueTypes['members_update_column'][];
        where?: ValueTypes['members_bool_exp'];
    };
    /** ordering options when selecting data from "members" */
    ['members_order_by']: {
        created_at?: ValueTypes['order_by'];
        group?: ValueTypes['groups_order_by'];
        group_id?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        payments_aggregate?: ValueTypes['payments_aggregate_order_by'];
        updated_at?: ValueTypes['order_by'];
        user?: ValueTypes['users_order_by'];
        user_id?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "members" */
    ['members_pk_columns_input']: {
        id: ValueTypes['uuid'];
    };
    /** select columns of table "members" */
    ['members_select_column']: members_select_column;
    /** input type for updating data in table "members" */
    ['members_set_input']: {
        created_at?: ValueTypes['timestamptz'];
        group_id?: ValueTypes['uuid'];
        id?: ValueTypes['uuid'];
        updated_at?: ValueTypes['timestamptz'];
        user_id?: ValueTypes['uuid'];
    };
    /** update columns of table "members" */
    ['members_update_column']: members_update_column;
    /** mutation root */
    ['mutation_root']: AliasType<{
        action_create_group?: [
            { arg: ValueTypes['ActionCreateGroupInput'] },
            ValueTypes['ActionCreateGroupOutput']
        ];
        action_request_payment?: [
            { arg: ValueTypes['ActionRequestPaymentInput'] },
            ValueTypes['ActionRequestPaymentOutput']
        ];
        action_withdraw_payment?: [
            { args: ValueTypes['ActionWithdrawPaymentInput'] },
            ValueTypes['ActionWithdrawPaymentOutput']
        ];
        delete_auth_account_providers?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['auth_account_providers_bool_exp'];
            },
            ValueTypes['auth_account_providers_mutation_response']
        ];
        delete_auth_account_providers_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['auth_account_providers']
        ];
        delete_auth_account_roles?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['auth_account_roles_bool_exp'];
            },
            ValueTypes['auth_account_roles_mutation_response']
        ];
        delete_auth_account_roles_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['auth_account_roles']
        ];
        delete_auth_accounts?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['auth_accounts_bool_exp'];
            },
            ValueTypes['auth_accounts_mutation_response']
        ];
        delete_auth_accounts_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['auth_accounts']
        ];
        delete_auth_providers?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['auth_providers_bool_exp'];
            },
            ValueTypes['auth_providers_mutation_response']
        ];
        delete_auth_providers_by_pk?: [
            { provider: string },
            ValueTypes['auth_providers']
        ];
        delete_auth_refresh_tokens?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['auth_refresh_tokens_bool_exp'];
            },
            ValueTypes['auth_refresh_tokens_mutation_response']
        ];
        delete_auth_refresh_tokens_by_pk?: [
            { refresh_token: ValueTypes['uuid'] },
            ValueTypes['auth_refresh_tokens']
        ];
        delete_auth_roles?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['auth_roles_bool_exp'];
            },
            ValueTypes['auth_roles_mutation_response']
        ];
        delete_auth_roles_by_pk?: [{ role: string }, ValueTypes['auth_roles']];
        delete_contribution_types?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['contribution_types_bool_exp'];
            },
            ValueTypes['contribution_types_mutation_response']
        ];
        delete_contribution_types_by_pk?: [
            { value: string },
            ValueTypes['contribution_types']
        ];
        delete_group_cycles?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['group_cycles_bool_exp'];
            },
            ValueTypes['group_cycles_mutation_response']
        ];
        delete_group_cycles_by_pk?: [
            { value: string },
            ValueTypes['group_cycles']
        ];
        delete_group_recurrencies?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['group_recurrencies_bool_exp'];
            },
            ValueTypes['group_recurrencies_mutation_response']
        ];
        delete_group_recurrencies_by_pk?: [
            { value: string },
            ValueTypes['group_recurrencies']
        ];
        delete_groups?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups_mutation_response']
        ];
        delete_groups_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['groups']
        ];
        delete_members?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['members_bool_exp'];
            },
            ValueTypes['members_mutation_response']
        ];
        delete_members_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['members']
        ];
        delete_payment_statuses?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['payment_statuses_bool_exp'];
            },
            ValueTypes['payment_statuses_mutation_response']
        ];
        delete_payment_statuses_by_pk?: [
            { value: string },
            ValueTypes['payment_statuses']
        ];
        delete_payment_types?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['payment_types_bool_exp'];
            },
            ValueTypes['payment_types_mutation_response']
        ];
        delete_payment_types_by_pk?: [
            { value: string },
            ValueTypes['payment_types']
        ];
        delete_payments?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments_mutation_response']
        ];
        delete_payments_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['payments']
        ];
        delete_periods?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['periods_bool_exp'];
            },
            ValueTypes['periods_mutation_response']
        ];
        delete_periods_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['periods']
        ];
        delete_users?: [
            {
                /** filter the rows which have to be deleted */
                where: ValueTypes['users_bool_exp'];
            },
            ValueTypes['users_mutation_response']
        ];
        delete_users_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['users']];
        insert_auth_account_providers?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['auth_account_providers_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_account_providers_on_conflict'];
            },
            ValueTypes['auth_account_providers_mutation_response']
        ];
        insert_auth_account_providers_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['auth_account_providers_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_account_providers_on_conflict'];
            },
            ValueTypes['auth_account_providers']
        ];
        insert_auth_account_roles?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['auth_account_roles_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_account_roles_on_conflict'];
            },
            ValueTypes['auth_account_roles_mutation_response']
        ];
        insert_auth_account_roles_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['auth_account_roles_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_account_roles_on_conflict'];
            },
            ValueTypes['auth_account_roles']
        ];
        insert_auth_accounts?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['auth_accounts_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_accounts_on_conflict'];
            },
            ValueTypes['auth_accounts_mutation_response']
        ];
        insert_auth_accounts_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['auth_accounts_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_accounts_on_conflict'];
            },
            ValueTypes['auth_accounts']
        ];
        insert_auth_providers?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['auth_providers_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_providers_on_conflict'];
            },
            ValueTypes['auth_providers_mutation_response']
        ];
        insert_auth_providers_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['auth_providers_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_providers_on_conflict'];
            },
            ValueTypes['auth_providers']
        ];
        insert_auth_refresh_tokens?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['auth_refresh_tokens_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_refresh_tokens_on_conflict'];
            },
            ValueTypes['auth_refresh_tokens_mutation_response']
        ];
        insert_auth_refresh_tokens_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['auth_refresh_tokens_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_refresh_tokens_on_conflict'];
            },
            ValueTypes['auth_refresh_tokens']
        ];
        insert_auth_roles?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['auth_roles_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_roles_on_conflict'];
            },
            ValueTypes['auth_roles_mutation_response']
        ];
        insert_auth_roles_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['auth_roles_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['auth_roles_on_conflict'];
            },
            ValueTypes['auth_roles']
        ];
        insert_contribution_types?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['contribution_types_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['contribution_types_on_conflict'];
            },
            ValueTypes['contribution_types_mutation_response']
        ];
        insert_contribution_types_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['contribution_types_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['contribution_types_on_conflict'];
            },
            ValueTypes['contribution_types']
        ];
        insert_group_cycles?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['group_cycles_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['group_cycles_on_conflict'];
            },
            ValueTypes['group_cycles_mutation_response']
        ];
        insert_group_cycles_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['group_cycles_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['group_cycles_on_conflict'];
            },
            ValueTypes['group_cycles']
        ];
        insert_group_recurrencies?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['group_recurrencies_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['group_recurrencies_on_conflict'];
            },
            ValueTypes['group_recurrencies_mutation_response']
        ];
        insert_group_recurrencies_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['group_recurrencies_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['group_recurrencies_on_conflict'];
            },
            ValueTypes['group_recurrencies']
        ];
        insert_groups?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['groups_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['groups_on_conflict'];
            },
            ValueTypes['groups_mutation_response']
        ];
        insert_groups_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['groups_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['groups_on_conflict'];
            },
            ValueTypes['groups']
        ];
        insert_members?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['members_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['members_on_conflict'];
            },
            ValueTypes['members_mutation_response']
        ];
        insert_members_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['members_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['members_on_conflict'];
            },
            ValueTypes['members']
        ];
        insert_payment_statuses?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['payment_statuses_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['payment_statuses_on_conflict'];
            },
            ValueTypes['payment_statuses_mutation_response']
        ];
        insert_payment_statuses_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['payment_statuses_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['payment_statuses_on_conflict'];
            },
            ValueTypes['payment_statuses']
        ];
        insert_payment_types?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['payment_types_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['payment_types_on_conflict'];
            },
            ValueTypes['payment_types_mutation_response']
        ];
        insert_payment_types_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['payment_types_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['payment_types_on_conflict'];
            },
            ValueTypes['payment_types']
        ];
        insert_payments?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['payments_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['payments_on_conflict'];
            },
            ValueTypes['payments_mutation_response']
        ];
        insert_payments_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['payments_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['payments_on_conflict'];
            },
            ValueTypes['payments']
        ];
        insert_periods?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['periods_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['periods_on_conflict'];
            },
            ValueTypes['periods_mutation_response']
        ];
        insert_periods_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['periods_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['periods_on_conflict'];
            },
            ValueTypes['periods']
        ];
        insert_users?: [
            {
                /** the rows to be inserted */
                objects: ValueTypes['users_insert_input'][] /** on conflict condition */;
                on_conflict?: ValueTypes['users_on_conflict'];
            },
            ValueTypes['users_mutation_response']
        ];
        insert_users_one?: [
            {
                /** the row to be inserted */
                object: ValueTypes['users_insert_input'] /** on conflict condition */;
                on_conflict?: ValueTypes['users_on_conflict'];
            },
            ValueTypes['users']
        ];
        update_auth_account_providers?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['auth_account_providers_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['auth_account_providers_bool_exp'];
            },
            ValueTypes['auth_account_providers_mutation_response']
        ];
        update_auth_account_providers_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['auth_account_providers_set_input'];
                pk_columns: ValueTypes['auth_account_providers_pk_columns_input'];
            },
            ValueTypes['auth_account_providers']
        ];
        update_auth_account_roles?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['auth_account_roles_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['auth_account_roles_bool_exp'];
            },
            ValueTypes['auth_account_roles_mutation_response']
        ];
        update_auth_account_roles_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['auth_account_roles_set_input'];
                pk_columns: ValueTypes['auth_account_roles_pk_columns_input'];
            },
            ValueTypes['auth_account_roles']
        ];
        update_auth_accounts?: [
            {
                /** append existing jsonb value of filtered columns with new jsonb value */
                _append?: ValueTypes['auth_accounts_append_input'] /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */;
                _delete_at_path?: ValueTypes['auth_accounts_delete_at_path_input'] /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */;
                _delete_elem?: ValueTypes['auth_accounts_delete_elem_input'] /** delete key/value pair or string element. key/value pairs are matched based on their key value */;
                _delete_key?: ValueTypes['auth_accounts_delete_key_input'] /** prepend existing jsonb value of filtered columns with new jsonb value */;
                _prepend?: ValueTypes['auth_accounts_prepend_input'] /** sets the columns of the filtered rows to the given values */;
                _set?: ValueTypes['auth_accounts_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['auth_accounts_bool_exp'];
            },
            ValueTypes['auth_accounts_mutation_response']
        ];
        update_auth_accounts_by_pk?: [
            {
                /** append existing jsonb value of filtered columns with new jsonb value */
                _append?: ValueTypes['auth_accounts_append_input'] /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */;
                _delete_at_path?: ValueTypes['auth_accounts_delete_at_path_input'] /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */;
                _delete_elem?: ValueTypes['auth_accounts_delete_elem_input'] /** delete key/value pair or string element. key/value pairs are matched based on their key value */;
                _delete_key?: ValueTypes['auth_accounts_delete_key_input'] /** prepend existing jsonb value of filtered columns with new jsonb value */;
                _prepend?: ValueTypes['auth_accounts_prepend_input'] /** sets the columns of the filtered rows to the given values */;
                _set?: ValueTypes['auth_accounts_set_input'];
                pk_columns: ValueTypes['auth_accounts_pk_columns_input'];
            },
            ValueTypes['auth_accounts']
        ];
        update_auth_providers?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['auth_providers_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['auth_providers_bool_exp'];
            },
            ValueTypes['auth_providers_mutation_response']
        ];
        update_auth_providers_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['auth_providers_set_input'];
                pk_columns: ValueTypes['auth_providers_pk_columns_input'];
            },
            ValueTypes['auth_providers']
        ];
        update_auth_refresh_tokens?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['auth_refresh_tokens_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['auth_refresh_tokens_bool_exp'];
            },
            ValueTypes['auth_refresh_tokens_mutation_response']
        ];
        update_auth_refresh_tokens_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['auth_refresh_tokens_set_input'];
                pk_columns: ValueTypes['auth_refresh_tokens_pk_columns_input'];
            },
            ValueTypes['auth_refresh_tokens']
        ];
        update_auth_roles?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['auth_roles_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['auth_roles_bool_exp'];
            },
            ValueTypes['auth_roles_mutation_response']
        ];
        update_auth_roles_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['auth_roles_set_input'];
                pk_columns: ValueTypes['auth_roles_pk_columns_input'];
            },
            ValueTypes['auth_roles']
        ];
        update_contribution_types?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['contribution_types_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['contribution_types_bool_exp'];
            },
            ValueTypes['contribution_types_mutation_response']
        ];
        update_contribution_types_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['contribution_types_set_input'];
                pk_columns: ValueTypes['contribution_types_pk_columns_input'];
            },
            ValueTypes['contribution_types']
        ];
        update_group_cycles?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['group_cycles_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['group_cycles_bool_exp'];
            },
            ValueTypes['group_cycles_mutation_response']
        ];
        update_group_cycles_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['group_cycles_set_input'];
                pk_columns: ValueTypes['group_cycles_pk_columns_input'];
            },
            ValueTypes['group_cycles']
        ];
        update_group_recurrencies?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['group_recurrencies_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['group_recurrencies_bool_exp'];
            },
            ValueTypes['group_recurrencies_mutation_response']
        ];
        update_group_recurrencies_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['group_recurrencies_set_input'];
                pk_columns: ValueTypes['group_recurrencies_pk_columns_input'];
            },
            ValueTypes['group_recurrencies']
        ];
        update_groups?: [
            {
                /** increments the integer columns with given value of the filtered values */
                _inc?: ValueTypes['groups_inc_input'] /** sets the columns of the filtered rows to the given values */;
                _set?: ValueTypes['groups_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups_mutation_response']
        ];
        update_groups_by_pk?: [
            {
                /** increments the integer columns with given value of the filtered values */
                _inc?: ValueTypes['groups_inc_input'] /** sets the columns of the filtered rows to the given values */;
                _set?: ValueTypes['groups_set_input'];
                pk_columns: ValueTypes['groups_pk_columns_input'];
            },
            ValueTypes['groups']
        ];
        update_members?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['members_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['members_bool_exp'];
            },
            ValueTypes['members_mutation_response']
        ];
        update_members_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['members_set_input'];
                pk_columns: ValueTypes['members_pk_columns_input'];
            },
            ValueTypes['members']
        ];
        update_payment_statuses?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['payment_statuses_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['payment_statuses_bool_exp'];
            },
            ValueTypes['payment_statuses_mutation_response']
        ];
        update_payment_statuses_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['payment_statuses_set_input'];
                pk_columns: ValueTypes['payment_statuses_pk_columns_input'];
            },
            ValueTypes['payment_statuses']
        ];
        update_payment_types?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['payment_types_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['payment_types_bool_exp'];
            },
            ValueTypes['payment_types_mutation_response']
        ];
        update_payment_types_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['payment_types_set_input'];
                pk_columns: ValueTypes['payment_types_pk_columns_input'];
            },
            ValueTypes['payment_types']
        ];
        update_payments?: [
            {
                /** increments the integer columns with given value of the filtered values */
                _inc?: ValueTypes['payments_inc_input'] /** sets the columns of the filtered rows to the given values */;
                _set?: ValueTypes['payments_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments_mutation_response']
        ];
        update_payments_by_pk?: [
            {
                /** increments the integer columns with given value of the filtered values */
                _inc?: ValueTypes['payments_inc_input'] /** sets the columns of the filtered rows to the given values */;
                _set?: ValueTypes['payments_set_input'];
                pk_columns: ValueTypes['payments_pk_columns_input'];
            },
            ValueTypes['payments']
        ];
        update_periods?: [
            {
                /** increments the integer columns with given value of the filtered values */
                _inc?: ValueTypes['periods_inc_input'] /** sets the columns of the filtered rows to the given values */;
                _set?: ValueTypes['periods_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['periods_bool_exp'];
            },
            ValueTypes['periods_mutation_response']
        ];
        update_periods_by_pk?: [
            {
                /** increments the integer columns with given value of the filtered values */
                _inc?: ValueTypes['periods_inc_input'] /** sets the columns of the filtered rows to the given values */;
                _set?: ValueTypes['periods_set_input'];
                pk_columns: ValueTypes['periods_pk_columns_input'];
            },
            ValueTypes['periods']
        ];
        update_users?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['users_set_input'] /** filter the rows which have to be updated */;
                where: ValueTypes['users_bool_exp'];
            },
            ValueTypes['users_mutation_response']
        ];
        update_users_by_pk?: [
            {
                /** sets the columns of the filtered rows to the given values */
                _set?: ValueTypes['users_set_input'];
                pk_columns: ValueTypes['users_pk_columns_input'];
            },
            ValueTypes['users']
        ];
        __typename?: true;
    }>;
    ['numeric']: unknown;
    /** expression to compare columns of type numeric. All fields are combined with logical 'AND'. */
    ['numeric_comparison_exp']: {
        _eq?: ValueTypes['numeric'];
        _gt?: ValueTypes['numeric'];
        _gte?: ValueTypes['numeric'];
        _in?: ValueTypes['numeric'][];
        _is_null?: boolean;
        _lt?: ValueTypes['numeric'];
        _lte?: ValueTypes['numeric'];
        _neq?: ValueTypes['numeric'];
        _nin?: ValueTypes['numeric'][];
    };
    /** column ordering options */
    ['order_by']: order_by;
    /** columns and relationships of "payment_statuses" */
    ['payment_statuses']: AliasType<{
        description?: true;
        payments?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments']
        ];
        payments_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments_aggregate']
        ];
        value?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "payment_statuses" */
    ['payment_statuses_aggregate']: AliasType<{
        aggregate?: ValueTypes['payment_statuses_aggregate_fields'];
        nodes?: ValueTypes['payment_statuses'];
        __typename?: true;
    }>;
    /** aggregate fields of "payment_statuses" */
    ['payment_statuses_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['payment_statuses_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['payment_statuses_max_fields'];
        min?: ValueTypes['payment_statuses_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "payment_statuses" */
    ['payment_statuses_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['payment_statuses_max_order_by'];
        min?: ValueTypes['payment_statuses_min_order_by'];
    };
    /** input type for inserting array relation for remote table "payment_statuses" */
    ['payment_statuses_arr_rel_insert_input']: {
        data: ValueTypes['payment_statuses_insert_input'][];
        on_conflict?: ValueTypes['payment_statuses_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "payment_statuses". All fields are combined with a logical 'AND'. */
    ['payment_statuses_bool_exp']: {
        _and?: (ValueTypes['payment_statuses_bool_exp'] | undefined)[];
        _not?: ValueTypes['payment_statuses_bool_exp'];
        _or?: (ValueTypes['payment_statuses_bool_exp'] | undefined)[];
        description?: ValueTypes['String_comparison_exp'];
        payments?: ValueTypes['payments_bool_exp'];
        value?: ValueTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "payment_statuses" */
    ['payment_statuses_constraint']: payment_statuses_constraint;
    ['payment_statuses_enum']: payment_statuses_enum;
    /** expression to compare columns of type payment_statuses_enum. All fields are combined with logical 'AND'. */
    ['payment_statuses_enum_comparison_exp']: {
        _eq?: ValueTypes['payment_statuses_enum'];
        _in?: ValueTypes['payment_statuses_enum'][];
        _is_null?: boolean;
        _neq?: ValueTypes['payment_statuses_enum'];
        _nin?: ValueTypes['payment_statuses_enum'][];
    };
    /** input type for inserting data into table "payment_statuses" */
    ['payment_statuses_insert_input']: {
        description?: string;
        payments?: ValueTypes['payments_arr_rel_insert_input'];
        value?: string;
    };
    /** aggregate max on columns */
    ['payment_statuses_max_fields']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "payment_statuses" */
    ['payment_statuses_max_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['payment_statuses_min_fields']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "payment_statuses" */
    ['payment_statuses_min_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "payment_statuses" */
    ['payment_statuses_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['payment_statuses'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "payment_statuses" */
    ['payment_statuses_obj_rel_insert_input']: {
        data: ValueTypes['payment_statuses_insert_input'];
        on_conflict?: ValueTypes['payment_statuses_on_conflict'];
    };
    /** on conflict condition type for table "payment_statuses" */
    ['payment_statuses_on_conflict']: {
        constraint: ValueTypes['payment_statuses_constraint'];
        update_columns: ValueTypes['payment_statuses_update_column'][];
        where?: ValueTypes['payment_statuses_bool_exp'];
    };
    /** ordering options when selecting data from "payment_statuses" */
    ['payment_statuses_order_by']: {
        description?: ValueTypes['order_by'];
        payments_aggregate?: ValueTypes['payments_aggregate_order_by'];
        value?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "payment_statuses" */
    ['payment_statuses_pk_columns_input']: {
        value: string;
    };
    /** select columns of table "payment_statuses" */
    ['payment_statuses_select_column']: payment_statuses_select_column;
    /** input type for updating data in table "payment_statuses" */
    ['payment_statuses_set_input']: {
        description?: string;
        value?: string;
    };
    /** update columns of table "payment_statuses" */
    ['payment_statuses_update_column']: payment_statuses_update_column;
    /** columns and relationships of "payment_types" */
    ['payment_types']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "payment_types" */
    ['payment_types_aggregate']: AliasType<{
        aggregate?: ValueTypes['payment_types_aggregate_fields'];
        nodes?: ValueTypes['payment_types'];
        __typename?: true;
    }>;
    /** aggregate fields of "payment_types" */
    ['payment_types_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['payment_types_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['payment_types_max_fields'];
        min?: ValueTypes['payment_types_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "payment_types" */
    ['payment_types_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['payment_types_max_order_by'];
        min?: ValueTypes['payment_types_min_order_by'];
    };
    /** input type for inserting array relation for remote table "payment_types" */
    ['payment_types_arr_rel_insert_input']: {
        data: ValueTypes['payment_types_insert_input'][];
        on_conflict?: ValueTypes['payment_types_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "payment_types". All fields are combined with a logical 'AND'. */
    ['payment_types_bool_exp']: {
        _and?: (ValueTypes['payment_types_bool_exp'] | undefined)[];
        _not?: ValueTypes['payment_types_bool_exp'];
        _or?: (ValueTypes['payment_types_bool_exp'] | undefined)[];
        description?: ValueTypes['String_comparison_exp'];
        value?: ValueTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "payment_types" */
    ['payment_types_constraint']: payment_types_constraint;
    ['payment_types_enum']: payment_types_enum;
    /** expression to compare columns of type payment_types_enum. All fields are combined with logical 'AND'. */
    ['payment_types_enum_comparison_exp']: {
        _eq?: ValueTypes['payment_types_enum'];
        _in?: ValueTypes['payment_types_enum'][];
        _is_null?: boolean;
        _neq?: ValueTypes['payment_types_enum'];
        _nin?: ValueTypes['payment_types_enum'][];
    };
    /** input type for inserting data into table "payment_types" */
    ['payment_types_insert_input']: {
        description?: string;
        value?: string;
    };
    /** aggregate max on columns */
    ['payment_types_max_fields']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "payment_types" */
    ['payment_types_max_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['payment_types_min_fields']: AliasType<{
        description?: true;
        value?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "payment_types" */
    ['payment_types_min_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "payment_types" */
    ['payment_types_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['payment_types'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "payment_types" */
    ['payment_types_obj_rel_insert_input']: {
        data: ValueTypes['payment_types_insert_input'];
        on_conflict?: ValueTypes['payment_types_on_conflict'];
    };
    /** on conflict condition type for table "payment_types" */
    ['payment_types_on_conflict']: {
        constraint: ValueTypes['payment_types_constraint'];
        update_columns: ValueTypes['payment_types_update_column'][];
        where?: ValueTypes['payment_types_bool_exp'];
    };
    /** ordering options when selecting data from "payment_types" */
    ['payment_types_order_by']: {
        description?: ValueTypes['order_by'];
        value?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "payment_types" */
    ['payment_types_pk_columns_input']: {
        value: string;
    };
    /** select columns of table "payment_types" */
    ['payment_types_select_column']: payment_types_select_column;
    /** input type for updating data in table "payment_types" */
    ['payment_types_set_input']: {
        description?: string;
        value?: string;
    };
    /** update columns of table "payment_types" */
    ['payment_types_update_column']: payment_types_update_column;
    /** columns and relationships of "payments" */
    ['payments']: AliasType<{
        created_at?: true;
        /** An object relationship */
        group?: ValueTypes['groups'];
        group_id?: true;
        id?: true;
        /** An object relationship */
        member?: ValueTypes['members'];
        member_id?: true;
        /** An object relationship */
        paymentStatusByPaymentStatus?: ValueTypes['payment_statuses'];
        payment_amount?: true;
        payment_status?: true;
        payment_type?: true;
        /** An object relationship */
        period?: ValueTypes['periods'];
        period_id?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "payments" */
    ['payments_aggregate']: AliasType<{
        aggregate?: ValueTypes['payments_aggregate_fields'];
        nodes?: ValueTypes['payments'];
        __typename?: true;
    }>;
    /** aggregate fields of "payments" */
    ['payments_aggregate_fields']: AliasType<{
        avg?: ValueTypes['payments_avg_fields'];
        count?: [
            {
                columns?: ValueTypes['payments_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['payments_max_fields'];
        min?: ValueTypes['payments_min_fields'];
        stddev?: ValueTypes['payments_stddev_fields'];
        stddev_pop?: ValueTypes['payments_stddev_pop_fields'];
        stddev_samp?: ValueTypes['payments_stddev_samp_fields'];
        sum?: ValueTypes['payments_sum_fields'];
        var_pop?: ValueTypes['payments_var_pop_fields'];
        var_samp?: ValueTypes['payments_var_samp_fields'];
        variance?: ValueTypes['payments_variance_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "payments" */
    ['payments_aggregate_order_by']: {
        avg?: ValueTypes['payments_avg_order_by'];
        count?: ValueTypes['order_by'];
        max?: ValueTypes['payments_max_order_by'];
        min?: ValueTypes['payments_min_order_by'];
        stddev?: ValueTypes['payments_stddev_order_by'];
        stddev_pop?: ValueTypes['payments_stddev_pop_order_by'];
        stddev_samp?: ValueTypes['payments_stddev_samp_order_by'];
        sum?: ValueTypes['payments_sum_order_by'];
        var_pop?: ValueTypes['payments_var_pop_order_by'];
        var_samp?: ValueTypes['payments_var_samp_order_by'];
        variance?: ValueTypes['payments_variance_order_by'];
    };
    /** input type for inserting array relation for remote table "payments" */
    ['payments_arr_rel_insert_input']: {
        data: ValueTypes['payments_insert_input'][];
        on_conflict?: ValueTypes['payments_on_conflict'];
    };
    /** aggregate avg on columns */
    ['payments_avg_fields']: AliasType<{
        payment_amount?: true;
        __typename?: true;
    }>;
    /** order by avg() on columns of table "payments" */
    ['payments_avg_order_by']: {
        payment_amount?: ValueTypes['order_by'];
    };
    /** Boolean expression to filter rows from the table "payments". All fields are combined with a logical 'AND'. */
    ['payments_bool_exp']: {
        _and?: (ValueTypes['payments_bool_exp'] | undefined)[];
        _not?: ValueTypes['payments_bool_exp'];
        _or?: (ValueTypes['payments_bool_exp'] | undefined)[];
        created_at?: ValueTypes['timestamptz_comparison_exp'];
        group?: ValueTypes['groups_bool_exp'];
        group_id?: ValueTypes['uuid_comparison_exp'];
        id?: ValueTypes['uuid_comparison_exp'];
        member?: ValueTypes['members_bool_exp'];
        member_id?: ValueTypes['uuid_comparison_exp'];
        paymentStatusByPaymentStatus?: ValueTypes['payment_statuses_bool_exp'];
        payment_amount?: ValueTypes['numeric_comparison_exp'];
        payment_status?: ValueTypes['payment_statuses_enum_comparison_exp'];
        payment_type?: ValueTypes['payment_types_enum_comparison_exp'];
        period?: ValueTypes['periods_bool_exp'];
        period_id?: ValueTypes['uuid_comparison_exp'];
        updated_at?: ValueTypes['timestamptz_comparison_exp'];
    };
    /** unique or primary key constraints on table "payments" */
    ['payments_constraint']: payments_constraint;
    /** input type for incrementing integer column in table "payments" */
    ['payments_inc_input']: {
        payment_amount?: ValueTypes['numeric'];
    };
    /** input type for inserting data into table "payments" */
    ['payments_insert_input']: {
        created_at?: ValueTypes['timestamptz'];
        group?: ValueTypes['groups_obj_rel_insert_input'];
        group_id?: ValueTypes['uuid'];
        id?: ValueTypes['uuid'];
        member?: ValueTypes['members_obj_rel_insert_input'];
        member_id?: ValueTypes['uuid'];
        paymentStatusByPaymentStatus?: ValueTypes['payment_statuses_obj_rel_insert_input'];
        payment_amount?: ValueTypes['numeric'];
        payment_status?: ValueTypes['payment_statuses_enum'];
        payment_type?: ValueTypes['payment_types_enum'];
        period?: ValueTypes['periods_obj_rel_insert_input'];
        period_id?: ValueTypes['uuid'];
        updated_at?: ValueTypes['timestamptz'];
    };
    /** aggregate max on columns */
    ['payments_max_fields']: AliasType<{
        created_at?: true;
        group_id?: true;
        id?: true;
        member_id?: true;
        payment_amount?: true;
        period_id?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "payments" */
    ['payments_max_order_by']: {
        created_at?: ValueTypes['order_by'];
        group_id?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        member_id?: ValueTypes['order_by'];
        payment_amount?: ValueTypes['order_by'];
        period_id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['payments_min_fields']: AliasType<{
        created_at?: true;
        group_id?: true;
        id?: true;
        member_id?: true;
        payment_amount?: true;
        period_id?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "payments" */
    ['payments_min_order_by']: {
        created_at?: ValueTypes['order_by'];
        group_id?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        member_id?: ValueTypes['order_by'];
        payment_amount?: ValueTypes['order_by'];
        period_id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "payments" */
    ['payments_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['payments'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "payments" */
    ['payments_obj_rel_insert_input']: {
        data: ValueTypes['payments_insert_input'];
        on_conflict?: ValueTypes['payments_on_conflict'];
    };
    /** on conflict condition type for table "payments" */
    ['payments_on_conflict']: {
        constraint: ValueTypes['payments_constraint'];
        update_columns: ValueTypes['payments_update_column'][];
        where?: ValueTypes['payments_bool_exp'];
    };
    /** ordering options when selecting data from "payments" */
    ['payments_order_by']: {
        created_at?: ValueTypes['order_by'];
        group?: ValueTypes['groups_order_by'];
        group_id?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        member?: ValueTypes['members_order_by'];
        member_id?: ValueTypes['order_by'];
        paymentStatusByPaymentStatus?: ValueTypes['payment_statuses_order_by'];
        payment_amount?: ValueTypes['order_by'];
        payment_status?: ValueTypes['order_by'];
        payment_type?: ValueTypes['order_by'];
        period?: ValueTypes['periods_order_by'];
        period_id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "payments" */
    ['payments_pk_columns_input']: {
        id: ValueTypes['uuid'];
    };
    /** select columns of table "payments" */
    ['payments_select_column']: payments_select_column;
    /** input type for updating data in table "payments" */
    ['payments_set_input']: {
        created_at?: ValueTypes['timestamptz'];
        group_id?: ValueTypes['uuid'];
        id?: ValueTypes['uuid'];
        member_id?: ValueTypes['uuid'];
        payment_amount?: ValueTypes['numeric'];
        payment_status?: ValueTypes['payment_statuses_enum'];
        payment_type?: ValueTypes['payment_types_enum'];
        period_id?: ValueTypes['uuid'];
        updated_at?: ValueTypes['timestamptz'];
    };
    /** aggregate stddev on columns */
    ['payments_stddev_fields']: AliasType<{
        payment_amount?: true;
        __typename?: true;
    }>;
    /** order by stddev() on columns of table "payments" */
    ['payments_stddev_order_by']: {
        payment_amount?: ValueTypes['order_by'];
    };
    /** aggregate stddev_pop on columns */
    ['payments_stddev_pop_fields']: AliasType<{
        payment_amount?: true;
        __typename?: true;
    }>;
    /** order by stddev_pop() on columns of table "payments" */
    ['payments_stddev_pop_order_by']: {
        payment_amount?: ValueTypes['order_by'];
    };
    /** aggregate stddev_samp on columns */
    ['payments_stddev_samp_fields']: AliasType<{
        payment_amount?: true;
        __typename?: true;
    }>;
    /** order by stddev_samp() on columns of table "payments" */
    ['payments_stddev_samp_order_by']: {
        payment_amount?: ValueTypes['order_by'];
    };
    /** aggregate sum on columns */
    ['payments_sum_fields']: AliasType<{
        payment_amount?: true;
        __typename?: true;
    }>;
    /** order by sum() on columns of table "payments" */
    ['payments_sum_order_by']: {
        payment_amount?: ValueTypes['order_by'];
    };
    /** update columns of table "payments" */
    ['payments_update_column']: payments_update_column;
    /** aggregate var_pop on columns */
    ['payments_var_pop_fields']: AliasType<{
        payment_amount?: true;
        __typename?: true;
    }>;
    /** order by var_pop() on columns of table "payments" */
    ['payments_var_pop_order_by']: {
        payment_amount?: ValueTypes['order_by'];
    };
    /** aggregate var_samp on columns */
    ['payments_var_samp_fields']: AliasType<{
        payment_amount?: true;
        __typename?: true;
    }>;
    /** order by var_samp() on columns of table "payments" */
    ['payments_var_samp_order_by']: {
        payment_amount?: ValueTypes['order_by'];
    };
    /** aggregate variance on columns */
    ['payments_variance_fields']: AliasType<{
        payment_amount?: true;
        __typename?: true;
    }>;
    /** order by variance() on columns of table "payments" */
    ['payments_variance_order_by']: {
        payment_amount?: ValueTypes['order_by'];
    };
    /** columns and relationships of "periods" */
    ['periods']: AliasType<{
        created_at?: true;
        /** An object relationship */
        group?: ValueTypes['groups'];
        group_id?: true;
        id?: true;
        payments?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments']
        ];
        payments_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments_aggregate']
        ];
        period_active?: true;
        period_completed_at?: true;
        period_index?: true;
        period_progression?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "periods" */
    ['periods_aggregate']: AliasType<{
        aggregate?: ValueTypes['periods_aggregate_fields'];
        nodes?: ValueTypes['periods'];
        __typename?: true;
    }>;
    /** aggregate fields of "periods" */
    ['periods_aggregate_fields']: AliasType<{
        avg?: ValueTypes['periods_avg_fields'];
        count?: [
            {
                columns?: ValueTypes['periods_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['periods_max_fields'];
        min?: ValueTypes['periods_min_fields'];
        stddev?: ValueTypes['periods_stddev_fields'];
        stddev_pop?: ValueTypes['periods_stddev_pop_fields'];
        stddev_samp?: ValueTypes['periods_stddev_samp_fields'];
        sum?: ValueTypes['periods_sum_fields'];
        var_pop?: ValueTypes['periods_var_pop_fields'];
        var_samp?: ValueTypes['periods_var_samp_fields'];
        variance?: ValueTypes['periods_variance_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "periods" */
    ['periods_aggregate_order_by']: {
        avg?: ValueTypes['periods_avg_order_by'];
        count?: ValueTypes['order_by'];
        max?: ValueTypes['periods_max_order_by'];
        min?: ValueTypes['periods_min_order_by'];
        stddev?: ValueTypes['periods_stddev_order_by'];
        stddev_pop?: ValueTypes['periods_stddev_pop_order_by'];
        stddev_samp?: ValueTypes['periods_stddev_samp_order_by'];
        sum?: ValueTypes['periods_sum_order_by'];
        var_pop?: ValueTypes['periods_var_pop_order_by'];
        var_samp?: ValueTypes['periods_var_samp_order_by'];
        variance?: ValueTypes['periods_variance_order_by'];
    };
    /** input type for inserting array relation for remote table "periods" */
    ['periods_arr_rel_insert_input']: {
        data: ValueTypes['periods_insert_input'][];
        on_conflict?: ValueTypes['periods_on_conflict'];
    };
    /** aggregate avg on columns */
    ['periods_avg_fields']: AliasType<{
        period_index?: true;
        period_progression?: true;
        __typename?: true;
    }>;
    /** order by avg() on columns of table "periods" */
    ['periods_avg_order_by']: {
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
    };
    /** Boolean expression to filter rows from the table "periods". All fields are combined with a logical 'AND'. */
    ['periods_bool_exp']: {
        _and?: (ValueTypes['periods_bool_exp'] | undefined)[];
        _not?: ValueTypes['periods_bool_exp'];
        _or?: (ValueTypes['periods_bool_exp'] | undefined)[];
        created_at?: ValueTypes['timestamptz_comparison_exp'];
        group?: ValueTypes['groups_bool_exp'];
        group_id?: ValueTypes['uuid_comparison_exp'];
        id?: ValueTypes['uuid_comparison_exp'];
        payments?: ValueTypes['payments_bool_exp'];
        period_active?: ValueTypes['Boolean_comparison_exp'];
        period_completed_at?: ValueTypes['timestamp_comparison_exp'];
        period_index?: ValueTypes['Int_comparison_exp'];
        period_progression?: ValueTypes['numeric_comparison_exp'];
        updated_at?: ValueTypes['timestamptz_comparison_exp'];
    };
    /** unique or primary key constraints on table "periods" */
    ['periods_constraint']: periods_constraint;
    /** input type for incrementing integer column in table "periods" */
    ['periods_inc_input']: {
        period_index?: number;
        period_progression?: ValueTypes['numeric'];
    };
    /** input type for inserting data into table "periods" */
    ['periods_insert_input']: {
        created_at?: ValueTypes['timestamptz'];
        group?: ValueTypes['groups_obj_rel_insert_input'];
        group_id?: ValueTypes['uuid'];
        id?: ValueTypes['uuid'];
        payments?: ValueTypes['payments_arr_rel_insert_input'];
        period_active?: boolean;
        period_completed_at?: ValueTypes['timestamp'];
        period_index?: number;
        period_progression?: ValueTypes['numeric'];
        updated_at?: ValueTypes['timestamptz'];
    };
    /** aggregate max on columns */
    ['periods_max_fields']: AliasType<{
        created_at?: true;
        group_id?: true;
        id?: true;
        period_completed_at?: true;
        period_index?: true;
        period_progression?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "periods" */
    ['periods_max_order_by']: {
        created_at?: ValueTypes['order_by'];
        group_id?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        period_completed_at?: ValueTypes['order_by'];
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['periods_min_fields']: AliasType<{
        created_at?: true;
        group_id?: true;
        id?: true;
        period_completed_at?: true;
        period_index?: true;
        period_progression?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "periods" */
    ['periods_min_order_by']: {
        created_at?: ValueTypes['order_by'];
        group_id?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        period_completed_at?: ValueTypes['order_by'];
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "periods" */
    ['periods_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['periods'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "periods" */
    ['periods_obj_rel_insert_input']: {
        data: ValueTypes['periods_insert_input'];
        on_conflict?: ValueTypes['periods_on_conflict'];
    };
    /** on conflict condition type for table "periods" */
    ['periods_on_conflict']: {
        constraint: ValueTypes['periods_constraint'];
        update_columns: ValueTypes['periods_update_column'][];
        where?: ValueTypes['periods_bool_exp'];
    };
    /** ordering options when selecting data from "periods" */
    ['periods_order_by']: {
        created_at?: ValueTypes['order_by'];
        group?: ValueTypes['groups_order_by'];
        group_id?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        payments_aggregate?: ValueTypes['payments_aggregate_order_by'];
        period_active?: ValueTypes['order_by'];
        period_completed_at?: ValueTypes['order_by'];
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "periods" */
    ['periods_pk_columns_input']: {
        id: ValueTypes['uuid'];
    };
    /** select columns of table "periods" */
    ['periods_select_column']: periods_select_column;
    /** input type for updating data in table "periods" */
    ['periods_set_input']: {
        created_at?: ValueTypes['timestamptz'];
        group_id?: ValueTypes['uuid'];
        id?: ValueTypes['uuid'];
        period_active?: boolean;
        period_completed_at?: ValueTypes['timestamp'];
        period_index?: number;
        period_progression?: ValueTypes['numeric'];
        updated_at?: ValueTypes['timestamptz'];
    };
    /** aggregate stddev on columns */
    ['periods_stddev_fields']: AliasType<{
        period_index?: true;
        period_progression?: true;
        __typename?: true;
    }>;
    /** order by stddev() on columns of table "periods" */
    ['periods_stddev_order_by']: {
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
    };
    /** aggregate stddev_pop on columns */
    ['periods_stddev_pop_fields']: AliasType<{
        period_index?: true;
        period_progression?: true;
        __typename?: true;
    }>;
    /** order by stddev_pop() on columns of table "periods" */
    ['periods_stddev_pop_order_by']: {
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
    };
    /** aggregate stddev_samp on columns */
    ['periods_stddev_samp_fields']: AliasType<{
        period_index?: true;
        period_progression?: true;
        __typename?: true;
    }>;
    /** order by stddev_samp() on columns of table "periods" */
    ['periods_stddev_samp_order_by']: {
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
    };
    /** aggregate sum on columns */
    ['periods_sum_fields']: AliasType<{
        period_index?: true;
        period_progression?: true;
        __typename?: true;
    }>;
    /** order by sum() on columns of table "periods" */
    ['periods_sum_order_by']: {
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
    };
    /** update columns of table "periods" */
    ['periods_update_column']: periods_update_column;
    /** aggregate var_pop on columns */
    ['periods_var_pop_fields']: AliasType<{
        period_index?: true;
        period_progression?: true;
        __typename?: true;
    }>;
    /** order by var_pop() on columns of table "periods" */
    ['periods_var_pop_order_by']: {
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
    };
    /** aggregate var_samp on columns */
    ['periods_var_samp_fields']: AliasType<{
        period_index?: true;
        period_progression?: true;
        __typename?: true;
    }>;
    /** order by var_samp() on columns of table "periods" */
    ['periods_var_samp_order_by']: {
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
    };
    /** aggregate variance on columns */
    ['periods_variance_fields']: AliasType<{
        period_index?: true;
        period_progression?: true;
        __typename?: true;
    }>;
    /** order by variance() on columns of table "periods" */
    ['periods_variance_order_by']: {
        period_index?: ValueTypes['order_by'];
        period_progression?: ValueTypes['order_by'];
    };
    /** query root */
    ['query_root']: AliasType<{
        auth_account_providers?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_providers_bool_exp'];
            },
            ValueTypes['auth_account_providers']
        ];
        auth_account_providers_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_providers_bool_exp'];
            },
            ValueTypes['auth_account_providers_aggregate']
        ];
        auth_account_providers_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['auth_account_providers']
        ];
        auth_account_roles?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_roles_bool_exp'];
            },
            ValueTypes['auth_account_roles']
        ];
        auth_account_roles_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_roles_bool_exp'];
            },
            ValueTypes['auth_account_roles_aggregate']
        ];
        auth_account_roles_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['auth_account_roles']
        ];
        auth_accounts?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_accounts_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_accounts_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_accounts_bool_exp'];
            },
            ValueTypes['auth_accounts']
        ];
        auth_accounts_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_accounts_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_accounts_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_accounts_bool_exp'];
            },
            ValueTypes['auth_accounts_aggregate']
        ];
        auth_accounts_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['auth_accounts']
        ];
        auth_providers?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_providers_bool_exp'];
            },
            ValueTypes['auth_providers']
        ];
        auth_providers_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_providers_bool_exp'];
            },
            ValueTypes['auth_providers_aggregate']
        ];
        auth_providers_by_pk?: [
            { provider: string },
            ValueTypes['auth_providers']
        ];
        auth_refresh_tokens?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_refresh_tokens_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_refresh_tokens_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_refresh_tokens_bool_exp'];
            },
            ValueTypes['auth_refresh_tokens']
        ];
        auth_refresh_tokens_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_refresh_tokens_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_refresh_tokens_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_refresh_tokens_bool_exp'];
            },
            ValueTypes['auth_refresh_tokens_aggregate']
        ];
        auth_refresh_tokens_by_pk?: [
            { refresh_token: ValueTypes['uuid'] },
            ValueTypes['auth_refresh_tokens']
        ];
        auth_roles?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_roles_bool_exp'];
            },
            ValueTypes['auth_roles']
        ];
        auth_roles_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_roles_bool_exp'];
            },
            ValueTypes['auth_roles_aggregate']
        ];
        auth_roles_by_pk?: [{ role: string }, ValueTypes['auth_roles']];
        contribution_types?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['contribution_types_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['contribution_types_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['contribution_types_bool_exp'];
            },
            ValueTypes['contribution_types']
        ];
        contribution_types_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['contribution_types_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['contribution_types_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['contribution_types_bool_exp'];
            },
            ValueTypes['contribution_types_aggregate']
        ];
        contribution_types_by_pk?: [
            { value: string },
            ValueTypes['contribution_types']
        ];
        group_cycles?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['group_cycles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['group_cycles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['group_cycles_bool_exp'];
            },
            ValueTypes['group_cycles']
        ];
        group_cycles_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['group_cycles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['group_cycles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['group_cycles_bool_exp'];
            },
            ValueTypes['group_cycles_aggregate']
        ];
        group_cycles_by_pk?: [{ value: string }, ValueTypes['group_cycles']];
        group_recurrencies?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['group_recurrencies_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['group_recurrencies_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['group_recurrencies_bool_exp'];
            },
            ValueTypes['group_recurrencies']
        ];
        group_recurrencies_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['group_recurrencies_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['group_recurrencies_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['group_recurrencies_bool_exp'];
            },
            ValueTypes['group_recurrencies_aggregate']
        ];
        group_recurrencies_by_pk?: [
            { value: string },
            ValueTypes['group_recurrencies']
        ];
        groups?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['groups_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['groups_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups']
        ];
        groups_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['groups_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['groups_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups_aggregate']
        ];
        groups_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['groups']];
        members?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['members_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['members_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['members_bool_exp'];
            },
            ValueTypes['members']
        ];
        members_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['members_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['members_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['members_bool_exp'];
            },
            ValueTypes['members_aggregate']
        ];
        members_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['members']];
        payment_statuses?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payment_statuses_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payment_statuses_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payment_statuses_bool_exp'];
            },
            ValueTypes['payment_statuses']
        ];
        payment_statuses_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payment_statuses_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payment_statuses_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payment_statuses_bool_exp'];
            },
            ValueTypes['payment_statuses_aggregate']
        ];
        payment_statuses_by_pk?: [
            { value: string },
            ValueTypes['payment_statuses']
        ];
        payment_types?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payment_types_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payment_types_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payment_types_bool_exp'];
            },
            ValueTypes['payment_types']
        ];
        payment_types_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payment_types_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payment_types_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payment_types_bool_exp'];
            },
            ValueTypes['payment_types_aggregate']
        ];
        payment_types_by_pk?: [{ value: string }, ValueTypes['payment_types']];
        payments?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments']
        ];
        payments_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments_aggregate']
        ];
        payments_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['payments']];
        periods?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['periods_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['periods_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['periods_bool_exp'];
            },
            ValueTypes['periods']
        ];
        periods_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['periods_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['periods_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['periods_bool_exp'];
            },
            ValueTypes['periods_aggregate']
        ];
        periods_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['periods']];
        users?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['users_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['users_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['users_bool_exp'];
            },
            ValueTypes['users']
        ];
        users_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['users_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['users_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['users_bool_exp'];
            },
            ValueTypes['users_aggregate']
        ];
        users_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['users']];
        __typename?: true;
    }>;
    /** subscription root */
    ['subscription_root']: AliasType<{
        auth_account_providers?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_providers_bool_exp'];
            },
            ValueTypes['auth_account_providers']
        ];
        auth_account_providers_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_providers_bool_exp'];
            },
            ValueTypes['auth_account_providers_aggregate']
        ];
        auth_account_providers_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['auth_account_providers']
        ];
        auth_account_roles?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_roles_bool_exp'];
            },
            ValueTypes['auth_account_roles']
        ];
        auth_account_roles_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_account_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_account_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_account_roles_bool_exp'];
            },
            ValueTypes['auth_account_roles_aggregate']
        ];
        auth_account_roles_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['auth_account_roles']
        ];
        auth_accounts?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_accounts_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_accounts_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_accounts_bool_exp'];
            },
            ValueTypes['auth_accounts']
        ];
        auth_accounts_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_accounts_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_accounts_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_accounts_bool_exp'];
            },
            ValueTypes['auth_accounts_aggregate']
        ];
        auth_accounts_by_pk?: [
            { id: ValueTypes['uuid'] },
            ValueTypes['auth_accounts']
        ];
        auth_providers?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_providers_bool_exp'];
            },
            ValueTypes['auth_providers']
        ];
        auth_providers_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_providers_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_providers_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_providers_bool_exp'];
            },
            ValueTypes['auth_providers_aggregate']
        ];
        auth_providers_by_pk?: [
            { provider: string },
            ValueTypes['auth_providers']
        ];
        auth_refresh_tokens?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_refresh_tokens_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_refresh_tokens_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_refresh_tokens_bool_exp'];
            },
            ValueTypes['auth_refresh_tokens']
        ];
        auth_refresh_tokens_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_refresh_tokens_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_refresh_tokens_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_refresh_tokens_bool_exp'];
            },
            ValueTypes['auth_refresh_tokens_aggregate']
        ];
        auth_refresh_tokens_by_pk?: [
            { refresh_token: ValueTypes['uuid'] },
            ValueTypes['auth_refresh_tokens']
        ];
        auth_roles?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_roles_bool_exp'];
            },
            ValueTypes['auth_roles']
        ];
        auth_roles_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['auth_roles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['auth_roles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['auth_roles_bool_exp'];
            },
            ValueTypes['auth_roles_aggregate']
        ];
        auth_roles_by_pk?: [{ role: string }, ValueTypes['auth_roles']];
        contribution_types?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['contribution_types_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['contribution_types_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['contribution_types_bool_exp'];
            },
            ValueTypes['contribution_types']
        ];
        contribution_types_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['contribution_types_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['contribution_types_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['contribution_types_bool_exp'];
            },
            ValueTypes['contribution_types_aggregate']
        ];
        contribution_types_by_pk?: [
            { value: string },
            ValueTypes['contribution_types']
        ];
        group_cycles?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['group_cycles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['group_cycles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['group_cycles_bool_exp'];
            },
            ValueTypes['group_cycles']
        ];
        group_cycles_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['group_cycles_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['group_cycles_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['group_cycles_bool_exp'];
            },
            ValueTypes['group_cycles_aggregate']
        ];
        group_cycles_by_pk?: [{ value: string }, ValueTypes['group_cycles']];
        group_recurrencies?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['group_recurrencies_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['group_recurrencies_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['group_recurrencies_bool_exp'];
            },
            ValueTypes['group_recurrencies']
        ];
        group_recurrencies_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['group_recurrencies_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['group_recurrencies_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['group_recurrencies_bool_exp'];
            },
            ValueTypes['group_recurrencies_aggregate']
        ];
        group_recurrencies_by_pk?: [
            { value: string },
            ValueTypes['group_recurrencies']
        ];
        groups?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['groups_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['groups_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups']
        ];
        groups_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['groups_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['groups_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups_aggregate']
        ];
        groups_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['groups']];
        members?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['members_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['members_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['members_bool_exp'];
            },
            ValueTypes['members']
        ];
        members_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['members_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['members_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['members_bool_exp'];
            },
            ValueTypes['members_aggregate']
        ];
        members_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['members']];
        payment_statuses?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payment_statuses_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payment_statuses_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payment_statuses_bool_exp'];
            },
            ValueTypes['payment_statuses']
        ];
        payment_statuses_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payment_statuses_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payment_statuses_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payment_statuses_bool_exp'];
            },
            ValueTypes['payment_statuses_aggregate']
        ];
        payment_statuses_by_pk?: [
            { value: string },
            ValueTypes['payment_statuses']
        ];
        payment_types?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payment_types_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payment_types_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payment_types_bool_exp'];
            },
            ValueTypes['payment_types']
        ];
        payment_types_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payment_types_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payment_types_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payment_types_bool_exp'];
            },
            ValueTypes['payment_types_aggregate']
        ];
        payment_types_by_pk?: [{ value: string }, ValueTypes['payment_types']];
        payments?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments']
        ];
        payments_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['payments_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['payments_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['payments_bool_exp'];
            },
            ValueTypes['payments_aggregate']
        ];
        payments_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['payments']];
        periods?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['periods_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['periods_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['periods_bool_exp'];
            },
            ValueTypes['periods']
        ];
        periods_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['periods_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['periods_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['periods_bool_exp'];
            },
            ValueTypes['periods_aggregate']
        ];
        periods_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['periods']];
        users?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['users_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['users_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['users_bool_exp'];
            },
            ValueTypes['users']
        ];
        users_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['users_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['users_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['users_bool_exp'];
            },
            ValueTypes['users_aggregate']
        ];
        users_by_pk?: [{ id: ValueTypes['uuid'] }, ValueTypes['users']];
        __typename?: true;
    }>;
    ['timestamp']: unknown;
    /** expression to compare columns of type timestamp. All fields are combined with logical 'AND'. */
    ['timestamp_comparison_exp']: {
        _eq?: ValueTypes['timestamp'];
        _gt?: ValueTypes['timestamp'];
        _gte?: ValueTypes['timestamp'];
        _in?: ValueTypes['timestamp'][];
        _is_null?: boolean;
        _lt?: ValueTypes['timestamp'];
        _lte?: ValueTypes['timestamp'];
        _neq?: ValueTypes['timestamp'];
        _nin?: ValueTypes['timestamp'][];
    };
    ['timestamptz']: unknown;
    /** expression to compare columns of type timestamptz. All fields are combined with logical 'AND'. */
    ['timestamptz_comparison_exp']: {
        _eq?: ValueTypes['timestamptz'];
        _gt?: ValueTypes['timestamptz'];
        _gte?: ValueTypes['timestamptz'];
        _in?: ValueTypes['timestamptz'][];
        _is_null?: boolean;
        _lt?: ValueTypes['timestamptz'];
        _lte?: ValueTypes['timestamptz'];
        _neq?: ValueTypes['timestamptz'];
        _nin?: ValueTypes['timestamptz'][];
    };
    /** columns and relationships of "users" */
    ['users']: AliasType<{
        /** An object relationship */
        account?: ValueTypes['auth_accounts'];
        avatar_url?: true;
        created_at?: true;
        display_name?: true;
        expo_push_token?: true;
        groups?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['groups_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['groups_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups']
        ];
        groups_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['groups_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['groups_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['groups_bool_exp'];
            },
            ValueTypes['groups_aggregate']
        ];
        id?: true;
        members?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['members_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['members_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['members_bool_exp'];
            },
            ValueTypes['members']
        ];
        members_aggregate?: [
            {
                /** distinct select on columns */
                distinct_on?: ValueTypes['members_select_column'][] /** limit the number of rows returned */;
                limit?: number /** skip the first n rows. Use only with order_by */;
                offset?: number /** sort the rows by one or more columns */;
                order_by?: ValueTypes['members_order_by'][] /** filter the rows returned */;
                where?: ValueTypes['members_bool_exp'];
            },
            ValueTypes['members_aggregate']
        ];
        updated_at?: true;
        __typename?: true;
    }>;
    /** aggregated selection of "users" */
    ['users_aggregate']: AliasType<{
        aggregate?: ValueTypes['users_aggregate_fields'];
        nodes?: ValueTypes['users'];
        __typename?: true;
    }>;
    /** aggregate fields of "users" */
    ['users_aggregate_fields']: AliasType<{
        count?: [
            {
                columns?: ValueTypes['users_select_column'][];
                distinct?: boolean;
            },
            true
        ];
        max?: ValueTypes['users_max_fields'];
        min?: ValueTypes['users_min_fields'];
        __typename?: true;
    }>;
    /** order by aggregate values of table "users" */
    ['users_aggregate_order_by']: {
        count?: ValueTypes['order_by'];
        max?: ValueTypes['users_max_order_by'];
        min?: ValueTypes['users_min_order_by'];
    };
    /** input type for inserting array relation for remote table "users" */
    ['users_arr_rel_insert_input']: {
        data: ValueTypes['users_insert_input'][];
        on_conflict?: ValueTypes['users_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "users". All fields are combined with a logical 'AND'. */
    ['users_bool_exp']: {
        _and?: (ValueTypes['users_bool_exp'] | undefined)[];
        _not?: ValueTypes['users_bool_exp'];
        _or?: (ValueTypes['users_bool_exp'] | undefined)[];
        account?: ValueTypes['auth_accounts_bool_exp'];
        avatar_url?: ValueTypes['String_comparison_exp'];
        created_at?: ValueTypes['timestamptz_comparison_exp'];
        display_name?: ValueTypes['String_comparison_exp'];
        expo_push_token?: ValueTypes['String_comparison_exp'];
        groups?: ValueTypes['groups_bool_exp'];
        id?: ValueTypes['uuid_comparison_exp'];
        members?: ValueTypes['members_bool_exp'];
        updated_at?: ValueTypes['timestamptz_comparison_exp'];
    };
    /** unique or primary key constraints on table "users" */
    ['users_constraint']: users_constraint;
    /** input type for inserting data into table "users" */
    ['users_insert_input']: {
        account?: ValueTypes['auth_accounts_obj_rel_insert_input'];
        avatar_url?: string;
        created_at?: ValueTypes['timestamptz'];
        display_name?: string;
        expo_push_token?: string;
        groups?: ValueTypes['groups_arr_rel_insert_input'];
        id?: ValueTypes['uuid'];
        members?: ValueTypes['members_arr_rel_insert_input'];
        updated_at?: ValueTypes['timestamptz'];
    };
    /** aggregate max on columns */
    ['users_max_fields']: AliasType<{
        avatar_url?: true;
        created_at?: true;
        display_name?: true;
        expo_push_token?: true;
        id?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** order by max() on columns of table "users" */
    ['users_max_order_by']: {
        avatar_url?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        display_name?: ValueTypes['order_by'];
        expo_push_token?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** aggregate min on columns */
    ['users_min_fields']: AliasType<{
        avatar_url?: true;
        created_at?: true;
        display_name?: true;
        expo_push_token?: true;
        id?: true;
        updated_at?: true;
        __typename?: true;
    }>;
    /** order by min() on columns of table "users" */
    ['users_min_order_by']: {
        avatar_url?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        display_name?: ValueTypes['order_by'];
        expo_push_token?: ValueTypes['order_by'];
        id?: ValueTypes['order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** response of any mutation on the table "users" */
    ['users_mutation_response']: AliasType<{
        /** number of affected rows by the mutation */
        affected_rows?: true;
        /** data of the affected rows by the mutation */
        returning?: ValueTypes['users'];
        __typename?: true;
    }>;
    /** input type for inserting object relation for remote table "users" */
    ['users_obj_rel_insert_input']: {
        data: ValueTypes['users_insert_input'];
        on_conflict?: ValueTypes['users_on_conflict'];
    };
    /** on conflict condition type for table "users" */
    ['users_on_conflict']: {
        constraint: ValueTypes['users_constraint'];
        update_columns: ValueTypes['users_update_column'][];
        where?: ValueTypes['users_bool_exp'];
    };
    /** ordering options when selecting data from "users" */
    ['users_order_by']: {
        account?: ValueTypes['auth_accounts_order_by'];
        avatar_url?: ValueTypes['order_by'];
        created_at?: ValueTypes['order_by'];
        display_name?: ValueTypes['order_by'];
        expo_push_token?: ValueTypes['order_by'];
        groups_aggregate?: ValueTypes['groups_aggregate_order_by'];
        id?: ValueTypes['order_by'];
        members_aggregate?: ValueTypes['members_aggregate_order_by'];
        updated_at?: ValueTypes['order_by'];
    };
    /** primary key columns input for table: "users" */
    ['users_pk_columns_input']: {
        id: ValueTypes['uuid'];
    };
    /** select columns of table "users" */
    ['users_select_column']: users_select_column;
    /** input type for updating data in table "users" */
    ['users_set_input']: {
        avatar_url?: string;
        created_at?: ValueTypes['timestamptz'];
        display_name?: string;
        expo_push_token?: string;
        id?: ValueTypes['uuid'];
        updated_at?: ValueTypes['timestamptz'];
    };
    /** update columns of table "users" */
    ['users_update_column']: users_update_column;
    ['uuid']: unknown;
    /** expression to compare columns of type uuid. All fields are combined with logical 'AND'. */
    ['uuid_comparison_exp']: {
        _eq?: ValueTypes['uuid'];
        _gt?: ValueTypes['uuid'];
        _gte?: ValueTypes['uuid'];
        _in?: ValueTypes['uuid'][];
        _is_null?: boolean;
        _lt?: ValueTypes['uuid'];
        _lte?: ValueTypes['uuid'];
        _neq?: ValueTypes['uuid'];
        _nin?: ValueTypes['uuid'][];
    };
};

export type ModelTypes = {
    ['ActionCreateGroupInput']: GraphQLTypes['ActionCreateGroupInput'];
    ['ActionCreateGroupOutpu']: {
        ok: boolean;
    };
    ['ActionCreateGroupOutput']: {
        ok: boolean;
    };
    ['ActionRequestPaymentInput']: GraphQLTypes['ActionRequestPaymentInput'];
    ['ActionRequestPaymentOutput']: {
        id: ModelTypes['uuid'];
        url: string;
    };
    ['ActionWithdrawPaymentInput']: GraphQLTypes['ActionWithdrawPaymentInput'];
    ['ActionWithdrawPaymentOutput']: {
        id: ModelTypes['uuid'];
    };
    /** expression to compare columns of type Boolean. All fields are combined with logical 'AND'. */
    ['Boolean_comparison_exp']: GraphQLTypes['Boolean_comparison_exp'];
    ['InsertCreateInput']: GraphQLTypes['InsertCreateInput'];
    ['InsertPaymentOneActionInput']: GraphQLTypes['InsertPaymentOneActionInput'];
    ['InsertPaymentOneActionOutput']: {
        url: string;
    };
    ['InsertPaymentOutput']: {
        id: ModelTypes['uuid'];
        url: string;
    };
    /** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
    ['Int_comparison_exp']: GraphQLTypes['Int_comparison_exp'];
    ['PaymentCreateInput']: GraphQLTypes['PaymentCreateInput'];
    ['PaymentCreatedOutput']: {
        url: string;
    };
    /** expression to compare columns of type String. All fields are combined with logical 'AND'. */
    ['String_comparison_exp']: GraphQLTypes['String_comparison_exp'];
    /** columns and relationships of "auth.account_providers" */
    ['auth_account_providers']: {
        /** An object relationship */
        account: ModelTypes['auth_accounts'];
        account_id: ModelTypes['uuid'];
        auth_provider: string;
        auth_provider_unique_id: string;
        created_at: ModelTypes['timestamptz'];
        id: ModelTypes['uuid'];
        /** An object relationship */
        provider: ModelTypes['auth_providers'];
        updated_at: ModelTypes['timestamptz'];
    };
    /** aggregated selection of "auth.account_providers" */
    ['auth_account_providers_aggregate']: {
        aggregate?: ModelTypes['auth_account_providers_aggregate_fields'];
        nodes: ModelTypes['auth_account_providers'][];
    };
    /** aggregate fields of "auth.account_providers" */
    ['auth_account_providers_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['auth_account_providers_max_fields'];
        min?: ModelTypes['auth_account_providers_min_fields'];
    };
    /** order by aggregate values of table "auth.account_providers" */
    ['auth_account_providers_aggregate_order_by']: GraphQLTypes['auth_account_providers_aggregate_order_by'];
    /** input type for inserting array relation for remote table "auth.account_providers" */
    ['auth_account_providers_arr_rel_insert_input']: GraphQLTypes['auth_account_providers_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "auth.account_providers". All fields are combined with a logical 'AND'. */
    ['auth_account_providers_bool_exp']: GraphQLTypes['auth_account_providers_bool_exp'];
    /** unique or primary key constraints on table "auth.account_providers" */
    ['auth_account_providers_constraint']: GraphQLTypes['auth_account_providers_constraint'];
    /** input type for inserting data into table "auth.account_providers" */
    ['auth_account_providers_insert_input']: GraphQLTypes['auth_account_providers_insert_input'];
    /** aggregate max on columns */
    ['auth_account_providers_max_fields']: {
        account_id?: ModelTypes['uuid'];
        auth_provider?: string;
        auth_provider_unique_id?: string;
        created_at?: ModelTypes['timestamptz'];
        id?: ModelTypes['uuid'];
        updated_at?: ModelTypes['timestamptz'];
    };
    /** order by max() on columns of table "auth.account_providers" */
    ['auth_account_providers_max_order_by']: GraphQLTypes['auth_account_providers_max_order_by'];
    /** aggregate min on columns */
    ['auth_account_providers_min_fields']: {
        account_id?: ModelTypes['uuid'];
        auth_provider?: string;
        auth_provider_unique_id?: string;
        created_at?: ModelTypes['timestamptz'];
        id?: ModelTypes['uuid'];
        updated_at?: ModelTypes['timestamptz'];
    };
    /** order by min() on columns of table "auth.account_providers" */
    ['auth_account_providers_min_order_by']: GraphQLTypes['auth_account_providers_min_order_by'];
    /** response of any mutation on the table "auth.account_providers" */
    ['auth_account_providers_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['auth_account_providers'][];
    };
    /** input type for inserting object relation for remote table "auth.account_providers" */
    ['auth_account_providers_obj_rel_insert_input']: GraphQLTypes['auth_account_providers_obj_rel_insert_input'];
    /** on conflict condition type for table "auth.account_providers" */
    ['auth_account_providers_on_conflict']: GraphQLTypes['auth_account_providers_on_conflict'];
    /** ordering options when selecting data from "auth.account_providers" */
    ['auth_account_providers_order_by']: GraphQLTypes['auth_account_providers_order_by'];
    /** primary key columns input for table: "auth.account_providers" */
    ['auth_account_providers_pk_columns_input']: GraphQLTypes['auth_account_providers_pk_columns_input'];
    /** select columns of table "auth.account_providers" */
    ['auth_account_providers_select_column']: GraphQLTypes['auth_account_providers_select_column'];
    /** input type for updating data in table "auth.account_providers" */
    ['auth_account_providers_set_input']: GraphQLTypes['auth_account_providers_set_input'];
    /** update columns of table "auth.account_providers" */
    ['auth_account_providers_update_column']: GraphQLTypes['auth_account_providers_update_column'];
    /** columns and relationships of "auth.account_roles" */
    ['auth_account_roles']: {
        /** An object relationship */
        account: ModelTypes['auth_accounts'];
        account_id: ModelTypes['uuid'];
        created_at: ModelTypes['timestamptz'];
        id: ModelTypes['uuid'];
        role: string;
        /** An object relationship */
        roleByRole: ModelTypes['auth_roles'];
    };
    /** aggregated selection of "auth.account_roles" */
    ['auth_account_roles_aggregate']: {
        aggregate?: ModelTypes['auth_account_roles_aggregate_fields'];
        nodes: ModelTypes['auth_account_roles'][];
    };
    /** aggregate fields of "auth.account_roles" */
    ['auth_account_roles_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['auth_account_roles_max_fields'];
        min?: ModelTypes['auth_account_roles_min_fields'];
    };
    /** order by aggregate values of table "auth.account_roles" */
    ['auth_account_roles_aggregate_order_by']: GraphQLTypes['auth_account_roles_aggregate_order_by'];
    /** input type for inserting array relation for remote table "auth.account_roles" */
    ['auth_account_roles_arr_rel_insert_input']: GraphQLTypes['auth_account_roles_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "auth.account_roles". All fields are combined with a logical 'AND'. */
    ['auth_account_roles_bool_exp']: GraphQLTypes['auth_account_roles_bool_exp'];
    /** unique or primary key constraints on table "auth.account_roles" */
    ['auth_account_roles_constraint']: GraphQLTypes['auth_account_roles_constraint'];
    /** input type for inserting data into table "auth.account_roles" */
    ['auth_account_roles_insert_input']: GraphQLTypes['auth_account_roles_insert_input'];
    /** aggregate max on columns */
    ['auth_account_roles_max_fields']: {
        account_id?: ModelTypes['uuid'];
        created_at?: ModelTypes['timestamptz'];
        id?: ModelTypes['uuid'];
        role?: string;
    };
    /** order by max() on columns of table "auth.account_roles" */
    ['auth_account_roles_max_order_by']: GraphQLTypes['auth_account_roles_max_order_by'];
    /** aggregate min on columns */
    ['auth_account_roles_min_fields']: {
        account_id?: ModelTypes['uuid'];
        created_at?: ModelTypes['timestamptz'];
        id?: ModelTypes['uuid'];
        role?: string;
    };
    /** order by min() on columns of table "auth.account_roles" */
    ['auth_account_roles_min_order_by']: GraphQLTypes['auth_account_roles_min_order_by'];
    /** response of any mutation on the table "auth.account_roles" */
    ['auth_account_roles_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['auth_account_roles'][];
    };
    /** input type for inserting object relation for remote table "auth.account_roles" */
    ['auth_account_roles_obj_rel_insert_input']: GraphQLTypes['auth_account_roles_obj_rel_insert_input'];
    /** on conflict condition type for table "auth.account_roles" */
    ['auth_account_roles_on_conflict']: GraphQLTypes['auth_account_roles_on_conflict'];
    /** ordering options when selecting data from "auth.account_roles" */
    ['auth_account_roles_order_by']: GraphQLTypes['auth_account_roles_order_by'];
    /** primary key columns input for table: "auth.account_roles" */
    ['auth_account_roles_pk_columns_input']: GraphQLTypes['auth_account_roles_pk_columns_input'];
    /** select columns of table "auth.account_roles" */
    ['auth_account_roles_select_column']: GraphQLTypes['auth_account_roles_select_column'];
    /** input type for updating data in table "auth.account_roles" */
    ['auth_account_roles_set_input']: GraphQLTypes['auth_account_roles_set_input'];
    /** update columns of table "auth.account_roles" */
    ['auth_account_roles_update_column']: GraphQLTypes['auth_account_roles_update_column'];
    /** columns and relationships of "auth.accounts" */
    ['auth_accounts']: {
        /** An array relationship */
        account_providers: ModelTypes['auth_account_providers'][];
        /** An aggregated array relationship */
        account_providers_aggregate: ModelTypes['auth_account_providers_aggregate'];
        /** An array relationship */
        account_roles: ModelTypes['auth_account_roles'][];
        /** An aggregated array relationship */
        account_roles_aggregate: ModelTypes['auth_account_roles_aggregate'];
        active: boolean;
        created_at: ModelTypes['timestamptz'];
        custom_register_data?: ModelTypes['jsonb'];
        default_role: string;
        email?: ModelTypes['citext'];
        id: ModelTypes['uuid'];
        is_anonymous: boolean;
        mfa_enabled: boolean;
        new_email?: ModelTypes['citext'];
        otp_secret?: string;
        password_hash?: string;
        /** An array relationship */
        refresh_tokens: ModelTypes['auth_refresh_tokens'][];
        /** An aggregated array relationship */
        refresh_tokens_aggregate: ModelTypes['auth_refresh_tokens_aggregate'];
        /** An object relationship */
        role: ModelTypes['auth_roles'];
        ticket: ModelTypes['uuid'];
        ticket_expires_at: ModelTypes['timestamptz'];
        updated_at: ModelTypes['timestamptz'];
        /** An object relationship */
        user: ModelTypes['users'];
        user_id: ModelTypes['uuid'];
    };
    /** aggregated selection of "auth.accounts" */
    ['auth_accounts_aggregate']: {
        aggregate?: ModelTypes['auth_accounts_aggregate_fields'];
        nodes: ModelTypes['auth_accounts'][];
    };
    /** aggregate fields of "auth.accounts" */
    ['auth_accounts_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['auth_accounts_max_fields'];
        min?: ModelTypes['auth_accounts_min_fields'];
    };
    /** order by aggregate values of table "auth.accounts" */
    ['auth_accounts_aggregate_order_by']: GraphQLTypes['auth_accounts_aggregate_order_by'];
    /** append existing jsonb value of filtered columns with new jsonb value */
    ['auth_accounts_append_input']: GraphQLTypes['auth_accounts_append_input'];
    /** input type for inserting array relation for remote table "auth.accounts" */
    ['auth_accounts_arr_rel_insert_input']: GraphQLTypes['auth_accounts_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "auth.accounts". All fields are combined with a logical 'AND'. */
    ['auth_accounts_bool_exp']: GraphQLTypes['auth_accounts_bool_exp'];
    /** unique or primary key constraints on table "auth.accounts" */
    ['auth_accounts_constraint']: GraphQLTypes['auth_accounts_constraint'];
    /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
    ['auth_accounts_delete_at_path_input']: GraphQLTypes['auth_accounts_delete_at_path_input'];
    /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
    ['auth_accounts_delete_elem_input']: GraphQLTypes['auth_accounts_delete_elem_input'];
    /** delete key/value pair or string element. key/value pairs are matched based on their key value */
    ['auth_accounts_delete_key_input']: GraphQLTypes['auth_accounts_delete_key_input'];
    /** input type for inserting data into table "auth.accounts" */
    ['auth_accounts_insert_input']: GraphQLTypes['auth_accounts_insert_input'];
    /** aggregate max on columns */
    ['auth_accounts_max_fields']: {
        created_at?: ModelTypes['timestamptz'];
        default_role?: string;
        email?: ModelTypes['citext'];
        id?: ModelTypes['uuid'];
        new_email?: ModelTypes['citext'];
        otp_secret?: string;
        password_hash?: string;
        ticket?: ModelTypes['uuid'];
        ticket_expires_at?: ModelTypes['timestamptz'];
        updated_at?: ModelTypes['timestamptz'];
        user_id?: ModelTypes['uuid'];
    };
    /** order by max() on columns of table "auth.accounts" */
    ['auth_accounts_max_order_by']: GraphQLTypes['auth_accounts_max_order_by'];
    /** aggregate min on columns */
    ['auth_accounts_min_fields']: {
        created_at?: ModelTypes['timestamptz'];
        default_role?: string;
        email?: ModelTypes['citext'];
        id?: ModelTypes['uuid'];
        new_email?: ModelTypes['citext'];
        otp_secret?: string;
        password_hash?: string;
        ticket?: ModelTypes['uuid'];
        ticket_expires_at?: ModelTypes['timestamptz'];
        updated_at?: ModelTypes['timestamptz'];
        user_id?: ModelTypes['uuid'];
    };
    /** order by min() on columns of table "auth.accounts" */
    ['auth_accounts_min_order_by']: GraphQLTypes['auth_accounts_min_order_by'];
    /** response of any mutation on the table "auth.accounts" */
    ['auth_accounts_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['auth_accounts'][];
    };
    /** input type for inserting object relation for remote table "auth.accounts" */
    ['auth_accounts_obj_rel_insert_input']: GraphQLTypes['auth_accounts_obj_rel_insert_input'];
    /** on conflict condition type for table "auth.accounts" */
    ['auth_accounts_on_conflict']: GraphQLTypes['auth_accounts_on_conflict'];
    /** ordering options when selecting data from "auth.accounts" */
    ['auth_accounts_order_by']: GraphQLTypes['auth_accounts_order_by'];
    /** primary key columns input for table: "auth.accounts" */
    ['auth_accounts_pk_columns_input']: GraphQLTypes['auth_accounts_pk_columns_input'];
    /** prepend existing jsonb value of filtered columns with new jsonb value */
    ['auth_accounts_prepend_input']: GraphQLTypes['auth_accounts_prepend_input'];
    /** select columns of table "auth.accounts" */
    ['auth_accounts_select_column']: GraphQLTypes['auth_accounts_select_column'];
    /** input type for updating data in table "auth.accounts" */
    ['auth_accounts_set_input']: GraphQLTypes['auth_accounts_set_input'];
    /** update columns of table "auth.accounts" */
    ['auth_accounts_update_column']: GraphQLTypes['auth_accounts_update_column'];
    /** columns and relationships of "auth.providers" */
    ['auth_providers']: {
        /** An array relationship */
        account_providers: ModelTypes['auth_account_providers'][];
        /** An aggregated array relationship */
        account_providers_aggregate: ModelTypes['auth_account_providers_aggregate'];
        provider: string;
    };
    /** aggregated selection of "auth.providers" */
    ['auth_providers_aggregate']: {
        aggregate?: ModelTypes['auth_providers_aggregate_fields'];
        nodes: ModelTypes['auth_providers'][];
    };
    /** aggregate fields of "auth.providers" */
    ['auth_providers_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['auth_providers_max_fields'];
        min?: ModelTypes['auth_providers_min_fields'];
    };
    /** order by aggregate values of table "auth.providers" */
    ['auth_providers_aggregate_order_by']: GraphQLTypes['auth_providers_aggregate_order_by'];
    /** input type for inserting array relation for remote table "auth.providers" */
    ['auth_providers_arr_rel_insert_input']: GraphQLTypes['auth_providers_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "auth.providers". All fields are combined with a logical 'AND'. */
    ['auth_providers_bool_exp']: GraphQLTypes['auth_providers_bool_exp'];
    /** unique or primary key constraints on table "auth.providers" */
    ['auth_providers_constraint']: GraphQLTypes['auth_providers_constraint'];
    /** input type for inserting data into table "auth.providers" */
    ['auth_providers_insert_input']: GraphQLTypes['auth_providers_insert_input'];
    /** aggregate max on columns */
    ['auth_providers_max_fields']: {
        provider?: string;
    };
    /** order by max() on columns of table "auth.providers" */
    ['auth_providers_max_order_by']: GraphQLTypes['auth_providers_max_order_by'];
    /** aggregate min on columns */
    ['auth_providers_min_fields']: {
        provider?: string;
    };
    /** order by min() on columns of table "auth.providers" */
    ['auth_providers_min_order_by']: GraphQLTypes['auth_providers_min_order_by'];
    /** response of any mutation on the table "auth.providers" */
    ['auth_providers_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['auth_providers'][];
    };
    /** input type for inserting object relation for remote table "auth.providers" */
    ['auth_providers_obj_rel_insert_input']: GraphQLTypes['auth_providers_obj_rel_insert_input'];
    /** on conflict condition type for table "auth.providers" */
    ['auth_providers_on_conflict']: GraphQLTypes['auth_providers_on_conflict'];
    /** ordering options when selecting data from "auth.providers" */
    ['auth_providers_order_by']: GraphQLTypes['auth_providers_order_by'];
    /** primary key columns input for table: "auth.providers" */
    ['auth_providers_pk_columns_input']: GraphQLTypes['auth_providers_pk_columns_input'];
    /** select columns of table "auth.providers" */
    ['auth_providers_select_column']: GraphQLTypes['auth_providers_select_column'];
    /** input type for updating data in table "auth.providers" */
    ['auth_providers_set_input']: GraphQLTypes['auth_providers_set_input'];
    /** update columns of table "auth.providers" */
    ['auth_providers_update_column']: GraphQLTypes['auth_providers_update_column'];
    /** columns and relationships of "auth.refresh_tokens" */
    ['auth_refresh_tokens']: {
        /** An object relationship */
        account: ModelTypes['auth_accounts'];
        account_id: ModelTypes['uuid'];
        created_at: ModelTypes['timestamptz'];
        expires_at: ModelTypes['timestamptz'];
        refresh_token: ModelTypes['uuid'];
    };
    /** aggregated selection of "auth.refresh_tokens" */
    ['auth_refresh_tokens_aggregate']: {
        aggregate?: ModelTypes['auth_refresh_tokens_aggregate_fields'];
        nodes: ModelTypes['auth_refresh_tokens'][];
    };
    /** aggregate fields of "auth.refresh_tokens" */
    ['auth_refresh_tokens_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['auth_refresh_tokens_max_fields'];
        min?: ModelTypes['auth_refresh_tokens_min_fields'];
    };
    /** order by aggregate values of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_aggregate_order_by']: GraphQLTypes['auth_refresh_tokens_aggregate_order_by'];
    /** input type for inserting array relation for remote table "auth.refresh_tokens" */
    ['auth_refresh_tokens_arr_rel_insert_input']: GraphQLTypes['auth_refresh_tokens_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "auth.refresh_tokens". All fields are combined with a logical 'AND'. */
    ['auth_refresh_tokens_bool_exp']: GraphQLTypes['auth_refresh_tokens_bool_exp'];
    /** unique or primary key constraints on table "auth.refresh_tokens" */
    ['auth_refresh_tokens_constraint']: GraphQLTypes['auth_refresh_tokens_constraint'];
    /** input type for inserting data into table "auth.refresh_tokens" */
    ['auth_refresh_tokens_insert_input']: GraphQLTypes['auth_refresh_tokens_insert_input'];
    /** aggregate max on columns */
    ['auth_refresh_tokens_max_fields']: {
        account_id?: ModelTypes['uuid'];
        created_at?: ModelTypes['timestamptz'];
        expires_at?: ModelTypes['timestamptz'];
        refresh_token?: ModelTypes['uuid'];
    };
    /** order by max() on columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_max_order_by']: GraphQLTypes['auth_refresh_tokens_max_order_by'];
    /** aggregate min on columns */
    ['auth_refresh_tokens_min_fields']: {
        account_id?: ModelTypes['uuid'];
        created_at?: ModelTypes['timestamptz'];
        expires_at?: ModelTypes['timestamptz'];
        refresh_token?: ModelTypes['uuid'];
    };
    /** order by min() on columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_min_order_by']: GraphQLTypes['auth_refresh_tokens_min_order_by'];
    /** response of any mutation on the table "auth.refresh_tokens" */
    ['auth_refresh_tokens_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['auth_refresh_tokens'][];
    };
    /** input type for inserting object relation for remote table "auth.refresh_tokens" */
    ['auth_refresh_tokens_obj_rel_insert_input']: GraphQLTypes['auth_refresh_tokens_obj_rel_insert_input'];
    /** on conflict condition type for table "auth.refresh_tokens" */
    ['auth_refresh_tokens_on_conflict']: GraphQLTypes['auth_refresh_tokens_on_conflict'];
    /** ordering options when selecting data from "auth.refresh_tokens" */
    ['auth_refresh_tokens_order_by']: GraphQLTypes['auth_refresh_tokens_order_by'];
    /** primary key columns input for table: "auth.refresh_tokens" */
    ['auth_refresh_tokens_pk_columns_input']: GraphQLTypes['auth_refresh_tokens_pk_columns_input'];
    /** select columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_select_column']: GraphQLTypes['auth_refresh_tokens_select_column'];
    /** input type for updating data in table "auth.refresh_tokens" */
    ['auth_refresh_tokens_set_input']: GraphQLTypes['auth_refresh_tokens_set_input'];
    /** update columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_update_column']: GraphQLTypes['auth_refresh_tokens_update_column'];
    /** columns and relationships of "auth.roles" */
    ['auth_roles']: {
        /** An array relationship */
        account_roles: ModelTypes['auth_account_roles'][];
        /** An aggregated array relationship */
        account_roles_aggregate: ModelTypes['auth_account_roles_aggregate'];
        /** An array relationship */
        accounts: ModelTypes['auth_accounts'][];
        /** An aggregated array relationship */
        accounts_aggregate: ModelTypes['auth_accounts_aggregate'];
        role: string;
    };
    /** aggregated selection of "auth.roles" */
    ['auth_roles_aggregate']: {
        aggregate?: ModelTypes['auth_roles_aggregate_fields'];
        nodes: ModelTypes['auth_roles'][];
    };
    /** aggregate fields of "auth.roles" */
    ['auth_roles_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['auth_roles_max_fields'];
        min?: ModelTypes['auth_roles_min_fields'];
    };
    /** order by aggregate values of table "auth.roles" */
    ['auth_roles_aggregate_order_by']: GraphQLTypes['auth_roles_aggregate_order_by'];
    /** input type for inserting array relation for remote table "auth.roles" */
    ['auth_roles_arr_rel_insert_input']: GraphQLTypes['auth_roles_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "auth.roles". All fields are combined with a logical 'AND'. */
    ['auth_roles_bool_exp']: GraphQLTypes['auth_roles_bool_exp'];
    /** unique or primary key constraints on table "auth.roles" */
    ['auth_roles_constraint']: GraphQLTypes['auth_roles_constraint'];
    /** input type for inserting data into table "auth.roles" */
    ['auth_roles_insert_input']: GraphQLTypes['auth_roles_insert_input'];
    /** aggregate max on columns */
    ['auth_roles_max_fields']: {
        role?: string;
    };
    /** order by max() on columns of table "auth.roles" */
    ['auth_roles_max_order_by']: GraphQLTypes['auth_roles_max_order_by'];
    /** aggregate min on columns */
    ['auth_roles_min_fields']: {
        role?: string;
    };
    /** order by min() on columns of table "auth.roles" */
    ['auth_roles_min_order_by']: GraphQLTypes['auth_roles_min_order_by'];
    /** response of any mutation on the table "auth.roles" */
    ['auth_roles_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['auth_roles'][];
    };
    /** input type for inserting object relation for remote table "auth.roles" */
    ['auth_roles_obj_rel_insert_input']: GraphQLTypes['auth_roles_obj_rel_insert_input'];
    /** on conflict condition type for table "auth.roles" */
    ['auth_roles_on_conflict']: GraphQLTypes['auth_roles_on_conflict'];
    /** ordering options when selecting data from "auth.roles" */
    ['auth_roles_order_by']: GraphQLTypes['auth_roles_order_by'];
    /** primary key columns input for table: "auth.roles" */
    ['auth_roles_pk_columns_input']: GraphQLTypes['auth_roles_pk_columns_input'];
    /** select columns of table "auth.roles" */
    ['auth_roles_select_column']: GraphQLTypes['auth_roles_select_column'];
    /** input type for updating data in table "auth.roles" */
    ['auth_roles_set_input']: GraphQLTypes['auth_roles_set_input'];
    /** update columns of table "auth.roles" */
    ['auth_roles_update_column']: GraphQLTypes['auth_roles_update_column'];
    ['citext']: any;
    /** expression to compare columns of type citext. All fields are combined with logical 'AND'. */
    ['citext_comparison_exp']: GraphQLTypes['citext_comparison_exp'];
    /** columns and relationships of "contribution_types" */
    ['contribution_types']: {
        description: string;
        /** An array relationship */
        groups: ModelTypes['groups'][];
        /** An aggregated array relationship */
        groups_aggregate: ModelTypes['groups_aggregate'];
        value: string;
    };
    /** aggregated selection of "contribution_types" */
    ['contribution_types_aggregate']: {
        aggregate?: ModelTypes['contribution_types_aggregate_fields'];
        nodes: ModelTypes['contribution_types'][];
    };
    /** aggregate fields of "contribution_types" */
    ['contribution_types_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['contribution_types_max_fields'];
        min?: ModelTypes['contribution_types_min_fields'];
    };
    /** order by aggregate values of table "contribution_types" */
    ['contribution_types_aggregate_order_by']: GraphQLTypes['contribution_types_aggregate_order_by'];
    /** input type for inserting array relation for remote table "contribution_types" */
    ['contribution_types_arr_rel_insert_input']: GraphQLTypes['contribution_types_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "contribution_types". All fields are combined with a logical 'AND'. */
    ['contribution_types_bool_exp']: GraphQLTypes['contribution_types_bool_exp'];
    /** unique or primary key constraints on table "contribution_types" */
    ['contribution_types_constraint']: GraphQLTypes['contribution_types_constraint'];
    ['contribution_types_enum']: GraphQLTypes['contribution_types_enum'];
    /** expression to compare columns of type contribution_types_enum. All fields are combined with logical 'AND'. */
    ['contribution_types_enum_comparison_exp']: GraphQLTypes['contribution_types_enum_comparison_exp'];
    /** input type for inserting data into table "contribution_types" */
    ['contribution_types_insert_input']: GraphQLTypes['contribution_types_insert_input'];
    /** aggregate max on columns */
    ['contribution_types_max_fields']: {
        description?: string;
        value?: string;
    };
    /** order by max() on columns of table "contribution_types" */
    ['contribution_types_max_order_by']: GraphQLTypes['contribution_types_max_order_by'];
    /** aggregate min on columns */
    ['contribution_types_min_fields']: {
        description?: string;
        value?: string;
    };
    /** order by min() on columns of table "contribution_types" */
    ['contribution_types_min_order_by']: GraphQLTypes['contribution_types_min_order_by'];
    /** response of any mutation on the table "contribution_types" */
    ['contribution_types_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['contribution_types'][];
    };
    /** input type for inserting object relation for remote table "contribution_types" */
    ['contribution_types_obj_rel_insert_input']: GraphQLTypes['contribution_types_obj_rel_insert_input'];
    /** on conflict condition type for table "contribution_types" */
    ['contribution_types_on_conflict']: GraphQLTypes['contribution_types_on_conflict'];
    /** ordering options when selecting data from "contribution_types" */
    ['contribution_types_order_by']: GraphQLTypes['contribution_types_order_by'];
    /** primary key columns input for table: "contribution_types" */
    ['contribution_types_pk_columns_input']: GraphQLTypes['contribution_types_pk_columns_input'];
    /** select columns of table "contribution_types" */
    ['contribution_types_select_column']: GraphQLTypes['contribution_types_select_column'];
    /** input type for updating data in table "contribution_types" */
    ['contribution_types_set_input']: GraphQLTypes['contribution_types_set_input'];
    /** update columns of table "contribution_types" */
    ['contribution_types_update_column']: GraphQLTypes['contribution_types_update_column'];
    /** columns and relationships of "group_cycles" */
    ['group_cycles']: {
        description: string;
        value: string;
    };
    /** aggregated selection of "group_cycles" */
    ['group_cycles_aggregate']: {
        aggregate?: ModelTypes['group_cycles_aggregate_fields'];
        nodes: ModelTypes['group_cycles'][];
    };
    /** aggregate fields of "group_cycles" */
    ['group_cycles_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['group_cycles_max_fields'];
        min?: ModelTypes['group_cycles_min_fields'];
    };
    /** order by aggregate values of table "group_cycles" */
    ['group_cycles_aggregate_order_by']: GraphQLTypes['group_cycles_aggregate_order_by'];
    /** input type for inserting array relation for remote table "group_cycles" */
    ['group_cycles_arr_rel_insert_input']: GraphQLTypes['group_cycles_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "group_cycles". All fields are combined with a logical 'AND'. */
    ['group_cycles_bool_exp']: GraphQLTypes['group_cycles_bool_exp'];
    /** unique or primary key constraints on table "group_cycles" */
    ['group_cycles_constraint']: GraphQLTypes['group_cycles_constraint'];
    /** input type for inserting data into table "group_cycles" */
    ['group_cycles_insert_input']: GraphQLTypes['group_cycles_insert_input'];
    /** aggregate max on columns */
    ['group_cycles_max_fields']: {
        description?: string;
        value?: string;
    };
    /** order by max() on columns of table "group_cycles" */
    ['group_cycles_max_order_by']: GraphQLTypes['group_cycles_max_order_by'];
    /** aggregate min on columns */
    ['group_cycles_min_fields']: {
        description?: string;
        value?: string;
    };
    /** order by min() on columns of table "group_cycles" */
    ['group_cycles_min_order_by']: GraphQLTypes['group_cycles_min_order_by'];
    /** response of any mutation on the table "group_cycles" */
    ['group_cycles_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['group_cycles'][];
    };
    /** input type for inserting object relation for remote table "group_cycles" */
    ['group_cycles_obj_rel_insert_input']: GraphQLTypes['group_cycles_obj_rel_insert_input'];
    /** on conflict condition type for table "group_cycles" */
    ['group_cycles_on_conflict']: GraphQLTypes['group_cycles_on_conflict'];
    /** ordering options when selecting data from "group_cycles" */
    ['group_cycles_order_by']: GraphQLTypes['group_cycles_order_by'];
    /** primary key columns input for table: "group_cycles" */
    ['group_cycles_pk_columns_input']: GraphQLTypes['group_cycles_pk_columns_input'];
    /** select columns of table "group_cycles" */
    ['group_cycles_select_column']: GraphQLTypes['group_cycles_select_column'];
    /** input type for updating data in table "group_cycles" */
    ['group_cycles_set_input']: GraphQLTypes['group_cycles_set_input'];
    /** update columns of table "group_cycles" */
    ['group_cycles_update_column']: GraphQLTypes['group_cycles_update_column'];
    /** columns and relationships of "group_recurrencies" */
    ['group_recurrencies']: {
        description: string;
        /** An array relationship */
        groups: ModelTypes['groups'][];
        /** An aggregated array relationship */
        groups_aggregate: ModelTypes['groups_aggregate'];
        value: string;
    };
    /** aggregated selection of "group_recurrencies" */
    ['group_recurrencies_aggregate']: {
        aggregate?: ModelTypes['group_recurrencies_aggregate_fields'];
        nodes: ModelTypes['group_recurrencies'][];
    };
    /** aggregate fields of "group_recurrencies" */
    ['group_recurrencies_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['group_recurrencies_max_fields'];
        min?: ModelTypes['group_recurrencies_min_fields'];
    };
    /** order by aggregate values of table "group_recurrencies" */
    ['group_recurrencies_aggregate_order_by']: GraphQLTypes['group_recurrencies_aggregate_order_by'];
    /** input type for inserting array relation for remote table "group_recurrencies" */
    ['group_recurrencies_arr_rel_insert_input']: GraphQLTypes['group_recurrencies_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "group_recurrencies". All fields are combined with a logical 'AND'. */
    ['group_recurrencies_bool_exp']: GraphQLTypes['group_recurrencies_bool_exp'];
    /** unique or primary key constraints on table "group_recurrencies" */
    ['group_recurrencies_constraint']: GraphQLTypes['group_recurrencies_constraint'];
    ['group_recurrencies_enum']: GraphQLTypes['group_recurrencies_enum'];
    /** expression to compare columns of type group_recurrencies_enum. All fields are combined with logical 'AND'. */
    ['group_recurrencies_enum_comparison_exp']: GraphQLTypes['group_recurrencies_enum_comparison_exp'];
    /** input type for inserting data into table "group_recurrencies" */
    ['group_recurrencies_insert_input']: GraphQLTypes['group_recurrencies_insert_input'];
    /** aggregate max on columns */
    ['group_recurrencies_max_fields']: {
        description?: string;
        value?: string;
    };
    /** order by max() on columns of table "group_recurrencies" */
    ['group_recurrencies_max_order_by']: GraphQLTypes['group_recurrencies_max_order_by'];
    /** aggregate min on columns */
    ['group_recurrencies_min_fields']: {
        description?: string;
        value?: string;
    };
    /** order by min() on columns of table "group_recurrencies" */
    ['group_recurrencies_min_order_by']: GraphQLTypes['group_recurrencies_min_order_by'];
    /** response of any mutation on the table "group_recurrencies" */
    ['group_recurrencies_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['group_recurrencies'][];
    };
    /** input type for inserting object relation for remote table "group_recurrencies" */
    ['group_recurrencies_obj_rel_insert_input']: GraphQLTypes['group_recurrencies_obj_rel_insert_input'];
    /** on conflict condition type for table "group_recurrencies" */
    ['group_recurrencies_on_conflict']: GraphQLTypes['group_recurrencies_on_conflict'];
    /** ordering options when selecting data from "group_recurrencies" */
    ['group_recurrencies_order_by']: GraphQLTypes['group_recurrencies_order_by'];
    /** primary key columns input for table: "group_recurrencies" */
    ['group_recurrencies_pk_columns_input']: GraphQLTypes['group_recurrencies_pk_columns_input'];
    /** select columns of table "group_recurrencies" */
    ['group_recurrencies_select_column']: GraphQLTypes['group_recurrencies_select_column'];
    /** input type for updating data in table "group_recurrencies" */
    ['group_recurrencies_set_input']: GraphQLTypes['group_recurrencies_set_input'];
    /** update columns of table "group_recurrencies" */
    ['group_recurrencies_update_column']: GraphQLTypes['group_recurrencies_update_column'];
    /** columns and relationships of "groups" */
    ['groups']: {
        /** An object relationship */
        contribution_type: ModelTypes['contribution_types'];
        created_at: ModelTypes['timestamp'];
        /** An object relationship */
        creator: ModelTypes['users'];
        creator_id: ModelTypes['uuid'];
        group_balance: ModelTypes['numeric'];
        group_code: string;
        group_contribution_amount?: ModelTypes['numeric'];
        group_contribution_type: ModelTypes['contribution_types_enum'];
        group_name: string;
        group_recurrency_amount?: ModelTypes['numeric'];
        group_recurrency_day?: number;
        group_recurrency_type: ModelTypes['group_recurrencies_enum'];
        id: ModelTypes['uuid'];
        /** An array relationship */
        members: ModelTypes['members'][];
        /** An aggregated array relationship */
        members_aggregate: ModelTypes['members_aggregate'];
        /** An object relationship */
        payment_frequency: ModelTypes['group_recurrencies'];
        /** An array relationship */
        payments: ModelTypes['payments'][];
        /** An aggregated array relationship */
        payments_aggregate: ModelTypes['payments_aggregate'];
        /** An array relationship */
        periods: ModelTypes['periods'][];
        /** An aggregated array relationship */
        periods_aggregate: ModelTypes['periods_aggregate'];
        updated_at: ModelTypes['timestamptz'];
    };
    /** aggregated selection of "groups" */
    ['groups_aggregate']: {
        aggregate?: ModelTypes['groups_aggregate_fields'];
        nodes: ModelTypes['groups'][];
    };
    /** aggregate fields of "groups" */
    ['groups_aggregate_fields']: {
        avg?: ModelTypes['groups_avg_fields'];
        count?: number;
        max?: ModelTypes['groups_max_fields'];
        min?: ModelTypes['groups_min_fields'];
        stddev?: ModelTypes['groups_stddev_fields'];
        stddev_pop?: ModelTypes['groups_stddev_pop_fields'];
        stddev_samp?: ModelTypes['groups_stddev_samp_fields'];
        sum?: ModelTypes['groups_sum_fields'];
        var_pop?: ModelTypes['groups_var_pop_fields'];
        var_samp?: ModelTypes['groups_var_samp_fields'];
        variance?: ModelTypes['groups_variance_fields'];
    };
    /** order by aggregate values of table "groups" */
    ['groups_aggregate_order_by']: GraphQLTypes['groups_aggregate_order_by'];
    /** input type for inserting array relation for remote table "groups" */
    ['groups_arr_rel_insert_input']: GraphQLTypes['groups_arr_rel_insert_input'];
    /** aggregate avg on columns */
    ['groups_avg_fields']: {
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by avg() on columns of table "groups" */
    ['groups_avg_order_by']: GraphQLTypes['groups_avg_order_by'];
    /** Boolean expression to filter rows from the table "groups". All fields are combined with a logical 'AND'. */
    ['groups_bool_exp']: GraphQLTypes['groups_bool_exp'];
    /** unique or primary key constraints on table "groups" */
    ['groups_constraint']: GraphQLTypes['groups_constraint'];
    /** input type for incrementing integer column in table "groups" */
    ['groups_inc_input']: GraphQLTypes['groups_inc_input'];
    /** input type for inserting data into table "groups" */
    ['groups_insert_input']: GraphQLTypes['groups_insert_input'];
    /** aggregate max on columns */
    ['groups_max_fields']: {
        created_at?: ModelTypes['timestamp'];
        creator_id?: ModelTypes['uuid'];
        group_balance?: ModelTypes['numeric'];
        group_code?: string;
        group_contribution_amount?: ModelTypes['numeric'];
        group_name?: string;
        group_recurrency_amount?: ModelTypes['numeric'];
        group_recurrency_day?: number;
        id?: ModelTypes['uuid'];
        updated_at?: ModelTypes['timestamptz'];
    };
    /** order by max() on columns of table "groups" */
    ['groups_max_order_by']: GraphQLTypes['groups_max_order_by'];
    /** aggregate min on columns */
    ['groups_min_fields']: {
        created_at?: ModelTypes['timestamp'];
        creator_id?: ModelTypes['uuid'];
        group_balance?: ModelTypes['numeric'];
        group_code?: string;
        group_contribution_amount?: ModelTypes['numeric'];
        group_name?: string;
        group_recurrency_amount?: ModelTypes['numeric'];
        group_recurrency_day?: number;
        id?: ModelTypes['uuid'];
        updated_at?: ModelTypes['timestamptz'];
    };
    /** order by min() on columns of table "groups" */
    ['groups_min_order_by']: GraphQLTypes['groups_min_order_by'];
    /** response of any mutation on the table "groups" */
    ['groups_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['groups'][];
    };
    /** input type for inserting object relation for remote table "groups" */
    ['groups_obj_rel_insert_input']: GraphQLTypes['groups_obj_rel_insert_input'];
    /** on conflict condition type for table "groups" */
    ['groups_on_conflict']: GraphQLTypes['groups_on_conflict'];
    /** ordering options when selecting data from "groups" */
    ['groups_order_by']: GraphQLTypes['groups_order_by'];
    /** primary key columns input for table: "groups" */
    ['groups_pk_columns_input']: GraphQLTypes['groups_pk_columns_input'];
    /** select columns of table "groups" */
    ['groups_select_column']: GraphQLTypes['groups_select_column'];
    /** input type for updating data in table "groups" */
    ['groups_set_input']: GraphQLTypes['groups_set_input'];
    /** aggregate stddev on columns */
    ['groups_stddev_fields']: {
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by stddev() on columns of table "groups" */
    ['groups_stddev_order_by']: GraphQLTypes['groups_stddev_order_by'];
    /** aggregate stddev_pop on columns */
    ['groups_stddev_pop_fields']: {
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by stddev_pop() on columns of table "groups" */
    ['groups_stddev_pop_order_by']: GraphQLTypes['groups_stddev_pop_order_by'];
    /** aggregate stddev_samp on columns */
    ['groups_stddev_samp_fields']: {
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by stddev_samp() on columns of table "groups" */
    ['groups_stddev_samp_order_by']: GraphQLTypes['groups_stddev_samp_order_by'];
    /** aggregate sum on columns */
    ['groups_sum_fields']: {
        group_balance?: ModelTypes['numeric'];
        group_contribution_amount?: ModelTypes['numeric'];
        group_recurrency_amount?: ModelTypes['numeric'];
        group_recurrency_day?: number;
    };
    /** order by sum() on columns of table "groups" */
    ['groups_sum_order_by']: GraphQLTypes['groups_sum_order_by'];
    /** update columns of table "groups" */
    ['groups_update_column']: GraphQLTypes['groups_update_column'];
    /** aggregate var_pop on columns */
    ['groups_var_pop_fields']: {
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by var_pop() on columns of table "groups" */
    ['groups_var_pop_order_by']: GraphQLTypes['groups_var_pop_order_by'];
    /** aggregate var_samp on columns */
    ['groups_var_samp_fields']: {
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by var_samp() on columns of table "groups" */
    ['groups_var_samp_order_by']: GraphQLTypes['groups_var_samp_order_by'];
    /** aggregate variance on columns */
    ['groups_variance_fields']: {
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by variance() on columns of table "groups" */
    ['groups_variance_order_by']: GraphQLTypes['groups_variance_order_by'];
    ['json']: any;
    /** expression to compare columns of type json. All fields are combined with logical 'AND'. */
    ['json_comparison_exp']: GraphQLTypes['json_comparison_exp'];
    ['jsonb']: any;
    /** expression to compare columns of type jsonb. All fields are combined with logical 'AND'. */
    ['jsonb_comparison_exp']: GraphQLTypes['jsonb_comparison_exp'];
    /** columns and relationships of "members" */
    ['members']: {
        created_at: ModelTypes['timestamptz'];
        /** An object relationship */
        group: ModelTypes['groups'];
        group_id: ModelTypes['uuid'];
        id: ModelTypes['uuid'];
        /** An array relationship */
        payments: ModelTypes['payments'][];
        /** An aggregated array relationship */
        payments_aggregate: ModelTypes['payments_aggregate'];
        updated_at: ModelTypes['timestamptz'];
        /** An object relationship */
        user: ModelTypes['users'];
        user_id: ModelTypes['uuid'];
    };
    /** aggregated selection of "members" */
    ['members_aggregate']: {
        aggregate?: ModelTypes['members_aggregate_fields'];
        nodes: ModelTypes['members'][];
    };
    /** aggregate fields of "members" */
    ['members_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['members_max_fields'];
        min?: ModelTypes['members_min_fields'];
    };
    /** order by aggregate values of table "members" */
    ['members_aggregate_order_by']: GraphQLTypes['members_aggregate_order_by'];
    /** input type for inserting array relation for remote table "members" */
    ['members_arr_rel_insert_input']: GraphQLTypes['members_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "members". All fields are combined with a logical 'AND'. */
    ['members_bool_exp']: GraphQLTypes['members_bool_exp'];
    /** unique or primary key constraints on table "members" */
    ['members_constraint']: GraphQLTypes['members_constraint'];
    /** input type for inserting data into table "members" */
    ['members_insert_input']: GraphQLTypes['members_insert_input'];
    /** aggregate max on columns */
    ['members_max_fields']: {
        created_at?: ModelTypes['timestamptz'];
        group_id?: ModelTypes['uuid'];
        id?: ModelTypes['uuid'];
        updated_at?: ModelTypes['timestamptz'];
        user_id?: ModelTypes['uuid'];
    };
    /** order by max() on columns of table "members" */
    ['members_max_order_by']: GraphQLTypes['members_max_order_by'];
    /** aggregate min on columns */
    ['members_min_fields']: {
        created_at?: ModelTypes['timestamptz'];
        group_id?: ModelTypes['uuid'];
        id?: ModelTypes['uuid'];
        updated_at?: ModelTypes['timestamptz'];
        user_id?: ModelTypes['uuid'];
    };
    /** order by min() on columns of table "members" */
    ['members_min_order_by']: GraphQLTypes['members_min_order_by'];
    /** response of any mutation on the table "members" */
    ['members_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['members'][];
    };
    /** input type for inserting object relation for remote table "members" */
    ['members_obj_rel_insert_input']: GraphQLTypes['members_obj_rel_insert_input'];
    /** on conflict condition type for table "members" */
    ['members_on_conflict']: GraphQLTypes['members_on_conflict'];
    /** ordering options when selecting data from "members" */
    ['members_order_by']: GraphQLTypes['members_order_by'];
    /** primary key columns input for table: "members" */
    ['members_pk_columns_input']: GraphQLTypes['members_pk_columns_input'];
    /** select columns of table "members" */
    ['members_select_column']: GraphQLTypes['members_select_column'];
    /** input type for updating data in table "members" */
    ['members_set_input']: GraphQLTypes['members_set_input'];
    /** update columns of table "members" */
    ['members_update_column']: GraphQLTypes['members_update_column'];
    /** mutation root */
    ['mutation_root']: {
        /** perform the action: "action_create_group" */
        action_create_group?: ModelTypes['ActionCreateGroupOutput'];
        /** perform the action: "action_request_payment" */
        action_request_payment?: ModelTypes['ActionRequestPaymentOutput'];
        /** perform the action: "action_withdraw_payment" */
        action_withdraw_payment?: ModelTypes['ActionWithdrawPaymentOutput'];
        /** delete data from the table: "auth.account_providers" */
        delete_auth_account_providers?: ModelTypes['auth_account_providers_mutation_response'];
        /** delete single row from the table: "auth.account_providers" */
        delete_auth_account_providers_by_pk?: ModelTypes['auth_account_providers'];
        /** delete data from the table: "auth.account_roles" */
        delete_auth_account_roles?: ModelTypes['auth_account_roles_mutation_response'];
        /** delete single row from the table: "auth.account_roles" */
        delete_auth_account_roles_by_pk?: ModelTypes['auth_account_roles'];
        /** delete data from the table: "auth.accounts" */
        delete_auth_accounts?: ModelTypes['auth_accounts_mutation_response'];
        /** delete single row from the table: "auth.accounts" */
        delete_auth_accounts_by_pk?: ModelTypes['auth_accounts'];
        /** delete data from the table: "auth.providers" */
        delete_auth_providers?: ModelTypes['auth_providers_mutation_response'];
        /** delete single row from the table: "auth.providers" */
        delete_auth_providers_by_pk?: ModelTypes['auth_providers'];
        /** delete data from the table: "auth.refresh_tokens" */
        delete_auth_refresh_tokens?: ModelTypes['auth_refresh_tokens_mutation_response'];
        /** delete single row from the table: "auth.refresh_tokens" */
        delete_auth_refresh_tokens_by_pk?: ModelTypes['auth_refresh_tokens'];
        /** delete data from the table: "auth.roles" */
        delete_auth_roles?: ModelTypes['auth_roles_mutation_response'];
        /** delete single row from the table: "auth.roles" */
        delete_auth_roles_by_pk?: ModelTypes['auth_roles'];
        /** delete data from the table: "contribution_types" */
        delete_contribution_types?: ModelTypes['contribution_types_mutation_response'];
        /** delete single row from the table: "contribution_types" */
        delete_contribution_types_by_pk?: ModelTypes['contribution_types'];
        /** delete data from the table: "group_cycles" */
        delete_group_cycles?: ModelTypes['group_cycles_mutation_response'];
        /** delete single row from the table: "group_cycles" */
        delete_group_cycles_by_pk?: ModelTypes['group_cycles'];
        /** delete data from the table: "group_recurrencies" */
        delete_group_recurrencies?: ModelTypes['group_recurrencies_mutation_response'];
        /** delete single row from the table: "group_recurrencies" */
        delete_group_recurrencies_by_pk?: ModelTypes['group_recurrencies'];
        /** delete data from the table: "groups" */
        delete_groups?: ModelTypes['groups_mutation_response'];
        /** delete single row from the table: "groups" */
        delete_groups_by_pk?: ModelTypes['groups'];
        /** delete data from the table: "members" */
        delete_members?: ModelTypes['members_mutation_response'];
        /** delete single row from the table: "members" */
        delete_members_by_pk?: ModelTypes['members'];
        /** delete data from the table: "payment_statuses" */
        delete_payment_statuses?: ModelTypes['payment_statuses_mutation_response'];
        /** delete single row from the table: "payment_statuses" */
        delete_payment_statuses_by_pk?: ModelTypes['payment_statuses'];
        /** delete data from the table: "payment_types" */
        delete_payment_types?: ModelTypes['payment_types_mutation_response'];
        /** delete single row from the table: "payment_types" */
        delete_payment_types_by_pk?: ModelTypes['payment_types'];
        /** delete data from the table: "payments" */
        delete_payments?: ModelTypes['payments_mutation_response'];
        /** delete single row from the table: "payments" */
        delete_payments_by_pk?: ModelTypes['payments'];
        /** delete data from the table: "periods" */
        delete_periods?: ModelTypes['periods_mutation_response'];
        /** delete single row from the table: "periods" */
        delete_periods_by_pk?: ModelTypes['periods'];
        /** delete data from the table: "users" */
        delete_users?: ModelTypes['users_mutation_response'];
        /** delete single row from the table: "users" */
        delete_users_by_pk?: ModelTypes['users'];
        /** insert data into the table: "auth.account_providers" */
        insert_auth_account_providers?: ModelTypes['auth_account_providers_mutation_response'];
        /** insert a single row into the table: "auth.account_providers" */
        insert_auth_account_providers_one?: ModelTypes['auth_account_providers'];
        /** insert data into the table: "auth.account_roles" */
        insert_auth_account_roles?: ModelTypes['auth_account_roles_mutation_response'];
        /** insert a single row into the table: "auth.account_roles" */
        insert_auth_account_roles_one?: ModelTypes['auth_account_roles'];
        /** insert data into the table: "auth.accounts" */
        insert_auth_accounts?: ModelTypes['auth_accounts_mutation_response'];
        /** insert a single row into the table: "auth.accounts" */
        insert_auth_accounts_one?: ModelTypes['auth_accounts'];
        /** insert data into the table: "auth.providers" */
        insert_auth_providers?: ModelTypes['auth_providers_mutation_response'];
        /** insert a single row into the table: "auth.providers" */
        insert_auth_providers_one?: ModelTypes['auth_providers'];
        /** insert data into the table: "auth.refresh_tokens" */
        insert_auth_refresh_tokens?: ModelTypes['auth_refresh_tokens_mutation_response'];
        /** insert a single row into the table: "auth.refresh_tokens" */
        insert_auth_refresh_tokens_one?: ModelTypes['auth_refresh_tokens'];
        /** insert data into the table: "auth.roles" */
        insert_auth_roles?: ModelTypes['auth_roles_mutation_response'];
        /** insert a single row into the table: "auth.roles" */
        insert_auth_roles_one?: ModelTypes['auth_roles'];
        /** insert data into the table: "contribution_types" */
        insert_contribution_types?: ModelTypes['contribution_types_mutation_response'];
        /** insert a single row into the table: "contribution_types" */
        insert_contribution_types_one?: ModelTypes['contribution_types'];
        /** insert data into the table: "group_cycles" */
        insert_group_cycles?: ModelTypes['group_cycles_mutation_response'];
        /** insert a single row into the table: "group_cycles" */
        insert_group_cycles_one?: ModelTypes['group_cycles'];
        /** insert data into the table: "group_recurrencies" */
        insert_group_recurrencies?: ModelTypes['group_recurrencies_mutation_response'];
        /** insert a single row into the table: "group_recurrencies" */
        insert_group_recurrencies_one?: ModelTypes['group_recurrencies'];
        /** insert data into the table: "groups" */
        insert_groups?: ModelTypes['groups_mutation_response'];
        /** insert a single row into the table: "groups" */
        insert_groups_one?: ModelTypes['groups'];
        /** insert data into the table: "members" */
        insert_members?: ModelTypes['members_mutation_response'];
        /** insert a single row into the table: "members" */
        insert_members_one?: ModelTypes['members'];
        /** insert data into the table: "payment_statuses" */
        insert_payment_statuses?: ModelTypes['payment_statuses_mutation_response'];
        /** insert a single row into the table: "payment_statuses" */
        insert_payment_statuses_one?: ModelTypes['payment_statuses'];
        /** insert data into the table: "payment_types" */
        insert_payment_types?: ModelTypes['payment_types_mutation_response'];
        /** insert a single row into the table: "payment_types" */
        insert_payment_types_one?: ModelTypes['payment_types'];
        /** insert data into the table: "payments" */
        insert_payments?: ModelTypes['payments_mutation_response'];
        /** insert a single row into the table: "payments" */
        insert_payments_one?: ModelTypes['payments'];
        /** insert data into the table: "periods" */
        insert_periods?: ModelTypes['periods_mutation_response'];
        /** insert a single row into the table: "periods" */
        insert_periods_one?: ModelTypes['periods'];
        /** insert data into the table: "users" */
        insert_users?: ModelTypes['users_mutation_response'];
        /** insert a single row into the table: "users" */
        insert_users_one?: ModelTypes['users'];
        /** update data of the table: "auth.account_providers" */
        update_auth_account_providers?: ModelTypes['auth_account_providers_mutation_response'];
        /** update single row of the table: "auth.account_providers" */
        update_auth_account_providers_by_pk?: ModelTypes['auth_account_providers'];
        /** update data of the table: "auth.account_roles" */
        update_auth_account_roles?: ModelTypes['auth_account_roles_mutation_response'];
        /** update single row of the table: "auth.account_roles" */
        update_auth_account_roles_by_pk?: ModelTypes['auth_account_roles'];
        /** update data of the table: "auth.accounts" */
        update_auth_accounts?: ModelTypes['auth_accounts_mutation_response'];
        /** update single row of the table: "auth.accounts" */
        update_auth_accounts_by_pk?: ModelTypes['auth_accounts'];
        /** update data of the table: "auth.providers" */
        update_auth_providers?: ModelTypes['auth_providers_mutation_response'];
        /** update single row of the table: "auth.providers" */
        update_auth_providers_by_pk?: ModelTypes['auth_providers'];
        /** update data of the table: "auth.refresh_tokens" */
        update_auth_refresh_tokens?: ModelTypes['auth_refresh_tokens_mutation_response'];
        /** update single row of the table: "auth.refresh_tokens" */
        update_auth_refresh_tokens_by_pk?: ModelTypes['auth_refresh_tokens'];
        /** update data of the table: "auth.roles" */
        update_auth_roles?: ModelTypes['auth_roles_mutation_response'];
        /** update single row of the table: "auth.roles" */
        update_auth_roles_by_pk?: ModelTypes['auth_roles'];
        /** update data of the table: "contribution_types" */
        update_contribution_types?: ModelTypes['contribution_types_mutation_response'];
        /** update single row of the table: "contribution_types" */
        update_contribution_types_by_pk?: ModelTypes['contribution_types'];
        /** update data of the table: "group_cycles" */
        update_group_cycles?: ModelTypes['group_cycles_mutation_response'];
        /** update single row of the table: "group_cycles" */
        update_group_cycles_by_pk?: ModelTypes['group_cycles'];
        /** update data of the table: "group_recurrencies" */
        update_group_recurrencies?: ModelTypes['group_recurrencies_mutation_response'];
        /** update single row of the table: "group_recurrencies" */
        update_group_recurrencies_by_pk?: ModelTypes['group_recurrencies'];
        /** update data of the table: "groups" */
        update_groups?: ModelTypes['groups_mutation_response'];
        /** update single row of the table: "groups" */
        update_groups_by_pk?: ModelTypes['groups'];
        /** update data of the table: "members" */
        update_members?: ModelTypes['members_mutation_response'];
        /** update single row of the table: "members" */
        update_members_by_pk?: ModelTypes['members'];
        /** update data of the table: "payment_statuses" */
        update_payment_statuses?: ModelTypes['payment_statuses_mutation_response'];
        /** update single row of the table: "payment_statuses" */
        update_payment_statuses_by_pk?: ModelTypes['payment_statuses'];
        /** update data of the table: "payment_types" */
        update_payment_types?: ModelTypes['payment_types_mutation_response'];
        /** update single row of the table: "payment_types" */
        update_payment_types_by_pk?: ModelTypes['payment_types'];
        /** update data of the table: "payments" */
        update_payments?: ModelTypes['payments_mutation_response'];
        /** update single row of the table: "payments" */
        update_payments_by_pk?: ModelTypes['payments'];
        /** update data of the table: "periods" */
        update_periods?: ModelTypes['periods_mutation_response'];
        /** update single row of the table: "periods" */
        update_periods_by_pk?: ModelTypes['periods'];
        /** update data of the table: "users" */
        update_users?: ModelTypes['users_mutation_response'];
        /** update single row of the table: "users" */
        update_users_by_pk?: ModelTypes['users'];
    };
    ['numeric']: any;
    /** expression to compare columns of type numeric. All fields are combined with logical 'AND'. */
    ['numeric_comparison_exp']: GraphQLTypes['numeric_comparison_exp'];
    /** column ordering options */
    ['order_by']: GraphQLTypes['order_by'];
    /** columns and relationships of "payment_statuses" */
    ['payment_statuses']: {
        description: string;
        /** An array relationship */
        payments: ModelTypes['payments'][];
        /** An aggregated array relationship */
        payments_aggregate: ModelTypes['payments_aggregate'];
        value: string;
    };
    /** aggregated selection of "payment_statuses" */
    ['payment_statuses_aggregate']: {
        aggregate?: ModelTypes['payment_statuses_aggregate_fields'];
        nodes: ModelTypes['payment_statuses'][];
    };
    /** aggregate fields of "payment_statuses" */
    ['payment_statuses_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['payment_statuses_max_fields'];
        min?: ModelTypes['payment_statuses_min_fields'];
    };
    /** order by aggregate values of table "payment_statuses" */
    ['payment_statuses_aggregate_order_by']: GraphQLTypes['payment_statuses_aggregate_order_by'];
    /** input type for inserting array relation for remote table "payment_statuses" */
    ['payment_statuses_arr_rel_insert_input']: GraphQLTypes['payment_statuses_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "payment_statuses". All fields are combined with a logical 'AND'. */
    ['payment_statuses_bool_exp']: GraphQLTypes['payment_statuses_bool_exp'];
    /** unique or primary key constraints on table "payment_statuses" */
    ['payment_statuses_constraint']: GraphQLTypes['payment_statuses_constraint'];
    ['payment_statuses_enum']: GraphQLTypes['payment_statuses_enum'];
    /** expression to compare columns of type payment_statuses_enum. All fields are combined with logical 'AND'. */
    ['payment_statuses_enum_comparison_exp']: GraphQLTypes['payment_statuses_enum_comparison_exp'];
    /** input type for inserting data into table "payment_statuses" */
    ['payment_statuses_insert_input']: GraphQLTypes['payment_statuses_insert_input'];
    /** aggregate max on columns */
    ['payment_statuses_max_fields']: {
        description?: string;
        value?: string;
    };
    /** order by max() on columns of table "payment_statuses" */
    ['payment_statuses_max_order_by']: GraphQLTypes['payment_statuses_max_order_by'];
    /** aggregate min on columns */
    ['payment_statuses_min_fields']: {
        description?: string;
        value?: string;
    };
    /** order by min() on columns of table "payment_statuses" */
    ['payment_statuses_min_order_by']: GraphQLTypes['payment_statuses_min_order_by'];
    /** response of any mutation on the table "payment_statuses" */
    ['payment_statuses_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['payment_statuses'][];
    };
    /** input type for inserting object relation for remote table "payment_statuses" */
    ['payment_statuses_obj_rel_insert_input']: GraphQLTypes['payment_statuses_obj_rel_insert_input'];
    /** on conflict condition type for table "payment_statuses" */
    ['payment_statuses_on_conflict']: GraphQLTypes['payment_statuses_on_conflict'];
    /** ordering options when selecting data from "payment_statuses" */
    ['payment_statuses_order_by']: GraphQLTypes['payment_statuses_order_by'];
    /** primary key columns input for table: "payment_statuses" */
    ['payment_statuses_pk_columns_input']: GraphQLTypes['payment_statuses_pk_columns_input'];
    /** select columns of table "payment_statuses" */
    ['payment_statuses_select_column']: GraphQLTypes['payment_statuses_select_column'];
    /** input type for updating data in table "payment_statuses" */
    ['payment_statuses_set_input']: GraphQLTypes['payment_statuses_set_input'];
    /** update columns of table "payment_statuses" */
    ['payment_statuses_update_column']: GraphQLTypes['payment_statuses_update_column'];
    /** columns and relationships of "payment_types" */
    ['payment_types']: {
        description: string;
        value: string;
    };
    /** aggregated selection of "payment_types" */
    ['payment_types_aggregate']: {
        aggregate?: ModelTypes['payment_types_aggregate_fields'];
        nodes: ModelTypes['payment_types'][];
    };
    /** aggregate fields of "payment_types" */
    ['payment_types_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['payment_types_max_fields'];
        min?: ModelTypes['payment_types_min_fields'];
    };
    /** order by aggregate values of table "payment_types" */
    ['payment_types_aggregate_order_by']: GraphQLTypes['payment_types_aggregate_order_by'];
    /** input type for inserting array relation for remote table "payment_types" */
    ['payment_types_arr_rel_insert_input']: GraphQLTypes['payment_types_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "payment_types". All fields are combined with a logical 'AND'. */
    ['payment_types_bool_exp']: GraphQLTypes['payment_types_bool_exp'];
    /** unique or primary key constraints on table "payment_types" */
    ['payment_types_constraint']: GraphQLTypes['payment_types_constraint'];
    ['payment_types_enum']: GraphQLTypes['payment_types_enum'];
    /** expression to compare columns of type payment_types_enum. All fields are combined with logical 'AND'. */
    ['payment_types_enum_comparison_exp']: GraphQLTypes['payment_types_enum_comparison_exp'];
    /** input type for inserting data into table "payment_types" */
    ['payment_types_insert_input']: GraphQLTypes['payment_types_insert_input'];
    /** aggregate max on columns */
    ['payment_types_max_fields']: {
        description?: string;
        value?: string;
    };
    /** order by max() on columns of table "payment_types" */
    ['payment_types_max_order_by']: GraphQLTypes['payment_types_max_order_by'];
    /** aggregate min on columns */
    ['payment_types_min_fields']: {
        description?: string;
        value?: string;
    };
    /** order by min() on columns of table "payment_types" */
    ['payment_types_min_order_by']: GraphQLTypes['payment_types_min_order_by'];
    /** response of any mutation on the table "payment_types" */
    ['payment_types_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['payment_types'][];
    };
    /** input type for inserting object relation for remote table "payment_types" */
    ['payment_types_obj_rel_insert_input']: GraphQLTypes['payment_types_obj_rel_insert_input'];
    /** on conflict condition type for table "payment_types" */
    ['payment_types_on_conflict']: GraphQLTypes['payment_types_on_conflict'];
    /** ordering options when selecting data from "payment_types" */
    ['payment_types_order_by']: GraphQLTypes['payment_types_order_by'];
    /** primary key columns input for table: "payment_types" */
    ['payment_types_pk_columns_input']: GraphQLTypes['payment_types_pk_columns_input'];
    /** select columns of table "payment_types" */
    ['payment_types_select_column']: GraphQLTypes['payment_types_select_column'];
    /** input type for updating data in table "payment_types" */
    ['payment_types_set_input']: GraphQLTypes['payment_types_set_input'];
    /** update columns of table "payment_types" */
    ['payment_types_update_column']: GraphQLTypes['payment_types_update_column'];
    /** columns and relationships of "payments" */
    ['payments']: {
        created_at: ModelTypes['timestamptz'];
        /** An object relationship */
        group: ModelTypes['groups'];
        group_id: ModelTypes['uuid'];
        id: ModelTypes['uuid'];
        /** An object relationship */
        member: ModelTypes['members'];
        member_id: ModelTypes['uuid'];
        /** An object relationship */
        paymentStatusByPaymentStatus: ModelTypes['payment_statuses'];
        payment_amount: ModelTypes['numeric'];
        payment_status: ModelTypes['payment_statuses_enum'];
        payment_type: ModelTypes['payment_types_enum'];
        /** An object relationship */
        period?: ModelTypes['periods'];
        period_id?: ModelTypes['uuid'];
        updated_at: ModelTypes['timestamptz'];
    };
    /** aggregated selection of "payments" */
    ['payments_aggregate']: {
        aggregate?: ModelTypes['payments_aggregate_fields'];
        nodes: ModelTypes['payments'][];
    };
    /** aggregate fields of "payments" */
    ['payments_aggregate_fields']: {
        avg?: ModelTypes['payments_avg_fields'];
        count?: number;
        max?: ModelTypes['payments_max_fields'];
        min?: ModelTypes['payments_min_fields'];
        stddev?: ModelTypes['payments_stddev_fields'];
        stddev_pop?: ModelTypes['payments_stddev_pop_fields'];
        stddev_samp?: ModelTypes['payments_stddev_samp_fields'];
        sum?: ModelTypes['payments_sum_fields'];
        var_pop?: ModelTypes['payments_var_pop_fields'];
        var_samp?: ModelTypes['payments_var_samp_fields'];
        variance?: ModelTypes['payments_variance_fields'];
    };
    /** order by aggregate values of table "payments" */
    ['payments_aggregate_order_by']: GraphQLTypes['payments_aggregate_order_by'];
    /** input type for inserting array relation for remote table "payments" */
    ['payments_arr_rel_insert_input']: GraphQLTypes['payments_arr_rel_insert_input'];
    /** aggregate avg on columns */
    ['payments_avg_fields']: {
        payment_amount?: number;
    };
    /** order by avg() on columns of table "payments" */
    ['payments_avg_order_by']: GraphQLTypes['payments_avg_order_by'];
    /** Boolean expression to filter rows from the table "payments". All fields are combined with a logical 'AND'. */
    ['payments_bool_exp']: GraphQLTypes['payments_bool_exp'];
    /** unique or primary key constraints on table "payments" */
    ['payments_constraint']: GraphQLTypes['payments_constraint'];
    /** input type for incrementing integer column in table "payments" */
    ['payments_inc_input']: GraphQLTypes['payments_inc_input'];
    /** input type for inserting data into table "payments" */
    ['payments_insert_input']: GraphQLTypes['payments_insert_input'];
    /** aggregate max on columns */
    ['payments_max_fields']: {
        created_at?: ModelTypes['timestamptz'];
        group_id?: ModelTypes['uuid'];
        id?: ModelTypes['uuid'];
        member_id?: ModelTypes['uuid'];
        payment_amount?: ModelTypes['numeric'];
        period_id?: ModelTypes['uuid'];
        updated_at?: ModelTypes['timestamptz'];
    };
    /** order by max() on columns of table "payments" */
    ['payments_max_order_by']: GraphQLTypes['payments_max_order_by'];
    /** aggregate min on columns */
    ['payments_min_fields']: {
        created_at?: ModelTypes['timestamptz'];
        group_id?: ModelTypes['uuid'];
        id?: ModelTypes['uuid'];
        member_id?: ModelTypes['uuid'];
        payment_amount?: ModelTypes['numeric'];
        period_id?: ModelTypes['uuid'];
        updated_at?: ModelTypes['timestamptz'];
    };
    /** order by min() on columns of table "payments" */
    ['payments_min_order_by']: GraphQLTypes['payments_min_order_by'];
    /** response of any mutation on the table "payments" */
    ['payments_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['payments'][];
    };
    /** input type for inserting object relation for remote table "payments" */
    ['payments_obj_rel_insert_input']: GraphQLTypes['payments_obj_rel_insert_input'];
    /** on conflict condition type for table "payments" */
    ['payments_on_conflict']: GraphQLTypes['payments_on_conflict'];
    /** ordering options when selecting data from "payments" */
    ['payments_order_by']: GraphQLTypes['payments_order_by'];
    /** primary key columns input for table: "payments" */
    ['payments_pk_columns_input']: GraphQLTypes['payments_pk_columns_input'];
    /** select columns of table "payments" */
    ['payments_select_column']: GraphQLTypes['payments_select_column'];
    /** input type for updating data in table "payments" */
    ['payments_set_input']: GraphQLTypes['payments_set_input'];
    /** aggregate stddev on columns */
    ['payments_stddev_fields']: {
        payment_amount?: number;
    };
    /** order by stddev() on columns of table "payments" */
    ['payments_stddev_order_by']: GraphQLTypes['payments_stddev_order_by'];
    /** aggregate stddev_pop on columns */
    ['payments_stddev_pop_fields']: {
        payment_amount?: number;
    };
    /** order by stddev_pop() on columns of table "payments" */
    ['payments_stddev_pop_order_by']: GraphQLTypes['payments_stddev_pop_order_by'];
    /** aggregate stddev_samp on columns */
    ['payments_stddev_samp_fields']: {
        payment_amount?: number;
    };
    /** order by stddev_samp() on columns of table "payments" */
    ['payments_stddev_samp_order_by']: GraphQLTypes['payments_stddev_samp_order_by'];
    /** aggregate sum on columns */
    ['payments_sum_fields']: {
        payment_amount?: ModelTypes['numeric'];
    };
    /** order by sum() on columns of table "payments" */
    ['payments_sum_order_by']: GraphQLTypes['payments_sum_order_by'];
    /** update columns of table "payments" */
    ['payments_update_column']: GraphQLTypes['payments_update_column'];
    /** aggregate var_pop on columns */
    ['payments_var_pop_fields']: {
        payment_amount?: number;
    };
    /** order by var_pop() on columns of table "payments" */
    ['payments_var_pop_order_by']: GraphQLTypes['payments_var_pop_order_by'];
    /** aggregate var_samp on columns */
    ['payments_var_samp_fields']: {
        payment_amount?: number;
    };
    /** order by var_samp() on columns of table "payments" */
    ['payments_var_samp_order_by']: GraphQLTypes['payments_var_samp_order_by'];
    /** aggregate variance on columns */
    ['payments_variance_fields']: {
        payment_amount?: number;
    };
    /** order by variance() on columns of table "payments" */
    ['payments_variance_order_by']: GraphQLTypes['payments_variance_order_by'];
    /** columns and relationships of "periods" */
    ['periods']: {
        created_at: ModelTypes['timestamptz'];
        /** An object relationship */
        group: ModelTypes['groups'];
        group_id: ModelTypes['uuid'];
        id: ModelTypes['uuid'];
        /** An array relationship */
        payments: ModelTypes['payments'][];
        /** An aggregated array relationship */
        payments_aggregate: ModelTypes['payments_aggregate'];
        period_active: boolean;
        period_completed_at: ModelTypes['timestamp'];
        period_index: number;
        period_progression: ModelTypes['numeric'];
        updated_at: ModelTypes['timestamptz'];
    };
    /** aggregated selection of "periods" */
    ['periods_aggregate']: {
        aggregate?: ModelTypes['periods_aggregate_fields'];
        nodes: ModelTypes['periods'][];
    };
    /** aggregate fields of "periods" */
    ['periods_aggregate_fields']: {
        avg?: ModelTypes['periods_avg_fields'];
        count?: number;
        max?: ModelTypes['periods_max_fields'];
        min?: ModelTypes['periods_min_fields'];
        stddev?: ModelTypes['periods_stddev_fields'];
        stddev_pop?: ModelTypes['periods_stddev_pop_fields'];
        stddev_samp?: ModelTypes['periods_stddev_samp_fields'];
        sum?: ModelTypes['periods_sum_fields'];
        var_pop?: ModelTypes['periods_var_pop_fields'];
        var_samp?: ModelTypes['periods_var_samp_fields'];
        variance?: ModelTypes['periods_variance_fields'];
    };
    /** order by aggregate values of table "periods" */
    ['periods_aggregate_order_by']: GraphQLTypes['periods_aggregate_order_by'];
    /** input type for inserting array relation for remote table "periods" */
    ['periods_arr_rel_insert_input']: GraphQLTypes['periods_arr_rel_insert_input'];
    /** aggregate avg on columns */
    ['periods_avg_fields']: {
        period_index?: number;
        period_progression?: number;
    };
    /** order by avg() on columns of table "periods" */
    ['periods_avg_order_by']: GraphQLTypes['periods_avg_order_by'];
    /** Boolean expression to filter rows from the table "periods". All fields are combined with a logical 'AND'. */
    ['periods_bool_exp']: GraphQLTypes['periods_bool_exp'];
    /** unique or primary key constraints on table "periods" */
    ['periods_constraint']: GraphQLTypes['periods_constraint'];
    /** input type for incrementing integer column in table "periods" */
    ['periods_inc_input']: GraphQLTypes['periods_inc_input'];
    /** input type for inserting data into table "periods" */
    ['periods_insert_input']: GraphQLTypes['periods_insert_input'];
    /** aggregate max on columns */
    ['periods_max_fields']: {
        created_at?: ModelTypes['timestamptz'];
        group_id?: ModelTypes['uuid'];
        id?: ModelTypes['uuid'];
        period_completed_at?: ModelTypes['timestamp'];
        period_index?: number;
        period_progression?: ModelTypes['numeric'];
        updated_at?: ModelTypes['timestamptz'];
    };
    /** order by max() on columns of table "periods" */
    ['periods_max_order_by']: GraphQLTypes['periods_max_order_by'];
    /** aggregate min on columns */
    ['periods_min_fields']: {
        created_at?: ModelTypes['timestamptz'];
        group_id?: ModelTypes['uuid'];
        id?: ModelTypes['uuid'];
        period_completed_at?: ModelTypes['timestamp'];
        period_index?: number;
        period_progression?: ModelTypes['numeric'];
        updated_at?: ModelTypes['timestamptz'];
    };
    /** order by min() on columns of table "periods" */
    ['periods_min_order_by']: GraphQLTypes['periods_min_order_by'];
    /** response of any mutation on the table "periods" */
    ['periods_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['periods'][];
    };
    /** input type for inserting object relation for remote table "periods" */
    ['periods_obj_rel_insert_input']: GraphQLTypes['periods_obj_rel_insert_input'];
    /** on conflict condition type for table "periods" */
    ['periods_on_conflict']: GraphQLTypes['periods_on_conflict'];
    /** ordering options when selecting data from "periods" */
    ['periods_order_by']: GraphQLTypes['periods_order_by'];
    /** primary key columns input for table: "periods" */
    ['periods_pk_columns_input']: GraphQLTypes['periods_pk_columns_input'];
    /** select columns of table "periods" */
    ['periods_select_column']: GraphQLTypes['periods_select_column'];
    /** input type for updating data in table "periods" */
    ['periods_set_input']: GraphQLTypes['periods_set_input'];
    /** aggregate stddev on columns */
    ['periods_stddev_fields']: {
        period_index?: number;
        period_progression?: number;
    };
    /** order by stddev() on columns of table "periods" */
    ['periods_stddev_order_by']: GraphQLTypes['periods_stddev_order_by'];
    /** aggregate stddev_pop on columns */
    ['periods_stddev_pop_fields']: {
        period_index?: number;
        period_progression?: number;
    };
    /** order by stddev_pop() on columns of table "periods" */
    ['periods_stddev_pop_order_by']: GraphQLTypes['periods_stddev_pop_order_by'];
    /** aggregate stddev_samp on columns */
    ['periods_stddev_samp_fields']: {
        period_index?: number;
        period_progression?: number;
    };
    /** order by stddev_samp() on columns of table "periods" */
    ['periods_stddev_samp_order_by']: GraphQLTypes['periods_stddev_samp_order_by'];
    /** aggregate sum on columns */
    ['periods_sum_fields']: {
        period_index?: number;
        period_progression?: ModelTypes['numeric'];
    };
    /** order by sum() on columns of table "periods" */
    ['periods_sum_order_by']: GraphQLTypes['periods_sum_order_by'];
    /** update columns of table "periods" */
    ['periods_update_column']: GraphQLTypes['periods_update_column'];
    /** aggregate var_pop on columns */
    ['periods_var_pop_fields']: {
        period_index?: number;
        period_progression?: number;
    };
    /** order by var_pop() on columns of table "periods" */
    ['periods_var_pop_order_by']: GraphQLTypes['periods_var_pop_order_by'];
    /** aggregate var_samp on columns */
    ['periods_var_samp_fields']: {
        period_index?: number;
        period_progression?: number;
    };
    /** order by var_samp() on columns of table "periods" */
    ['periods_var_samp_order_by']: GraphQLTypes['periods_var_samp_order_by'];
    /** aggregate variance on columns */
    ['periods_variance_fields']: {
        period_index?: number;
        period_progression?: number;
    };
    /** order by variance() on columns of table "periods" */
    ['periods_variance_order_by']: GraphQLTypes['periods_variance_order_by'];
    /** query root */
    ['query_root']: {
        /** fetch data from the table: "auth.account_providers" */
        auth_account_providers: ModelTypes['auth_account_providers'][];
        /** fetch aggregated fields from the table: "auth.account_providers" */
        auth_account_providers_aggregate: ModelTypes['auth_account_providers_aggregate'];
        /** fetch data from the table: "auth.account_providers" using primary key columns */
        auth_account_providers_by_pk?: ModelTypes['auth_account_providers'];
        /** fetch data from the table: "auth.account_roles" */
        auth_account_roles: ModelTypes['auth_account_roles'][];
        /** fetch aggregated fields from the table: "auth.account_roles" */
        auth_account_roles_aggregate: ModelTypes['auth_account_roles_aggregate'];
        /** fetch data from the table: "auth.account_roles" using primary key columns */
        auth_account_roles_by_pk?: ModelTypes['auth_account_roles'];
        /** fetch data from the table: "auth.accounts" */
        auth_accounts: ModelTypes['auth_accounts'][];
        /** fetch aggregated fields from the table: "auth.accounts" */
        auth_accounts_aggregate: ModelTypes['auth_accounts_aggregate'];
        /** fetch data from the table: "auth.accounts" using primary key columns */
        auth_accounts_by_pk?: ModelTypes['auth_accounts'];
        /** fetch data from the table: "auth.providers" */
        auth_providers: ModelTypes['auth_providers'][];
        /** fetch aggregated fields from the table: "auth.providers" */
        auth_providers_aggregate: ModelTypes['auth_providers_aggregate'];
        /** fetch data from the table: "auth.providers" using primary key columns */
        auth_providers_by_pk?: ModelTypes['auth_providers'];
        /** fetch data from the table: "auth.refresh_tokens" */
        auth_refresh_tokens: ModelTypes['auth_refresh_tokens'][];
        /** fetch aggregated fields from the table: "auth.refresh_tokens" */
        auth_refresh_tokens_aggregate: ModelTypes['auth_refresh_tokens_aggregate'];
        /** fetch data from the table: "auth.refresh_tokens" using primary key columns */
        auth_refresh_tokens_by_pk?: ModelTypes['auth_refresh_tokens'];
        /** fetch data from the table: "auth.roles" */
        auth_roles: ModelTypes['auth_roles'][];
        /** fetch aggregated fields from the table: "auth.roles" */
        auth_roles_aggregate: ModelTypes['auth_roles_aggregate'];
        /** fetch data from the table: "auth.roles" using primary key columns */
        auth_roles_by_pk?: ModelTypes['auth_roles'];
        /** fetch data from the table: "contribution_types" */
        contribution_types: ModelTypes['contribution_types'][];
        /** fetch aggregated fields from the table: "contribution_types" */
        contribution_types_aggregate: ModelTypes['contribution_types_aggregate'];
        /** fetch data from the table: "contribution_types" using primary key columns */
        contribution_types_by_pk?: ModelTypes['contribution_types'];
        /** fetch data from the table: "group_cycles" */
        group_cycles: ModelTypes['group_cycles'][];
        /** fetch aggregated fields from the table: "group_cycles" */
        group_cycles_aggregate: ModelTypes['group_cycles_aggregate'];
        /** fetch data from the table: "group_cycles" using primary key columns */
        group_cycles_by_pk?: ModelTypes['group_cycles'];
        /** fetch data from the table: "group_recurrencies" */
        group_recurrencies: ModelTypes['group_recurrencies'][];
        /** fetch aggregated fields from the table: "group_recurrencies" */
        group_recurrencies_aggregate: ModelTypes['group_recurrencies_aggregate'];
        /** fetch data from the table: "group_recurrencies" using primary key columns */
        group_recurrencies_by_pk?: ModelTypes['group_recurrencies'];
        /** fetch data from the table: "groups" */
        groups: ModelTypes['groups'][];
        /** fetch aggregated fields from the table: "groups" */
        groups_aggregate: ModelTypes['groups_aggregate'];
        /** fetch data from the table: "groups" using primary key columns */
        groups_by_pk?: ModelTypes['groups'];
        /** fetch data from the table: "members" */
        members: ModelTypes['members'][];
        /** fetch aggregated fields from the table: "members" */
        members_aggregate: ModelTypes['members_aggregate'];
        /** fetch data from the table: "members" using primary key columns */
        members_by_pk?: ModelTypes['members'];
        /** fetch data from the table: "payment_statuses" */
        payment_statuses: ModelTypes['payment_statuses'][];
        /** fetch aggregated fields from the table: "payment_statuses" */
        payment_statuses_aggregate: ModelTypes['payment_statuses_aggregate'];
        /** fetch data from the table: "payment_statuses" using primary key columns */
        payment_statuses_by_pk?: ModelTypes['payment_statuses'];
        /** fetch data from the table: "payment_types" */
        payment_types: ModelTypes['payment_types'][];
        /** fetch aggregated fields from the table: "payment_types" */
        payment_types_aggregate: ModelTypes['payment_types_aggregate'];
        /** fetch data from the table: "payment_types" using primary key columns */
        payment_types_by_pk?: ModelTypes['payment_types'];
        /** fetch data from the table: "payments" */
        payments: ModelTypes['payments'][];
        /** fetch aggregated fields from the table: "payments" */
        payments_aggregate: ModelTypes['payments_aggregate'];
        /** fetch data from the table: "payments" using primary key columns */
        payments_by_pk?: ModelTypes['payments'];
        /** fetch data from the table: "periods" */
        periods: ModelTypes['periods'][];
        /** fetch aggregated fields from the table: "periods" */
        periods_aggregate: ModelTypes['periods_aggregate'];
        /** fetch data from the table: "periods" using primary key columns */
        periods_by_pk?: ModelTypes['periods'];
        /** fetch data from the table: "users" */
        users: ModelTypes['users'][];
        /** fetch aggregated fields from the table: "users" */
        users_aggregate: ModelTypes['users_aggregate'];
        /** fetch data from the table: "users" using primary key columns */
        users_by_pk?: ModelTypes['users'];
    };
    /** subscription root */
    ['subscription_root']: {
        /** fetch data from the table: "auth.account_providers" */
        auth_account_providers: ModelTypes['auth_account_providers'][];
        /** fetch aggregated fields from the table: "auth.account_providers" */
        auth_account_providers_aggregate: ModelTypes['auth_account_providers_aggregate'];
        /** fetch data from the table: "auth.account_providers" using primary key columns */
        auth_account_providers_by_pk?: ModelTypes['auth_account_providers'];
        /** fetch data from the table: "auth.account_roles" */
        auth_account_roles: ModelTypes['auth_account_roles'][];
        /** fetch aggregated fields from the table: "auth.account_roles" */
        auth_account_roles_aggregate: ModelTypes['auth_account_roles_aggregate'];
        /** fetch data from the table: "auth.account_roles" using primary key columns */
        auth_account_roles_by_pk?: ModelTypes['auth_account_roles'];
        /** fetch data from the table: "auth.accounts" */
        auth_accounts: ModelTypes['auth_accounts'][];
        /** fetch aggregated fields from the table: "auth.accounts" */
        auth_accounts_aggregate: ModelTypes['auth_accounts_aggregate'];
        /** fetch data from the table: "auth.accounts" using primary key columns */
        auth_accounts_by_pk?: ModelTypes['auth_accounts'];
        /** fetch data from the table: "auth.providers" */
        auth_providers: ModelTypes['auth_providers'][];
        /** fetch aggregated fields from the table: "auth.providers" */
        auth_providers_aggregate: ModelTypes['auth_providers_aggregate'];
        /** fetch data from the table: "auth.providers" using primary key columns */
        auth_providers_by_pk?: ModelTypes['auth_providers'];
        /** fetch data from the table: "auth.refresh_tokens" */
        auth_refresh_tokens: ModelTypes['auth_refresh_tokens'][];
        /** fetch aggregated fields from the table: "auth.refresh_tokens" */
        auth_refresh_tokens_aggregate: ModelTypes['auth_refresh_tokens_aggregate'];
        /** fetch data from the table: "auth.refresh_tokens" using primary key columns */
        auth_refresh_tokens_by_pk?: ModelTypes['auth_refresh_tokens'];
        /** fetch data from the table: "auth.roles" */
        auth_roles: ModelTypes['auth_roles'][];
        /** fetch aggregated fields from the table: "auth.roles" */
        auth_roles_aggregate: ModelTypes['auth_roles_aggregate'];
        /** fetch data from the table: "auth.roles" using primary key columns */
        auth_roles_by_pk?: ModelTypes['auth_roles'];
        /** fetch data from the table: "contribution_types" */
        contribution_types: ModelTypes['contribution_types'][];
        /** fetch aggregated fields from the table: "contribution_types" */
        contribution_types_aggregate: ModelTypes['contribution_types_aggregate'];
        /** fetch data from the table: "contribution_types" using primary key columns */
        contribution_types_by_pk?: ModelTypes['contribution_types'];
        /** fetch data from the table: "group_cycles" */
        group_cycles: ModelTypes['group_cycles'][];
        /** fetch aggregated fields from the table: "group_cycles" */
        group_cycles_aggregate: ModelTypes['group_cycles_aggregate'];
        /** fetch data from the table: "group_cycles" using primary key columns */
        group_cycles_by_pk?: ModelTypes['group_cycles'];
        /** fetch data from the table: "group_recurrencies" */
        group_recurrencies: ModelTypes['group_recurrencies'][];
        /** fetch aggregated fields from the table: "group_recurrencies" */
        group_recurrencies_aggregate: ModelTypes['group_recurrencies_aggregate'];
        /** fetch data from the table: "group_recurrencies" using primary key columns */
        group_recurrencies_by_pk?: ModelTypes['group_recurrencies'];
        /** fetch data from the table: "groups" */
        groups: ModelTypes['groups'][];
        /** fetch aggregated fields from the table: "groups" */
        groups_aggregate: ModelTypes['groups_aggregate'];
        /** fetch data from the table: "groups" using primary key columns */
        groups_by_pk?: ModelTypes['groups'];
        /** fetch data from the table: "members" */
        members: ModelTypes['members'][];
        /** fetch aggregated fields from the table: "members" */
        members_aggregate: ModelTypes['members_aggregate'];
        /** fetch data from the table: "members" using primary key columns */
        members_by_pk?: ModelTypes['members'];
        /** fetch data from the table: "payment_statuses" */
        payment_statuses: ModelTypes['payment_statuses'][];
        /** fetch aggregated fields from the table: "payment_statuses" */
        payment_statuses_aggregate: ModelTypes['payment_statuses_aggregate'];
        /** fetch data from the table: "payment_statuses" using primary key columns */
        payment_statuses_by_pk?: ModelTypes['payment_statuses'];
        /** fetch data from the table: "payment_types" */
        payment_types: ModelTypes['payment_types'][];
        /** fetch aggregated fields from the table: "payment_types" */
        payment_types_aggregate: ModelTypes['payment_types_aggregate'];
        /** fetch data from the table: "payment_types" using primary key columns */
        payment_types_by_pk?: ModelTypes['payment_types'];
        /** fetch data from the table: "payments" */
        payments: ModelTypes['payments'][];
        /** fetch aggregated fields from the table: "payments" */
        payments_aggregate: ModelTypes['payments_aggregate'];
        /** fetch data from the table: "payments" using primary key columns */
        payments_by_pk?: ModelTypes['payments'];
        /** fetch data from the table: "periods" */
        periods: ModelTypes['periods'][];
        /** fetch aggregated fields from the table: "periods" */
        periods_aggregate: ModelTypes['periods_aggregate'];
        /** fetch data from the table: "periods" using primary key columns */
        periods_by_pk?: ModelTypes['periods'];
        /** fetch data from the table: "users" */
        users: ModelTypes['users'][];
        /** fetch aggregated fields from the table: "users" */
        users_aggregate: ModelTypes['users_aggregate'];
        /** fetch data from the table: "users" using primary key columns */
        users_by_pk?: ModelTypes['users'];
    };
    ['timestamp']: any;
    /** expression to compare columns of type timestamp. All fields are combined with logical 'AND'. */
    ['timestamp_comparison_exp']: GraphQLTypes['timestamp_comparison_exp'];
    ['timestamptz']: any;
    /** expression to compare columns of type timestamptz. All fields are combined with logical 'AND'. */
    ['timestamptz_comparison_exp']: GraphQLTypes['timestamptz_comparison_exp'];
    /** columns and relationships of "users" */
    ['users']: {
        /** An object relationship */
        account?: ModelTypes['auth_accounts'];
        avatar_url?: string;
        created_at: ModelTypes['timestamptz'];
        display_name?: string;
        expo_push_token?: string;
        /** An array relationship */
        groups: ModelTypes['groups'][];
        /** An aggregated array relationship */
        groups_aggregate: ModelTypes['groups_aggregate'];
        id: ModelTypes['uuid'];
        /** An array relationship */
        members: ModelTypes['members'][];
        /** An aggregated array relationship */
        members_aggregate: ModelTypes['members_aggregate'];
        updated_at: ModelTypes['timestamptz'];
    };
    /** aggregated selection of "users" */
    ['users_aggregate']: {
        aggregate?: ModelTypes['users_aggregate_fields'];
        nodes: ModelTypes['users'][];
    };
    /** aggregate fields of "users" */
    ['users_aggregate_fields']: {
        count?: number;
        max?: ModelTypes['users_max_fields'];
        min?: ModelTypes['users_min_fields'];
    };
    /** order by aggregate values of table "users" */
    ['users_aggregate_order_by']: GraphQLTypes['users_aggregate_order_by'];
    /** input type for inserting array relation for remote table "users" */
    ['users_arr_rel_insert_input']: GraphQLTypes['users_arr_rel_insert_input'];
    /** Boolean expression to filter rows from the table "users". All fields are combined with a logical 'AND'. */
    ['users_bool_exp']: GraphQLTypes['users_bool_exp'];
    /** unique or primary key constraints on table "users" */
    ['users_constraint']: GraphQLTypes['users_constraint'];
    /** input type for inserting data into table "users" */
    ['users_insert_input']: GraphQLTypes['users_insert_input'];
    /** aggregate max on columns */
    ['users_max_fields']: {
        avatar_url?: string;
        created_at?: ModelTypes['timestamptz'];
        display_name?: string;
        expo_push_token?: string;
        id?: ModelTypes['uuid'];
        updated_at?: ModelTypes['timestamptz'];
    };
    /** order by max() on columns of table "users" */
    ['users_max_order_by']: GraphQLTypes['users_max_order_by'];
    /** aggregate min on columns */
    ['users_min_fields']: {
        avatar_url?: string;
        created_at?: ModelTypes['timestamptz'];
        display_name?: string;
        expo_push_token?: string;
        id?: ModelTypes['uuid'];
        updated_at?: ModelTypes['timestamptz'];
    };
    /** order by min() on columns of table "users" */
    ['users_min_order_by']: GraphQLTypes['users_min_order_by'];
    /** response of any mutation on the table "users" */
    ['users_mutation_response']: {
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: ModelTypes['users'][];
    };
    /** input type for inserting object relation for remote table "users" */
    ['users_obj_rel_insert_input']: GraphQLTypes['users_obj_rel_insert_input'];
    /** on conflict condition type for table "users" */
    ['users_on_conflict']: GraphQLTypes['users_on_conflict'];
    /** ordering options when selecting data from "users" */
    ['users_order_by']: GraphQLTypes['users_order_by'];
    /** primary key columns input for table: "users" */
    ['users_pk_columns_input']: GraphQLTypes['users_pk_columns_input'];
    /** select columns of table "users" */
    ['users_select_column']: GraphQLTypes['users_select_column'];
    /** input type for updating data in table "users" */
    ['users_set_input']: GraphQLTypes['users_set_input'];
    /** update columns of table "users" */
    ['users_update_column']: GraphQLTypes['users_update_column'];
    ['uuid']: any;
    /** expression to compare columns of type uuid. All fields are combined with logical 'AND'. */
    ['uuid_comparison_exp']: GraphQLTypes['uuid_comparison_exp'];
};

export type GraphQLTypes = {
    ['ActionCreateGroupInput']: {
        creator_id: GraphQLTypes['uuid'];
        group_contribution_amount?: GraphQLTypes['numeric'];
        group_contribution_type: string;
        group_name: string;
        group_recurrency_day?: GraphQLTypes['numeric'];
        group_recurrency_type: string;
    };
    ['ActionCreateGroupOutpu']: {
        __typename: 'ActionCreateGroupOutpu';
        ok: boolean;
    };
    ['ActionCreateGroupOutput']: {
        __typename: 'ActionCreateGroupOutput';
        ok: boolean;
    };
    ['ActionRequestPaymentInput']: {
        amount: GraphQLTypes['numeric'];
        group_id: GraphQLTypes['uuid'];
        member_id: GraphQLTypes['uuid'];
        period_id: GraphQLTypes['uuid'];
    };
    ['ActionRequestPaymentOutput']: {
        __typename: 'ActionRequestPaymentOutput';
        id: GraphQLTypes['uuid'];
        url: string;
    };
    ['ActionWithdrawPaymentInput']: {
        amount: GraphQLTypes['numeric'];
        group_id: GraphQLTypes['uuid'];
        member_id: GraphQLTypes['uuid'];
        period_id: GraphQLTypes['uuid'];
        senderName: string;
        senderPhone: string;
    };
    ['ActionWithdrawPaymentOutput']: {
        __typename: 'ActionWithdrawPaymentOutput';
        id: GraphQLTypes['uuid'];
    };
    /** expression to compare columns of type Boolean. All fields are combined with logical 'AND'. */
    ['Boolean_comparison_exp']: {
        _eq?: boolean;
        _gt?: boolean;
        _gte?: boolean;
        _in?: Array<boolean>;
        _is_null?: boolean;
        _lt?: boolean;
        _lte?: boolean;
        _neq?: boolean;
        _nin?: Array<boolean>;
    };
    ['InsertCreateInput']: {
        amount: GraphQLTypes['numeric'];
        group_id: GraphQLTypes['uuid'];
        member_id: GraphQLTypes['uuid'];
        period_id: GraphQLTypes['uuid'];
    };
    ['InsertPaymentOneActionInput']: {
        amount: GraphQLTypes['numeric'];
        group_id: GraphQLTypes['uuid'];
        period_id: GraphQLTypes['uuid'];
        user_id: GraphQLTypes['uuid'];
    };
    ['InsertPaymentOneActionOutput']: {
        __typename: 'InsertPaymentOneActionOutput';
        url: string;
    };
    ['InsertPaymentOutput']: {
        __typename: 'InsertPaymentOutput';
        id: GraphQLTypes['uuid'];
        url: string;
    };
    /** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
    ['Int_comparison_exp']: {
        _eq?: number;
        _gt?: number;
        _gte?: number;
        _in?: Array<number>;
        _is_null?: boolean;
        _lt?: number;
        _lte?: number;
        _neq?: number;
        _nin?: Array<number>;
    };
    ['PaymentCreateInput']: {
        amount: GraphQLTypes['numeric'];
        group_id: GraphQLTypes['uuid'];
        period_id: GraphQLTypes['uuid'];
        user_id: GraphQLTypes['uuid'];
    };
    ['PaymentCreatedOutput']: {
        __typename: 'PaymentCreatedOutput';
        url: string;
    };
    /** expression to compare columns of type String. All fields are combined with logical 'AND'. */
    ['String_comparison_exp']: {
        _eq?: string;
        _gt?: string;
        _gte?: string;
        _ilike?: string;
        _in?: Array<string>;
        _is_null?: boolean;
        _like?: string;
        _lt?: string;
        _lte?: string;
        _neq?: string;
        _nilike?: string;
        _nin?: Array<string>;
        _nlike?: string;
        _nsimilar?: string;
        _similar?: string;
    };
    /** columns and relationships of "auth.account_providers" */
    ['auth_account_providers']: {
        __typename: 'auth_account_providers';
        /** An object relationship */
        account: GraphQLTypes['auth_accounts'];
        account_id: GraphQLTypes['uuid'];
        auth_provider: string;
        auth_provider_unique_id: string;
        created_at: GraphQLTypes['timestamptz'];
        id: GraphQLTypes['uuid'];
        /** An object relationship */
        provider: GraphQLTypes['auth_providers'];
        updated_at: GraphQLTypes['timestamptz'];
    };
    /** aggregated selection of "auth.account_providers" */
    ['auth_account_providers_aggregate']: {
        __typename: 'auth_account_providers_aggregate';
        aggregate?: GraphQLTypes['auth_account_providers_aggregate_fields'];
        nodes: Array<GraphQLTypes['auth_account_providers']>;
    };
    /** aggregate fields of "auth.account_providers" */
    ['auth_account_providers_aggregate_fields']: {
        __typename: 'auth_account_providers_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['auth_account_providers_max_fields'];
        min?: GraphQLTypes['auth_account_providers_min_fields'];
    };
    /** order by aggregate values of table "auth.account_providers" */
    ['auth_account_providers_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['auth_account_providers_max_order_by'];
        min?: GraphQLTypes['auth_account_providers_min_order_by'];
    };
    /** input type for inserting array relation for remote table "auth.account_providers" */
    ['auth_account_providers_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['auth_account_providers_insert_input']>;
        on_conflict?: GraphQLTypes['auth_account_providers_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.account_providers". All fields are combined with a logical 'AND'. */
    ['auth_account_providers_bool_exp']: {
        _and?: Array<
            GraphQLTypes['auth_account_providers_bool_exp'] | undefined
        >;
        _not?: GraphQLTypes['auth_account_providers_bool_exp'];
        _or?: Array<
            GraphQLTypes['auth_account_providers_bool_exp'] | undefined
        >;
        account?: GraphQLTypes['auth_accounts_bool_exp'];
        account_id?: GraphQLTypes['uuid_comparison_exp'];
        auth_provider?: GraphQLTypes['String_comparison_exp'];
        auth_provider_unique_id?: GraphQLTypes['String_comparison_exp'];
        created_at?: GraphQLTypes['timestamptz_comparison_exp'];
        id?: GraphQLTypes['uuid_comparison_exp'];
        provider?: GraphQLTypes['auth_providers_bool_exp'];
        updated_at?: GraphQLTypes['timestamptz_comparison_exp'];
    };
    /** unique or primary key constraints on table "auth.account_providers" */
    ['auth_account_providers_constraint']: auth_account_providers_constraint;
    /** input type for inserting data into table "auth.account_providers" */
    ['auth_account_providers_insert_input']: {
        account?: GraphQLTypes['auth_accounts_obj_rel_insert_input'];
        account_id?: GraphQLTypes['uuid'];
        auth_provider?: string;
        auth_provider_unique_id?: string;
        created_at?: GraphQLTypes['timestamptz'];
        id?: GraphQLTypes['uuid'];
        provider?: GraphQLTypes['auth_providers_obj_rel_insert_input'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** aggregate max on columns */
    ['auth_account_providers_max_fields']: {
        __typename: 'auth_account_providers_max_fields';
        account_id?: GraphQLTypes['uuid'];
        auth_provider?: string;
        auth_provider_unique_id?: string;
        created_at?: GraphQLTypes['timestamptz'];
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** order by max() on columns of table "auth.account_providers" */
    ['auth_account_providers_max_order_by']: {
        account_id?: GraphQLTypes['order_by'];
        auth_provider?: GraphQLTypes['order_by'];
        auth_provider_unique_id?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_account_providers_min_fields']: {
        __typename: 'auth_account_providers_min_fields';
        account_id?: GraphQLTypes['uuid'];
        auth_provider?: string;
        auth_provider_unique_id?: string;
        created_at?: GraphQLTypes['timestamptz'];
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** order by min() on columns of table "auth.account_providers" */
    ['auth_account_providers_min_order_by']: {
        account_id?: GraphQLTypes['order_by'];
        auth_provider?: GraphQLTypes['order_by'];
        auth_provider_unique_id?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "auth.account_providers" */
    ['auth_account_providers_mutation_response']: {
        __typename: 'auth_account_providers_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['auth_account_providers']>;
    };
    /** input type for inserting object relation for remote table "auth.account_providers" */
    ['auth_account_providers_obj_rel_insert_input']: {
        data: GraphQLTypes['auth_account_providers_insert_input'];
        on_conflict?: GraphQLTypes['auth_account_providers_on_conflict'];
    };
    /** on conflict condition type for table "auth.account_providers" */
    ['auth_account_providers_on_conflict']: {
        constraint: GraphQLTypes['auth_account_providers_constraint'];
        update_columns: Array<
            GraphQLTypes['auth_account_providers_update_column']
        >;
        where?: GraphQLTypes['auth_account_providers_bool_exp'];
    };
    /** ordering options when selecting data from "auth.account_providers" */
    ['auth_account_providers_order_by']: {
        account?: GraphQLTypes['auth_accounts_order_by'];
        account_id?: GraphQLTypes['order_by'];
        auth_provider?: GraphQLTypes['order_by'];
        auth_provider_unique_id?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        provider?: GraphQLTypes['auth_providers_order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "auth.account_providers" */
    ['auth_account_providers_pk_columns_input']: {
        id: GraphQLTypes['uuid'];
    };
    /** select columns of table "auth.account_providers" */
    ['auth_account_providers_select_column']: auth_account_providers_select_column;
    /** input type for updating data in table "auth.account_providers" */
    ['auth_account_providers_set_input']: {
        account_id?: GraphQLTypes['uuid'];
        auth_provider?: string;
        auth_provider_unique_id?: string;
        created_at?: GraphQLTypes['timestamptz'];
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** update columns of table "auth.account_providers" */
    ['auth_account_providers_update_column']: auth_account_providers_update_column;
    /** columns and relationships of "auth.account_roles" */
    ['auth_account_roles']: {
        __typename: 'auth_account_roles';
        /** An object relationship */
        account: GraphQLTypes['auth_accounts'];
        account_id: GraphQLTypes['uuid'];
        created_at: GraphQLTypes['timestamptz'];
        id: GraphQLTypes['uuid'];
        role: string;
        /** An object relationship */
        roleByRole: GraphQLTypes['auth_roles'];
    };
    /** aggregated selection of "auth.account_roles" */
    ['auth_account_roles_aggregate']: {
        __typename: 'auth_account_roles_aggregate';
        aggregate?: GraphQLTypes['auth_account_roles_aggregate_fields'];
        nodes: Array<GraphQLTypes['auth_account_roles']>;
    };
    /** aggregate fields of "auth.account_roles" */
    ['auth_account_roles_aggregate_fields']: {
        __typename: 'auth_account_roles_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['auth_account_roles_max_fields'];
        min?: GraphQLTypes['auth_account_roles_min_fields'];
    };
    /** order by aggregate values of table "auth.account_roles" */
    ['auth_account_roles_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['auth_account_roles_max_order_by'];
        min?: GraphQLTypes['auth_account_roles_min_order_by'];
    };
    /** input type for inserting array relation for remote table "auth.account_roles" */
    ['auth_account_roles_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['auth_account_roles_insert_input']>;
        on_conflict?: GraphQLTypes['auth_account_roles_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.account_roles". All fields are combined with a logical 'AND'. */
    ['auth_account_roles_bool_exp']: {
        _and?: Array<GraphQLTypes['auth_account_roles_bool_exp'] | undefined>;
        _not?: GraphQLTypes['auth_account_roles_bool_exp'];
        _or?: Array<GraphQLTypes['auth_account_roles_bool_exp'] | undefined>;
        account?: GraphQLTypes['auth_accounts_bool_exp'];
        account_id?: GraphQLTypes['uuid_comparison_exp'];
        created_at?: GraphQLTypes['timestamptz_comparison_exp'];
        id?: GraphQLTypes['uuid_comparison_exp'];
        role?: GraphQLTypes['String_comparison_exp'];
        roleByRole?: GraphQLTypes['auth_roles_bool_exp'];
    };
    /** unique or primary key constraints on table "auth.account_roles" */
    ['auth_account_roles_constraint']: auth_account_roles_constraint;
    /** input type for inserting data into table "auth.account_roles" */
    ['auth_account_roles_insert_input']: {
        account?: GraphQLTypes['auth_accounts_obj_rel_insert_input'];
        account_id?: GraphQLTypes['uuid'];
        created_at?: GraphQLTypes['timestamptz'];
        id?: GraphQLTypes['uuid'];
        role?: string;
        roleByRole?: GraphQLTypes['auth_roles_obj_rel_insert_input'];
    };
    /** aggregate max on columns */
    ['auth_account_roles_max_fields']: {
        __typename: 'auth_account_roles_max_fields';
        account_id?: GraphQLTypes['uuid'];
        created_at?: GraphQLTypes['timestamptz'];
        id?: GraphQLTypes['uuid'];
        role?: string;
    };
    /** order by max() on columns of table "auth.account_roles" */
    ['auth_account_roles_max_order_by']: {
        account_id?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        role?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_account_roles_min_fields']: {
        __typename: 'auth_account_roles_min_fields';
        account_id?: GraphQLTypes['uuid'];
        created_at?: GraphQLTypes['timestamptz'];
        id?: GraphQLTypes['uuid'];
        role?: string;
    };
    /** order by min() on columns of table "auth.account_roles" */
    ['auth_account_roles_min_order_by']: {
        account_id?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        role?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "auth.account_roles" */
    ['auth_account_roles_mutation_response']: {
        __typename: 'auth_account_roles_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['auth_account_roles']>;
    };
    /** input type for inserting object relation for remote table "auth.account_roles" */
    ['auth_account_roles_obj_rel_insert_input']: {
        data: GraphQLTypes['auth_account_roles_insert_input'];
        on_conflict?: GraphQLTypes['auth_account_roles_on_conflict'];
    };
    /** on conflict condition type for table "auth.account_roles" */
    ['auth_account_roles_on_conflict']: {
        constraint: GraphQLTypes['auth_account_roles_constraint'];
        update_columns: Array<GraphQLTypes['auth_account_roles_update_column']>;
        where?: GraphQLTypes['auth_account_roles_bool_exp'];
    };
    /** ordering options when selecting data from "auth.account_roles" */
    ['auth_account_roles_order_by']: {
        account?: GraphQLTypes['auth_accounts_order_by'];
        account_id?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        role?: GraphQLTypes['order_by'];
        roleByRole?: GraphQLTypes['auth_roles_order_by'];
    };
    /** primary key columns input for table: "auth.account_roles" */
    ['auth_account_roles_pk_columns_input']: {
        id: GraphQLTypes['uuid'];
    };
    /** select columns of table "auth.account_roles" */
    ['auth_account_roles_select_column']: auth_account_roles_select_column;
    /** input type for updating data in table "auth.account_roles" */
    ['auth_account_roles_set_input']: {
        account_id?: GraphQLTypes['uuid'];
        created_at?: GraphQLTypes['timestamptz'];
        id?: GraphQLTypes['uuid'];
        role?: string;
    };
    /** update columns of table "auth.account_roles" */
    ['auth_account_roles_update_column']: auth_account_roles_update_column;
    /** columns and relationships of "auth.accounts" */
    ['auth_accounts']: {
        __typename: 'auth_accounts';
        /** An array relationship */
        account_providers: Array<GraphQLTypes['auth_account_providers']>;
        /** An aggregated array relationship */
        account_providers_aggregate: GraphQLTypes['auth_account_providers_aggregate'];
        /** An array relationship */
        account_roles: Array<GraphQLTypes['auth_account_roles']>;
        /** An aggregated array relationship */
        account_roles_aggregate: GraphQLTypes['auth_account_roles_aggregate'];
        active: boolean;
        created_at: GraphQLTypes['timestamptz'];
        custom_register_data?: GraphQLTypes['jsonb'];
        default_role: string;
        email?: GraphQLTypes['citext'];
        id: GraphQLTypes['uuid'];
        is_anonymous: boolean;
        mfa_enabled: boolean;
        new_email?: GraphQLTypes['citext'];
        otp_secret?: string;
        password_hash?: string;
        /** An array relationship */
        refresh_tokens: Array<GraphQLTypes['auth_refresh_tokens']>;
        /** An aggregated array relationship */
        refresh_tokens_aggregate: GraphQLTypes['auth_refresh_tokens_aggregate'];
        /** An object relationship */
        role: GraphQLTypes['auth_roles'];
        ticket: GraphQLTypes['uuid'];
        ticket_expires_at: GraphQLTypes['timestamptz'];
        updated_at: GraphQLTypes['timestamptz'];
        /** An object relationship */
        user: GraphQLTypes['users'];
        user_id: GraphQLTypes['uuid'];
    };
    /** aggregated selection of "auth.accounts" */
    ['auth_accounts_aggregate']: {
        __typename: 'auth_accounts_aggregate';
        aggregate?: GraphQLTypes['auth_accounts_aggregate_fields'];
        nodes: Array<GraphQLTypes['auth_accounts']>;
    };
    /** aggregate fields of "auth.accounts" */
    ['auth_accounts_aggregate_fields']: {
        __typename: 'auth_accounts_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['auth_accounts_max_fields'];
        min?: GraphQLTypes['auth_accounts_min_fields'];
    };
    /** order by aggregate values of table "auth.accounts" */
    ['auth_accounts_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['auth_accounts_max_order_by'];
        min?: GraphQLTypes['auth_accounts_min_order_by'];
    };
    /** append existing jsonb value of filtered columns with new jsonb value */
    ['auth_accounts_append_input']: {
        custom_register_data?: GraphQLTypes['jsonb'];
    };
    /** input type for inserting array relation for remote table "auth.accounts" */
    ['auth_accounts_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['auth_accounts_insert_input']>;
        on_conflict?: GraphQLTypes['auth_accounts_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.accounts". All fields are combined with a logical 'AND'. */
    ['auth_accounts_bool_exp']: {
        _and?: Array<GraphQLTypes['auth_accounts_bool_exp'] | undefined>;
        _not?: GraphQLTypes['auth_accounts_bool_exp'];
        _or?: Array<GraphQLTypes['auth_accounts_bool_exp'] | undefined>;
        account_providers?: GraphQLTypes['auth_account_providers_bool_exp'];
        account_roles?: GraphQLTypes['auth_account_roles_bool_exp'];
        active?: GraphQLTypes['Boolean_comparison_exp'];
        created_at?: GraphQLTypes['timestamptz_comparison_exp'];
        custom_register_data?: GraphQLTypes['jsonb_comparison_exp'];
        default_role?: GraphQLTypes['String_comparison_exp'];
        email?: GraphQLTypes['citext_comparison_exp'];
        id?: GraphQLTypes['uuid_comparison_exp'];
        is_anonymous?: GraphQLTypes['Boolean_comparison_exp'];
        mfa_enabled?: GraphQLTypes['Boolean_comparison_exp'];
        new_email?: GraphQLTypes['citext_comparison_exp'];
        otp_secret?: GraphQLTypes['String_comparison_exp'];
        password_hash?: GraphQLTypes['String_comparison_exp'];
        refresh_tokens?: GraphQLTypes['auth_refresh_tokens_bool_exp'];
        role?: GraphQLTypes['auth_roles_bool_exp'];
        ticket?: GraphQLTypes['uuid_comparison_exp'];
        ticket_expires_at?: GraphQLTypes['timestamptz_comparison_exp'];
        updated_at?: GraphQLTypes['timestamptz_comparison_exp'];
        user?: GraphQLTypes['users_bool_exp'];
        user_id?: GraphQLTypes['uuid_comparison_exp'];
    };
    /** unique or primary key constraints on table "auth.accounts" */
    ['auth_accounts_constraint']: auth_accounts_constraint;
    /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
    ['auth_accounts_delete_at_path_input']: {
        custom_register_data?: Array<string | undefined>;
    };
    /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
    ['auth_accounts_delete_elem_input']: {
        custom_register_data?: number;
    };
    /** delete key/value pair or string element. key/value pairs are matched based on their key value */
    ['auth_accounts_delete_key_input']: {
        custom_register_data?: string;
    };
    /** input type for inserting data into table "auth.accounts" */
    ['auth_accounts_insert_input']: {
        account_providers?: GraphQLTypes['auth_account_providers_arr_rel_insert_input'];
        account_roles?: GraphQLTypes['auth_account_roles_arr_rel_insert_input'];
        active?: boolean;
        created_at?: GraphQLTypes['timestamptz'];
        custom_register_data?: GraphQLTypes['jsonb'];
        default_role?: string;
        email?: GraphQLTypes['citext'];
        id?: GraphQLTypes['uuid'];
        is_anonymous?: boolean;
        mfa_enabled?: boolean;
        new_email?: GraphQLTypes['citext'];
        otp_secret?: string;
        password_hash?: string;
        refresh_tokens?: GraphQLTypes['auth_refresh_tokens_arr_rel_insert_input'];
        role?: GraphQLTypes['auth_roles_obj_rel_insert_input'];
        ticket?: GraphQLTypes['uuid'];
        ticket_expires_at?: GraphQLTypes['timestamptz'];
        updated_at?: GraphQLTypes['timestamptz'];
        user?: GraphQLTypes['users_obj_rel_insert_input'];
        user_id?: GraphQLTypes['uuid'];
    };
    /** aggregate max on columns */
    ['auth_accounts_max_fields']: {
        __typename: 'auth_accounts_max_fields';
        created_at?: GraphQLTypes['timestamptz'];
        default_role?: string;
        email?: GraphQLTypes['citext'];
        id?: GraphQLTypes['uuid'];
        new_email?: GraphQLTypes['citext'];
        otp_secret?: string;
        password_hash?: string;
        ticket?: GraphQLTypes['uuid'];
        ticket_expires_at?: GraphQLTypes['timestamptz'];
        updated_at?: GraphQLTypes['timestamptz'];
        user_id?: GraphQLTypes['uuid'];
    };
    /** order by max() on columns of table "auth.accounts" */
    ['auth_accounts_max_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        default_role?: GraphQLTypes['order_by'];
        email?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        new_email?: GraphQLTypes['order_by'];
        otp_secret?: GraphQLTypes['order_by'];
        password_hash?: GraphQLTypes['order_by'];
        ticket?: GraphQLTypes['order_by'];
        ticket_expires_at?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
        user_id?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_accounts_min_fields']: {
        __typename: 'auth_accounts_min_fields';
        created_at?: GraphQLTypes['timestamptz'];
        default_role?: string;
        email?: GraphQLTypes['citext'];
        id?: GraphQLTypes['uuid'];
        new_email?: GraphQLTypes['citext'];
        otp_secret?: string;
        password_hash?: string;
        ticket?: GraphQLTypes['uuid'];
        ticket_expires_at?: GraphQLTypes['timestamptz'];
        updated_at?: GraphQLTypes['timestamptz'];
        user_id?: GraphQLTypes['uuid'];
    };
    /** order by min() on columns of table "auth.accounts" */
    ['auth_accounts_min_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        default_role?: GraphQLTypes['order_by'];
        email?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        new_email?: GraphQLTypes['order_by'];
        otp_secret?: GraphQLTypes['order_by'];
        password_hash?: GraphQLTypes['order_by'];
        ticket?: GraphQLTypes['order_by'];
        ticket_expires_at?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
        user_id?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "auth.accounts" */
    ['auth_accounts_mutation_response']: {
        __typename: 'auth_accounts_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['auth_accounts']>;
    };
    /** input type for inserting object relation for remote table "auth.accounts" */
    ['auth_accounts_obj_rel_insert_input']: {
        data: GraphQLTypes['auth_accounts_insert_input'];
        on_conflict?: GraphQLTypes['auth_accounts_on_conflict'];
    };
    /** on conflict condition type for table "auth.accounts" */
    ['auth_accounts_on_conflict']: {
        constraint: GraphQLTypes['auth_accounts_constraint'];
        update_columns: Array<GraphQLTypes['auth_accounts_update_column']>;
        where?: GraphQLTypes['auth_accounts_bool_exp'];
    };
    /** ordering options when selecting data from "auth.accounts" */
    ['auth_accounts_order_by']: {
        account_providers_aggregate?: GraphQLTypes['auth_account_providers_aggregate_order_by'];
        account_roles_aggregate?: GraphQLTypes['auth_account_roles_aggregate_order_by'];
        active?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        custom_register_data?: GraphQLTypes['order_by'];
        default_role?: GraphQLTypes['order_by'];
        email?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        is_anonymous?: GraphQLTypes['order_by'];
        mfa_enabled?: GraphQLTypes['order_by'];
        new_email?: GraphQLTypes['order_by'];
        otp_secret?: GraphQLTypes['order_by'];
        password_hash?: GraphQLTypes['order_by'];
        refresh_tokens_aggregate?: GraphQLTypes['auth_refresh_tokens_aggregate_order_by'];
        role?: GraphQLTypes['auth_roles_order_by'];
        ticket?: GraphQLTypes['order_by'];
        ticket_expires_at?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
        user?: GraphQLTypes['users_order_by'];
        user_id?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "auth.accounts" */
    ['auth_accounts_pk_columns_input']: {
        id: GraphQLTypes['uuid'];
    };
    /** prepend existing jsonb value of filtered columns with new jsonb value */
    ['auth_accounts_prepend_input']: {
        custom_register_data?: GraphQLTypes['jsonb'];
    };
    /** select columns of table "auth.accounts" */
    ['auth_accounts_select_column']: auth_accounts_select_column;
    /** input type for updating data in table "auth.accounts" */
    ['auth_accounts_set_input']: {
        active?: boolean;
        created_at?: GraphQLTypes['timestamptz'];
        custom_register_data?: GraphQLTypes['jsonb'];
        default_role?: string;
        email?: GraphQLTypes['citext'];
        id?: GraphQLTypes['uuid'];
        is_anonymous?: boolean;
        mfa_enabled?: boolean;
        new_email?: GraphQLTypes['citext'];
        otp_secret?: string;
        password_hash?: string;
        ticket?: GraphQLTypes['uuid'];
        ticket_expires_at?: GraphQLTypes['timestamptz'];
        updated_at?: GraphQLTypes['timestamptz'];
        user_id?: GraphQLTypes['uuid'];
    };
    /** update columns of table "auth.accounts" */
    ['auth_accounts_update_column']: auth_accounts_update_column;
    /** columns and relationships of "auth.providers" */
    ['auth_providers']: {
        __typename: 'auth_providers';
        /** An array relationship */
        account_providers: Array<GraphQLTypes['auth_account_providers']>;
        /** An aggregated array relationship */
        account_providers_aggregate: GraphQLTypes['auth_account_providers_aggregate'];
        provider: string;
    };
    /** aggregated selection of "auth.providers" */
    ['auth_providers_aggregate']: {
        __typename: 'auth_providers_aggregate';
        aggregate?: GraphQLTypes['auth_providers_aggregate_fields'];
        nodes: Array<GraphQLTypes['auth_providers']>;
    };
    /** aggregate fields of "auth.providers" */
    ['auth_providers_aggregate_fields']: {
        __typename: 'auth_providers_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['auth_providers_max_fields'];
        min?: GraphQLTypes['auth_providers_min_fields'];
    };
    /** order by aggregate values of table "auth.providers" */
    ['auth_providers_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['auth_providers_max_order_by'];
        min?: GraphQLTypes['auth_providers_min_order_by'];
    };
    /** input type for inserting array relation for remote table "auth.providers" */
    ['auth_providers_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['auth_providers_insert_input']>;
        on_conflict?: GraphQLTypes['auth_providers_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.providers". All fields are combined with a logical 'AND'. */
    ['auth_providers_bool_exp']: {
        _and?: Array<GraphQLTypes['auth_providers_bool_exp'] | undefined>;
        _not?: GraphQLTypes['auth_providers_bool_exp'];
        _or?: Array<GraphQLTypes['auth_providers_bool_exp'] | undefined>;
        account_providers?: GraphQLTypes['auth_account_providers_bool_exp'];
        provider?: GraphQLTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "auth.providers" */
    ['auth_providers_constraint']: auth_providers_constraint;
    /** input type for inserting data into table "auth.providers" */
    ['auth_providers_insert_input']: {
        account_providers?: GraphQLTypes['auth_account_providers_arr_rel_insert_input'];
        provider?: string;
    };
    /** aggregate max on columns */
    ['auth_providers_max_fields']: {
        __typename: 'auth_providers_max_fields';
        provider?: string;
    };
    /** order by max() on columns of table "auth.providers" */
    ['auth_providers_max_order_by']: {
        provider?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_providers_min_fields']: {
        __typename: 'auth_providers_min_fields';
        provider?: string;
    };
    /** order by min() on columns of table "auth.providers" */
    ['auth_providers_min_order_by']: {
        provider?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "auth.providers" */
    ['auth_providers_mutation_response']: {
        __typename: 'auth_providers_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['auth_providers']>;
    };
    /** input type for inserting object relation for remote table "auth.providers" */
    ['auth_providers_obj_rel_insert_input']: {
        data: GraphQLTypes['auth_providers_insert_input'];
        on_conflict?: GraphQLTypes['auth_providers_on_conflict'];
    };
    /** on conflict condition type for table "auth.providers" */
    ['auth_providers_on_conflict']: {
        constraint: GraphQLTypes['auth_providers_constraint'];
        update_columns: Array<GraphQLTypes['auth_providers_update_column']>;
        where?: GraphQLTypes['auth_providers_bool_exp'];
    };
    /** ordering options when selecting data from "auth.providers" */
    ['auth_providers_order_by']: {
        account_providers_aggregate?: GraphQLTypes['auth_account_providers_aggregate_order_by'];
        provider?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "auth.providers" */
    ['auth_providers_pk_columns_input']: {
        provider: string;
    };
    /** select columns of table "auth.providers" */
    ['auth_providers_select_column']: auth_providers_select_column;
    /** input type for updating data in table "auth.providers" */
    ['auth_providers_set_input']: {
        provider?: string;
    };
    /** update columns of table "auth.providers" */
    ['auth_providers_update_column']: auth_providers_update_column;
    /** columns and relationships of "auth.refresh_tokens" */
    ['auth_refresh_tokens']: {
        __typename: 'auth_refresh_tokens';
        /** An object relationship */
        account: GraphQLTypes['auth_accounts'];
        account_id: GraphQLTypes['uuid'];
        created_at: GraphQLTypes['timestamptz'];
        expires_at: GraphQLTypes['timestamptz'];
        refresh_token: GraphQLTypes['uuid'];
    };
    /** aggregated selection of "auth.refresh_tokens" */
    ['auth_refresh_tokens_aggregate']: {
        __typename: 'auth_refresh_tokens_aggregate';
        aggregate?: GraphQLTypes['auth_refresh_tokens_aggregate_fields'];
        nodes: Array<GraphQLTypes['auth_refresh_tokens']>;
    };
    /** aggregate fields of "auth.refresh_tokens" */
    ['auth_refresh_tokens_aggregate_fields']: {
        __typename: 'auth_refresh_tokens_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['auth_refresh_tokens_max_fields'];
        min?: GraphQLTypes['auth_refresh_tokens_min_fields'];
    };
    /** order by aggregate values of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['auth_refresh_tokens_max_order_by'];
        min?: GraphQLTypes['auth_refresh_tokens_min_order_by'];
    };
    /** input type for inserting array relation for remote table "auth.refresh_tokens" */
    ['auth_refresh_tokens_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['auth_refresh_tokens_insert_input']>;
        on_conflict?: GraphQLTypes['auth_refresh_tokens_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.refresh_tokens". All fields are combined with a logical 'AND'. */
    ['auth_refresh_tokens_bool_exp']: {
        _and?: Array<GraphQLTypes['auth_refresh_tokens_bool_exp'] | undefined>;
        _not?: GraphQLTypes['auth_refresh_tokens_bool_exp'];
        _or?: Array<GraphQLTypes['auth_refresh_tokens_bool_exp'] | undefined>;
        account?: GraphQLTypes['auth_accounts_bool_exp'];
        account_id?: GraphQLTypes['uuid_comparison_exp'];
        created_at?: GraphQLTypes['timestamptz_comparison_exp'];
        expires_at?: GraphQLTypes['timestamptz_comparison_exp'];
        refresh_token?: GraphQLTypes['uuid_comparison_exp'];
    };
    /** unique or primary key constraints on table "auth.refresh_tokens" */
    ['auth_refresh_tokens_constraint']: auth_refresh_tokens_constraint;
    /** input type for inserting data into table "auth.refresh_tokens" */
    ['auth_refresh_tokens_insert_input']: {
        account?: GraphQLTypes['auth_accounts_obj_rel_insert_input'];
        account_id?: GraphQLTypes['uuid'];
        created_at?: GraphQLTypes['timestamptz'];
        expires_at?: GraphQLTypes['timestamptz'];
        refresh_token?: GraphQLTypes['uuid'];
    };
    /** aggregate max on columns */
    ['auth_refresh_tokens_max_fields']: {
        __typename: 'auth_refresh_tokens_max_fields';
        account_id?: GraphQLTypes['uuid'];
        created_at?: GraphQLTypes['timestamptz'];
        expires_at?: GraphQLTypes['timestamptz'];
        refresh_token?: GraphQLTypes['uuid'];
    };
    /** order by max() on columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_max_order_by']: {
        account_id?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        expires_at?: GraphQLTypes['order_by'];
        refresh_token?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_refresh_tokens_min_fields']: {
        __typename: 'auth_refresh_tokens_min_fields';
        account_id?: GraphQLTypes['uuid'];
        created_at?: GraphQLTypes['timestamptz'];
        expires_at?: GraphQLTypes['timestamptz'];
        refresh_token?: GraphQLTypes['uuid'];
    };
    /** order by min() on columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_min_order_by']: {
        account_id?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        expires_at?: GraphQLTypes['order_by'];
        refresh_token?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "auth.refresh_tokens" */
    ['auth_refresh_tokens_mutation_response']: {
        __typename: 'auth_refresh_tokens_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['auth_refresh_tokens']>;
    };
    /** input type for inserting object relation for remote table "auth.refresh_tokens" */
    ['auth_refresh_tokens_obj_rel_insert_input']: {
        data: GraphQLTypes['auth_refresh_tokens_insert_input'];
        on_conflict?: GraphQLTypes['auth_refresh_tokens_on_conflict'];
    };
    /** on conflict condition type for table "auth.refresh_tokens" */
    ['auth_refresh_tokens_on_conflict']: {
        constraint: GraphQLTypes['auth_refresh_tokens_constraint'];
        update_columns: Array<
            GraphQLTypes['auth_refresh_tokens_update_column']
        >;
        where?: GraphQLTypes['auth_refresh_tokens_bool_exp'];
    };
    /** ordering options when selecting data from "auth.refresh_tokens" */
    ['auth_refresh_tokens_order_by']: {
        account?: GraphQLTypes['auth_accounts_order_by'];
        account_id?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        expires_at?: GraphQLTypes['order_by'];
        refresh_token?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "auth.refresh_tokens" */
    ['auth_refresh_tokens_pk_columns_input']: {
        refresh_token: GraphQLTypes['uuid'];
    };
    /** select columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_select_column']: auth_refresh_tokens_select_column;
    /** input type for updating data in table "auth.refresh_tokens" */
    ['auth_refresh_tokens_set_input']: {
        account_id?: GraphQLTypes['uuid'];
        created_at?: GraphQLTypes['timestamptz'];
        expires_at?: GraphQLTypes['timestamptz'];
        refresh_token?: GraphQLTypes['uuid'];
    };
    /** update columns of table "auth.refresh_tokens" */
    ['auth_refresh_tokens_update_column']: auth_refresh_tokens_update_column;
    /** columns and relationships of "auth.roles" */
    ['auth_roles']: {
        __typename: 'auth_roles';
        /** An array relationship */
        account_roles: Array<GraphQLTypes['auth_account_roles']>;
        /** An aggregated array relationship */
        account_roles_aggregate: GraphQLTypes['auth_account_roles_aggregate'];
        /** An array relationship */
        accounts: Array<GraphQLTypes['auth_accounts']>;
        /** An aggregated array relationship */
        accounts_aggregate: GraphQLTypes['auth_accounts_aggregate'];
        role: string;
    };
    /** aggregated selection of "auth.roles" */
    ['auth_roles_aggregate']: {
        __typename: 'auth_roles_aggregate';
        aggregate?: GraphQLTypes['auth_roles_aggregate_fields'];
        nodes: Array<GraphQLTypes['auth_roles']>;
    };
    /** aggregate fields of "auth.roles" */
    ['auth_roles_aggregate_fields']: {
        __typename: 'auth_roles_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['auth_roles_max_fields'];
        min?: GraphQLTypes['auth_roles_min_fields'];
    };
    /** order by aggregate values of table "auth.roles" */
    ['auth_roles_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['auth_roles_max_order_by'];
        min?: GraphQLTypes['auth_roles_min_order_by'];
    };
    /** input type for inserting array relation for remote table "auth.roles" */
    ['auth_roles_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['auth_roles_insert_input']>;
        on_conflict?: GraphQLTypes['auth_roles_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "auth.roles". All fields are combined with a logical 'AND'. */
    ['auth_roles_bool_exp']: {
        _and?: Array<GraphQLTypes['auth_roles_bool_exp'] | undefined>;
        _not?: GraphQLTypes['auth_roles_bool_exp'];
        _or?: Array<GraphQLTypes['auth_roles_bool_exp'] | undefined>;
        account_roles?: GraphQLTypes['auth_account_roles_bool_exp'];
        accounts?: GraphQLTypes['auth_accounts_bool_exp'];
        role?: GraphQLTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "auth.roles" */
    ['auth_roles_constraint']: auth_roles_constraint;
    /** input type for inserting data into table "auth.roles" */
    ['auth_roles_insert_input']: {
        account_roles?: GraphQLTypes['auth_account_roles_arr_rel_insert_input'];
        accounts?: GraphQLTypes['auth_accounts_arr_rel_insert_input'];
        role?: string;
    };
    /** aggregate max on columns */
    ['auth_roles_max_fields']: {
        __typename: 'auth_roles_max_fields';
        role?: string;
    };
    /** order by max() on columns of table "auth.roles" */
    ['auth_roles_max_order_by']: {
        role?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['auth_roles_min_fields']: {
        __typename: 'auth_roles_min_fields';
        role?: string;
    };
    /** order by min() on columns of table "auth.roles" */
    ['auth_roles_min_order_by']: {
        role?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "auth.roles" */
    ['auth_roles_mutation_response']: {
        __typename: 'auth_roles_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['auth_roles']>;
    };
    /** input type for inserting object relation for remote table "auth.roles" */
    ['auth_roles_obj_rel_insert_input']: {
        data: GraphQLTypes['auth_roles_insert_input'];
        on_conflict?: GraphQLTypes['auth_roles_on_conflict'];
    };
    /** on conflict condition type for table "auth.roles" */
    ['auth_roles_on_conflict']: {
        constraint: GraphQLTypes['auth_roles_constraint'];
        update_columns: Array<GraphQLTypes['auth_roles_update_column']>;
        where?: GraphQLTypes['auth_roles_bool_exp'];
    };
    /** ordering options when selecting data from "auth.roles" */
    ['auth_roles_order_by']: {
        account_roles_aggregate?: GraphQLTypes['auth_account_roles_aggregate_order_by'];
        accounts_aggregate?: GraphQLTypes['auth_accounts_aggregate_order_by'];
        role?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "auth.roles" */
    ['auth_roles_pk_columns_input']: {
        role: string;
    };
    /** select columns of table "auth.roles" */
    ['auth_roles_select_column']: auth_roles_select_column;
    /** input type for updating data in table "auth.roles" */
    ['auth_roles_set_input']: {
        role?: string;
    };
    /** update columns of table "auth.roles" */
    ['auth_roles_update_column']: auth_roles_update_column;
    ['citext']: any;
    /** expression to compare columns of type citext. All fields are combined with logical 'AND'. */
    ['citext_comparison_exp']: {
        _eq?: GraphQLTypes['citext'];
        _gt?: GraphQLTypes['citext'];
        _gte?: GraphQLTypes['citext'];
        _ilike?: string;
        _in?: Array<GraphQLTypes['citext']>;
        _is_null?: boolean;
        _like?: string;
        _lt?: GraphQLTypes['citext'];
        _lte?: GraphQLTypes['citext'];
        _neq?: GraphQLTypes['citext'];
        _nilike?: string;
        _nin?: Array<GraphQLTypes['citext']>;
        _nlike?: string;
        _nsimilar?: string;
        _similar?: string;
    };
    /** columns and relationships of "contribution_types" */
    ['contribution_types']: {
        __typename: 'contribution_types';
        description: string;
        /** An array relationship */
        groups: Array<GraphQLTypes['groups']>;
        /** An aggregated array relationship */
        groups_aggregate: GraphQLTypes['groups_aggregate'];
        value: string;
    };
    /** aggregated selection of "contribution_types" */
    ['contribution_types_aggregate']: {
        __typename: 'contribution_types_aggregate';
        aggregate?: GraphQLTypes['contribution_types_aggregate_fields'];
        nodes: Array<GraphQLTypes['contribution_types']>;
    };
    /** aggregate fields of "contribution_types" */
    ['contribution_types_aggregate_fields']: {
        __typename: 'contribution_types_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['contribution_types_max_fields'];
        min?: GraphQLTypes['contribution_types_min_fields'];
    };
    /** order by aggregate values of table "contribution_types" */
    ['contribution_types_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['contribution_types_max_order_by'];
        min?: GraphQLTypes['contribution_types_min_order_by'];
    };
    /** input type for inserting array relation for remote table "contribution_types" */
    ['contribution_types_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['contribution_types_insert_input']>;
        on_conflict?: GraphQLTypes['contribution_types_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "contribution_types". All fields are combined with a logical 'AND'. */
    ['contribution_types_bool_exp']: {
        _and?: Array<GraphQLTypes['contribution_types_bool_exp'] | undefined>;
        _not?: GraphQLTypes['contribution_types_bool_exp'];
        _or?: Array<GraphQLTypes['contribution_types_bool_exp'] | undefined>;
        description?: GraphQLTypes['String_comparison_exp'];
        groups?: GraphQLTypes['groups_bool_exp'];
        value?: GraphQLTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "contribution_types" */
    ['contribution_types_constraint']: contribution_types_constraint;
    ['contribution_types_enum']: contribution_types_enum;
    /** expression to compare columns of type contribution_types_enum. All fields are combined with logical 'AND'. */
    ['contribution_types_enum_comparison_exp']: {
        _eq?: GraphQLTypes['contribution_types_enum'];
        _in?: Array<GraphQLTypes['contribution_types_enum']>;
        _is_null?: boolean;
        _neq?: GraphQLTypes['contribution_types_enum'];
        _nin?: Array<GraphQLTypes['contribution_types_enum']>;
    };
    /** input type for inserting data into table "contribution_types" */
    ['contribution_types_insert_input']: {
        description?: string;
        groups?: GraphQLTypes['groups_arr_rel_insert_input'];
        value?: string;
    };
    /** aggregate max on columns */
    ['contribution_types_max_fields']: {
        __typename: 'contribution_types_max_fields';
        description?: string;
        value?: string;
    };
    /** order by max() on columns of table "contribution_types" */
    ['contribution_types_max_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['contribution_types_min_fields']: {
        __typename: 'contribution_types_min_fields';
        description?: string;
        value?: string;
    };
    /** order by min() on columns of table "contribution_types" */
    ['contribution_types_min_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "contribution_types" */
    ['contribution_types_mutation_response']: {
        __typename: 'contribution_types_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['contribution_types']>;
    };
    /** input type for inserting object relation for remote table "contribution_types" */
    ['contribution_types_obj_rel_insert_input']: {
        data: GraphQLTypes['contribution_types_insert_input'];
        on_conflict?: GraphQLTypes['contribution_types_on_conflict'];
    };
    /** on conflict condition type for table "contribution_types" */
    ['contribution_types_on_conflict']: {
        constraint: GraphQLTypes['contribution_types_constraint'];
        update_columns: Array<GraphQLTypes['contribution_types_update_column']>;
        where?: GraphQLTypes['contribution_types_bool_exp'];
    };
    /** ordering options when selecting data from "contribution_types" */
    ['contribution_types_order_by']: {
        description?: GraphQLTypes['order_by'];
        groups_aggregate?: GraphQLTypes['groups_aggregate_order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "contribution_types" */
    ['contribution_types_pk_columns_input']: {
        value: string;
    };
    /** select columns of table "contribution_types" */
    ['contribution_types_select_column']: contribution_types_select_column;
    /** input type for updating data in table "contribution_types" */
    ['contribution_types_set_input']: {
        description?: string;
        value?: string;
    };
    /** update columns of table "contribution_types" */
    ['contribution_types_update_column']: contribution_types_update_column;
    /** columns and relationships of "group_cycles" */
    ['group_cycles']: {
        __typename: 'group_cycles';
        description: string;
        value: string;
    };
    /** aggregated selection of "group_cycles" */
    ['group_cycles_aggregate']: {
        __typename: 'group_cycles_aggregate';
        aggregate?: GraphQLTypes['group_cycles_aggregate_fields'];
        nodes: Array<GraphQLTypes['group_cycles']>;
    };
    /** aggregate fields of "group_cycles" */
    ['group_cycles_aggregate_fields']: {
        __typename: 'group_cycles_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['group_cycles_max_fields'];
        min?: GraphQLTypes['group_cycles_min_fields'];
    };
    /** order by aggregate values of table "group_cycles" */
    ['group_cycles_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['group_cycles_max_order_by'];
        min?: GraphQLTypes['group_cycles_min_order_by'];
    };
    /** input type for inserting array relation for remote table "group_cycles" */
    ['group_cycles_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['group_cycles_insert_input']>;
        on_conflict?: GraphQLTypes['group_cycles_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "group_cycles". All fields are combined with a logical 'AND'. */
    ['group_cycles_bool_exp']: {
        _and?: Array<GraphQLTypes['group_cycles_bool_exp'] | undefined>;
        _not?: GraphQLTypes['group_cycles_bool_exp'];
        _or?: Array<GraphQLTypes['group_cycles_bool_exp'] | undefined>;
        description?: GraphQLTypes['String_comparison_exp'];
        value?: GraphQLTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "group_cycles" */
    ['group_cycles_constraint']: group_cycles_constraint;
    /** input type for inserting data into table "group_cycles" */
    ['group_cycles_insert_input']: {
        description?: string;
        value?: string;
    };
    /** aggregate max on columns */
    ['group_cycles_max_fields']: {
        __typename: 'group_cycles_max_fields';
        description?: string;
        value?: string;
    };
    /** order by max() on columns of table "group_cycles" */
    ['group_cycles_max_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['group_cycles_min_fields']: {
        __typename: 'group_cycles_min_fields';
        description?: string;
        value?: string;
    };
    /** order by min() on columns of table "group_cycles" */
    ['group_cycles_min_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "group_cycles" */
    ['group_cycles_mutation_response']: {
        __typename: 'group_cycles_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['group_cycles']>;
    };
    /** input type for inserting object relation for remote table "group_cycles" */
    ['group_cycles_obj_rel_insert_input']: {
        data: GraphQLTypes['group_cycles_insert_input'];
        on_conflict?: GraphQLTypes['group_cycles_on_conflict'];
    };
    /** on conflict condition type for table "group_cycles" */
    ['group_cycles_on_conflict']: {
        constraint: GraphQLTypes['group_cycles_constraint'];
        update_columns: Array<GraphQLTypes['group_cycles_update_column']>;
        where?: GraphQLTypes['group_cycles_bool_exp'];
    };
    /** ordering options when selecting data from "group_cycles" */
    ['group_cycles_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "group_cycles" */
    ['group_cycles_pk_columns_input']: {
        value: string;
    };
    /** select columns of table "group_cycles" */
    ['group_cycles_select_column']: group_cycles_select_column;
    /** input type for updating data in table "group_cycles" */
    ['group_cycles_set_input']: {
        description?: string;
        value?: string;
    };
    /** update columns of table "group_cycles" */
    ['group_cycles_update_column']: group_cycles_update_column;
    /** columns and relationships of "group_recurrencies" */
    ['group_recurrencies']: {
        __typename: 'group_recurrencies';
        description: string;
        /** An array relationship */
        groups: Array<GraphQLTypes['groups']>;
        /** An aggregated array relationship */
        groups_aggregate: GraphQLTypes['groups_aggregate'];
        value: string;
    };
    /** aggregated selection of "group_recurrencies" */
    ['group_recurrencies_aggregate']: {
        __typename: 'group_recurrencies_aggregate';
        aggregate?: GraphQLTypes['group_recurrencies_aggregate_fields'];
        nodes: Array<GraphQLTypes['group_recurrencies']>;
    };
    /** aggregate fields of "group_recurrencies" */
    ['group_recurrencies_aggregate_fields']: {
        __typename: 'group_recurrencies_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['group_recurrencies_max_fields'];
        min?: GraphQLTypes['group_recurrencies_min_fields'];
    };
    /** order by aggregate values of table "group_recurrencies" */
    ['group_recurrencies_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['group_recurrencies_max_order_by'];
        min?: GraphQLTypes['group_recurrencies_min_order_by'];
    };
    /** input type for inserting array relation for remote table "group_recurrencies" */
    ['group_recurrencies_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['group_recurrencies_insert_input']>;
        on_conflict?: GraphQLTypes['group_recurrencies_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "group_recurrencies". All fields are combined with a logical 'AND'. */
    ['group_recurrencies_bool_exp']: {
        _and?: Array<GraphQLTypes['group_recurrencies_bool_exp'] | undefined>;
        _not?: GraphQLTypes['group_recurrencies_bool_exp'];
        _or?: Array<GraphQLTypes['group_recurrencies_bool_exp'] | undefined>;
        description?: GraphQLTypes['String_comparison_exp'];
        groups?: GraphQLTypes['groups_bool_exp'];
        value?: GraphQLTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "group_recurrencies" */
    ['group_recurrencies_constraint']: group_recurrencies_constraint;
    ['group_recurrencies_enum']: group_recurrencies_enum;
    /** expression to compare columns of type group_recurrencies_enum. All fields are combined with logical 'AND'. */
    ['group_recurrencies_enum_comparison_exp']: {
        _eq?: GraphQLTypes['group_recurrencies_enum'];
        _in?: Array<GraphQLTypes['group_recurrencies_enum']>;
        _is_null?: boolean;
        _neq?: GraphQLTypes['group_recurrencies_enum'];
        _nin?: Array<GraphQLTypes['group_recurrencies_enum']>;
    };
    /** input type for inserting data into table "group_recurrencies" */
    ['group_recurrencies_insert_input']: {
        description?: string;
        groups?: GraphQLTypes['groups_arr_rel_insert_input'];
        value?: string;
    };
    /** aggregate max on columns */
    ['group_recurrencies_max_fields']: {
        __typename: 'group_recurrencies_max_fields';
        description?: string;
        value?: string;
    };
    /** order by max() on columns of table "group_recurrencies" */
    ['group_recurrencies_max_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['group_recurrencies_min_fields']: {
        __typename: 'group_recurrencies_min_fields';
        description?: string;
        value?: string;
    };
    /** order by min() on columns of table "group_recurrencies" */
    ['group_recurrencies_min_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "group_recurrencies" */
    ['group_recurrencies_mutation_response']: {
        __typename: 'group_recurrencies_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['group_recurrencies']>;
    };
    /** input type for inserting object relation for remote table "group_recurrencies" */
    ['group_recurrencies_obj_rel_insert_input']: {
        data: GraphQLTypes['group_recurrencies_insert_input'];
        on_conflict?: GraphQLTypes['group_recurrencies_on_conflict'];
    };
    /** on conflict condition type for table "group_recurrencies" */
    ['group_recurrencies_on_conflict']: {
        constraint: GraphQLTypes['group_recurrencies_constraint'];
        update_columns: Array<GraphQLTypes['group_recurrencies_update_column']>;
        where?: GraphQLTypes['group_recurrencies_bool_exp'];
    };
    /** ordering options when selecting data from "group_recurrencies" */
    ['group_recurrencies_order_by']: {
        description?: GraphQLTypes['order_by'];
        groups_aggregate?: GraphQLTypes['groups_aggregate_order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "group_recurrencies" */
    ['group_recurrencies_pk_columns_input']: {
        value: string;
    };
    /** select columns of table "group_recurrencies" */
    ['group_recurrencies_select_column']: group_recurrencies_select_column;
    /** input type for updating data in table "group_recurrencies" */
    ['group_recurrencies_set_input']: {
        description?: string;
        value?: string;
    };
    /** update columns of table "group_recurrencies" */
    ['group_recurrencies_update_column']: group_recurrencies_update_column;
    /** columns and relationships of "groups" */
    ['groups']: {
        __typename: 'groups';
        /** An object relationship */
        contribution_type: GraphQLTypes['contribution_types'];
        created_at: GraphQLTypes['timestamp'];
        /** An object relationship */
        creator: GraphQLTypes['users'];
        creator_id: GraphQLTypes['uuid'];
        group_balance: GraphQLTypes['numeric'];
        group_code: string;
        group_contribution_amount?: GraphQLTypes['numeric'];
        group_contribution_type: GraphQLTypes['contribution_types_enum'];
        group_name: string;
        group_recurrency_amount?: GraphQLTypes['numeric'];
        group_recurrency_day?: number;
        group_recurrency_type: GraphQLTypes['group_recurrencies_enum'];
        id: GraphQLTypes['uuid'];
        /** An array relationship */
        members: Array<GraphQLTypes['members']>;
        /** An aggregated array relationship */
        members_aggregate: GraphQLTypes['members_aggregate'];
        /** An object relationship */
        payment_frequency: GraphQLTypes['group_recurrencies'];
        /** An array relationship */
        payments: Array<GraphQLTypes['payments']>;
        /** An aggregated array relationship */
        payments_aggregate: GraphQLTypes['payments_aggregate'];
        /** An array relationship */
        periods: Array<GraphQLTypes['periods']>;
        /** An aggregated array relationship */
        periods_aggregate: GraphQLTypes['periods_aggregate'];
        updated_at: GraphQLTypes['timestamptz'];
    };
    /** aggregated selection of "groups" */
    ['groups_aggregate']: {
        __typename: 'groups_aggregate';
        aggregate?: GraphQLTypes['groups_aggregate_fields'];
        nodes: Array<GraphQLTypes['groups']>;
    };
    /** aggregate fields of "groups" */
    ['groups_aggregate_fields']: {
        __typename: 'groups_aggregate_fields';
        avg?: GraphQLTypes['groups_avg_fields'];
        count?: number;
        max?: GraphQLTypes['groups_max_fields'];
        min?: GraphQLTypes['groups_min_fields'];
        stddev?: GraphQLTypes['groups_stddev_fields'];
        stddev_pop?: GraphQLTypes['groups_stddev_pop_fields'];
        stddev_samp?: GraphQLTypes['groups_stddev_samp_fields'];
        sum?: GraphQLTypes['groups_sum_fields'];
        var_pop?: GraphQLTypes['groups_var_pop_fields'];
        var_samp?: GraphQLTypes['groups_var_samp_fields'];
        variance?: GraphQLTypes['groups_variance_fields'];
    };
    /** order by aggregate values of table "groups" */
    ['groups_aggregate_order_by']: {
        avg?: GraphQLTypes['groups_avg_order_by'];
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['groups_max_order_by'];
        min?: GraphQLTypes['groups_min_order_by'];
        stddev?: GraphQLTypes['groups_stddev_order_by'];
        stddev_pop?: GraphQLTypes['groups_stddev_pop_order_by'];
        stddev_samp?: GraphQLTypes['groups_stddev_samp_order_by'];
        sum?: GraphQLTypes['groups_sum_order_by'];
        var_pop?: GraphQLTypes['groups_var_pop_order_by'];
        var_samp?: GraphQLTypes['groups_var_samp_order_by'];
        variance?: GraphQLTypes['groups_variance_order_by'];
    };
    /** input type for inserting array relation for remote table "groups" */
    ['groups_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['groups_insert_input']>;
        on_conflict?: GraphQLTypes['groups_on_conflict'];
    };
    /** aggregate avg on columns */
    ['groups_avg_fields']: {
        __typename: 'groups_avg_fields';
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by avg() on columns of table "groups" */
    ['groups_avg_order_by']: {
        group_balance?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
    };
    /** Boolean expression to filter rows from the table "groups". All fields are combined with a logical 'AND'. */
    ['groups_bool_exp']: {
        _and?: Array<GraphQLTypes['groups_bool_exp'] | undefined>;
        _not?: GraphQLTypes['groups_bool_exp'];
        _or?: Array<GraphQLTypes['groups_bool_exp'] | undefined>;
        contribution_type?: GraphQLTypes['contribution_types_bool_exp'];
        created_at?: GraphQLTypes['timestamp_comparison_exp'];
        creator?: GraphQLTypes['users_bool_exp'];
        creator_id?: GraphQLTypes['uuid_comparison_exp'];
        group_balance?: GraphQLTypes['numeric_comparison_exp'];
        group_code?: GraphQLTypes['String_comparison_exp'];
        group_contribution_amount?: GraphQLTypes['numeric_comparison_exp'];
        group_contribution_type?: GraphQLTypes['contribution_types_enum_comparison_exp'];
        group_name?: GraphQLTypes['String_comparison_exp'];
        group_recurrency_amount?: GraphQLTypes['numeric_comparison_exp'];
        group_recurrency_day?: GraphQLTypes['Int_comparison_exp'];
        group_recurrency_type?: GraphQLTypes['group_recurrencies_enum_comparison_exp'];
        id?: GraphQLTypes['uuid_comparison_exp'];
        members?: GraphQLTypes['members_bool_exp'];
        payment_frequency?: GraphQLTypes['group_recurrencies_bool_exp'];
        payments?: GraphQLTypes['payments_bool_exp'];
        periods?: GraphQLTypes['periods_bool_exp'];
        updated_at?: GraphQLTypes['timestamptz_comparison_exp'];
    };
    /** unique or primary key constraints on table "groups" */
    ['groups_constraint']: groups_constraint;
    /** input type for incrementing integer column in table "groups" */
    ['groups_inc_input']: {
        group_balance?: GraphQLTypes['numeric'];
        group_contribution_amount?: GraphQLTypes['numeric'];
        group_recurrency_amount?: GraphQLTypes['numeric'];
        group_recurrency_day?: number;
    };
    /** input type for inserting data into table "groups" */
    ['groups_insert_input']: {
        contribution_type?: GraphQLTypes['contribution_types_obj_rel_insert_input'];
        created_at?: GraphQLTypes['timestamp'];
        creator?: GraphQLTypes['users_obj_rel_insert_input'];
        creator_id?: GraphQLTypes['uuid'];
        group_balance?: GraphQLTypes['numeric'];
        group_code?: string;
        group_contribution_amount?: GraphQLTypes['numeric'];
        group_contribution_type?: GraphQLTypes['contribution_types_enum'];
        group_name?: string;
        group_recurrency_amount?: GraphQLTypes['numeric'];
        group_recurrency_day?: number;
        group_recurrency_type?: GraphQLTypes['group_recurrencies_enum'];
        id?: GraphQLTypes['uuid'];
        members?: GraphQLTypes['members_arr_rel_insert_input'];
        payment_frequency?: GraphQLTypes['group_recurrencies_obj_rel_insert_input'];
        payments?: GraphQLTypes['payments_arr_rel_insert_input'];
        periods?: GraphQLTypes['periods_arr_rel_insert_input'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** aggregate max on columns */
    ['groups_max_fields']: {
        __typename: 'groups_max_fields';
        created_at?: GraphQLTypes['timestamp'];
        creator_id?: GraphQLTypes['uuid'];
        group_balance?: GraphQLTypes['numeric'];
        group_code?: string;
        group_contribution_amount?: GraphQLTypes['numeric'];
        group_name?: string;
        group_recurrency_amount?: GraphQLTypes['numeric'];
        group_recurrency_day?: number;
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** order by max() on columns of table "groups" */
    ['groups_max_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        creator_id?: GraphQLTypes['order_by'];
        group_balance?: GraphQLTypes['order_by'];
        group_code?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_name?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['groups_min_fields']: {
        __typename: 'groups_min_fields';
        created_at?: GraphQLTypes['timestamp'];
        creator_id?: GraphQLTypes['uuid'];
        group_balance?: GraphQLTypes['numeric'];
        group_code?: string;
        group_contribution_amount?: GraphQLTypes['numeric'];
        group_name?: string;
        group_recurrency_amount?: GraphQLTypes['numeric'];
        group_recurrency_day?: number;
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** order by min() on columns of table "groups" */
    ['groups_min_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        creator_id?: GraphQLTypes['order_by'];
        group_balance?: GraphQLTypes['order_by'];
        group_code?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_name?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "groups" */
    ['groups_mutation_response']: {
        __typename: 'groups_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['groups']>;
    };
    /** input type for inserting object relation for remote table "groups" */
    ['groups_obj_rel_insert_input']: {
        data: GraphQLTypes['groups_insert_input'];
        on_conflict?: GraphQLTypes['groups_on_conflict'];
    };
    /** on conflict condition type for table "groups" */
    ['groups_on_conflict']: {
        constraint: GraphQLTypes['groups_constraint'];
        update_columns: Array<GraphQLTypes['groups_update_column']>;
        where?: GraphQLTypes['groups_bool_exp'];
    };
    /** ordering options when selecting data from "groups" */
    ['groups_order_by']: {
        contribution_type?: GraphQLTypes['contribution_types_order_by'];
        created_at?: GraphQLTypes['order_by'];
        creator?: GraphQLTypes['users_order_by'];
        creator_id?: GraphQLTypes['order_by'];
        group_balance?: GraphQLTypes['order_by'];
        group_code?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_contribution_type?: GraphQLTypes['order_by'];
        group_name?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
        group_recurrency_type?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        members_aggregate?: GraphQLTypes['members_aggregate_order_by'];
        payment_frequency?: GraphQLTypes['group_recurrencies_order_by'];
        payments_aggregate?: GraphQLTypes['payments_aggregate_order_by'];
        periods_aggregate?: GraphQLTypes['periods_aggregate_order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "groups" */
    ['groups_pk_columns_input']: {
        id: GraphQLTypes['uuid'];
    };
    /** select columns of table "groups" */
    ['groups_select_column']: groups_select_column;
    /** input type for updating data in table "groups" */
    ['groups_set_input']: {
        created_at?: GraphQLTypes['timestamp'];
        creator_id?: GraphQLTypes['uuid'];
        group_balance?: GraphQLTypes['numeric'];
        group_code?: string;
        group_contribution_amount?: GraphQLTypes['numeric'];
        group_contribution_type?: GraphQLTypes['contribution_types_enum'];
        group_name?: string;
        group_recurrency_amount?: GraphQLTypes['numeric'];
        group_recurrency_day?: number;
        group_recurrency_type?: GraphQLTypes['group_recurrencies_enum'];
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** aggregate stddev on columns */
    ['groups_stddev_fields']: {
        __typename: 'groups_stddev_fields';
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by stddev() on columns of table "groups" */
    ['groups_stddev_order_by']: {
        group_balance?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
    };
    /** aggregate stddev_pop on columns */
    ['groups_stddev_pop_fields']: {
        __typename: 'groups_stddev_pop_fields';
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by stddev_pop() on columns of table "groups" */
    ['groups_stddev_pop_order_by']: {
        group_balance?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
    };
    /** aggregate stddev_samp on columns */
    ['groups_stddev_samp_fields']: {
        __typename: 'groups_stddev_samp_fields';
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by stddev_samp() on columns of table "groups" */
    ['groups_stddev_samp_order_by']: {
        group_balance?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
    };
    /** aggregate sum on columns */
    ['groups_sum_fields']: {
        __typename: 'groups_sum_fields';
        group_balance?: GraphQLTypes['numeric'];
        group_contribution_amount?: GraphQLTypes['numeric'];
        group_recurrency_amount?: GraphQLTypes['numeric'];
        group_recurrency_day?: number;
    };
    /** order by sum() on columns of table "groups" */
    ['groups_sum_order_by']: {
        group_balance?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
    };
    /** update columns of table "groups" */
    ['groups_update_column']: groups_update_column;
    /** aggregate var_pop on columns */
    ['groups_var_pop_fields']: {
        __typename: 'groups_var_pop_fields';
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by var_pop() on columns of table "groups" */
    ['groups_var_pop_order_by']: {
        group_balance?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
    };
    /** aggregate var_samp on columns */
    ['groups_var_samp_fields']: {
        __typename: 'groups_var_samp_fields';
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by var_samp() on columns of table "groups" */
    ['groups_var_samp_order_by']: {
        group_balance?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
    };
    /** aggregate variance on columns */
    ['groups_variance_fields']: {
        __typename: 'groups_variance_fields';
        group_balance?: number;
        group_contribution_amount?: number;
        group_recurrency_amount?: number;
        group_recurrency_day?: number;
    };
    /** order by variance() on columns of table "groups" */
    ['groups_variance_order_by']: {
        group_balance?: GraphQLTypes['order_by'];
        group_contribution_amount?: GraphQLTypes['order_by'];
        group_recurrency_amount?: GraphQLTypes['order_by'];
        group_recurrency_day?: GraphQLTypes['order_by'];
    };
    ['json']: any;
    /** expression to compare columns of type json. All fields are combined with logical 'AND'. */
    ['json_comparison_exp']: {
        _eq?: GraphQLTypes['json'];
        _gt?: GraphQLTypes['json'];
        _gte?: GraphQLTypes['json'];
        _in?: Array<GraphQLTypes['json']>;
        _is_null?: boolean;
        _lt?: GraphQLTypes['json'];
        _lte?: GraphQLTypes['json'];
        _neq?: GraphQLTypes['json'];
        _nin?: Array<GraphQLTypes['json']>;
    };
    ['jsonb']: any;
    /** expression to compare columns of type jsonb. All fields are combined with logical 'AND'. */
    ['jsonb_comparison_exp']: {
        /** is the column contained in the given json value */
        _contained_in?: GraphQLTypes['jsonb'];
        /** does the column contain the given json value at the top level */
        _contains?: GraphQLTypes['jsonb'];
        _eq?: GraphQLTypes['jsonb'];
        _gt?: GraphQLTypes['jsonb'];
        _gte?: GraphQLTypes['jsonb'];
        /** does the string exist as a top-level key in the column */
        _has_key?: string;
        /** do all of these strings exist as top-level keys in the column */
        _has_keys_all?: Array<string>;
        /** do any of these strings exist as top-level keys in the column */
        _has_keys_any?: Array<string>;
        _in?: Array<GraphQLTypes['jsonb']>;
        _is_null?: boolean;
        _lt?: GraphQLTypes['jsonb'];
        _lte?: GraphQLTypes['jsonb'];
        _neq?: GraphQLTypes['jsonb'];
        _nin?: Array<GraphQLTypes['jsonb']>;
    };
    /** columns and relationships of "members" */
    ['members']: {
        __typename: 'members';
        created_at: GraphQLTypes['timestamptz'];
        /** An object relationship */
        group: GraphQLTypes['groups'];
        group_id: GraphQLTypes['uuid'];
        id: GraphQLTypes['uuid'];
        /** An array relationship */
        payments: Array<GraphQLTypes['payments']>;
        /** An aggregated array relationship */
        payments_aggregate: GraphQLTypes['payments_aggregate'];
        updated_at: GraphQLTypes['timestamptz'];
        /** An object relationship */
        user: GraphQLTypes['users'];
        user_id: GraphQLTypes['uuid'];
    };
    /** aggregated selection of "members" */
    ['members_aggregate']: {
        __typename: 'members_aggregate';
        aggregate?: GraphQLTypes['members_aggregate_fields'];
        nodes: Array<GraphQLTypes['members']>;
    };
    /** aggregate fields of "members" */
    ['members_aggregate_fields']: {
        __typename: 'members_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['members_max_fields'];
        min?: GraphQLTypes['members_min_fields'];
    };
    /** order by aggregate values of table "members" */
    ['members_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['members_max_order_by'];
        min?: GraphQLTypes['members_min_order_by'];
    };
    /** input type for inserting array relation for remote table "members" */
    ['members_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['members_insert_input']>;
        on_conflict?: GraphQLTypes['members_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "members". All fields are combined with a logical 'AND'. */
    ['members_bool_exp']: {
        _and?: Array<GraphQLTypes['members_bool_exp'] | undefined>;
        _not?: GraphQLTypes['members_bool_exp'];
        _or?: Array<GraphQLTypes['members_bool_exp'] | undefined>;
        created_at?: GraphQLTypes['timestamptz_comparison_exp'];
        group?: GraphQLTypes['groups_bool_exp'];
        group_id?: GraphQLTypes['uuid_comparison_exp'];
        id?: GraphQLTypes['uuid_comparison_exp'];
        payments?: GraphQLTypes['payments_bool_exp'];
        updated_at?: GraphQLTypes['timestamptz_comparison_exp'];
        user?: GraphQLTypes['users_bool_exp'];
        user_id?: GraphQLTypes['uuid_comparison_exp'];
    };
    /** unique or primary key constraints on table "members" */
    ['members_constraint']: members_constraint;
    /** input type for inserting data into table "members" */
    ['members_insert_input']: {
        created_at?: GraphQLTypes['timestamptz'];
        group?: GraphQLTypes['groups_obj_rel_insert_input'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        payments?: GraphQLTypes['payments_arr_rel_insert_input'];
        updated_at?: GraphQLTypes['timestamptz'];
        user?: GraphQLTypes['users_obj_rel_insert_input'];
        user_id?: GraphQLTypes['uuid'];
    };
    /** aggregate max on columns */
    ['members_max_fields']: {
        __typename: 'members_max_fields';
        created_at?: GraphQLTypes['timestamptz'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
        user_id?: GraphQLTypes['uuid'];
    };
    /** order by max() on columns of table "members" */
    ['members_max_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        group_id?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
        user_id?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['members_min_fields']: {
        __typename: 'members_min_fields';
        created_at?: GraphQLTypes['timestamptz'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
        user_id?: GraphQLTypes['uuid'];
    };
    /** order by min() on columns of table "members" */
    ['members_min_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        group_id?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
        user_id?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "members" */
    ['members_mutation_response']: {
        __typename: 'members_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['members']>;
    };
    /** input type for inserting object relation for remote table "members" */
    ['members_obj_rel_insert_input']: {
        data: GraphQLTypes['members_insert_input'];
        on_conflict?: GraphQLTypes['members_on_conflict'];
    };
    /** on conflict condition type for table "members" */
    ['members_on_conflict']: {
        constraint: GraphQLTypes['members_constraint'];
        update_columns: Array<GraphQLTypes['members_update_column']>;
        where?: GraphQLTypes['members_bool_exp'];
    };
    /** ordering options when selecting data from "members" */
    ['members_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        group?: GraphQLTypes['groups_order_by'];
        group_id?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        payments_aggregate?: GraphQLTypes['payments_aggregate_order_by'];
        updated_at?: GraphQLTypes['order_by'];
        user?: GraphQLTypes['users_order_by'];
        user_id?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "members" */
    ['members_pk_columns_input']: {
        id: GraphQLTypes['uuid'];
    };
    /** select columns of table "members" */
    ['members_select_column']: members_select_column;
    /** input type for updating data in table "members" */
    ['members_set_input']: {
        created_at?: GraphQLTypes['timestamptz'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
        user_id?: GraphQLTypes['uuid'];
    };
    /** update columns of table "members" */
    ['members_update_column']: members_update_column;
    /** mutation root */
    ['mutation_root']: {
        __typename: 'mutation_root';
        /** perform the action: "action_create_group" */
        action_create_group?: GraphQLTypes['ActionCreateGroupOutput'];
        /** perform the action: "action_request_payment" */
        action_request_payment?: GraphQLTypes['ActionRequestPaymentOutput'];
        /** perform the action: "action_withdraw_payment" */
        action_withdraw_payment?: GraphQLTypes['ActionWithdrawPaymentOutput'];
        /** delete data from the table: "auth.account_providers" */
        delete_auth_account_providers?: GraphQLTypes['auth_account_providers_mutation_response'];
        /** delete single row from the table: "auth.account_providers" */
        delete_auth_account_providers_by_pk?: GraphQLTypes['auth_account_providers'];
        /** delete data from the table: "auth.account_roles" */
        delete_auth_account_roles?: GraphQLTypes['auth_account_roles_mutation_response'];
        /** delete single row from the table: "auth.account_roles" */
        delete_auth_account_roles_by_pk?: GraphQLTypes['auth_account_roles'];
        /** delete data from the table: "auth.accounts" */
        delete_auth_accounts?: GraphQLTypes['auth_accounts_mutation_response'];
        /** delete single row from the table: "auth.accounts" */
        delete_auth_accounts_by_pk?: GraphQLTypes['auth_accounts'];
        /** delete data from the table: "auth.providers" */
        delete_auth_providers?: GraphQLTypes['auth_providers_mutation_response'];
        /** delete single row from the table: "auth.providers" */
        delete_auth_providers_by_pk?: GraphQLTypes['auth_providers'];
        /** delete data from the table: "auth.refresh_tokens" */
        delete_auth_refresh_tokens?: GraphQLTypes['auth_refresh_tokens_mutation_response'];
        /** delete single row from the table: "auth.refresh_tokens" */
        delete_auth_refresh_tokens_by_pk?: GraphQLTypes['auth_refresh_tokens'];
        /** delete data from the table: "auth.roles" */
        delete_auth_roles?: GraphQLTypes['auth_roles_mutation_response'];
        /** delete single row from the table: "auth.roles" */
        delete_auth_roles_by_pk?: GraphQLTypes['auth_roles'];
        /** delete data from the table: "contribution_types" */
        delete_contribution_types?: GraphQLTypes['contribution_types_mutation_response'];
        /** delete single row from the table: "contribution_types" */
        delete_contribution_types_by_pk?: GraphQLTypes['contribution_types'];
        /** delete data from the table: "group_cycles" */
        delete_group_cycles?: GraphQLTypes['group_cycles_mutation_response'];
        /** delete single row from the table: "group_cycles" */
        delete_group_cycles_by_pk?: GraphQLTypes['group_cycles'];
        /** delete data from the table: "group_recurrencies" */
        delete_group_recurrencies?: GraphQLTypes['group_recurrencies_mutation_response'];
        /** delete single row from the table: "group_recurrencies" */
        delete_group_recurrencies_by_pk?: GraphQLTypes['group_recurrencies'];
        /** delete data from the table: "groups" */
        delete_groups?: GraphQLTypes['groups_mutation_response'];
        /** delete single row from the table: "groups" */
        delete_groups_by_pk?: GraphQLTypes['groups'];
        /** delete data from the table: "members" */
        delete_members?: GraphQLTypes['members_mutation_response'];
        /** delete single row from the table: "members" */
        delete_members_by_pk?: GraphQLTypes['members'];
        /** delete data from the table: "payment_statuses" */
        delete_payment_statuses?: GraphQLTypes['payment_statuses_mutation_response'];
        /** delete single row from the table: "payment_statuses" */
        delete_payment_statuses_by_pk?: GraphQLTypes['payment_statuses'];
        /** delete data from the table: "payment_types" */
        delete_payment_types?: GraphQLTypes['payment_types_mutation_response'];
        /** delete single row from the table: "payment_types" */
        delete_payment_types_by_pk?: GraphQLTypes['payment_types'];
        /** delete data from the table: "payments" */
        delete_payments?: GraphQLTypes['payments_mutation_response'];
        /** delete single row from the table: "payments" */
        delete_payments_by_pk?: GraphQLTypes['payments'];
        /** delete data from the table: "periods" */
        delete_periods?: GraphQLTypes['periods_mutation_response'];
        /** delete single row from the table: "periods" */
        delete_periods_by_pk?: GraphQLTypes['periods'];
        /** delete data from the table: "users" */
        delete_users?: GraphQLTypes['users_mutation_response'];
        /** delete single row from the table: "users" */
        delete_users_by_pk?: GraphQLTypes['users'];
        /** insert data into the table: "auth.account_providers" */
        insert_auth_account_providers?: GraphQLTypes['auth_account_providers_mutation_response'];
        /** insert a single row into the table: "auth.account_providers" */
        insert_auth_account_providers_one?: GraphQLTypes['auth_account_providers'];
        /** insert data into the table: "auth.account_roles" */
        insert_auth_account_roles?: GraphQLTypes['auth_account_roles_mutation_response'];
        /** insert a single row into the table: "auth.account_roles" */
        insert_auth_account_roles_one?: GraphQLTypes['auth_account_roles'];
        /** insert data into the table: "auth.accounts" */
        insert_auth_accounts?: GraphQLTypes['auth_accounts_mutation_response'];
        /** insert a single row into the table: "auth.accounts" */
        insert_auth_accounts_one?: GraphQLTypes['auth_accounts'];
        /** insert data into the table: "auth.providers" */
        insert_auth_providers?: GraphQLTypes['auth_providers_mutation_response'];
        /** insert a single row into the table: "auth.providers" */
        insert_auth_providers_one?: GraphQLTypes['auth_providers'];
        /** insert data into the table: "auth.refresh_tokens" */
        insert_auth_refresh_tokens?: GraphQLTypes['auth_refresh_tokens_mutation_response'];
        /** insert a single row into the table: "auth.refresh_tokens" */
        insert_auth_refresh_tokens_one?: GraphQLTypes['auth_refresh_tokens'];
        /** insert data into the table: "auth.roles" */
        insert_auth_roles?: GraphQLTypes['auth_roles_mutation_response'];
        /** insert a single row into the table: "auth.roles" */
        insert_auth_roles_one?: GraphQLTypes['auth_roles'];
        /** insert data into the table: "contribution_types" */
        insert_contribution_types?: GraphQLTypes['contribution_types_mutation_response'];
        /** insert a single row into the table: "contribution_types" */
        insert_contribution_types_one?: GraphQLTypes['contribution_types'];
        /** insert data into the table: "group_cycles" */
        insert_group_cycles?: GraphQLTypes['group_cycles_mutation_response'];
        /** insert a single row into the table: "group_cycles" */
        insert_group_cycles_one?: GraphQLTypes['group_cycles'];
        /** insert data into the table: "group_recurrencies" */
        insert_group_recurrencies?: GraphQLTypes['group_recurrencies_mutation_response'];
        /** insert a single row into the table: "group_recurrencies" */
        insert_group_recurrencies_one?: GraphQLTypes['group_recurrencies'];
        /** insert data into the table: "groups" */
        insert_groups?: GraphQLTypes['groups_mutation_response'];
        /** insert a single row into the table: "groups" */
        insert_groups_one?: GraphQLTypes['groups'];
        /** insert data into the table: "members" */
        insert_members?: GraphQLTypes['members_mutation_response'];
        /** insert a single row into the table: "members" */
        insert_members_one?: GraphQLTypes['members'];
        /** insert data into the table: "payment_statuses" */
        insert_payment_statuses?: GraphQLTypes['payment_statuses_mutation_response'];
        /** insert a single row into the table: "payment_statuses" */
        insert_payment_statuses_one?: GraphQLTypes['payment_statuses'];
        /** insert data into the table: "payment_types" */
        insert_payment_types?: GraphQLTypes['payment_types_mutation_response'];
        /** insert a single row into the table: "payment_types" */
        insert_payment_types_one?: GraphQLTypes['payment_types'];
        /** insert data into the table: "payments" */
        insert_payments?: GraphQLTypes['payments_mutation_response'];
        /** insert a single row into the table: "payments" */
        insert_payments_one?: GraphQLTypes['payments'];
        /** insert data into the table: "periods" */
        insert_periods?: GraphQLTypes['periods_mutation_response'];
        /** insert a single row into the table: "periods" */
        insert_periods_one?: GraphQLTypes['periods'];
        /** insert data into the table: "users" */
        insert_users?: GraphQLTypes['users_mutation_response'];
        /** insert a single row into the table: "users" */
        insert_users_one?: GraphQLTypes['users'];
        /** update data of the table: "auth.account_providers" */
        update_auth_account_providers?: GraphQLTypes['auth_account_providers_mutation_response'];
        /** update single row of the table: "auth.account_providers" */
        update_auth_account_providers_by_pk?: GraphQLTypes['auth_account_providers'];
        /** update data of the table: "auth.account_roles" */
        update_auth_account_roles?: GraphQLTypes['auth_account_roles_mutation_response'];
        /** update single row of the table: "auth.account_roles" */
        update_auth_account_roles_by_pk?: GraphQLTypes['auth_account_roles'];
        /** update data of the table: "auth.accounts" */
        update_auth_accounts?: GraphQLTypes['auth_accounts_mutation_response'];
        /** update single row of the table: "auth.accounts" */
        update_auth_accounts_by_pk?: GraphQLTypes['auth_accounts'];
        /** update data of the table: "auth.providers" */
        update_auth_providers?: GraphQLTypes['auth_providers_mutation_response'];
        /** update single row of the table: "auth.providers" */
        update_auth_providers_by_pk?: GraphQLTypes['auth_providers'];
        /** update data of the table: "auth.refresh_tokens" */
        update_auth_refresh_tokens?: GraphQLTypes['auth_refresh_tokens_mutation_response'];
        /** update single row of the table: "auth.refresh_tokens" */
        update_auth_refresh_tokens_by_pk?: GraphQLTypes['auth_refresh_tokens'];
        /** update data of the table: "auth.roles" */
        update_auth_roles?: GraphQLTypes['auth_roles_mutation_response'];
        /** update single row of the table: "auth.roles" */
        update_auth_roles_by_pk?: GraphQLTypes['auth_roles'];
        /** update data of the table: "contribution_types" */
        update_contribution_types?: GraphQLTypes['contribution_types_mutation_response'];
        /** update single row of the table: "contribution_types" */
        update_contribution_types_by_pk?: GraphQLTypes['contribution_types'];
        /** update data of the table: "group_cycles" */
        update_group_cycles?: GraphQLTypes['group_cycles_mutation_response'];
        /** update single row of the table: "group_cycles" */
        update_group_cycles_by_pk?: GraphQLTypes['group_cycles'];
        /** update data of the table: "group_recurrencies" */
        update_group_recurrencies?: GraphQLTypes['group_recurrencies_mutation_response'];
        /** update single row of the table: "group_recurrencies" */
        update_group_recurrencies_by_pk?: GraphQLTypes['group_recurrencies'];
        /** update data of the table: "groups" */
        update_groups?: GraphQLTypes['groups_mutation_response'];
        /** update single row of the table: "groups" */
        update_groups_by_pk?: GraphQLTypes['groups'];
        /** update data of the table: "members" */
        update_members?: GraphQLTypes['members_mutation_response'];
        /** update single row of the table: "members" */
        update_members_by_pk?: GraphQLTypes['members'];
        /** update data of the table: "payment_statuses" */
        update_payment_statuses?: GraphQLTypes['payment_statuses_mutation_response'];
        /** update single row of the table: "payment_statuses" */
        update_payment_statuses_by_pk?: GraphQLTypes['payment_statuses'];
        /** update data of the table: "payment_types" */
        update_payment_types?: GraphQLTypes['payment_types_mutation_response'];
        /** update single row of the table: "payment_types" */
        update_payment_types_by_pk?: GraphQLTypes['payment_types'];
        /** update data of the table: "payments" */
        update_payments?: GraphQLTypes['payments_mutation_response'];
        /** update single row of the table: "payments" */
        update_payments_by_pk?: GraphQLTypes['payments'];
        /** update data of the table: "periods" */
        update_periods?: GraphQLTypes['periods_mutation_response'];
        /** update single row of the table: "periods" */
        update_periods_by_pk?: GraphQLTypes['periods'];
        /** update data of the table: "users" */
        update_users?: GraphQLTypes['users_mutation_response'];
        /** update single row of the table: "users" */
        update_users_by_pk?: GraphQLTypes['users'];
    };
    ['numeric']: any;
    /** expression to compare columns of type numeric. All fields are combined with logical 'AND'. */
    ['numeric_comparison_exp']: {
        _eq?: GraphQLTypes['numeric'];
        _gt?: GraphQLTypes['numeric'];
        _gte?: GraphQLTypes['numeric'];
        _in?: Array<GraphQLTypes['numeric']>;
        _is_null?: boolean;
        _lt?: GraphQLTypes['numeric'];
        _lte?: GraphQLTypes['numeric'];
        _neq?: GraphQLTypes['numeric'];
        _nin?: Array<GraphQLTypes['numeric']>;
    };
    /** column ordering options */
    ['order_by']: order_by;
    /** columns and relationships of "payment_statuses" */
    ['payment_statuses']: {
        __typename: 'payment_statuses';
        description: string;
        /** An array relationship */
        payments: Array<GraphQLTypes['payments']>;
        /** An aggregated array relationship */
        payments_aggregate: GraphQLTypes['payments_aggregate'];
        value: string;
    };
    /** aggregated selection of "payment_statuses" */
    ['payment_statuses_aggregate']: {
        __typename: 'payment_statuses_aggregate';
        aggregate?: GraphQLTypes['payment_statuses_aggregate_fields'];
        nodes: Array<GraphQLTypes['payment_statuses']>;
    };
    /** aggregate fields of "payment_statuses" */
    ['payment_statuses_aggregate_fields']: {
        __typename: 'payment_statuses_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['payment_statuses_max_fields'];
        min?: GraphQLTypes['payment_statuses_min_fields'];
    };
    /** order by aggregate values of table "payment_statuses" */
    ['payment_statuses_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['payment_statuses_max_order_by'];
        min?: GraphQLTypes['payment_statuses_min_order_by'];
    };
    /** input type for inserting array relation for remote table "payment_statuses" */
    ['payment_statuses_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['payment_statuses_insert_input']>;
        on_conflict?: GraphQLTypes['payment_statuses_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "payment_statuses". All fields are combined with a logical 'AND'. */
    ['payment_statuses_bool_exp']: {
        _and?: Array<GraphQLTypes['payment_statuses_bool_exp'] | undefined>;
        _not?: GraphQLTypes['payment_statuses_bool_exp'];
        _or?: Array<GraphQLTypes['payment_statuses_bool_exp'] | undefined>;
        description?: GraphQLTypes['String_comparison_exp'];
        payments?: GraphQLTypes['payments_bool_exp'];
        value?: GraphQLTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "payment_statuses" */
    ['payment_statuses_constraint']: payment_statuses_constraint;
    ['payment_statuses_enum']: payment_statuses_enum;
    /** expression to compare columns of type payment_statuses_enum. All fields are combined with logical 'AND'. */
    ['payment_statuses_enum_comparison_exp']: {
        _eq?: GraphQLTypes['payment_statuses_enum'];
        _in?: Array<GraphQLTypes['payment_statuses_enum']>;
        _is_null?: boolean;
        _neq?: GraphQLTypes['payment_statuses_enum'];
        _nin?: Array<GraphQLTypes['payment_statuses_enum']>;
    };
    /** input type for inserting data into table "payment_statuses" */
    ['payment_statuses_insert_input']: {
        description?: string;
        payments?: GraphQLTypes['payments_arr_rel_insert_input'];
        value?: string;
    };
    /** aggregate max on columns */
    ['payment_statuses_max_fields']: {
        __typename: 'payment_statuses_max_fields';
        description?: string;
        value?: string;
    };
    /** order by max() on columns of table "payment_statuses" */
    ['payment_statuses_max_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['payment_statuses_min_fields']: {
        __typename: 'payment_statuses_min_fields';
        description?: string;
        value?: string;
    };
    /** order by min() on columns of table "payment_statuses" */
    ['payment_statuses_min_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "payment_statuses" */
    ['payment_statuses_mutation_response']: {
        __typename: 'payment_statuses_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['payment_statuses']>;
    };
    /** input type for inserting object relation for remote table "payment_statuses" */
    ['payment_statuses_obj_rel_insert_input']: {
        data: GraphQLTypes['payment_statuses_insert_input'];
        on_conflict?: GraphQLTypes['payment_statuses_on_conflict'];
    };
    /** on conflict condition type for table "payment_statuses" */
    ['payment_statuses_on_conflict']: {
        constraint: GraphQLTypes['payment_statuses_constraint'];
        update_columns: Array<GraphQLTypes['payment_statuses_update_column']>;
        where?: GraphQLTypes['payment_statuses_bool_exp'];
    };
    /** ordering options when selecting data from "payment_statuses" */
    ['payment_statuses_order_by']: {
        description?: GraphQLTypes['order_by'];
        payments_aggregate?: GraphQLTypes['payments_aggregate_order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "payment_statuses" */
    ['payment_statuses_pk_columns_input']: {
        value: string;
    };
    /** select columns of table "payment_statuses" */
    ['payment_statuses_select_column']: payment_statuses_select_column;
    /** input type for updating data in table "payment_statuses" */
    ['payment_statuses_set_input']: {
        description?: string;
        value?: string;
    };
    /** update columns of table "payment_statuses" */
    ['payment_statuses_update_column']: payment_statuses_update_column;
    /** columns and relationships of "payment_types" */
    ['payment_types']: {
        __typename: 'payment_types';
        description: string;
        value: string;
    };
    /** aggregated selection of "payment_types" */
    ['payment_types_aggregate']: {
        __typename: 'payment_types_aggregate';
        aggregate?: GraphQLTypes['payment_types_aggregate_fields'];
        nodes: Array<GraphQLTypes['payment_types']>;
    };
    /** aggregate fields of "payment_types" */
    ['payment_types_aggregate_fields']: {
        __typename: 'payment_types_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['payment_types_max_fields'];
        min?: GraphQLTypes['payment_types_min_fields'];
    };
    /** order by aggregate values of table "payment_types" */
    ['payment_types_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['payment_types_max_order_by'];
        min?: GraphQLTypes['payment_types_min_order_by'];
    };
    /** input type for inserting array relation for remote table "payment_types" */
    ['payment_types_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['payment_types_insert_input']>;
        on_conflict?: GraphQLTypes['payment_types_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "payment_types". All fields are combined with a logical 'AND'. */
    ['payment_types_bool_exp']: {
        _and?: Array<GraphQLTypes['payment_types_bool_exp'] | undefined>;
        _not?: GraphQLTypes['payment_types_bool_exp'];
        _or?: Array<GraphQLTypes['payment_types_bool_exp'] | undefined>;
        description?: GraphQLTypes['String_comparison_exp'];
        value?: GraphQLTypes['String_comparison_exp'];
    };
    /** unique or primary key constraints on table "payment_types" */
    ['payment_types_constraint']: payment_types_constraint;
    ['payment_types_enum']: payment_types_enum;
    /** expression to compare columns of type payment_types_enum. All fields are combined with logical 'AND'. */
    ['payment_types_enum_comparison_exp']: {
        _eq?: GraphQLTypes['payment_types_enum'];
        _in?: Array<GraphQLTypes['payment_types_enum']>;
        _is_null?: boolean;
        _neq?: GraphQLTypes['payment_types_enum'];
        _nin?: Array<GraphQLTypes['payment_types_enum']>;
    };
    /** input type for inserting data into table "payment_types" */
    ['payment_types_insert_input']: {
        description?: string;
        value?: string;
    };
    /** aggregate max on columns */
    ['payment_types_max_fields']: {
        __typename: 'payment_types_max_fields';
        description?: string;
        value?: string;
    };
    /** order by max() on columns of table "payment_types" */
    ['payment_types_max_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['payment_types_min_fields']: {
        __typename: 'payment_types_min_fields';
        description?: string;
        value?: string;
    };
    /** order by min() on columns of table "payment_types" */
    ['payment_types_min_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "payment_types" */
    ['payment_types_mutation_response']: {
        __typename: 'payment_types_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['payment_types']>;
    };
    /** input type for inserting object relation for remote table "payment_types" */
    ['payment_types_obj_rel_insert_input']: {
        data: GraphQLTypes['payment_types_insert_input'];
        on_conflict?: GraphQLTypes['payment_types_on_conflict'];
    };
    /** on conflict condition type for table "payment_types" */
    ['payment_types_on_conflict']: {
        constraint: GraphQLTypes['payment_types_constraint'];
        update_columns: Array<GraphQLTypes['payment_types_update_column']>;
        where?: GraphQLTypes['payment_types_bool_exp'];
    };
    /** ordering options when selecting data from "payment_types" */
    ['payment_types_order_by']: {
        description?: GraphQLTypes['order_by'];
        value?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "payment_types" */
    ['payment_types_pk_columns_input']: {
        value: string;
    };
    /** select columns of table "payment_types" */
    ['payment_types_select_column']: payment_types_select_column;
    /** input type for updating data in table "payment_types" */
    ['payment_types_set_input']: {
        description?: string;
        value?: string;
    };
    /** update columns of table "payment_types" */
    ['payment_types_update_column']: payment_types_update_column;
    /** columns and relationships of "payments" */
    ['payments']: {
        __typename: 'payments';
        created_at: GraphQLTypes['timestamptz'];
        /** An object relationship */
        group: GraphQLTypes['groups'];
        group_id: GraphQLTypes['uuid'];
        id: GraphQLTypes['uuid'];
        /** An object relationship */
        member: GraphQLTypes['members'];
        member_id: GraphQLTypes['uuid'];
        /** An object relationship */
        paymentStatusByPaymentStatus: GraphQLTypes['payment_statuses'];
        payment_amount: GraphQLTypes['numeric'];
        payment_status: GraphQLTypes['payment_statuses_enum'];
        payment_type: GraphQLTypes['payment_types_enum'];
        /** An object relationship */
        period?: GraphQLTypes['periods'];
        period_id?: GraphQLTypes['uuid'];
        updated_at: GraphQLTypes['timestamptz'];
    };
    /** aggregated selection of "payments" */
    ['payments_aggregate']: {
        __typename: 'payments_aggregate';
        aggregate?: GraphQLTypes['payments_aggregate_fields'];
        nodes: Array<GraphQLTypes['payments']>;
    };
    /** aggregate fields of "payments" */
    ['payments_aggregate_fields']: {
        __typename: 'payments_aggregate_fields';
        avg?: GraphQLTypes['payments_avg_fields'];
        count?: number;
        max?: GraphQLTypes['payments_max_fields'];
        min?: GraphQLTypes['payments_min_fields'];
        stddev?: GraphQLTypes['payments_stddev_fields'];
        stddev_pop?: GraphQLTypes['payments_stddev_pop_fields'];
        stddev_samp?: GraphQLTypes['payments_stddev_samp_fields'];
        sum?: GraphQLTypes['payments_sum_fields'];
        var_pop?: GraphQLTypes['payments_var_pop_fields'];
        var_samp?: GraphQLTypes['payments_var_samp_fields'];
        variance?: GraphQLTypes['payments_variance_fields'];
    };
    /** order by aggregate values of table "payments" */
    ['payments_aggregate_order_by']: {
        avg?: GraphQLTypes['payments_avg_order_by'];
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['payments_max_order_by'];
        min?: GraphQLTypes['payments_min_order_by'];
        stddev?: GraphQLTypes['payments_stddev_order_by'];
        stddev_pop?: GraphQLTypes['payments_stddev_pop_order_by'];
        stddev_samp?: GraphQLTypes['payments_stddev_samp_order_by'];
        sum?: GraphQLTypes['payments_sum_order_by'];
        var_pop?: GraphQLTypes['payments_var_pop_order_by'];
        var_samp?: GraphQLTypes['payments_var_samp_order_by'];
        variance?: GraphQLTypes['payments_variance_order_by'];
    };
    /** input type for inserting array relation for remote table "payments" */
    ['payments_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['payments_insert_input']>;
        on_conflict?: GraphQLTypes['payments_on_conflict'];
    };
    /** aggregate avg on columns */
    ['payments_avg_fields']: {
        __typename: 'payments_avg_fields';
        payment_amount?: number;
    };
    /** order by avg() on columns of table "payments" */
    ['payments_avg_order_by']: {
        payment_amount?: GraphQLTypes['order_by'];
    };
    /** Boolean expression to filter rows from the table "payments". All fields are combined with a logical 'AND'. */
    ['payments_bool_exp']: {
        _and?: Array<GraphQLTypes['payments_bool_exp'] | undefined>;
        _not?: GraphQLTypes['payments_bool_exp'];
        _or?: Array<GraphQLTypes['payments_bool_exp'] | undefined>;
        created_at?: GraphQLTypes['timestamptz_comparison_exp'];
        group?: GraphQLTypes['groups_bool_exp'];
        group_id?: GraphQLTypes['uuid_comparison_exp'];
        id?: GraphQLTypes['uuid_comparison_exp'];
        member?: GraphQLTypes['members_bool_exp'];
        member_id?: GraphQLTypes['uuid_comparison_exp'];
        paymentStatusByPaymentStatus?: GraphQLTypes['payment_statuses_bool_exp'];
        payment_amount?: GraphQLTypes['numeric_comparison_exp'];
        payment_status?: GraphQLTypes['payment_statuses_enum_comparison_exp'];
        payment_type?: GraphQLTypes['payment_types_enum_comparison_exp'];
        period?: GraphQLTypes['periods_bool_exp'];
        period_id?: GraphQLTypes['uuid_comparison_exp'];
        updated_at?: GraphQLTypes['timestamptz_comparison_exp'];
    };
    /** unique or primary key constraints on table "payments" */
    ['payments_constraint']: payments_constraint;
    /** input type for incrementing integer column in table "payments" */
    ['payments_inc_input']: {
        payment_amount?: GraphQLTypes['numeric'];
    };
    /** input type for inserting data into table "payments" */
    ['payments_insert_input']: {
        created_at?: GraphQLTypes['timestamptz'];
        group?: GraphQLTypes['groups_obj_rel_insert_input'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        member?: GraphQLTypes['members_obj_rel_insert_input'];
        member_id?: GraphQLTypes['uuid'];
        paymentStatusByPaymentStatus?: GraphQLTypes['payment_statuses_obj_rel_insert_input'];
        payment_amount?: GraphQLTypes['numeric'];
        payment_status?: GraphQLTypes['payment_statuses_enum'];
        payment_type?: GraphQLTypes['payment_types_enum'];
        period?: GraphQLTypes['periods_obj_rel_insert_input'];
        period_id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** aggregate max on columns */
    ['payments_max_fields']: {
        __typename: 'payments_max_fields';
        created_at?: GraphQLTypes['timestamptz'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        member_id?: GraphQLTypes['uuid'];
        payment_amount?: GraphQLTypes['numeric'];
        period_id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** order by max() on columns of table "payments" */
    ['payments_max_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        group_id?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        member_id?: GraphQLTypes['order_by'];
        payment_amount?: GraphQLTypes['order_by'];
        period_id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['payments_min_fields']: {
        __typename: 'payments_min_fields';
        created_at?: GraphQLTypes['timestamptz'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        member_id?: GraphQLTypes['uuid'];
        payment_amount?: GraphQLTypes['numeric'];
        period_id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** order by min() on columns of table "payments" */
    ['payments_min_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        group_id?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        member_id?: GraphQLTypes['order_by'];
        payment_amount?: GraphQLTypes['order_by'];
        period_id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "payments" */
    ['payments_mutation_response']: {
        __typename: 'payments_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['payments']>;
    };
    /** input type for inserting object relation for remote table "payments" */
    ['payments_obj_rel_insert_input']: {
        data: GraphQLTypes['payments_insert_input'];
        on_conflict?: GraphQLTypes['payments_on_conflict'];
    };
    /** on conflict condition type for table "payments" */
    ['payments_on_conflict']: {
        constraint: GraphQLTypes['payments_constraint'];
        update_columns: Array<GraphQLTypes['payments_update_column']>;
        where?: GraphQLTypes['payments_bool_exp'];
    };
    /** ordering options when selecting data from "payments" */
    ['payments_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        group?: GraphQLTypes['groups_order_by'];
        group_id?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        member?: GraphQLTypes['members_order_by'];
        member_id?: GraphQLTypes['order_by'];
        paymentStatusByPaymentStatus?: GraphQLTypes['payment_statuses_order_by'];
        payment_amount?: GraphQLTypes['order_by'];
        payment_status?: GraphQLTypes['order_by'];
        payment_type?: GraphQLTypes['order_by'];
        period?: GraphQLTypes['periods_order_by'];
        period_id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "payments" */
    ['payments_pk_columns_input']: {
        id: GraphQLTypes['uuid'];
    };
    /** select columns of table "payments" */
    ['payments_select_column']: payments_select_column;
    /** input type for updating data in table "payments" */
    ['payments_set_input']: {
        created_at?: GraphQLTypes['timestamptz'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        member_id?: GraphQLTypes['uuid'];
        payment_amount?: GraphQLTypes['numeric'];
        payment_status?: GraphQLTypes['payment_statuses_enum'];
        payment_type?: GraphQLTypes['payment_types_enum'];
        period_id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** aggregate stddev on columns */
    ['payments_stddev_fields']: {
        __typename: 'payments_stddev_fields';
        payment_amount?: number;
    };
    /** order by stddev() on columns of table "payments" */
    ['payments_stddev_order_by']: {
        payment_amount?: GraphQLTypes['order_by'];
    };
    /** aggregate stddev_pop on columns */
    ['payments_stddev_pop_fields']: {
        __typename: 'payments_stddev_pop_fields';
        payment_amount?: number;
    };
    /** order by stddev_pop() on columns of table "payments" */
    ['payments_stddev_pop_order_by']: {
        payment_amount?: GraphQLTypes['order_by'];
    };
    /** aggregate stddev_samp on columns */
    ['payments_stddev_samp_fields']: {
        __typename: 'payments_stddev_samp_fields';
        payment_amount?: number;
    };
    /** order by stddev_samp() on columns of table "payments" */
    ['payments_stddev_samp_order_by']: {
        payment_amount?: GraphQLTypes['order_by'];
    };
    /** aggregate sum on columns */
    ['payments_sum_fields']: {
        __typename: 'payments_sum_fields';
        payment_amount?: GraphQLTypes['numeric'];
    };
    /** order by sum() on columns of table "payments" */
    ['payments_sum_order_by']: {
        payment_amount?: GraphQLTypes['order_by'];
    };
    /** update columns of table "payments" */
    ['payments_update_column']: payments_update_column;
    /** aggregate var_pop on columns */
    ['payments_var_pop_fields']: {
        __typename: 'payments_var_pop_fields';
        payment_amount?: number;
    };
    /** order by var_pop() on columns of table "payments" */
    ['payments_var_pop_order_by']: {
        payment_amount?: GraphQLTypes['order_by'];
    };
    /** aggregate var_samp on columns */
    ['payments_var_samp_fields']: {
        __typename: 'payments_var_samp_fields';
        payment_amount?: number;
    };
    /** order by var_samp() on columns of table "payments" */
    ['payments_var_samp_order_by']: {
        payment_amount?: GraphQLTypes['order_by'];
    };
    /** aggregate variance on columns */
    ['payments_variance_fields']: {
        __typename: 'payments_variance_fields';
        payment_amount?: number;
    };
    /** order by variance() on columns of table "payments" */
    ['payments_variance_order_by']: {
        payment_amount?: GraphQLTypes['order_by'];
    };
    /** columns and relationships of "periods" */
    ['periods']: {
        __typename: 'periods';
        created_at: GraphQLTypes['timestamptz'];
        /** An object relationship */
        group: GraphQLTypes['groups'];
        group_id: GraphQLTypes['uuid'];
        id: GraphQLTypes['uuid'];
        /** An array relationship */
        payments: Array<GraphQLTypes['payments']>;
        /** An aggregated array relationship */
        payments_aggregate: GraphQLTypes['payments_aggregate'];
        period_active: boolean;
        period_completed_at: GraphQLTypes['timestamp'];
        period_index: number;
        period_progression: GraphQLTypes['numeric'];
        updated_at: GraphQLTypes['timestamptz'];
    };
    /** aggregated selection of "periods" */
    ['periods_aggregate']: {
        __typename: 'periods_aggregate';
        aggregate?: GraphQLTypes['periods_aggregate_fields'];
        nodes: Array<GraphQLTypes['periods']>;
    };
    /** aggregate fields of "periods" */
    ['periods_aggregate_fields']: {
        __typename: 'periods_aggregate_fields';
        avg?: GraphQLTypes['periods_avg_fields'];
        count?: number;
        max?: GraphQLTypes['periods_max_fields'];
        min?: GraphQLTypes['periods_min_fields'];
        stddev?: GraphQLTypes['periods_stddev_fields'];
        stddev_pop?: GraphQLTypes['periods_stddev_pop_fields'];
        stddev_samp?: GraphQLTypes['periods_stddev_samp_fields'];
        sum?: GraphQLTypes['periods_sum_fields'];
        var_pop?: GraphQLTypes['periods_var_pop_fields'];
        var_samp?: GraphQLTypes['periods_var_samp_fields'];
        variance?: GraphQLTypes['periods_variance_fields'];
    };
    /** order by aggregate values of table "periods" */
    ['periods_aggregate_order_by']: {
        avg?: GraphQLTypes['periods_avg_order_by'];
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['periods_max_order_by'];
        min?: GraphQLTypes['periods_min_order_by'];
        stddev?: GraphQLTypes['periods_stddev_order_by'];
        stddev_pop?: GraphQLTypes['periods_stddev_pop_order_by'];
        stddev_samp?: GraphQLTypes['periods_stddev_samp_order_by'];
        sum?: GraphQLTypes['periods_sum_order_by'];
        var_pop?: GraphQLTypes['periods_var_pop_order_by'];
        var_samp?: GraphQLTypes['periods_var_samp_order_by'];
        variance?: GraphQLTypes['periods_variance_order_by'];
    };
    /** input type for inserting array relation for remote table "periods" */
    ['periods_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['periods_insert_input']>;
        on_conflict?: GraphQLTypes['periods_on_conflict'];
    };
    /** aggregate avg on columns */
    ['periods_avg_fields']: {
        __typename: 'periods_avg_fields';
        period_index?: number;
        period_progression?: number;
    };
    /** order by avg() on columns of table "periods" */
    ['periods_avg_order_by']: {
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
    };
    /** Boolean expression to filter rows from the table "periods". All fields are combined with a logical 'AND'. */
    ['periods_bool_exp']: {
        _and?: Array<GraphQLTypes['periods_bool_exp'] | undefined>;
        _not?: GraphQLTypes['periods_bool_exp'];
        _or?: Array<GraphQLTypes['periods_bool_exp'] | undefined>;
        created_at?: GraphQLTypes['timestamptz_comparison_exp'];
        group?: GraphQLTypes['groups_bool_exp'];
        group_id?: GraphQLTypes['uuid_comparison_exp'];
        id?: GraphQLTypes['uuid_comparison_exp'];
        payments?: GraphQLTypes['payments_bool_exp'];
        period_active?: GraphQLTypes['Boolean_comparison_exp'];
        period_completed_at?: GraphQLTypes['timestamp_comparison_exp'];
        period_index?: GraphQLTypes['Int_comparison_exp'];
        period_progression?: GraphQLTypes['numeric_comparison_exp'];
        updated_at?: GraphQLTypes['timestamptz_comparison_exp'];
    };
    /** unique or primary key constraints on table "periods" */
    ['periods_constraint']: periods_constraint;
    /** input type for incrementing integer column in table "periods" */
    ['periods_inc_input']: {
        period_index?: number;
        period_progression?: GraphQLTypes['numeric'];
    };
    /** input type for inserting data into table "periods" */
    ['periods_insert_input']: {
        created_at?: GraphQLTypes['timestamptz'];
        group?: GraphQLTypes['groups_obj_rel_insert_input'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        payments?: GraphQLTypes['payments_arr_rel_insert_input'];
        period_active?: boolean;
        period_completed_at?: GraphQLTypes['timestamp'];
        period_index?: number;
        period_progression?: GraphQLTypes['numeric'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** aggregate max on columns */
    ['periods_max_fields']: {
        __typename: 'periods_max_fields';
        created_at?: GraphQLTypes['timestamptz'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        period_completed_at?: GraphQLTypes['timestamp'];
        period_index?: number;
        period_progression?: GraphQLTypes['numeric'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** order by max() on columns of table "periods" */
    ['periods_max_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        group_id?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        period_completed_at?: GraphQLTypes['order_by'];
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['periods_min_fields']: {
        __typename: 'periods_min_fields';
        created_at?: GraphQLTypes['timestamptz'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        period_completed_at?: GraphQLTypes['timestamp'];
        period_index?: number;
        period_progression?: GraphQLTypes['numeric'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** order by min() on columns of table "periods" */
    ['periods_min_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        group_id?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        period_completed_at?: GraphQLTypes['order_by'];
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "periods" */
    ['periods_mutation_response']: {
        __typename: 'periods_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['periods']>;
    };
    /** input type for inserting object relation for remote table "periods" */
    ['periods_obj_rel_insert_input']: {
        data: GraphQLTypes['periods_insert_input'];
        on_conflict?: GraphQLTypes['periods_on_conflict'];
    };
    /** on conflict condition type for table "periods" */
    ['periods_on_conflict']: {
        constraint: GraphQLTypes['periods_constraint'];
        update_columns: Array<GraphQLTypes['periods_update_column']>;
        where?: GraphQLTypes['periods_bool_exp'];
    };
    /** ordering options when selecting data from "periods" */
    ['periods_order_by']: {
        created_at?: GraphQLTypes['order_by'];
        group?: GraphQLTypes['groups_order_by'];
        group_id?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        payments_aggregate?: GraphQLTypes['payments_aggregate_order_by'];
        period_active?: GraphQLTypes['order_by'];
        period_completed_at?: GraphQLTypes['order_by'];
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "periods" */
    ['periods_pk_columns_input']: {
        id: GraphQLTypes['uuid'];
    };
    /** select columns of table "periods" */
    ['periods_select_column']: periods_select_column;
    /** input type for updating data in table "periods" */
    ['periods_set_input']: {
        created_at?: GraphQLTypes['timestamptz'];
        group_id?: GraphQLTypes['uuid'];
        id?: GraphQLTypes['uuid'];
        period_active?: boolean;
        period_completed_at?: GraphQLTypes['timestamp'];
        period_index?: number;
        period_progression?: GraphQLTypes['numeric'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** aggregate stddev on columns */
    ['periods_stddev_fields']: {
        __typename: 'periods_stddev_fields';
        period_index?: number;
        period_progression?: number;
    };
    /** order by stddev() on columns of table "periods" */
    ['periods_stddev_order_by']: {
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
    };
    /** aggregate stddev_pop on columns */
    ['periods_stddev_pop_fields']: {
        __typename: 'periods_stddev_pop_fields';
        period_index?: number;
        period_progression?: number;
    };
    /** order by stddev_pop() on columns of table "periods" */
    ['periods_stddev_pop_order_by']: {
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
    };
    /** aggregate stddev_samp on columns */
    ['periods_stddev_samp_fields']: {
        __typename: 'periods_stddev_samp_fields';
        period_index?: number;
        period_progression?: number;
    };
    /** order by stddev_samp() on columns of table "periods" */
    ['periods_stddev_samp_order_by']: {
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
    };
    /** aggregate sum on columns */
    ['periods_sum_fields']: {
        __typename: 'periods_sum_fields';
        period_index?: number;
        period_progression?: GraphQLTypes['numeric'];
    };
    /** order by sum() on columns of table "periods" */
    ['periods_sum_order_by']: {
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
    };
    /** update columns of table "periods" */
    ['periods_update_column']: periods_update_column;
    /** aggregate var_pop on columns */
    ['periods_var_pop_fields']: {
        __typename: 'periods_var_pop_fields';
        period_index?: number;
        period_progression?: number;
    };
    /** order by var_pop() on columns of table "periods" */
    ['periods_var_pop_order_by']: {
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
    };
    /** aggregate var_samp on columns */
    ['periods_var_samp_fields']: {
        __typename: 'periods_var_samp_fields';
        period_index?: number;
        period_progression?: number;
    };
    /** order by var_samp() on columns of table "periods" */
    ['periods_var_samp_order_by']: {
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
    };
    /** aggregate variance on columns */
    ['periods_variance_fields']: {
        __typename: 'periods_variance_fields';
        period_index?: number;
        period_progression?: number;
    };
    /** order by variance() on columns of table "periods" */
    ['periods_variance_order_by']: {
        period_index?: GraphQLTypes['order_by'];
        period_progression?: GraphQLTypes['order_by'];
    };
    /** query root */
    ['query_root']: {
        __typename: 'query_root';
        /** fetch data from the table: "auth.account_providers" */
        auth_account_providers: Array<GraphQLTypes['auth_account_providers']>;
        /** fetch aggregated fields from the table: "auth.account_providers" */
        auth_account_providers_aggregate: GraphQLTypes['auth_account_providers_aggregate'];
        /** fetch data from the table: "auth.account_providers" using primary key columns */
        auth_account_providers_by_pk?: GraphQLTypes['auth_account_providers'];
        /** fetch data from the table: "auth.account_roles" */
        auth_account_roles: Array<GraphQLTypes['auth_account_roles']>;
        /** fetch aggregated fields from the table: "auth.account_roles" */
        auth_account_roles_aggregate: GraphQLTypes['auth_account_roles_aggregate'];
        /** fetch data from the table: "auth.account_roles" using primary key columns */
        auth_account_roles_by_pk?: GraphQLTypes['auth_account_roles'];
        /** fetch data from the table: "auth.accounts" */
        auth_accounts: Array<GraphQLTypes['auth_accounts']>;
        /** fetch aggregated fields from the table: "auth.accounts" */
        auth_accounts_aggregate: GraphQLTypes['auth_accounts_aggregate'];
        /** fetch data from the table: "auth.accounts" using primary key columns */
        auth_accounts_by_pk?: GraphQLTypes['auth_accounts'];
        /** fetch data from the table: "auth.providers" */
        auth_providers: Array<GraphQLTypes['auth_providers']>;
        /** fetch aggregated fields from the table: "auth.providers" */
        auth_providers_aggregate: GraphQLTypes['auth_providers_aggregate'];
        /** fetch data from the table: "auth.providers" using primary key columns */
        auth_providers_by_pk?: GraphQLTypes['auth_providers'];
        /** fetch data from the table: "auth.refresh_tokens" */
        auth_refresh_tokens: Array<GraphQLTypes['auth_refresh_tokens']>;
        /** fetch aggregated fields from the table: "auth.refresh_tokens" */
        auth_refresh_tokens_aggregate: GraphQLTypes['auth_refresh_tokens_aggregate'];
        /** fetch data from the table: "auth.refresh_tokens" using primary key columns */
        auth_refresh_tokens_by_pk?: GraphQLTypes['auth_refresh_tokens'];
        /** fetch data from the table: "auth.roles" */
        auth_roles: Array<GraphQLTypes['auth_roles']>;
        /** fetch aggregated fields from the table: "auth.roles" */
        auth_roles_aggregate: GraphQLTypes['auth_roles_aggregate'];
        /** fetch data from the table: "auth.roles" using primary key columns */
        auth_roles_by_pk?: GraphQLTypes['auth_roles'];
        /** fetch data from the table: "contribution_types" */
        contribution_types: Array<GraphQLTypes['contribution_types']>;
        /** fetch aggregated fields from the table: "contribution_types" */
        contribution_types_aggregate: GraphQLTypes['contribution_types_aggregate'];
        /** fetch data from the table: "contribution_types" using primary key columns */
        contribution_types_by_pk?: GraphQLTypes['contribution_types'];
        /** fetch data from the table: "group_cycles" */
        group_cycles: Array<GraphQLTypes['group_cycles']>;
        /** fetch aggregated fields from the table: "group_cycles" */
        group_cycles_aggregate: GraphQLTypes['group_cycles_aggregate'];
        /** fetch data from the table: "group_cycles" using primary key columns */
        group_cycles_by_pk?: GraphQLTypes['group_cycles'];
        /** fetch data from the table: "group_recurrencies" */
        group_recurrencies: Array<GraphQLTypes['group_recurrencies']>;
        /** fetch aggregated fields from the table: "group_recurrencies" */
        group_recurrencies_aggregate: GraphQLTypes['group_recurrencies_aggregate'];
        /** fetch data from the table: "group_recurrencies" using primary key columns */
        group_recurrencies_by_pk?: GraphQLTypes['group_recurrencies'];
        /** fetch data from the table: "groups" */
        groups: Array<GraphQLTypes['groups']>;
        /** fetch aggregated fields from the table: "groups" */
        groups_aggregate: GraphQLTypes['groups_aggregate'];
        /** fetch data from the table: "groups" using primary key columns */
        groups_by_pk?: GraphQLTypes['groups'];
        /** fetch data from the table: "members" */
        members: Array<GraphQLTypes['members']>;
        /** fetch aggregated fields from the table: "members" */
        members_aggregate: GraphQLTypes['members_aggregate'];
        /** fetch data from the table: "members" using primary key columns */
        members_by_pk?: GraphQLTypes['members'];
        /** fetch data from the table: "payment_statuses" */
        payment_statuses: Array<GraphQLTypes['payment_statuses']>;
        /** fetch aggregated fields from the table: "payment_statuses" */
        payment_statuses_aggregate: GraphQLTypes['payment_statuses_aggregate'];
        /** fetch data from the table: "payment_statuses" using primary key columns */
        payment_statuses_by_pk?: GraphQLTypes['payment_statuses'];
        /** fetch data from the table: "payment_types" */
        payment_types: Array<GraphQLTypes['payment_types']>;
        /** fetch aggregated fields from the table: "payment_types" */
        payment_types_aggregate: GraphQLTypes['payment_types_aggregate'];
        /** fetch data from the table: "payment_types" using primary key columns */
        payment_types_by_pk?: GraphQLTypes['payment_types'];
        /** fetch data from the table: "payments" */
        payments: Array<GraphQLTypes['payments']>;
        /** fetch aggregated fields from the table: "payments" */
        payments_aggregate: GraphQLTypes['payments_aggregate'];
        /** fetch data from the table: "payments" using primary key columns */
        payments_by_pk?: GraphQLTypes['payments'];
        /** fetch data from the table: "periods" */
        periods: Array<GraphQLTypes['periods']>;
        /** fetch aggregated fields from the table: "periods" */
        periods_aggregate: GraphQLTypes['periods_aggregate'];
        /** fetch data from the table: "periods" using primary key columns */
        periods_by_pk?: GraphQLTypes['periods'];
        /** fetch data from the table: "users" */
        users: Array<GraphQLTypes['users']>;
        /** fetch aggregated fields from the table: "users" */
        users_aggregate: GraphQLTypes['users_aggregate'];
        /** fetch data from the table: "users" using primary key columns */
        users_by_pk?: GraphQLTypes['users'];
    };
    /** subscription root */
    ['subscription_root']: {
        __typename: 'subscription_root';
        /** fetch data from the table: "auth.account_providers" */
        auth_account_providers: Array<GraphQLTypes['auth_account_providers']>;
        /** fetch aggregated fields from the table: "auth.account_providers" */
        auth_account_providers_aggregate: GraphQLTypes['auth_account_providers_aggregate'];
        /** fetch data from the table: "auth.account_providers" using primary key columns */
        auth_account_providers_by_pk?: GraphQLTypes['auth_account_providers'];
        /** fetch data from the table: "auth.account_roles" */
        auth_account_roles: Array<GraphQLTypes['auth_account_roles']>;
        /** fetch aggregated fields from the table: "auth.account_roles" */
        auth_account_roles_aggregate: GraphQLTypes['auth_account_roles_aggregate'];
        /** fetch data from the table: "auth.account_roles" using primary key columns */
        auth_account_roles_by_pk?: GraphQLTypes['auth_account_roles'];
        /** fetch data from the table: "auth.accounts" */
        auth_accounts: Array<GraphQLTypes['auth_accounts']>;
        /** fetch aggregated fields from the table: "auth.accounts" */
        auth_accounts_aggregate: GraphQLTypes['auth_accounts_aggregate'];
        /** fetch data from the table: "auth.accounts" using primary key columns */
        auth_accounts_by_pk?: GraphQLTypes['auth_accounts'];
        /** fetch data from the table: "auth.providers" */
        auth_providers: Array<GraphQLTypes['auth_providers']>;
        /** fetch aggregated fields from the table: "auth.providers" */
        auth_providers_aggregate: GraphQLTypes['auth_providers_aggregate'];
        /** fetch data from the table: "auth.providers" using primary key columns */
        auth_providers_by_pk?: GraphQLTypes['auth_providers'];
        /** fetch data from the table: "auth.refresh_tokens" */
        auth_refresh_tokens: Array<GraphQLTypes['auth_refresh_tokens']>;
        /** fetch aggregated fields from the table: "auth.refresh_tokens" */
        auth_refresh_tokens_aggregate: GraphQLTypes['auth_refresh_tokens_aggregate'];
        /** fetch data from the table: "auth.refresh_tokens" using primary key columns */
        auth_refresh_tokens_by_pk?: GraphQLTypes['auth_refresh_tokens'];
        /** fetch data from the table: "auth.roles" */
        auth_roles: Array<GraphQLTypes['auth_roles']>;
        /** fetch aggregated fields from the table: "auth.roles" */
        auth_roles_aggregate: GraphQLTypes['auth_roles_aggregate'];
        /** fetch data from the table: "auth.roles" using primary key columns */
        auth_roles_by_pk?: GraphQLTypes['auth_roles'];
        /** fetch data from the table: "contribution_types" */
        contribution_types: Array<GraphQLTypes['contribution_types']>;
        /** fetch aggregated fields from the table: "contribution_types" */
        contribution_types_aggregate: GraphQLTypes['contribution_types_aggregate'];
        /** fetch data from the table: "contribution_types" using primary key columns */
        contribution_types_by_pk?: GraphQLTypes['contribution_types'];
        /** fetch data from the table: "group_cycles" */
        group_cycles: Array<GraphQLTypes['group_cycles']>;
        /** fetch aggregated fields from the table: "group_cycles" */
        group_cycles_aggregate: GraphQLTypes['group_cycles_aggregate'];
        /** fetch data from the table: "group_cycles" using primary key columns */
        group_cycles_by_pk?: GraphQLTypes['group_cycles'];
        /** fetch data from the table: "group_recurrencies" */
        group_recurrencies: Array<GraphQLTypes['group_recurrencies']>;
        /** fetch aggregated fields from the table: "group_recurrencies" */
        group_recurrencies_aggregate: GraphQLTypes['group_recurrencies_aggregate'];
        /** fetch data from the table: "group_recurrencies" using primary key columns */
        group_recurrencies_by_pk?: GraphQLTypes['group_recurrencies'];
        /** fetch data from the table: "groups" */
        groups: Array<GraphQLTypes['groups']>;
        /** fetch aggregated fields from the table: "groups" */
        groups_aggregate: GraphQLTypes['groups_aggregate'];
        /** fetch data from the table: "groups" using primary key columns */
        groups_by_pk?: GraphQLTypes['groups'];
        /** fetch data from the table: "members" */
        members: Array<GraphQLTypes['members']>;
        /** fetch aggregated fields from the table: "members" */
        members_aggregate: GraphQLTypes['members_aggregate'];
        /** fetch data from the table: "members" using primary key columns */
        members_by_pk?: GraphQLTypes['members'];
        /** fetch data from the table: "payment_statuses" */
        payment_statuses: Array<GraphQLTypes['payment_statuses']>;
        /** fetch aggregated fields from the table: "payment_statuses" */
        payment_statuses_aggregate: GraphQLTypes['payment_statuses_aggregate'];
        /** fetch data from the table: "payment_statuses" using primary key columns */
        payment_statuses_by_pk?: GraphQLTypes['payment_statuses'];
        /** fetch data from the table: "payment_types" */
        payment_types: Array<GraphQLTypes['payment_types']>;
        /** fetch aggregated fields from the table: "payment_types" */
        payment_types_aggregate: GraphQLTypes['payment_types_aggregate'];
        /** fetch data from the table: "payment_types" using primary key columns */
        payment_types_by_pk?: GraphQLTypes['payment_types'];
        /** fetch data from the table: "payments" */
        payments: Array<GraphQLTypes['payments']>;
        /** fetch aggregated fields from the table: "payments" */
        payments_aggregate: GraphQLTypes['payments_aggregate'];
        /** fetch data from the table: "payments" using primary key columns */
        payments_by_pk?: GraphQLTypes['payments'];
        /** fetch data from the table: "periods" */
        periods: Array<GraphQLTypes['periods']>;
        /** fetch aggregated fields from the table: "periods" */
        periods_aggregate: GraphQLTypes['periods_aggregate'];
        /** fetch data from the table: "periods" using primary key columns */
        periods_by_pk?: GraphQLTypes['periods'];
        /** fetch data from the table: "users" */
        users: Array<GraphQLTypes['users']>;
        /** fetch aggregated fields from the table: "users" */
        users_aggregate: GraphQLTypes['users_aggregate'];
        /** fetch data from the table: "users" using primary key columns */
        users_by_pk?: GraphQLTypes['users'];
    };
    ['timestamp']: any;
    /** expression to compare columns of type timestamp. All fields are combined with logical 'AND'. */
    ['timestamp_comparison_exp']: {
        _eq?: GraphQLTypes['timestamp'];
        _gt?: GraphQLTypes['timestamp'];
        _gte?: GraphQLTypes['timestamp'];
        _in?: Array<GraphQLTypes['timestamp']>;
        _is_null?: boolean;
        _lt?: GraphQLTypes['timestamp'];
        _lte?: GraphQLTypes['timestamp'];
        _neq?: GraphQLTypes['timestamp'];
        _nin?: Array<GraphQLTypes['timestamp']>;
    };
    ['timestamptz']: any;
    /** expression to compare columns of type timestamptz. All fields are combined with logical 'AND'. */
    ['timestamptz_comparison_exp']: {
        _eq?: GraphQLTypes['timestamptz'];
        _gt?: GraphQLTypes['timestamptz'];
        _gte?: GraphQLTypes['timestamptz'];
        _in?: Array<GraphQLTypes['timestamptz']>;
        _is_null?: boolean;
        _lt?: GraphQLTypes['timestamptz'];
        _lte?: GraphQLTypes['timestamptz'];
        _neq?: GraphQLTypes['timestamptz'];
        _nin?: Array<GraphQLTypes['timestamptz']>;
    };
    /** columns and relationships of "users" */
    ['users']: {
        __typename: 'users';
        /** An object relationship */
        account?: GraphQLTypes['auth_accounts'];
        avatar_url?: string;
        created_at: GraphQLTypes['timestamptz'];
        display_name?: string;
        expo_push_token?: string;
        /** An array relationship */
        groups: Array<GraphQLTypes['groups']>;
        /** An aggregated array relationship */
        groups_aggregate: GraphQLTypes['groups_aggregate'];
        id: GraphQLTypes['uuid'];
        /** An array relationship */
        members: Array<GraphQLTypes['members']>;
        /** An aggregated array relationship */
        members_aggregate: GraphQLTypes['members_aggregate'];
        updated_at: GraphQLTypes['timestamptz'];
    };
    /** aggregated selection of "users" */
    ['users_aggregate']: {
        __typename: 'users_aggregate';
        aggregate?: GraphQLTypes['users_aggregate_fields'];
        nodes: Array<GraphQLTypes['users']>;
    };
    /** aggregate fields of "users" */
    ['users_aggregate_fields']: {
        __typename: 'users_aggregate_fields';
        count?: number;
        max?: GraphQLTypes['users_max_fields'];
        min?: GraphQLTypes['users_min_fields'];
    };
    /** order by aggregate values of table "users" */
    ['users_aggregate_order_by']: {
        count?: GraphQLTypes['order_by'];
        max?: GraphQLTypes['users_max_order_by'];
        min?: GraphQLTypes['users_min_order_by'];
    };
    /** input type for inserting array relation for remote table "users" */
    ['users_arr_rel_insert_input']: {
        data: Array<GraphQLTypes['users_insert_input']>;
        on_conflict?: GraphQLTypes['users_on_conflict'];
    };
    /** Boolean expression to filter rows from the table "users". All fields are combined with a logical 'AND'. */
    ['users_bool_exp']: {
        _and?: Array<GraphQLTypes['users_bool_exp'] | undefined>;
        _not?: GraphQLTypes['users_bool_exp'];
        _or?: Array<GraphQLTypes['users_bool_exp'] | undefined>;
        account?: GraphQLTypes['auth_accounts_bool_exp'];
        avatar_url?: GraphQLTypes['String_comparison_exp'];
        created_at?: GraphQLTypes['timestamptz_comparison_exp'];
        display_name?: GraphQLTypes['String_comparison_exp'];
        expo_push_token?: GraphQLTypes['String_comparison_exp'];
        groups?: GraphQLTypes['groups_bool_exp'];
        id?: GraphQLTypes['uuid_comparison_exp'];
        members?: GraphQLTypes['members_bool_exp'];
        updated_at?: GraphQLTypes['timestamptz_comparison_exp'];
    };
    /** unique or primary key constraints on table "users" */
    ['users_constraint']: users_constraint;
    /** input type for inserting data into table "users" */
    ['users_insert_input']: {
        account?: GraphQLTypes['auth_accounts_obj_rel_insert_input'];
        avatar_url?: string;
        created_at?: GraphQLTypes['timestamptz'];
        display_name?: string;
        expo_push_token?: string;
        groups?: GraphQLTypes['groups_arr_rel_insert_input'];
        id?: GraphQLTypes['uuid'];
        members?: GraphQLTypes['members_arr_rel_insert_input'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** aggregate max on columns */
    ['users_max_fields']: {
        __typename: 'users_max_fields';
        avatar_url?: string;
        created_at?: GraphQLTypes['timestamptz'];
        display_name?: string;
        expo_push_token?: string;
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** order by max() on columns of table "users" */
    ['users_max_order_by']: {
        avatar_url?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        display_name?: GraphQLTypes['order_by'];
        expo_push_token?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** aggregate min on columns */
    ['users_min_fields']: {
        __typename: 'users_min_fields';
        avatar_url?: string;
        created_at?: GraphQLTypes['timestamptz'];
        display_name?: string;
        expo_push_token?: string;
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** order by min() on columns of table "users" */
    ['users_min_order_by']: {
        avatar_url?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        display_name?: GraphQLTypes['order_by'];
        expo_push_token?: GraphQLTypes['order_by'];
        id?: GraphQLTypes['order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** response of any mutation on the table "users" */
    ['users_mutation_response']: {
        __typename: 'users_mutation_response';
        /** number of affected rows by the mutation */
        affected_rows: number;
        /** data of the affected rows by the mutation */
        returning: Array<GraphQLTypes['users']>;
    };
    /** input type for inserting object relation for remote table "users" */
    ['users_obj_rel_insert_input']: {
        data: GraphQLTypes['users_insert_input'];
        on_conflict?: GraphQLTypes['users_on_conflict'];
    };
    /** on conflict condition type for table "users" */
    ['users_on_conflict']: {
        constraint: GraphQLTypes['users_constraint'];
        update_columns: Array<GraphQLTypes['users_update_column']>;
        where?: GraphQLTypes['users_bool_exp'];
    };
    /** ordering options when selecting data from "users" */
    ['users_order_by']: {
        account?: GraphQLTypes['auth_accounts_order_by'];
        avatar_url?: GraphQLTypes['order_by'];
        created_at?: GraphQLTypes['order_by'];
        display_name?: GraphQLTypes['order_by'];
        expo_push_token?: GraphQLTypes['order_by'];
        groups_aggregate?: GraphQLTypes['groups_aggregate_order_by'];
        id?: GraphQLTypes['order_by'];
        members_aggregate?: GraphQLTypes['members_aggregate_order_by'];
        updated_at?: GraphQLTypes['order_by'];
    };
    /** primary key columns input for table: "users" */
    ['users_pk_columns_input']: {
        id: GraphQLTypes['uuid'];
    };
    /** select columns of table "users" */
    ['users_select_column']: users_select_column;
    /** input type for updating data in table "users" */
    ['users_set_input']: {
        avatar_url?: string;
        created_at?: GraphQLTypes['timestamptz'];
        display_name?: string;
        expo_push_token?: string;
        id?: GraphQLTypes['uuid'];
        updated_at?: GraphQLTypes['timestamptz'];
    };
    /** update columns of table "users" */
    ['users_update_column']: users_update_column;
    ['uuid']: any;
    /** expression to compare columns of type uuid. All fields are combined with logical 'AND'. */
    ['uuid_comparison_exp']: {
        _eq?: GraphQLTypes['uuid'];
        _gt?: GraphQLTypes['uuid'];
        _gte?: GraphQLTypes['uuid'];
        _in?: Array<GraphQLTypes['uuid']>;
        _is_null?: boolean;
        _lt?: GraphQLTypes['uuid'];
        _lte?: GraphQLTypes['uuid'];
        _neq?: GraphQLTypes['uuid'];
        _nin?: Array<GraphQLTypes['uuid']>;
    };
};
/** unique or primary key constraints on table "auth.account_providers" */
export enum auth_account_providers_constraint {
    account_providers_account_id_auth_provider_key = 'account_providers_account_id_auth_provider_key',
    account_providers_auth_provider_auth_provider_unique_id_key = 'account_providers_auth_provider_auth_provider_unique_id_key',
    account_providers_pkey = 'account_providers_pkey',
}
/** select columns of table "auth.account_providers" */
export enum auth_account_providers_select_column {
    account_id = 'account_id',
    auth_provider = 'auth_provider',
    auth_provider_unique_id = 'auth_provider_unique_id',
    created_at = 'created_at',
    id = 'id',
    updated_at = 'updated_at',
}
/** update columns of table "auth.account_providers" */
export enum auth_account_providers_update_column {
    account_id = 'account_id',
    auth_provider = 'auth_provider',
    auth_provider_unique_id = 'auth_provider_unique_id',
    created_at = 'created_at',
    id = 'id',
    updated_at = 'updated_at',
}
/** unique or primary key constraints on table "auth.account_roles" */
export enum auth_account_roles_constraint {
    account_roles_pkey = 'account_roles_pkey',
    user_roles_account_id_role_key = 'user_roles_account_id_role_key',
}
/** select columns of table "auth.account_roles" */
export enum auth_account_roles_select_column {
    account_id = 'account_id',
    created_at = 'created_at',
    id = 'id',
    role = 'role',
}
/** update columns of table "auth.account_roles" */
export enum auth_account_roles_update_column {
    account_id = 'account_id',
    created_at = 'created_at',
    id = 'id',
    role = 'role',
}
/** unique or primary key constraints on table "auth.accounts" */
export enum auth_accounts_constraint {
    accounts_email_key = 'accounts_email_key',
    accounts_new_email_key = 'accounts_new_email_key',
    accounts_pkey = 'accounts_pkey',
    accounts_user_id_key = 'accounts_user_id_key',
}
/** select columns of table "auth.accounts" */
export enum auth_accounts_select_column {
    active = 'active',
    created_at = 'created_at',
    custom_register_data = 'custom_register_data',
    default_role = 'default_role',
    email = 'email',
    id = 'id',
    is_anonymous = 'is_anonymous',
    mfa_enabled = 'mfa_enabled',
    new_email = 'new_email',
    otp_secret = 'otp_secret',
    password_hash = 'password_hash',
    ticket = 'ticket',
    ticket_expires_at = 'ticket_expires_at',
    updated_at = 'updated_at',
    user_id = 'user_id',
}
/** update columns of table "auth.accounts" */
export enum auth_accounts_update_column {
    active = 'active',
    created_at = 'created_at',
    custom_register_data = 'custom_register_data',
    default_role = 'default_role',
    email = 'email',
    id = 'id',
    is_anonymous = 'is_anonymous',
    mfa_enabled = 'mfa_enabled',
    new_email = 'new_email',
    otp_secret = 'otp_secret',
    password_hash = 'password_hash',
    ticket = 'ticket',
    ticket_expires_at = 'ticket_expires_at',
    updated_at = 'updated_at',
    user_id = 'user_id',
}
/** unique or primary key constraints on table "auth.providers" */
export enum auth_providers_constraint {
    providers_pkey = 'providers_pkey',
}
/** select columns of table "auth.providers" */
export enum auth_providers_select_column {
    provider = 'provider',
}
/** update columns of table "auth.providers" */
export enum auth_providers_update_column {
    provider = 'provider',
}
/** unique or primary key constraints on table "auth.refresh_tokens" */
export enum auth_refresh_tokens_constraint {
    refresh_tokens_pkey = 'refresh_tokens_pkey',
}
/** select columns of table "auth.refresh_tokens" */
export enum auth_refresh_tokens_select_column {
    account_id = 'account_id',
    created_at = 'created_at',
    expires_at = 'expires_at',
    refresh_token = 'refresh_token',
}
/** update columns of table "auth.refresh_tokens" */
export enum auth_refresh_tokens_update_column {
    account_id = 'account_id',
    created_at = 'created_at',
    expires_at = 'expires_at',
    refresh_token = 'refresh_token',
}
/** unique or primary key constraints on table "auth.roles" */
export enum auth_roles_constraint {
    roles_pkey = 'roles_pkey',
}
/** select columns of table "auth.roles" */
export enum auth_roles_select_column {
    role = 'role',
}
/** update columns of table "auth.roles" */
export enum auth_roles_update_column {
    role = 'role',
}
/** unique or primary key constraints on table "contribution_types" */
export enum contribution_types_constraint {
    contribution_types_description_key = 'contribution_types_description_key',
    contribution_types_pkey = 'contribution_types_pkey',
}
export enum contribution_types_enum {
    AnyAmount = 'AnyAmount',
    FixAmount = 'FixAmount',
    MinimumAmount = 'MinimumAmount',
}
/** select columns of table "contribution_types" */
export enum contribution_types_select_column {
    description = 'description',
    value = 'value',
}
/** update columns of table "contribution_types" */
export enum contribution_types_update_column {
    description = 'description',
    value = 'value',
}
/** unique or primary key constraints on table "group_cycles" */
export enum group_cycles_constraint {
    group_cycles_pkey = 'group_cycles_pkey',
}
/** select columns of table "group_cycles" */
export enum group_cycles_select_column {
    description = 'description',
    value = 'value',
}
/** update columns of table "group_cycles" */
export enum group_cycles_update_column {
    description = 'description',
    value = 'value',
}
/** unique or primary key constraints on table "group_recurrencies" */
export enum group_recurrencies_constraint {
    payment_frequencies_pkey = 'payment_frequencies_pkey',
}
export enum group_recurrencies_enum {
    Daily = 'Daily',
    Monthly = 'Monthly',
    None = 'None',
    Weekly = 'Weekly',
}
/** select columns of table "group_recurrencies" */
export enum group_recurrencies_select_column {
    description = 'description',
    value = 'value',
}
/** update columns of table "group_recurrencies" */
export enum group_recurrencies_update_column {
    description = 'description',
    value = 'value',
}
/** unique or primary key constraints on table "groups" */
export enum groups_constraint {
    groups_admin_id_group_name_key = 'groups_admin_id_group_name_key',
    groups_pkey = 'groups_pkey',
}
/** select columns of table "groups" */
export enum groups_select_column {
    created_at = 'created_at',
    creator_id = 'creator_id',
    group_balance = 'group_balance',
    group_code = 'group_code',
    group_contribution_amount = 'group_contribution_amount',
    group_contribution_type = 'group_contribution_type',
    group_name = 'group_name',
    group_recurrency_amount = 'group_recurrency_amount',
    group_recurrency_day = 'group_recurrency_day',
    group_recurrency_type = 'group_recurrency_type',
    id = 'id',
    updated_at = 'updated_at',
}
/** update columns of table "groups" */
export enum groups_update_column {
    created_at = 'created_at',
    creator_id = 'creator_id',
    group_balance = 'group_balance',
    group_code = 'group_code',
    group_contribution_amount = 'group_contribution_amount',
    group_contribution_type = 'group_contribution_type',
    group_name = 'group_name',
    group_recurrency_amount = 'group_recurrency_amount',
    group_recurrency_day = 'group_recurrency_day',
    group_recurrency_type = 'group_recurrency_type',
    id = 'id',
    updated_at = 'updated_at',
}
/** unique or primary key constraints on table "members" */
export enum members_constraint {
    members_group_id_user_id_key = 'members_group_id_user_id_key',
    members_pkey = 'members_pkey',
}
/** select columns of table "members" */
export enum members_select_column {
    created_at = 'created_at',
    group_id = 'group_id',
    id = 'id',
    updated_at = 'updated_at',
    user_id = 'user_id',
}
/** update columns of table "members" */
export enum members_update_column {
    created_at = 'created_at',
    group_id = 'group_id',
    id = 'id',
    updated_at = 'updated_at',
    user_id = 'user_id',
}
/** column ordering options */
export enum order_by {
    asc = 'asc',
    asc_nulls_first = 'asc_nulls_first',
    asc_nulls_last = 'asc_nulls_last',
    desc = 'desc',
    desc_nulls_first = 'desc_nulls_first',
    desc_nulls_last = 'desc_nulls_last',
}
/** unique or primary key constraints on table "payment_statuses" */
export enum payment_statuses_constraint {
    payment_status_pkey = 'payment_status_pkey',
}
export enum payment_statuses_enum {
    CANCELLED = 'CANCELLED',
    COMPLETED = 'COMPLETED',
    FAILED = 'FAILED',
    PENDING = 'PENDING',
}
/** select columns of table "payment_statuses" */
export enum payment_statuses_select_column {
    description = 'description',
    value = 'value',
}
/** update columns of table "payment_statuses" */
export enum payment_statuses_update_column {
    description = 'description',
    value = 'value',
}
/** unique or primary key constraints on table "payment_types" */
export enum payment_types_constraint {
    payment_types_pkey = 'payment_types_pkey',
}
export enum payment_types_enum {
    MoneyIn = 'MoneyIn',
    MoneyOut = 'MoneyOut',
}
/** select columns of table "payment_types" */
export enum payment_types_select_column {
    description = 'description',
    value = 'value',
}
/** update columns of table "payment_types" */
export enum payment_types_update_column {
    description = 'description',
    value = 'value',
}
/** unique or primary key constraints on table "payments" */
export enum payments_constraint {
    payments_pkey = 'payments_pkey',
}
/** select columns of table "payments" */
export enum payments_select_column {
    created_at = 'created_at',
    group_id = 'group_id',
    id = 'id',
    member_id = 'member_id',
    payment_amount = 'payment_amount',
    payment_status = 'payment_status',
    payment_type = 'payment_type',
    period_id = 'period_id',
    updated_at = 'updated_at',
}
/** update columns of table "payments" */
export enum payments_update_column {
    created_at = 'created_at',
    group_id = 'group_id',
    id = 'id',
    member_id = 'member_id',
    payment_amount = 'payment_amount',
    payment_status = 'payment_status',
    payment_type = 'payment_type',
    period_id = 'period_id',
    updated_at = 'updated_at',
}
/** unique or primary key constraints on table "periods" */
export enum periods_constraint {
    periods_group_id_period_index_key = 'periods_group_id_period_index_key',
    periods_pkey = 'periods_pkey',
}
/** select columns of table "periods" */
export enum periods_select_column {
    created_at = 'created_at',
    group_id = 'group_id',
    id = 'id',
    period_active = 'period_active',
    period_completed_at = 'period_completed_at',
    period_index = 'period_index',
    period_progression = 'period_progression',
    updated_at = 'updated_at',
}
/** update columns of table "periods" */
export enum periods_update_column {
    created_at = 'created_at',
    group_id = 'group_id',
    id = 'id',
    period_active = 'period_active',
    period_completed_at = 'period_completed_at',
    period_index = 'period_index',
    period_progression = 'period_progression',
    updated_at = 'updated_at',
}
/** unique or primary key constraints on table "users" */
export enum users_constraint {
    users_pkey = 'users_pkey',
}
/** select columns of table "users" */
export enum users_select_column {
    avatar_url = 'avatar_url',
    created_at = 'created_at',
    display_name = 'display_name',
    expo_push_token = 'expo_push_token',
    id = 'id',
    updated_at = 'updated_at',
}
/** update columns of table "users" */
export enum users_update_column {
    avatar_url = 'avatar_url',
    created_at = 'created_at',
    display_name = 'display_name',
    expo_push_token = 'expo_push_token',
    id = 'id',
    updated_at = 'updated_at',
}

export type UnwrapPromise<T> = T extends Promise<infer R> ? R : T;
export type ZeusState<T extends (...args: any[]) => Promise<any>> = NonNullable<
    UnwrapPromise<ReturnType<T>>
>;
export type ZeusHook<
    T extends (
        ...args: any[]
    ) => Record<string, (...args: any[]) => Promise<any>>,
    N extends keyof ReturnType<T>
> = ZeusState<ReturnType<T>[N]>;

type WithTypeNameValue<T> = T & {
    __typename?: true;
};
type AliasType<T> = WithTypeNameValue<T> & {
    __alias?: Record<string, WithTypeNameValue<T>>;
};
export interface GraphQLResponse {
    data?: Record<string, any>;
    errors?: Array<{
        message: string;
    }>;
}
type DeepAnify<T> = {
    [P in keyof T]?: any;
};
type IsPayLoad<T> = T extends [any, infer PayLoad] ? PayLoad : T;
type IsArray<T, U> = T extends Array<infer R>
    ? InputType<R, U>[]
    : InputType<T, U>;
type FlattenArray<T> = T extends Array<infer R> ? R : T;

type NotUnionTypes<SRC extends DeepAnify<DST>, DST> = {
    [P in keyof DST]: SRC[P] extends '__union' & infer R ? never : P;
}[keyof DST];

type ExtractUnions<SRC extends DeepAnify<DST>, DST> = {
    [P in keyof SRC]: SRC[P] extends '__union' & infer R
        ? P extends keyof DST
            ? IsArray<R, DST[P] & { __typename: true }>
            : {}
        : never;
}[keyof SRC];

type IsInterfaced<SRC extends DeepAnify<DST>, DST> = FlattenArray<SRC> extends
    | ZEUS_INTERFACES
    | ZEUS_UNIONS
    ? ExtractUnions<SRC, DST> &
          {
              [P in keyof Omit<
                  Pick<SRC, NotUnionTypes<SRC, DST>>,
                  '__typename'
              >]: DST[P] extends true ? SRC[P] : IsArray<SRC[P], DST[P]>;
          }
    : {
          [P in keyof Pick<SRC, keyof DST>]: IsPayLoad<DST[P]> extends true
              ? SRC[P]
              : IsArray<SRC[P], DST[P]>;
      };

export type MapType<SRC, DST> = SRC extends DeepAnify<DST>
    ? IsInterfaced<SRC, DST>
    : never;
type InputType<SRC, DST> = IsPayLoad<DST> extends { __alias: infer R }
    ? {
          [P in keyof R]: MapType<SRC, R[P]>;
      } &
          MapType<SRC, Omit<IsPayLoad<DST>, '__alias'>>
    : MapType<SRC, IsPayLoad<DST>>;
type Func<P extends any[], R> = (...args: P) => R;
type AnyFunc = Func<any, any>;
export type ArgsType<F extends AnyFunc> = F extends Func<infer P, any>
    ? P
    : never;
export type OperationToGraphQL<V, T> = <Z extends V>(
    o: Z | V,
    variables?: Record<string, any>
) => Promise<InputType<T, Z>>;
export type SubscriptionToGraphQL<V, T> = <Z extends V>(
    o: Z | V,
    variables?: Record<string, any>
) => {
    ws: WebSocket;
    on: (fn: (args: InputType<T, Z>) => void) => void;
    off: (e: {
        data?: InputType<T, Z>;
        code?: number;
        reason?: string;
        message?: string;
    }) => void;
    error: (e: { data?: InputType<T, Z>; message?: string }) => void;
    open: () => void;
};
export type CastToGraphQL<V, T> = (
    resultOfYourQuery: any
) => <Z extends V>(o: Z | V) => InputType<T, Z>;
export type SelectionFunction<V> = <T>(t: T | V) => T;
export type fetchOptions = ArgsType<typeof fetch>;
type websocketOptions = typeof WebSocket extends new (
    ...args: infer R
) => WebSocket
    ? R
    : never;
export type chainOptions =
    | [fetchOptions[0], fetchOptions[1] & { websocket?: websocketOptions }]
    | [fetchOptions[0]];
export type FetchFunction = (
    query: string,
    variables?: Record<string, any>
) => Promise<any>;
export type SubscriptionFunction = (
    query: string,
    variables?: Record<string, any>
) => void;
type NotUndefined<T> = T extends undefined ? never : T;
export type ResolverType<F> = NotUndefined<
    F extends [infer ARGS, any] ? ARGS : undefined
>;

export declare function Thunder(
    fn: FetchFunction,
    subscriptionFn: SubscriptionFunction
): {
    query: OperationToGraphQL<
        ValueTypes['query_root'],
        GraphQLTypes['query_root']
    >;
    mutation: OperationToGraphQL<
        ValueTypes['mutation_root'],
        GraphQLTypes['mutation_root']
    >;
    subscription: OperationToGraphQL<
        ValueTypes['subscription_root'],
        GraphQLTypes['subscription_root']
    >;
};

export declare function Chain(...options: chainOptions): {
    query: OperationToGraphQL<
        ValueTypes['query_root'],
        GraphQLTypes['query_root']
    >;
    mutation: OperationToGraphQL<
        ValueTypes['mutation_root'],
        GraphQLTypes['mutation_root']
    >;
    subscription: OperationToGraphQL<
        ValueTypes['subscription_root'],
        GraphQLTypes['subscription_root']
    >;
};

export declare const Zeus: {
    query: (o: ValueTypes['query_root']) => string;
    mutation: (o: ValueTypes['mutation_root']) => string;
    subscription: (o: ValueTypes['subscription_root']) => string;
};

export declare const Cast: {
    query: CastToGraphQL<ValueTypes['query_root'], GraphQLTypes['query_root']>;
    mutation: CastToGraphQL<
        ValueTypes['mutation_root'],
        GraphQLTypes['mutation_root']
    >;
    subscription: CastToGraphQL<
        ValueTypes['subscription_root'],
        GraphQLTypes['subscription_root']
    >;
};

export declare const Selectors: {
    query: SelectionFunction<ValueTypes['query_root']>;
    mutation: SelectionFunction<ValueTypes['mutation_root']>;
    subscription: SelectionFunction<ValueTypes['subscription_root']>;
};

export declare const resolverFor: <
    T extends keyof ValueTypes,
    Z extends keyof ValueTypes[T],
    Y extends (
        args: Required<ValueTypes[T]>[Z] extends [infer Input, any]
            ? Input
            : any,
        source: any
    ) => Z extends keyof ModelTypes[T]
        ? ModelTypes[T][Z] | Promise<ModelTypes[T][Z]>
        : any
>(
    type: T,
    field: Z,
    fn: Y
) => (args?: any, source?: any) => void;

export declare const Gql: ReturnType<typeof Chain>;
