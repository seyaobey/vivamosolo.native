module.exports = {
    schema: [
        {
            [process.env.ENV_GRAPHQL_URL]: {
                headers: {
                    'X-Hasura-Admin-Secret': process.env.ENV_HASURA_ADMIN,
                    'X-Hasura-Role': 'admin',
                },
            },
        },
    ],
    documents: ['./**/*.graphql'],
    overwrite: true,
    generates: {
        'src/modules/api-graphql/api-react-query.ts': {
            config: {
                namingConvention: 'change-case#pascalCase',
                transformUnderscore: false,
                skipTypename: true,
                fetcher: 'graphql-request',
            },
            plugins: [
                'typescript',
                'typescript-operations',
                'typescript-react-query',
            ],
        },
        'src/modules/api-graphql/api-apollo.tsx': {
            config: {
                namingConvention: 'change-case#pascalCase',
                transformUnderscore: false,
                withHOC: false,
                withHooks: true,
                withComponent: false,
                skipTypename: true,
            },
            plugins: [
                'typescript',
                'typescript-operations',
                'typescript-react-apollo',
            ],
        },
        'src/modules/api-client/api-sdk-generated.ts': {
            config: {
                namingConvention: 'change-case#pascalCase',
                transformUnderscore: false,
                operationResultSuffix: 'Result',
                skipTypename: true,
            },
            plugins: [
                'typescript',
                'typescript-operations',
                'typescript-graphql-request',
            ],
        },
    },
};
