import 'dotenv/config';

module.exports = ({ config }) => {
    const { android, ...rest } = config;

    return {
        ...rest,
        ios: {
            buildNumber: '1.3.0',
            supportsTablet: true,
            bundleIdentifier: 'com.seyaobey.vivamosolonative',
        },
        android: {
            ...android,
            adaptiveIcon: {
                foregroundImage: './assets/adaptive-icon.png',
                backgroundColor: '#FFFFFF',
            },
            package: 'com.seyaobey.vivamosolonative',
        },
        extra: {
            PUBLIC_GRAPHQL_URL: process.env.PUBLIC_GRAPHQL_URL,
            PUBLIC_NHOST_URL: process.env.PUBLIC_NHOST_URL,
            PUBLIC_HASURA_ADMIN: process.env.PUBLIC_HASURA_ADMIN,

            PUBLIC_GRAPHQL_URL_DEV: process.env.PUBLIC_GRAPHQL_URL_DEV,
            PUBLIC_NHOST_URL_DEV: process.env.PUBLIC_NHOST_URL_DEV,
            PUBLIC_HASURA_ADMIN_DEV: process.env.PUBLIC_HASURA_ADMIN_DEV,

            PUBLIC_AUTO_LOGIN: process.env.PUBLIC_AUTO_LOGIN,
            PUBLIC_CAN_USE_LOCAL: process.env.PUBLIC_CAN_USE_LOCAL,
            PUBLIC_IGNORE_NOTIFICATIONS:
                process.env.PUBLIC_IGNORE_NOTIFICATIONS,
        },
    };
};
